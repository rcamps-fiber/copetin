<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Plan de desarrollo de software");
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Aceptar-copanel.php";
mi_titulo("Plan de desarrollo de software");
$panta=$_POST["panta"];
switch($panta)
{
	case "graba_alta":
		//trace("Estoy en graba alta");
		$fecha_alta=a_fecha_sistema($_POST["fecha_alta"]);
		$fecha_prog=a_fecha_sistema($_POST["fecha_prog"]);
		$fecha_ini=a_fecha_sistema($_POST["fecha_ini"]);
		$fecha_fin=a_fecha_sistema($_POST["fecha_fin"]);
		$plazo_estimado=$_POST["plazo_estimado"];
		$plazo_real=$_POST["plazo_real"];
		$descripcion=$_POST["descripcion"];
		$presupuesto=$_POST["presupuesto"];
		$observaciones=$_POST["observaciones"];
		$estado=$_POST["estado"];
		$avance=$_POST["avance"];
		$id_cat=($_POST["id_cat"]=="Elegir")? 99 : $_POST["id_cat"];
		$id_prg=($_POST["id_prg"]=="Elegir") ? 99 : $_POST["id_prg"];
		$id_prio=($_POST["id_prio"]=="Elegir") ? 99 : $_POST["id_prio"];
		mi_query("insert into desarrollo_software set fecha_alta='$fecha_alta',fecha_prog='$fecha_prog',fecha_ini='$fecha_ini',fecha_fin='$fecha_fin',plazo_estimado='$plazo_estimado',plazo_real='$plazo_real',descripcion='$descripcion',presupuesto='$presupuesto',observaciones='$observaciones',estado='$estado',avance='$avance',categoria='$id_cat',programador='$id_prg',prioridad='$id_prio'","Error al insertar un nuevo evento");
		mensaje("Se agrego un nuevo sistema al plan de desarrollo.");
		delay();
		break;
	case "modi":
		$id_desa=$_POST["id_desa"];
		$cons=mi_query("select * from desarrollo_software where id_desa='$id_desa'","Error al obtener el registro");
		$datos=mysql_fetch_array($cons);
		$fecha_alta=a_fecha_arg($datos["fecha_alta"]);
		$fecha_prog=a_fecha_arg($datos["fecha_prog"]);
		$fecha_ini=a_fecha_arg($datos["fecha_ini"]);
		$fecha_fin=a_fecha_arg($datos["fecha_fin"]);
		$plazo_estimado=$datos["plazo_estimado"];
		$plazo_real=$datos["plazo_real"];
		$descripcion=$datos["descripcion"];
		$presupuesto=$datos["presupuesto"];
		$observaciones=$datos["observaciones"];
		$estado=$datos["estado"];
		$avance=$datos["avance"];
		$programador=$datos["programador"];
		$id_cat=($datos["categoria"]=="Elegir") ? 99 : $datos["categoria"];
		$id_prio=($datos["prioridad"]=="Elegir") ? 99 : $datos["prioridad"];
		$id_prg=($datos["programador"]=="Elegir") ? 99 : $datos["programador"];
		$prioridad=un_dato("select prioridad from prioridad where id_prio='$id_prio'");
		$programador=un_dato("select programador from programador where id_prg='$id_prg'");
		$categoria=un_dato("select categoria from categoria where id_cat='$id_cat'");
		
		$titulo="Modificacion de Plan de desarrollo";
		$tit_modi="MODIFICACION DE DESARROLLO";
		$campos=";%ROT-Id. ITEM</td><td><strong>$id_desa";
		$campos.=";%FEC-fecha alta-fecha_alta-$fecha_alta-10";
		$campos.=";%FEC-fecha programada-fecha_prog-$fecha_prog-10";
		$campos.=";%FEC-fecha inicio-fecha_ini-$fecha_ini-10";
		$campos.=";%FEC-fecha fin-fecha_fin-$fecha_fin-10";
		$campos.=";%TXT-plazo estimado (dias)-plazo_estimado-$plazo_estimado-10";
		$campos.=";%TXT-plazo real (dias)-plazo_real-$plazo_real-10";
		$campos.=";%ARE-descripcion-descripcion-$descripcion-5-55";
		$campos.=";%TXT-presupuesto-presupuesto-$presupuesto-5";
		$campos.=";%ARE-observaciones-observaciones-$observaciones-5-55";
		$campos.=";%SEL-estado-estado-$estado+$estado+ESPERA+ESPERA+DESARROLLO+DESARROLLO+TERMINADO+TERMINADO-0";
		$campos.=";%SEL-id_cat-categoria-select id_cat,categoria from categoria order by 2-categoria+id_cat-$categoria-$id_cat";
		$campos.=";%SEL-id_prg-programador-select id_prg,programador from programador order by 2-programador+id_prg-$programador-$id_prg";
		$campos.=";%SEL-id_prio-prioridad-select id_prio,prioridad from prioridad order by 2-prioridad+id_prio-$prioridad-$id_prio";
		$campos.=";%SEL-avance-avance %-$avance+$avance+0+0%+10+10%+20+20%+30+30%+40+40%+50+50%+60+60%+70+70%+80+80%+90+90%+100+100%-0";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-id_desa-$id_desa";
		$campos.=";%CHK-borrar-borrar-s-N";
		mi_panta($tit_modi,$campos,$submit);
		break;
	case "graba_modi":
		$id_desa=$_POST["id_desa"];
		$fecha_alta=a_fecha_sistema($_POST["fecha_alta"]);
		$fecha_prog=a_fecha_sistema($_POST["fecha_prog"]);
		$fecha_ini=a_fecha_sistema($_POST["fecha_ini"]);
		$fecha_fin=a_fecha_sistema($_POST["fecha_fin"]);
		$plazo_estimado=$_POST["plazo_estimado"];
		$plazo_real=$_POST["plazo_real"];
		$descripcion=$_POST["descripcion"];
		$presupuesto=$_POST["persupuesto"];
		$observaciones=$_POST["observaciones"];
		$estado=$_POST["estado"];
		$avance=$_POST["avance"];
		$id_cat=$_POST["id_cat"];
		$id_prg=$_POST["id_prg"];
		$id_prio=$_POST["id_prio"];
		$borrar=$_POST["borrar"];
		$titulo_evento=$_POST["titulo_evento"];
		if($borrar=="s")
		{
			mi_query("delete from desarrollo_software where id_desa='$id_desa'","Error al borrar el registro");
			mensaje("Se borro el registro $id_desa");
		}else
		{
			mi_query("update desarrollo_software set fecha_alta='$fecha_alta',fecha_prog='$fecha_prog',fecha_ini='$fecha_ini',fecha_fin='$fecha_fin',plazo_estimado='$plazo_estimado',plazo_real='$plazo_real',descripcion='$descripcion',presupuesto='$presupuesto',observaciones='$observaciones',estado='$estado',avance='$avance',categoria='$id_cat',programador='$id_prg',prioridad='$id_prio' where id_desa='$id_desa'","Error al modificar el plan de desarrollo");
			mensaje("Modificaci&oacute;n de $id_desa grabada");
		}
		delay();
		break;
	default:
		$tit_alta="NUEVO DESARROLLO";
		$campos="%FEC-fecha alta-fecha_alta--10";
		$campos.=";%FEC-fecha programada-fecha_prog--10";
		//$campos.=";%FEC-fecha inicio-fecha_ini--10";
		//$campos.=";%FEC-fecha fin-fecha_fin--10";
		$campos.=";%TXT-plazo estimado (dias)-plazo_estimado--10";
		//$campos.=";%TXT-plazo real (dias)-plazo_real--10";
		$campos.=";%ARE-descripcion-descripcion--5-55";
		$campos.=";%TXT-presupuesto-presupuesto--5";
		//$campos.=";%ARE-observaciones-observaciones--5-55";
		$campos.=";%SEL-estado-estado-ESPERA+ESPERA+DESARROLLO+DESARROLLO+TERMINADO+TERMINADO-0";
		$campos.=";%SEL-id_cat-categoria-select id_cat,categoria from categoria order by 2-categoria+id_cat";
		$campos.=";%SEL-id_prg-programador-select id_prg,programador from programador order by 2-programador+id_prg";
		$campos.=";%SEL-id_prio-prioridad-select id_prio,prioridad from prioridad order by 2-prioridad+id_prio";
		$campos.=";%SEL-avance-avance-0+0%+10+10%+20+20%+30+30%+40+40%+50+50%+60+60%+70+70%+80+80%+90+90%+100+100%-0";
		$campos.=";%OCU-panta-graba_alta";
		mi_panta($tit_alta,$campos,$submit);
		$hay=un_dato("select count(*) from desarrollo_software");
		if($hay)
		{
			raya();
			$que_filtro="";
			$filtro=$_GET['filtro'];
			if($filtro<>"")
			{
				if($filtro=="TODOS")
				{
					$que_filtro="";
					$subtitulo="Todos los sistemas";
				}else
				{
					$que_filtro=" and estado='$filtro'";
					$subtitulo="Sistemas en estado $filtro";
				}
			}else
			{
				$que_filtro=" and estado='ESPERA'";
				$subtitulo="Sistemas En espera";
			}
			$titulos="id;alta;programada;descripcion;observaciones;estado;avance %;categoria;programador;prioridad";
			$sql="select d.id_desa,d.fecha_alta,d.fecha_prog,d.descripcion,d.observaciones,d.estado,d.avance,c.categoria,p.programador,r.prioridad from desarrollo_software d,categoria c,prioridad r,programador p where d.programador=p.id_prg and d.categoria=c.id_cat and d.prioridad=r.id_prio $que_filtro order by r.id_prio, fecha_alta;coabm_desarrollo.php+id_desa+panta+modi";
			//trace($sql);
			mi_titulo("SISTEMAS PLANIFICADOS");
			mi_tabla("i");
			echo("<tr><td><a href=coabm_desarrollo.php?filtro=TERMINADO>Terminados</a></td>");
			echo("<td><a href=coabm_desarrollo.php?filtro=DESARROLLO>En desarrollo</a></td><td>");
			echo("<td><a href=coabm_desarrollo.php?filtro=ESPERA>En espera</a></td><td>");
			echo("<td><a href=coabm_desarrollo.php?filtro=TODOS>Todos</a></td><td>");
			mi_tabla("f");
			mi_titulo($subtitulo);
			tabla_cons($titulos,$sql,1,"silver","#8EC99F",0,"ACTUALIZ.","MODIFICAR","","Sistemas Planificados;Sistemas Planificados;sistemas_planificados");
		}else
		{
			mensaje("No hay sistemas planificados para mostrar");
		}
		un_boton("Volver","Volver","copanel.php");
		break;
}

cierre();
?>