<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Liquidacion de Abonos</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");

$submit="aceptar-Confirmar-copanel.php";
if(isset($_GET["panta"]))
{
	$panta=$_GET["panta"];
}else
{
	$panta=$_POST["panta"];
}
switch($panta)
{
	case "calcular":
		if(isset($_GET["mes"]))
		{
			$mes=$_GET["mes"];
			$tecnico=$_GET["tecnico"];
		}else
		{
			$mes=$_POST["mes"];
			$tecnico=$_POST["tecnico"];
		}
		//trace("El mes es $mes y el tecnico es $tecnico");
		//$mes=$_POST["mes"];
		//$tecnico=$_POST["tecnico"];
		$xmes=strtr($mes,'-','/');
		$mes=strtr($mes,'/','-');
		$ya_liquidado=un_dato("select count(*) from liquidacion where periodo='$xmes'");
		//trace("El mes es $xmes y ya liquidado es $ya_liquidado");
		if($ya_liquidado)
			mensaje("Periodo $mes ya liquidado");
		$sql_updt="update soltrab set horas_apr=horas_reales where horas_apr is null and left(fin,7)='$mes' and tecnico='$tecnico' and aprobado is not null and estado=4";
		mi_query($sql_updt,"Error al actualizar las horas aprobadas");
		$sql="select s.id_sol,s.tarea,s.obs_tec,s.obs_enc,s.horas_apr*60 as tiempo,s.horas_apr*a.valor_hora as importe,e.descripcion as estado from soltrab s,abonos a,estado_liq e where s.estado=4 and s.aprobado=e.estado and s.aprobado is not null and s.tecnico=a.tecnico and a.tecnico='$tecnico' and left(s.fin,7)='$mes';cofactu_abonos.php+id_sol+panta+detalle+mes+$xmes";
		//trace($sql);
		$rotulos="id;tarea;observ.tecnico;observ.encargado;tiempo;importe;estado";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales="0;0;0;0;2;2";
		$tit_lnk="DETALLE";
		$btn_lnk="MODIFICAR";
		$mes_desc=substr($mes,6,2) . "/" . substr($mes,0,4);
		mi_titulo("Liquidacion de abono del mes $mes_desc del tecnico $tecnico");
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		// Seguir aca con el calculo de las horas e importes totales.
		// Generar un documento y grabar en una tabla de facturaciones.
		$sql="select round(sum(s.horas_reales),2) as horas_reales,round(sum(s.horas_apr),2) as horas_apr,round(sum(s.horas_reales*a.valor_hora),2) as importe_real,round(sum(s.horas_apr*a.valor_hora),2) as importe_apr from soltrab s,abonos a where s.estado=4 and s.tecnico=a.tecnico and a.tecnico='$tecnico' and left(fin,7)='$mes'";
		$rotulos="hs. reales;hs. aprobadas;importe hs. reales;imp. hs. aprob.";
		$tit_lnk="ACCION";
		$btn_lnk="LIQUIDAR";
		$decimales=2;
		mi_titulo("Todas las tareas");
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		$maximo=un_dato("select max_importe from abonos where tecnico='$tecnico'");
		$sql="select round(sum(s.horas_apr),2) as horas,round(sum(s.horas_apr*a.valor_hora),2) as importe,$maximo as maximo from soltrab s,abonos a where s.estado=4 and s.aprobado=1 and s.tecnico=a.tecnico and a.tecnico='$tecnico' and left(fin,7)='$mes';cofactu_abonos.php+horas+panta+liquidar+mes+$mes+tecnico+$tecnico";
		$rotulos="hs.totales;importe total;maximo";
		$tit_lnk="ACCION";
		$btn_lnk="LIQUIDAR";
		$decimales=2;
		mi_titulo("Tareas Aprobadas");
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		un_boton();
		break;
	case "liquidar":
		mi_titulo("Liquidacion de abonos");
		$mes=$_POST["mes"];
		$mes=strtr($mes,"-","/");
		$tecnico=$_POST["tecnico"];
		$horas=$_POST["horas"];
		$importe=un_dato("select round(sum(s.horas_apr*a.valor_hora),2) as importe from soltrab s,abonos a where s.estado=4 and s.aprobado=1 and s.tecnico=a.tecnico and a.tecnico='$tecnico'");
		$existe=un_dato("select count(*) from liquidacion where tecnico='$tecnico' and periodo='$mes'");
		//trace("Existe es $existe para el tecnico $tecnico y el mes $mes");
		if($existe)
		{
			mensaje("Periodo ya liquidado con anterioridad");
			$submit="Reliquidar-Reliquidar-cofactu_abonos.php";
			$campos="%OCU-mes-$mes";
			$campos.=";%OCU-tecnico-$tecnico";
			$campos.=";%OCU-horas-$horas";
			$campos.=";%OCU-importe-$importe";
			$campos.=";%OCU-panta-reliquidar";
			mi_panta("Re-Liquidacion",$campos,$submit);
		}else
		{
			$maximo=un_dato("select max_importe from abonos where tecnico='$tecnico'");
			if($importe>$maximo)
			{
				mensaje("El importe supera al m&aacute;ximo. Se ajusta a $maximo.");
				$submit="Reliquidar-Reliquidar-cofactu_abonos.php";
				$campos="%OCU-mes-$mes";
				$campos.=";%OCU-tecnico-$tecnico";
				$campos.=";%OCU-horas-$horas";
				$campos.=";%OCU-importe-$maximo";
				$campos.=";%OCU-panta-reliquidar";
				mi_panta("Re-Liquidacion",$campos,$submit);
			}else
			{
				mi_query("insert into liquidacion set fecha_liq=curdate(),periodo='$mes',tecnico='$tecnico',usuario='$uid',horas='$horas',importe='
				$importe'","Error al grabar la liquidacion del mes $mes de $tecnico");
				mi_query("update soltrab s,abonos a set aprobado=5 where s.estado=4 and s.aprobado=1 and s.tecnico=a.tecnico and a.tecnico='$tecnico' and left(fin,7)='$mes'","Error al actualizar la tabla soltrab con la liquidacion");
				mensaje("Se grabo la liquidacion de $tecnico del periodo $mes de $horas hs por un importe de $ $importe");
				// Mail para el interesado
				$nombre=un_dato("select nombre from usuarios where usuario='$tecnico'");
				$admin=un_dato("select usuario from usuarios where perfil=1");
				$asunto="Liquidacion de Abono del $mes";
				$texto="Se autoriza el pago de $importe a $nombre en concepto de abono correspondiente al periodo $mes.";
				//trace("El usuario es $usuario");
				//trace("El tecnico es $tecnico");
				mandar_mail($tecnico,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);	
				un_boton();
			}
		}
		break;
	case "reliquidar":
		mi_titulo("Liquidacion de abonos");
		$mes=$_POST["mes"];
		$mes=strtr($mes,"-","/");
		$tecnico=$_POST["tecnico"];
		$horas=$_POST["horas"];
		$importe=$_POST["importe"];
		$maximo=un_dato("select max_importe from abonos where tecnico='$tecnico'");
		if($importe>$maximo)
		{
			mensaje("El importe supera al m&aacute;ximo. Se ajusta a $maximo.");
			$submit="Reliquidar-Reliquidar-cofactu_abonos.php";
			$campos="%OCU-mes-$mes";
			$campos.=";%OCU-tecnico-$tecnico";
			$campos.=";%OCU-horas-$horas";
			$campos.=";%OCU-importe-$maximo";
			$campos.=";%OCU-panta-reliquidar";
			mi_panta("Re-Liquidacion",$campos,$submit);
		}else
		{
			$existe=un_dato("select count(*) from liquidacion where tecnico='$tecnico' and periodo='$mes'");
			//trace("Existe es $existe para el tecnico $tecnico y el mes $mes");
			if($existe)
			{
				mi_query("update liquidacion set fecha_liq=curdate(),usuario='$uid',horas='$horas',importe='$importe' where periodo='$mes' and tecnico='$tecnico'","Error al actualizar la liquidacion del mes $mes de $tecnico");
				mensaje("Se actualiz&oacute; la liquidaci&oacute;n de $tecnico del periodo $mes de $horas hs por un importe de $ $importe");
				mi_query("update soltrab s,abonos a set aprobado=5 where s.estado=4 and s.aprobado=1 and s.tecnico=a.tecnico and a.tecnico='$tecnico' and left(fin,7)='$mes'","Error al actualizar la tabla soltrab con la liquidacion");
				// Mail para el interesado
				$nombre=un_dato("select nombre from usuarios where usuario='$tecnico'");
				$admin=un_dato("select usuario from usuarios where perfil=1");
				$asunto="Liquidacion de Abono del $mes";
				$texto="Se autoriza el pago de $importe a $nombre en concepto de abono correspondiente al periodo $mes.";
				//trace("El usuario es $usuario");
				//trace("El tecnico es $tecnico");
				mandar_mail($tecnico,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);	
				un_boton();
			}else
			{
				mi_query("insert into liquidacion set fecha_liq=curdate(),periodo='$mes',tecnico='$tecnico',usuario='$uid',horas='$horas',importe='$importe'","Error al grabar la liquidacion del mes $mes de $tecnico");
				mensaje("Se grabo la liquidacion de $tecnico del periodo $mes de $horas hs por un importe de $ $importe");
				mi_query("update soltrab s,abonos a set s.aprobado=5 where s.estado=4 and s.aprobado=1 and s.tecnico=a.tecnico and a.tecnico='$tecnico' and left(fin,7)='$mes'","Error al actualizar la tabla soltrab con la liquidacion");
				// Mail para el interesado
				$nombre=un_dato("select nombre from usuarios where usuario='$tecnico'");
				$admin=un_dato("select usuario from usuarios where perfil=1");
				$asunto="Liquidacion de Abono del $mes";
				$texto="Se autoriza el pago de $importe a $nombre en concepto de abono correspondiente al periodo $mes.";
				//trace("El usuario es $usuario");
				//trace("El tecnico es $tecnico");
				mandar_mail($tecnico,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);	
				un_boton();
			}
		}
		break;
	case "detalle":
		$id_sol=$_POST["id_sol"];
		$mes=$_POST["mes"];
		$sql="select s.fecha_prog,s.usuario,s.puesto,p.descripcion as desc_puesto,s.tipo_problema,t.problema,s.dispositivo,d.dispositivo as desc_dispo,s.descripcion as desc_prob,s.tecnico,s.tarea,s.obs_tec,s.observaciones,s.obs_enc,s.horas_est,s.horas_reales,s.horas_apr ";
		$sql.="from soltrab s,tipo_problema t,puestos p,dispositivo d ";
		$sql.="where s.id_sol='$id_sol' and s.tipo_problema=t.id and s.dispositivo=d.id and s.puesto=p.codigo";
		//trace($sql);
		$cns=mi_query($sql,"Error al obtener la ot");
		$datos=mysql_fetch_array($cns);
		$titulo="REVISION DE LA ORDEN DE TRABAJO Nro. $id_sol";
		$fecha_prog=a_fecha_arg($datos["fecha_prog"]);
		$usuario=$datos["usuario"];
		$puesto=$datos["puesto"];
		$desc_puesto=$datos["desc_puesto"];
		$descripcion=$datos["desc_prob"];
		$tipo_problema=$datos["tipo_problema"];
		$problema=$datos["problema"];
		$dispositivo=$datos["dispositivo"];
		$desc_dispo=$datos["desc_dispo"];
		$tecnico=$datos["tecnico"];
		$tarea=$datos["tarea"];
		$obs_tec=$datos["obs_tec"];
		$obs_enc=$datos["obs_enc"];
		$observaciones=$datos["observaciones"];
		$horas_est=$datos["horas_est"];
		$horas_reales=$datos["horas_reales"];
		$minutos_est=$horas_est*60;
		$minutos_reales=$horas_reales*60;
		$horas_apr=$datos["horas_apr"];
		$minutos_apr=$horas_apr*60;
		$campos="%ROT-FECHA PROG.</td><td>$fecha_prog";
		$campos.=";%ROT-SOLICITANTE</td><td>$usuario de $desc_puesto";
		$campos.=";%ROT-TIPO DE PROBLEMA</td><td>$problema";
		$campos.=";%ROT-DISPOSITIVO</td><td>$desc_dispo";
		$campos.=";%ROT-MOTIVO SOL.</td><td>$descripcion";
		$campos.=";%ROT-OBSERVACIONES</td><td>$observaciones";
		$campos.=";%ROT-TAREA REALIZADA</td><td>$tarea";
		$campos.=";%ROT-TECNICO</td><td>$tecnico";
		$campos.=";%ROT-OBSERV. TECNICO</td><td> $obs_tec";
		$campos.=";%SEL-aprobado-aprobado-1+APROBADO+0+PENDIENTE+2+RECHAZADO-0";
		$campos.=";%ARE-Observ. encargado-obs_enc-$obs_enc-5-80";
		$campos.=";%ROT-TIEMPO ESTIMADO</td><td>$minutos_est minutos";
		$campos.=";%ROT-TIEMPO REAL</td><td> $minutos_reales minutos";
		$campos.=";%TXT-Tiempo aprobado-minutos_apr-$minutos_apr-5";
		$campos.=";%OCU-id_sol-$id_sol";
		$campos.=";%OCU-panta-grabar";
		$campos.=";%OCU-fecha_sol-$fecha_prog";
		$campos.=";%OCU-usuario-$usuario";
		$campos.=";%OCU-desc_puesto-$desc_puesto";
		$campos.=";%OCU-problema-$problema";
		$campos.=";%OCU-dispositivo-$desc_dispo";
		$campos.=";%OCU-descripcion-$descripcion";
		$campos.=";%OCU-fecha_prog-$fecha_prog";
		$campos.=";%OCU-observaciones-$observaciones";
		$campos.=";%OCU-obs_tec-$obs_tec";
		$campos.=";%OCU-tarea-$tarea";
		$campos.=";%OCU-tecnico-$tecnico";
		$campos.=";%OCU-mes-$mes";
		mi_panta($titulo,$campos,$submit);
		break;
	case "grabar":
		require_once("Mail.php");
		require_once("Mail/mime.php");
		mi_titulo("Aprobacion de ordenes de trabajo");
		$id_sol=$_POST["id_sol"];
		$tarea=$_POST["tarea"];
		$obs_tec=$_POST["obs_tec"];
		$obs_enc=$_POST["obs_enc"];
		$aprobado=$_POST["aprobado"];
		$fecha_prog=$_POST["fecha_prog"];
		$tecnico=$_POST["tecnico"];
		$observaciones=$_POST["observaciones"];
		$usuario=$_POST["usuario"];
		$desc_puesto=$_POST["desc_puesto"];
		$problema=$_POST["problema"];
		$dispositivo=$_POST["dispositivo"];
		$descripcion=$_POST["descripcion"];
		$minutos_apr=$_POST["minutos_apr"];
		$horas_apr=$minutos_apr/60;
		$mes=$_POST["mes"];
		$correcto=1;
		$error="";
		if($horas_apr==0)
		{
			$correcto=0;
			$error.="No se completaron las horas aprobadas para la tarea.";
		}
		if($correcto)
		{
			//trace("Estoy en correcto");
			mi_query("update soltrab set fecha_mod=curdate(),obs_enc='$obs_enc',horas_apr='$horas_apr',aprobado='$aprobado' where id_sol='$id_sol'","Error al aprobar la ot");
			if($aprobado==1)
			{
				mensaje("Orden de Trabajo $id_sol Aprobada.");
				$apro_desc="Si";
			}else
			{
				mensaje("Orden de Trabajo $id_sol No aprobada.");
				$apro_desc="No";
			}
			$fecha_mod=a_fecha_arg(un_dato("select curdate()"));
			mi_tabla("i");
			echo("<tr><td>Orden de trabajo: $id_sol</td></tr>");
			echo("<tr><td>Usuario: $usuario</td></tr>");
			echo("<tr><td>Fecha programada: $fecha_prog</td></tr>");
			echo("<tr><td>Problema: $problema</td></tr>");
			echo("<tr><td>Dispositivo: $dispositivo</td></tr>");
			echo("<tr><td>Descripcion: $descripcion</td></tr>");
			echo("<tr><td>Fecha: $fecha_mod</td></tr>");
			echo("<tr><td>Tarea realizada: $tarea</td></tr>");
			echo("<tr><td>Tecnico: $tecnico</td></tr>");
			echo("<tr><td>Observ. tecnico: $obs_tec</td></tr>");
			echo("<tr><td>Observ. encargado: $obs_enc</td></tr>");
			echo("<tr><td>Aprobado.: $apro_desc</td></tr>");
			mi_tabla("f");
			$admin=un_dato("select usuario from usuarios where perfil=1");			
			// Mail para el usuario
			$admin=un_dato("select usuario from usuarios where perfil=1");
			$asunto="Aprobacion de Orden de Trabajo";
			$texto="En relaci&oacute;n a la solicitud nro. $id_sol solicitada por $usuario por un problema de $problema con $dispositivo, $apro_desc hemos aprobado la siguiente tarea: $tarea.";
			//trace("El usuario es $usuario");
			//trace("El tecnico es $tecnico");
			mandar_mail($tecnico,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
			delay("cofactu_abonos.php?tecnico=$tecnico&mes=$mes&panta=calcular");
			//un_boton("Aceptar","Aceptar","cofactu_abonos.php");
			break;
		}else
		{
			mensaje($error);
			$campos="%OCU-panta-detalle";
			$campos.=";%OCU-id_sol-$id_sol";
			$campos.=";%OCU-tarea-$tarea";
			$campos.=";%OCU-obs_tec-$obs_tec";
			$campos.=";%OCU-estado_ot-$estado_ot";
			mi_panta("",$campos,$submit);
		}
		break;
	default:
		$titulo="Liquidacion de Abonos";
		$campos="%SEL-tecnico-tecnico-select tecnico from abonos order by 1-tecnico";
		$campos.=";%SEL-mes-mes-select distinct left(fecha_mod,7) as codigo, concat(substr(fecha_mod,6,2),'/',left(fecha_mod,4)) as descripcion from soltrab where aprobado is not null and estado=4 order by 1 desc-descripcion+codigo";
		$campos.=";%OCU-panta-calcular";
		mi_panta($titulo,$campos,$submit);
		break;
}
?>
</BODY>
</HTML>
