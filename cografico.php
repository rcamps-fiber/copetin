<?
include('phplot/phplot.php');
include('cofunciones.php');
error_reporting(0);
//echo("Toy en grafico");
$alto=$_GET["alto"];
$ancho=$_GET["ancho"];
$data=$_GET["datos"];
$titulo=$_GET["titulo"];
$eje_x=$_GET["eje_x"];
$eje_y=$_GET["eje_y"];
$ticks_h=$_GET["ticks_h"];
$grafico = new PHPlot($ancho,$alto);
$datos=unserialize(base64_decode($data));
$tipo= $_GET["tipo"];
//$grafico->SetUseTTF(1);
//$grafico->SetDefaultTTFont('benjamingothic.ttf');
$grafico->SetDataValues($datos);
$grafico->SetIsInline(true);
$grafico->SetPlotType($tipo);
$grafico->SetTitle($titulo);
$grafico->SetXTitle($eje_x);
$grafico->SetYTitle($eje_y);
//$grafico->SetPlotAreaWorld("",0,"","");
//$grafico->SetPlotAreaWorld(0,10,0,10);
//$grafico->SetNumVerttTicks(2);
$grafico->SetNumHorizTicks($ticks_h);
//$grafico->SetHorizTickIncrement(1);
//$grafico->SetVertTickIncrement(200000);
$grafico->SetPrecisionY(0);
//$grafico->SetXAxisPosition(0);
$grafico->SetXDataLabelPos("plotdown");
$grafico->DrawGraph();
?>
