<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Carga de Precios</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$volver=($prf==7) ? "coinsumos.php" : "copanel.php";
$submit="aceptar-Aceptar-$volver";
mi_titulo("Carga de Precios");
$panta=$_POST["panta"];
switch($panta)
{
	case "modi":
		$orden=$_POST["orden"];
		$cod_int=un_dato("select cod_int from precios where orden=$orden");
		$cod_prv=un_dato("select cod_prv from precios where orden=$orden");
		$proveedor=un_dato("select proveedor from precios where orden=$orden");
		$precio=un_dato("select precio from precios where orden=$orden");
		$titulo="Actualizaci&oacute;n de Precio";
		$campos_pantalla="%OCU-orden-$orden;";
		$campos_pantalla.="%OCU-cod_int-$cod_int;";
		$campos_pantalla.="%OCU-cod_prv-$cod_prv;";
		$razon=un_dato("select razon from proveedores where codigo=$proveedor");
		$campos_pantalla.="%OCU-proveedor-$proveedor;";
		if($prf==1)
		{
			$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int=$cod_int");
			$campos_pantalla.="%ROT-<tr><td>Modelo</td><td>$cod_orig</td></tr>;";
		}
		if($prf==7)
		{
			$articulo=un_dato("select articulo from insumos where id_insumo='$cod_int'");
			$campos_pantalla.="%ROT-<tr><td>Art.</td><td>$articulo</td></tr>;";
		}
		$campos_pantalla.="%TXT-precio-precio-$precio-10;";
		$campos_pantalla.="%CHK-elimina-elimina-1-n;";
		$campos_pantalla.="%OCU-panta-graba_modi";
		//$submit="aceptar-Modificar-copanel.php";	
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
	case "graba_modi":
		$orden=$_POST["orden"];
		$elimina=$_POST["elimina"];
		$cod_int=$_POST["cod_int"];
		$cod_prv=$_POST["cod_prv"];
		$proveedor=$_POST["proveedor"];
		$precio=$_POST["precio"];
		$razon=un_dato("select razon from proveedores where codigo=$proveedor");
		if($prf==1)
		{
			$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int=$cod_int");
			$msg1="Se elimin&oacute; en la lista de precios el $cod_orig para el prov. $razon.";
			$msg2="Se actualiz&oacute; en la lista de precios el $cod_orig para el prov. $razon.";
		}
		if($prf==7)
		{
			$articulo=un_dato("select articulo from insumos where id_insumo='$cod_int'");
			$msg1="Se elimin&oacute; en la lista de precios el $articulo para el prov. $razon.";
			$msg2="Se actualiz&oacute; en la lista de precios el $articulo para el prov. $razon.";
		}
		if($elimina=="1")
		{
			mi_query("delete from precios where orden=$orden","coprecios.php Linea 49. Imposible eliminar el precio.");
			mensaje($msg1);
		}else
		{
			mi_query("update precios set cod_int=$cod_int,cod_prv='$cod_prv',proveedor=$proveedor,precio=$precio where orden=$orden","coprecios.php Linea 23. Imposible actualizar precios.");
			mensaje($msg2);
		}
		un_boton();
		break;
	case "graba_alta":
		$cod_int=$_POST["cod_int"];
		$cod_prv=$_POST["cod_prv"];
		$proveedor=$_POST["proveedor"];
		$precio=$_POST["precio"];
		$razon=un_dato("select razon from proveedores where codigo=$proveedor");
		if($prf==1)
		{
			$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int=$cod_int");
			$msg="Se agreg&oacute; a la lista de precios el $cod_orig para el prov. $razon.";
		}
		if($prf==7)
		{
			$articulo=un_dato("select articulo from insumos where id_insumo='$cod_int'");
			$msg="Se agreg&oacute; a la lista de precios el art. $articulo para el prov. $razon.";
		}
		mi_query("insert into precios set cod_int=$cod_int,cod_prv='$cod_prv',proveedor=$proveedor,precio=$precio","coprecios.php Linea 23. Imposible grabar alta de precios.");
		mensaje($msg);
		un_boton();
		break;
	default:
		$titulo="Alta de Precio";
		if($prf==1)
		{
			$def_cod=un_dato("select codigo from proveedores where id_clase not in(3,4,5,6) limit 1");
			$def_rot=un_dato("select razon from proveedores where id_clase not in(3,4,5,6) limit 1");
			$campos_pantalla="%SEL-cod_int-modelo-select codigo_orig,codigo_int from cartuchos-codigo_orig+codigo_int";
			$campos_pantalla.=";%SEL-proveedor-proveedor-select codigo,razon from proveedores where id_clase not in(3,4,5,6)-razon+codigo-$def_rot-$def_cod";
		}
		if($prf==7)
		{
			$def_cod=un_dato("select codigo from proveedores  where id_clase not in(1,2) limit 1");
			$def_rot=un_dato("select razon from proveedores  where id_clase not in(1,2) limit 1");
			$campos_pantalla="%SEL-cod_int-insumo-select id_insumo,articulo from insumos order by 2-articulo+id_insumo";
			$campos_pantalla.=";%SEL-proveedor-proveedor-select codigo,razon from proveedores where id_clase not in(1,2) order by 2-razon+codigo-$def_rot-$def_cod";
		}
		$campos_pantalla.=";%TXT-cod. segun prov.-cod_prv--15";
		$campos_pantalla.=";%TXT-precio-precio-0.00-10";
		$campos_pantalla.=";%OCU-panta-graba_alta";
		//$submit="aceptar-Grabar-copanel.php";	
		mi_panta($titulo,$campos_pantalla,$submit);		
		break;
}
if($prf==1)
{
	$filtro=" and x.id_clase not in(3,4,5,6)";
	$titulos="ord.;cod.Veinfar;cod.orig;cod.prov.;proveedor;marca;color;precio";
	$sql="select p.orden,p.cod_int,c.codigo_orig,p.cod_prv,razon,c.marca,c.color,p.precio";
	$sql.=" from precios p,cartuchos c,proveedores x where p.cod_int=c.codigo_int and x.codigo=p.proveedor $filtro order by 1;coprecios.php+orden+panta+modi";
	$decimales="0;0;0;0;0;0;0;2";
}
if($prf==7)
{
	$filtro=" and x.id_clase not in(1,2)";
	$titulos="ord.;cod.Veinfar;articulo;cod.prov.;proveedor;precio";
	$sql="select p.orden,p.cod_int,i.articulo,p.cod_prv,razon,p.precio";
	$sql.=" from precios p,proveedores x,insumos i where p.cod_int=i.id_insumo and x.codigo=p.proveedor $filtro order by 1;coprecios.php+orden+panta+modi";
	$decimales="0;0;0;0;0;2";
}
//$filtro="";
$proveedor=$_POST["proveedor"];
if(isset($proveedor))
	$filtro.=" and proveedor=$proveedor";
mi_titulo("Lista de Precios");
mi_query("delete from param_tmp");
tabla_cons($titulos,$sql,1,"silver","#8EC99F",$decimales,"ACTUALIZ.","Procesar","","Precios;precios;precios");
//tabla_cons($titulo,$sql,$borde,$color,$cuerpo,$decimales="0",$tit_lnk="DETALLE",$btn_lnk="Entrar",$tgt="",$param_excel="");

?>
</BODY>
</HTML>
