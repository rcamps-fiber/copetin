<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Bitacora</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("Consultas a la bitacora");
$panta=$_POST["panta"];
switch($panta)
{
	case "modi":
		$id_sol=$_POST["id_sol"];
		$cons=mi_query("select * from soltrab where id_sol='$id_sol'","Error al obtener la solicitud");
		$datos=mysql_fetch_array($cons);
		$fecha_sol=a_fecha_arg($datos["fecha_sol"]);
		$fecha_prog=a_fecha_arg($datos["fecha_prog"]);
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$tipo_problema=$datos["tipo_problema"];
		$def_tp=un_dato("select problema from tipo_problema where id='$tipo_problema'");
		$dispositivo=$datos["dispositivo"];
		$desc_dispo=un_dato("select dispositivo from dispositivo where id='$dispositivo'");
		$descripcion=$datos["descripcion"];
		$observaciones=$datos["observaciones"];
		$estado=$datos["estado"];
		$desc_estado=un_dato("select estado from estado_ot where id='$estado'");
		$obs_tec=$datos["obs_tec"];
		$tarea=$datos["tarea"];
		$fecha_mod=a_fecha_arg($datos["fecha_mod"]);
		$fin=a_fecha_arg($datos["fin"]);
		$prioridad=$datos["prioridad"];
		$tecnico=$datos["tecnico"];
		$titulo="Consulta de Orden de Trabajo Nro. $id_sol";
		$campos="%TXT-fecha solicitud-fecha_sol-$fecha_sol-10";
		$campos.=";%TXT-usuario-xusuario-$nombre-20";
		$campos.=";%ROT-PUESTO</td><td>$desc_puesto";
		$campos.=";%TXT-tipo problema-xtipo_problema-$def_tp-30";
		$campos.=";%TXT-dispositivo-xdispositivo-$desc_dispo-30";
		$campos.=";%ARE-descripcion-xdescripcion-$descripcion-3-55";
		$campos.=";%ARE-analisis preliminar-xobservaciones-$observaciones-3-55";
		$campos.=";%TXT-fecha programada-xfecha_prog-$fecha_prog-10";
		$campos.=";%TXT-estado-xestado-$desc_estado-20";
		$campos.=";%ARE-diagnostico-xobs_tec-$obs_tec-3-55";
		$campos.=";%ARE-trabajo realizado-xtarea-$tarea-3-55";
		$campos.=";%TXT-tecnico-xtecnico-$tecnico-30";
		$campos.=";%TXT-fecha trabajo-xfecha_mod-$fecha_mod-10";
		$campos.=";%TXT-fecha finalizacion-xfin-$fin-10";
		$campos.=";%TXT-prioridad-xprioridad-$prioridad-20";
		
		//$campos.=";%CHK-elimina-elimina-1-n";
		//$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-id_sol-$id_sol";
		$submit="aceptar-Aceptar-cobitacora.php";
		mi_panta($titulo,$campos,$submit);
		break;
	default:
		$titulo="FILTRO";
		$filtro="";
		$descripcion=$_POST["descripcion"];
		if($descripcion<>"")
		{
			$filtro.=" and instr(s.descripcion,'$descripcion')";
		}
		$observaciones=$_POST["observaciones"];
		if($observaciones<>"")
		{
			$filtro.=" and instr(s.observaciones,'$observaciones')";
		}
		$obs_tec=$_POST["obs_tec"];
		if($obs_tec<>"")
		{
			$filtro.=" and instr(s.obs_tec,'$obs_tec')";
		}
		$tarea=$_POST["tarea"];
		if($tarea<>"")
		{
			$filtro.=" and instr(s.tarea,'$tarea')";
		}
		$fecha_sol_desde=$_POST["fecha_sol_desde"];
		if($fecha_sol_desde=="" or $fecha_sol_desde=="dd/mm/aaaa")
		{
			$fecha_sol_desde="dd/mm/aaaa";
		}else
		{
			$filtro.=" and s.fecha_sol >= '" . a_fecha_sistema($fecha_sol_desde) ."' ";
		}
		$fecha_sol_hasta=$_POST["fecha_sol_hasta"];
		if($fecha_sol_hasta=="" or $fecha_sol_hasta=="dd/mm/aaaa")
		{
			$fecha_sol_hasta="dd/mm/aaaa";
		}else
		{
			$filtro.=" and s.fecha_sol <= '" . a_fecha_sistema($fecha_sol_hasta) . "' ";
		}
		$usuario=$_POST["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");		
		$tipo_problema=$_POST["tipo_problema"];
		$def_tp=un_dato("select problema from tipo_problema where id='$tipo_problema'");
		$vencidas=$_POST["vencidas"];
		if($vencidas=="" or $vencidas=="Elegir")
		{
			$vencidas="Elegir";
		}
		if($vencidas=="Si")
		{
			$filtro.=" and s.fecha_prog < curdate() ";
		}
		if($vencidas=="No")
		{
			$filtro.=" and s.fecha_prog>= curdate() ";
		}
		$estado=$_POST["estado"];
		$desc_estado=un_dato("select estado from estado_ot where id='$estado'");
		if($estado=="" or $estado=="Elegir")
		{
			$desc_estado="Elegir";
			$estado="Elegir";
		}else
		{
			$filtro.=" and s.estado='$estado' ";
		}
		$tecnico=$_POST["tecnico"];
		if($tecnico=="" or $tecnico=="Elegir")
		{
			$tecnico="Elegir";
		}else
		{
			$filtro.=" and s.tecnico='$tecnico' ";
		}
		if($tipo_problema=="" or $tipo_problema=="Elegir")
		{
			$tipo_problema="Elegir";
			$def_tp="Elegir";
		}else
		{
			$filtro.=" and s.tipo_problema='$tipo_problema' ";
		}
		if($usuario=="" or $usuario=="Elegir")
		{
			$usuario="Elegir";
			$nombre="Elegir";
		}else
		{
			$filtro.=" and s.usuario='$usuario' ";
		}
		$campos.=";%TXT-descripcion-descripcion--20";
		$campos.=";%TXT-analisis preliminar-observaciones--20";
		$campos.=";%TXT-diagnostico-obs_tec--20";
		$campos.=";%TXT-trabajo realizado-tarea--20";
		$campos.=";%SEL-estado-estado-select 'Elegir' as id,'Elegir' as estado from estado_ot union select id,estado from estado_ot-estado+id-$desc_estado-$estado";	$campos.=";%SEL-tecnico-tecnico-$tecnico+$tecnico+Elegir+Elegir+alejandro+alejandro+rcamps+rcamps-0";
		$campos.=";%SEL-vencidas-vencidas-Elegir+Elegir+Si+Si+No+No-0";
		$campos.=";%SEL-tipo_problema-tipo de problema-select 'Elegir' as id,'Elegir' as problema from tipo_problema union select id,problema from tipo_problema-problema+id-$def_tp-$tipo_problema";
		$campos.=";%SEL-usuario-usuario-select 'Elegir' as usuario,'Elegir' as nombre from usuarios  union select usuario,nombre from usuarios where perfil=2-nombre+usuario-$nombre-$usuario";
		$campos.=";%TXT-fecha solicitud desde-fecha_sol_desde-$fecha_sol_desde-10";
		$campos.=";%TXT-fecha solicitud hasta-fecha_sol_hasta-$fecha_sol_hasta-10";		
		$submit="aceptar-Filtrar-copanel.php";	
		mi_panta($titulo,$campos,$submit);
		//trace("El filtro es: <br>$filtro");
		$titulos="id;usuario;tipo de problema;dispositivo;descripcion;analisis preliminar;estado;diagnostico;trabajo realizado;tecnico";
		$sql="select s.id_sol,u.nombre,t.problema,d.dispositivo,s.descripcion as desc_problema,s.observaciones,e.estado,s.obs_tec,s.tarea,s.tecnico";
		$sql.=" from soltrab s,estado_ot e,puestos p,tipo_problema t,dispositivo d,usuarios u";
		$sql.=" where s.akb=1 and s.estado=e.id and s.puesto=p.codigo and s.tipo_problema=t.id and s.dispositivo=d.id and s.usuario=u.usuario $filtro";
		$sql.=" order by id_sol desc;cobitacora.php+id_sol+panta+modi";
		//mi_titulo("Ordenes de Trabajo");
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","DETALLE","CONSULTAR","","Listado");
		un_boton("Volver","Volver","copanel.php");
		break;
}

?>
</BODY>
</HTML>
