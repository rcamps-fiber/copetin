<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Recambio de cartucho</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("Recambio de Cartucho");
$panta=$_POST["panta"];
switch($panta)
{
	case "anula":
		$solicitud=$_POST["solicitud"];
		$usuario_sol=$_POST["usuario_sol"];
		$puesto=$_POST["puesto"];
		$impresora=$_POST["impresora"];
		$cartucho=$_POST["cartucho"];
		$usu_nom=un_dato("select nombre from usuarios where usuario='$usuario_sol'");
		$puesto_desc=un_dato("select descripcion from puestos where codigo=$puesto");
		$impre_desc=un_dato("select modelo from impresoras where codigo=$impresora");
		$codigo_orig=un_dato("select codigo_orig from cartuchos where codigo_int=$cartucho");
		$marca=un_dato("select marca from cartuchos where codigo_int=$cartucho");
		$color=un_dato("select color from cartuchos where codigo_int=$cartucho");
		$cartu_desc="$codigo_orig $marca $color";
		mi_query("update solicitudes set estado='ANULADA' where numero=$solicitud","corecambio.php. Linea 25. Imposible anular la sol. nro. $solicitud");
		mensaje("Se anul&oacute; la solicitud nro. $solicitud generada por el usuario $usu_nom para el cartucho $cartu_desc de la impresora $impre_desc");
		un_boton("Aceptar","Aceptar","copanel.php");
		break;
	case "graba":
		require_once 'Mail.php';
		require_once("Mail/mime.php");
		$motivo=$_POST["motivo"];
		$observaciones=$_POST["observaciones"];
		$solicitud=$_POST["solicitud"];
		$usuario_sol=$_POST["usuario_sol"];
		$puesto=$_POST["puesto"];
		$impresora=$_POST["impresora"];
		$cartucho=$_POST["cartucho"];
		// Actualizo stock
		mi_query("update stock set cantidad=cantidad-1 where cartucho=$cartucho and cantidad>0","corecambio.php.Linea 27. Imposible actualizar stock");
		// Actualizo solicitud
		mi_query("update solicitudes set estado='FINALIZADA' where numero=$solicitud","corecambio.php. Linea 29. Imposible actualizar solicitud.");
		
		// Nuevo mail
		$nom_admin=un_dato("select nombre from usuarios where perfil=1");
		$admin=un_dato("select usuario from usuarios where perfil=1");			
		$email=un_dato("select email from usuarios where usuario='$usuario_sol'");
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario_sol'");
		$email_admin=un_dato("select email from usuarios where usuario='$admin'");
		$codigo_orig=un_dato("select codigo_orig from cartuchos where codigo_int=$cartucho");
		$marca=un_dato("select marca from cartuchos where codigo_int=$cartucho");
		$color=un_dato("select color from cartuchos where codigo_int=$cartucho");
		$cartu_desc="$codigo_orig $marca $color";
		$impre_desc=un_dato("select modelo from impresoras where codigo=$impresora");
		$responsable=un_dato("select nombre from usuarios where usuario='$uid'");
		// Mail para el usuario
		$admin=un_dato("select usuario from usuarios where perfil=1");
		$asunto="Solicitud de Recambio de Cartucho";
		$texto="Hemos realizado el recambio del cartucho de tinta codigo $cartucho  - $cartu_desc solicitado por ud. mediante solicitud $solicitud para la impresora $impre_desc.";
		mandar_mail($usuario_sol,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
		$sql_graba="insert into cambios set fecha=sysdate(),cod_int=$cartucho,numero_sol=$solicitud,";
		$sql_graba.="usuario_sol='$usuario_sol',puesto=$puesto,impresora=$impresora,motivo='$motivo',";
		$sql_graba.="observaciones='$observaciones',usuario_cambio='$uid'";
		mi_query($sql_graba,"corecambio.php. Linea 29. Imposible grabar novedad de cambio.");
		mi_titulo("Cambio realizado");
		$numero=un_dato("select max(numero_cambio) from cambios where numero_sol=$solicitud");
		$conf_sql=mi_query("select * from cambios where numero_cambio=$numero","corecambio.php. Linea 31. Imposible obtener datos del cambio");
		$datos=mysql_fetch_array($conf_sql);
		$numero=$datos["numero_cambio"];
		$fecha=a_fecha_arg($datos["fecha"]);
		$cod_int=$datos["cod_int"];
		$numero_sol=$datos["numero_sol"];
		$usuario_sol=$datos["usuario_sol"];
		$puesto=$datos["puesto"];
		$puesto_desc=un_dato("select descripcion from puestos where codigo=$puesto");
		$impresora=$datos["impresora"];
		$impre_desc=un_dato("select modelo from impresoras where codigo=$impresora");
		$motivo=$datos["motivo"];
		$observaciones=$datos["observaciones"];
		$usuario_cambio=$datos["usuario_cambio"];
		
		mi_tabla("i");
		echo("<tr><td>Fecha</td><td>$fecha</td></tr>");
		echo("<tr><td>Cartucho</td><td>$cartu_desc</td></tr>");
		echo("<tr><td>Solicitud</td><td>$numero_sol</td></tr>");
		echo("<tr><td>Solicitante</td><td>$usuario_sol</td></tr>");
		echo("<tr><td>Puesto</td><td>$puesto_desc</td></tr>");
		echo("<tr><td>Impresora</td><td>$impre_desc</td></tr>");
		echo("<tr><td>Motivo</td><td>$motivo</td></tr>");
		echo("<tr><td>Observaciones</td><td>$observaciones</td></tr>");
		echo("<tr><td>Responsable cambio</td><td>$usuario_cambio</td></tr>");
		mi_tabla("f");
		un_boton("volver","Volver","copanel.php");
		break;
	default:
		$solicitud=$_POST["numero"];
		$sol_sql=mi_query("select * from solicitudes where numero=$solicitud","corecambio.php.Linea 22. Imposible obtener la solicitud nro. $solicitud");
		$datos_sol=mysql_fetch_array($sol_sql);
		$fecha_sol=$datos_sol["fecha"];
		$usuario_sol=$datos_sol["usuario"];
		$usu_nom=un_dato("select nombre from usuarios where usuario='$usuario_sol'");
		$puesto=$datos_sol["puesto"];
		$puesto_desc=un_dato("select descripcion from puestos where codigo=$puesto");
		$impresora=$datos_sol["impresora"];
		$impre_desc=un_dato("select modelo from impresoras where codigo=$impresora");
		$cartucho=$datos_sol["cartucho"];
		$codigo_orig=un_dato("select codigo_orig from cartuchos where codigo_int=$cartucho");
		$marca=un_dato("select marca from cartuchos where codigo_int=$cartucho");
		$color=un_dato("select color from cartuchos where codigo_int=$cartucho");
		$codigo_corto=un_dato("select codigo_corto from cartuchos where codigo_int=$cartucho");
		$cartu_desc="$codigo_orig $marca $color $codigo_corto";
		//trace($cartu_desc);
		$observaciones=$datos_sol["observaciones"];
		$estado=$datos_sol["estado"];
		$stock=un_dato("select cantidad from stock where cartucho=$cartucho");
		$titulo="Procesamiento de la solicitud $solicitud";
		$campo_fec_sol="%ROT-<tr><td><strong>Fecha: " . a_fecha_arg($fecha_sol) . "</strong></td></tr>;";
		$campo_usuario="%ROT-<tr><td><strong>Solicitante:</strong></td><td><strong>$usu_nom</strong></td></tr>;";
		$campo_puesto="%ROT-<tr><td><strong>Puesto:</strong></td><td><strong>$puesto_desc</strong></td></tr>;";
		$campo_impresora="%ROT-<tr><td><strong>Impresora:</strong></td><td><strong>$impre_desc</strong></td></tr>;";
		$campo_cartucho="%ROT-<tr><td><strong>Cartucho:</strong></td><td><strong>$cartu_desc</strong></td></tr>;";
		$campo_observaciones="%ROT-<tr><td><strong>Observaciones:</strong></td><td><strong>$observaciones</strong></td></tr>;";
		$campo_estado="%ROT-<tr><td><strong>Estado:</strong></td><td><strong>$estado</strong></td></tr>;";
		$campo_stock="%ROT-<tr><td><strong>Stock actual:</strong></td><td>$stock</td></tr>;";
		$campo_motivo="%ROT-<hr>;%TXT-motivo del cambio-motivo--15;";
		$campo_observ_cmb="%ARE-observaciones-observaciones--5-50;";
		$campos_ocultos="%OCU-solicitud-$solicitud;%OCU-cartucho-$cartucho;%OCU-usuario_sol-$usuario_sol;%OCU-puesto-$puesto;%OCU-impresora-$impresora;%OCU-panta-graba";
		$campos_pantalla=$campo_fec_sol . $campo_usuario . $campo_puesto . $campo_impresora . $campo_cartucho . $campo_observaciones . $campo_estado;
		$campos_pantalla.=$campo_stock . $campo_motivo . $campo_observ_cmb . $campos_ocultos;
		$submit="aceptar-Confirmar-copanel.php";
		mi_panta($titulo,$campos_pantalla,$submit);
		// Anulacion de solicitud de recambio
		echo("<hr>");
		$titulo="Anulaci&oacute;n de la Solicitud de Recambio Nro. $solicitud.";
$campos_pantalla="%OCU-solicitud-$solicitud;%OCU-cartucho-$cartucho;%OCU-usuario_sol-$usuario_sol;%OCU-puesto-$puesto;%OCU-impresora-$impresora;%OCU-panta-anula";
		$submit="aceptar-Anular-copanel.php";
		mi_panta($titulo,$campos_pantalla,$submit);
	break;
}
?>
</BODY>
</HTML>
