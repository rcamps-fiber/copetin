<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Relacion de impresoras y puestos</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("RELACION DE IMPRESORAS Y PUESTOS");
$panta=$_POST["panta"];
switch($panta)
{
	case "modi":
		$cod=$_POST["cod"];
		$impresora=un_dato("select impresora from puesto_imp where cod='$cod'");
		$puesto=un_dato("select puesto from puesto_imp where cod='$cod'");
		$descripcion=un_dato("select descripcion from puestos where codigo='$puesto'");
		$marca=un_dato("select concat(marca,' ',modelo,' ',descripcion) from impresoras where codigo='$impresora'");		
		$titulo="Modificaci&oacute;n de relacion entre impresoras y puestos";
		$campos_pantalla="%SEL-impresora-impresora-select codigo,concat(marca,' ',modelo,' ',descripcion) as descrip from impresoras where activa order by 1-descrip+codigo-$marca-$impresora";
		$campos_pantalla.=";%SEL-puesto-puesto-select codigo,descripcion from puestos order by 2-descripcion+codigo-$descripcion-$puesto";
		$campos_pantalla.=";%CHK-elimina-elimina-1-n";
		$campos_pantalla.=";%OCU-cod-$cod";
		$campos_pantalla.=";%OCU-panta-graba_modi";
		$submit="aceptar-Grabar-copanel.php";	
		$submit="aceptar-Modificar-copuesto_imp.php";	
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
	case "graba_modi":
		$cod=$_POST["cod"];
		$impresora=$_POST["impresora"];
		$puesto=$_POST["puesto"];
		$descripcion=un_dato("select descripcion from puestos where codigo='$puesto'");
		$marca=un_dato("select concat(marca,' ',modelo,' ',descripcion) from impresoras where codigo='$impresora'");
		$elimina=$_POST["elimina"];
		if($elimina=="1")
		{
			mi_query("delete from puesto_imp where cod='$cod'","Imposible eliminar la relacion $marca - $descripcion.");
			mensaje("Se elimin&oacute; la relaci&oacute;n $marca - $descripcion");
		}else
		{
			mi_query("update puesto_imp set impresora='$impresora',puesto='$puesto' where cod='$cod'","Imposible actualizar la relacion $marca - $descripcion.");
			mensaje("Se actualiz&oacute; la relaci&oacute;n $marca - $descripcion.");
		}
		un_boton();
		break;
	case "graba_alta":
		$impresora=$_POST["impresora"];
		$puesto=$_POST["puesto"];
		$descripcion=un_dato("select descripcion from puestos where codigo='$puesto'");
		$marca=un_dato("select concat(marca,' ',modelo,' ',descripcion) from impresoras where codigo='$impresora'");
		$no_existe=un_dato("select count(*) from puesto_imp where impresora='$impresora' and puesto='$puesto'");
		if($no_existe==0)
		{
			mi_query("insert into puesto_imp set impresora='$impresora',puesto='$puesto'","Error al grabar el alta de la rel. impresora-puesto");
			mensaje("Se agreg&oacute; la relaci&oacute;n $marca - $descripcion a la tabla puesto_imp.");
		}else
		{
			mensaje("La relaci&oacute;n $marca-$descripcion ya exist&iacute;a en el sistema.");
		}
		un_boton();
		break;
	default:
		$titulo="Alta de relacion entre impresoras y puestos";
		$campos_pantalla="%SEL-impresora-impresora-select codigo,concat(marca,' ',modelo,' ',descripcion) as descrip from impresoras where activa order by 1-descrip+codigo-Elegir-Elegir";
		$campos_pantalla.=";%SEL-puesto-puesto-select codigo,descripcion from puestos order by 2-descripcion+codigo-Elegir-Elegir";
		$campos_pantalla.=";%OCU-panta-graba_alta";
		$submit="aceptar-Grabar-copanel.php";	
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
}
$titulos="id;puesto;impresora";
$sql="select x.cod as cod,p.descripcion,concat(i.marca,' ',i.modelo,' ',i.descripcion) as impresora";
$sql.=" from impresoras i,puestos p,puesto_imp x";
$sql.=" where x.puesto=p.codigo and x.impresora=i.codigo order by 1;copuesto_imp.php+cod+panta+modi";
mi_titulo("Lista de impresoras/puestos");
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Procesar");
?>
</BODY>
</HTML>
