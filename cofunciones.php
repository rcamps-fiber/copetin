<?


function linea($espacios=0) {
    echo("<hr>");
    for ($i = 0; $i <= $espacios; $i++) {
        echo("<br>");
    }
}

function espacio($espacios=0) {
    for ($i = 0; $i <= $espacios; $i++) {
        echo("<br>");
    }
}

function tabla_cons_var($titulo,$sql,$bgcolor="#FFFF00",$color="#0000FF")
//Esta version de tabla_cons genera un string con todo el html de la tabla
//para usar por ej. dentro de un pdf. Es mucho mas simple que la version original        
{
    $TablaString="";
    //$TablaString.="<table border='1'><tr>";
    $TablaString.='<table border="0" cellpadding="2" cellspacing="2" nobr="true"><thead><tr style="background-color:' . $bgcolor . ';color:' . $color . ';">';
    //$sello2.='<thead><tr style="background-color:#FFFF00;color:#0000FF;"><td></td><td align="center">Tildar</td><td align="center">Especificar</td></tr></thead>';
	$hay_dec=0;
	if(!strpos($decimales,";")=== false)
	{
		$hay_dec=1;
		$dec=explode(";",$decimales);
	}
	$resultado=mysql_query($sql);
	$cab=explode(";",strtoupper($titulo));
	foreach($cab as $col){
		$TablaString.='<td align="center">' . $col . '</td>';
	}
	$TablaString.='</tr></thead>';
	//cuerpo
	while($fila=mysql_fetch_array($resultado,MYSQL_ASSOC))
	{
		//$alter=$alter==$cuerpo ? "" : $cuerpo;
		$TablaString.='<tr>';
		$cuenta_campos=0;
		while(list($clave,$valor)=each($fila))
		{
			$cuenta_campos++;
			if($hay_dec==1)
			{
				$decimales=$dec[$cuenta_campos-1];
			}
			if(is_numeric($valor)){
				if($clave=="anio" or $clave=="mes" or $clave=="dia"){
					$TablaString.='<td align="center">' . $valor . '</td>';
				}else
				{
					$TablaString.='<td align="right">' . number_format($valor,$decimales,",",".") . '</td>';
				}
			}else{
				if($valor=="")
				{
					$TablaString.='<td></td>';
				}else
				{
					if(fecha_ok($valor))
					{
						$TablaString.='<td align="center">' . a_fecha_arg($valor) . '</td>';
					}else
					{
						$TablaString.='<td align="left">' . $valor . '</td>';
					}
				}
			}		
		}
		$TablaString.='</tr>';
	}
	$TablaString.='</table>';
        return $TablaString;
}


function mi_planilla($titulo,$campos,$submit)
{
// Pantalla de ingreso de datos en forma de planilla, con multiples renglones.
	//icaja("relative",100,0,700,100,"black");
	//icaja("relative",-2,-2,700,100,"#077F09");
	mi_titulo(strtr($titulo,"+",";"));
	$que_form="mi_plani" . rand();
	mi_form("i",$que_form);
	mi_tabla("i",0);
	$fila_titulos="";
	$entrada_orig=explode(";",$campos);
	for($i=1; $i<=20; $i++)
	{
		$entrada=$entrada_orig;
		if($i==1)
		{
			echo("<tr>");
			foreach($entrada as $celda)
			{
				if(!(strpos($celda,"%SEL")=== false))
				{
					$param_sel=explode("-",$celda);
					$rotulo=strtoupper($param_sel[2]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%CHK")=== false))
				{
					$param_chk=explode("-",$celda);
					$rotulo=strtoupper($param_chk[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%TXT")=== false))
				{
					$param_txt=explode("-",$celda);
					$rotulo=strtoupper($param_txt[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%ARE")=== false))
				{
					$param_txt=explode("-",$celda);
					$rotulo=strtoupper($param_txt[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%FEC")=== false))
				{
					$param_txt=explode("-",$celda);
					$rotulo=strtoupper($param_txt[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%PWD")=== false))
				{
					$param_txt=explode("-",$celda);
					$rotulo=strtoupper($param_txt[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if (!(strpos($celda, "%ROT") === false)) {
				    $param_rot = explode("-", $celda);
				    $rotulo = $param_rot[1];
				    //$rotulo=strtr($rotulo,"+","</td><td><strong>");
				    if (strpos($rotulo, "+")) {
					if (strpos($rotulo, "<td>") === false)
					    $rotulo = "<td valign=top><strong>" . $rotulo . "</td>";
					if (strpos($rotulo, "<tr>") === false)
					    $rotulo = "<tr>" . $rotulo . "</tr>";
					$rotulo = str_replace("+", "</td><td valign=top>", $rotulo);
				    }
				    $rotulo = str_replace("@", "&nbsp;", $rotulo);
				    if (strpos($rotulo, "<td>") === false)
					$rotulo = "<td colspan=2 valign=top><strong>" . $rotulo;
				    if (strpos($rotulo, "<tr>") === false)
					$rotulo = "<tr>" . $rotulo;
				    if (strpos($rotulo, "</td>") === false)
					$rotulo.="</td>";
				    if (strpos($rotulo, "</tr>") === false)
					$rotulo.="</tr>";
				    echo($rotulo);
				}
				/*
				if(!(strpos($celda,"%ROT")=== false))
				{
					$param_txt=explode("-",$celda);
					$rotulo=strtoupper($param_txt[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				 * 
				 */
			}
			echo("</tr>");
		}
		echo("<tr>");
		foreach($entrada as $celda)
		{
			$largo=strlen($celda);
			// El campo es un select
			if(!(strpos($celda,"%SEL")=== false))
			{
				$param_sel=explode("-",$celda);
				$campo=$param_sel[1] . "_$i";
				$rotulo=$param_sel[2];
				$sql=$param_sel[3];
				$columna=$param_sel[4];
				if(count($param_sel)==5)
				{
					$def_rot="Elegir";
					$def_cod="Elegir";
				}else
				{
					$def_rot=$param_sel[5];
					$def_cod=$param_sel[6];
				}
				if($columna=="0")
				{
					$sql=strtr($sql,"+",";");
				}else
				{
					$columna=strtr($columna,"+",";");
				}
				mi_select_plan($campo,$sql,$columna,$def_rot,$def_cod);
			}
			// El campo es un check box
			if(!(strpos($celda,"%CHK")=== false))
			{
				$param_chk=explode("-",$celda);
				$rotulo=$param_chk[1];
				$campo=$param_chk[2] . "_$i";
				$ini=$param_chk[3];
				$check=$param_chk[4];
				mi_box_plan($campo,$ini,$check);
			}
			// El campo es un texto
			if(!(strpos($celda,"%TXT")=== false))
			{
				$param_txt=explode("-",$celda);
				$rotulo=$param_txt[1];
				$campo=$param_txt[2] . "_$i";
				$ini=$param_txt[3];
				$largo=$param_txt[4];
				mi_text_plan($campo,$ini,$largo);
			}
			if(!(strpos($celda,"%ARE")=== false))
			{
				$param_txt=explode("-",$celda);
				$rotulo=$param_txt[1];
				$campo=$param_txt[2] . "_$i";
				$ini=$param_txt[3];
				$filas=$param_txt[4];
				$columnas=$param_txt[5];
				mi_textarea_plan($campo,$ini,$filas,$columnas);
			}
			if(!(strpos($celda,"%FEC")=== false))
			{
				$param_txt=explode("-",$celda);
				$rotulo=strtoupper($param_txt[1]);
				$campo=$param_txt[2] . "_$i";
				$ini=$param_txt[3];
				if($ini==0 or $ini=="")
					$ini=un_dato("select curdate()");
				
				if((strpos($ini,"/")==4 or strpos($ini,"-")==4) and (strpos($ini,"/",5)==7 or strpos($ini,"-",5)==7))
				{
					$ini=a_fecha_arg($ini);
				}
				mi_text_plan($campo,$ini,10);
			}
			if(!(strpos($celda,"%PWD")=== false))
			{
				$param_txt=explode("-",$celda);
				$rotulo=$param_txt[1];
				$campo=$param_txt[2] . "_$i";
				$ini=$param_txt[3];
				$largo=$param_txt[4];
				mi_password_plan($campo,$ini,$largo);
			}
			// El campo es hidden
			if(!(strpos($celda,"%OCU")=== false))
			{
				$param_ocu=explode("-",$celda);
				$campo=$param_ocu[1];
				$valor=$param_ocu[2];
				mi_oculto($campo,$valor);
			}
			// El campo es solamente un rotulo
			if(!(strpos($celda,"%ROT")=== false))
			{
				$param_rot=explode("-",$celda);
				$rotulo=$param_rot[1];
				if(strpos($rotulo,"+"))
				{
					if(strpos($rotulo,"<td>") === false)
						$rotulo="<td valign='top'><strong>" . $rotulo . "</td>";
					$rotulo=str_replace("+","</td><td valign='top'>",$rotulo);
				}
				$rotulo=str_replace("@","&nbsp;",$rotulo);
				if(strpos($rotulo,"<td>") === false)
					$rotulo="<td valign='top'><strong>" . $rotulo;
				if(strpos($rotulo,"</td>") === false)
					$rotulo.="</td>";
				echo($rotulo);
			}
		}
		echo("</tr>");
	}
	mi_tabla("f",1);
	if(!(strpos($submit,"-")=== false))
	{
		$param_submit=explode("-",$submit);
		$variable=$param_submit[0];
		$rotulo=$param_submit[1];
		$anterior=$param_submit[2];
		
	}else
	{
		$variable="primera";
		$anterior=$submit;
	}
	botones_submit($variable,$rotulo,$anterior);
}			

function mi_box_plan($campo,$ini,$check)
{
//	Agrega un campo check box a un formulario
//	Requiere una tabla abierta y un formulario abierto
//	Agrega una fila a la tabla
	echo("<td><input type='checkbox' name='$campo' value='$ini'");
	if($check=="s")
	{
		echo("checked");
	}
	echo("></td>");
}
function mi_text_plan($campo,$ini,$largo)
{
	echo("<td><input type='text' name='$campo' value='$ini' size='$largo'></td>");
}
function mi_textarea_plan($campo,$ini='',$filas=4,$columnas=20)
{
	echo("<tr><td colspan=2><TEXTAREA name='$campo' rows='$filas' cols='$columnas'>$ini</TEXTAREA></td>");}

function mi_password_plan($campo,$ini,$largo)
{
	echo("<td><input type='password' name='$campo' value='$ini' size='$largo'></td>");
}


function mi_select_plan($campo,$sql,$columna,$default_rot="Elegir",$default_cod="Elegir",$tr=1)
{
	if($tr==1)
	{
		echo("<td><select name='$campo'>");
	}else{
		echo("<select name='$campo'>");
	}
	if($columna=="0")
	{
		$alista=explode(";",$sql);
		$total=count($alista);
		for( $i=0 ;$i<$total ; $i+2 )
		{
			$item=$alista[$i];
			echo("<option value='$item'>");
			$i++;
			$item=$alista[$i];
			echo("$item\n");
			$i++;
		}
	}else
	{		
		$lista=mysql_query($sql) or die("Error: no se pudo armar la lista $lista");
		$cols=explode(";",$columna);
		$total=count($cols);
		$mostrar=$cols[0];
		$resultado=$cols[1];
		if(strpos($columna,";")=== false)
		{
			$resultado=$mostrar;
		}
		echo("<option selected value='$default_cod'>$default_rot\n");
		while ($i=mysql_fetch_array($lista))
		{
			$dat=$i[$resultado];
			$rot=$i[$mostrar];
			$rot=strtr($rot,chr(209),"N");
			echo("<option value='$dat'>$rot\n");
		}
	}	
	echo("</select>");
	if($tr==1)
		echo("</td>");
}	


function caja($contenido, $pos="relative", $izq=0, $arriba=0, $ancho=800, $alto=100, $fondo="gray") {
    return '<div style="position: ' . $pos . '; left: ' . $izq . 'px; top:' . $arriba . 'px; background-color: ' . $fondo . '; height: ' . $alto . 'px; width:' . $ancho . 'px;>' . $contenido . '</div>';
}

// Inserta un div inicial
function icaja($pos="relative", $izq=0, $arriba=0, $ancho=800, $alto=100, $fondo="gray") {
    echo('<div style="position: ' . $pos . '; left: ' . $izq . 'px; top:' . $arriba . 'px; background-color: ' . $fondo . '; height: ' . $alto . 'px; width:' . $ancho . 'px;text-align:left">');
}

/*
function icaja($pos="relative", $izq=0, $arriba=0, $ancho=800, $alto=100, $fondo="gray") {
    echo('<div style="position: ' . $pos . '; left: ' . $izq . 'px; top:' . $arriba . 'px; background-color: ' . $fondo . '; height: ' . $alto . 'px; width:' . $ancho . 'px;">');
}
 * 
 */

function fcaja() {
    echo("</div>");
}

// Esta funcion dibuja una caja con div
// Se usa asi ver_caja(piripipi);
function ver_caja($contenido, $pos="relative", $izq=0, $arriba=0, $ancho=800, $alto=100, $fondo="gray") {
    icaja($pos, $izq, $arriba, $ancho, $alto, $fondo);
    echo($contenido);
    fcaja();
}


function pedido_a_gasto()
{
	// Actualizo la tabla de gastos
	$q_cab=mi_query("select numero from pedido_cab where estado='FINALIZADO'","Error al obtener los pedidos finalizados");
	while($datos0=mysql_fetch_array($q_cab))
	{
		$pedido=$datos0["numero"];
		$fecha_ing=un_dato("select fecha from pedido_cab where numero='$pedido'");
		$fecha_imput=$fecha_ing;
		$rubro="insumos";
		$proveedor=un_dato("select p.razon from proveedores p,pedido_cab c where c.numero='$pedido' and c.proveedor=p.codigo");
		$fac_numero="Pedido de tinta nro. $pedido";
		$qry_det=mi_query("select * from pedido_det where numero='$pedido'","Error al obtener el detalle del pedido");
		while($datos=mysql_fetch_array($qry_det))
		{
			$cantidad=$datos["cantidad"];
			$cod_orig=$datos["cod_orig"];
			$cod_int=$datos["cod_int"];
			$marca=un_dato("select marca from cartuchos where codigo_int='$cod_int'");
			$concepto="$cantidad cartuchos de tinta codigo $cod_orig $marca";
			$precio_conf=$datos["precio_conf"];
			$importe=$precio_conf*$cantidad/1.21;
			$observaciones=$datos["obs"];
			$importe_iva=$precio_conf*$cantidad*0.21;
			$importe_total=$precio_conf*$cantidad;
			mi_query("insert into gastos set fecha_ing='$fecha_ing',fecha_imput='$fecha_imput',rubro='$rubro',concepto='$concepto',importe='$importe',fac_numero='$fac_numero',proveedor='$proveedor',observaciones='$observaciones',importe_iva='$importe_iva',importe_total='$importe_total'","Error al agregar el item a la tabla de gastos");
		}
		trace("Se cargo en gastos el pedido $pedido<br><hr>");
	}

}


function mes($numero)
{
	$meses = array(1=>"enero",2=>"febrero",3=>"marzo",4=>"abril",5=>"mayo",6=>"junio",7=>"julio",8=>"agosto",9=>"septiembre",10=>"octubre",11=>"noviembre",12=>"diciembre");
	return $meses["$numero"];
}


function excel_lnk($xls,$pos="relative",$izq=0,$arriba=25,$ancho=0,$alto=0,$fondo="pink")
{
	$contenido="<table><tr><td><form action='".$xls."'><input type='image' name='xls' value='xls' alt='xls' src='imagenes/excel.gif'></form></td><td></td></tr></table>";
	ver_caja($contenido,$pos,$izq,$arriba,$ancho,$alto,$fondo);
}

function limpiar($algo)
{
	$algo=str_replace('<',"",$algo);
	$algo=str_replace('>',"",$algo);
	$algo=htmlspecialchars($algo);
	$algo=str_replace("'","",$algo);
	$algo=str_replace('"',"",$algo);
	$algo=str_replace('-',"",$algo);
	$algo=str_replace(';',"",$algo);
	$algo=str_replace('\\',"",$algo);
	return $algo;
}


function mi_file($rotulo)
{
	$rotulo=strtoupper($rotulo);
	echo("<tr><td><strong>$rotulo</strong></td><td><input type='file' name='file'/></tr></td>");
}


function subir_archivo($parte,$carpeta="repositorio")
{
	if(!$parte)
	{
		$volver=$_SERVER['PHP_SELF'];
		echo('<form action="$volver" method="post" enctype="multipart/form-data">');
		echo('<input type="file" name="file"/>');
		echo('<input type="submit" name="submit" value="Aceptar"/>');
		echo('</form> ');
	}else
	{
		if (!is_uploaded_file($_FILES['file']['tmp_name']))
		{
			$msg=0;
		}else
		{
			$nombre_completo=$carpeta . "/" . $_FILES['file']['name'];
			while(file_exists($nombre_completo))
			{
				$rnd=rand();
				$nombre_completo.="_" . $rnd;
				mensaje("Nombre ya existente. Se cambi&oacute; el nombre al archivo.");
			}
			move_uploaded_file( $_FILES['file']['tmp_name'],$nombre_completo);
			$msg=$nombre_completo;
		}
	}
	return $msg;
}


// Crea una etiqueta form html de inicio o fin
function mi_form($que_parte,$nombre_form="cualquiera")
{
	if(!(strpos($que_parte,"i")=== false) or !(strpos($que_parte,"w")=== false))
	{
		$esta_pagina=$_SERVER['PHP_SELF'];
		if($que_parte=="w")
		{
			if(!(strpos($que_parte,"multi")=== false))
			{
				echo("<form method='post' name='$nombre_form' action='$esta_pagina' target='_blank'>");
			}else
			{
				echo("<form method='post' name='$nombre_form' action='$esta_pagina' enctype='multipart/form-data' target='_blank'>");
			}
		}else
		{
			if(!(strpos($que_parte,"multi")=== false))
			{
				echo("<form method='post' name='$nombre_form' action='$esta_pagina'>");
			}else
			{
				echo("<form method='post' name='$nombre_form' action='$esta_pagina' enctype='multipart/form-data'>");
			}
		}
	}else
	{
		echo("</form>");
	}
}

function espacio_old()
{
	echo("<br>");
}

function jscript($script)
{
	echo("<script languaje='JavaScript'>$script</script>");
}


function ventana($url,$titulo="",$ancho=800,$alto=600)
{
	jscript("window.open('$url','$titulo','width=$ancho','height=$alto');");
}


function cerrar()
{
	jscript("window.close();");
}


function imprimir()
{
	jscript("print();");
}

function hoy($formato='a') {
    if($formato=='s'){
	$hoy=date('Y/m/d');
    }else{
	$hoy=date('d/m/Y');    
    }
    return $hoy;
}

function hoy_arg()
{
    $hoy_arg=date('d/m/Y');
	//return a_fecha_arg(un_dato("select curdate()"));
    return $hoy_arg;
}

function hoy_sis()
{
    $hoy_sis=date('Y/m/d');
	//return a_fecha_arg(un_dato("select curdate()"));
    return $hoy_sis;
}


 //03-05-2006
// Crea una etiqueta tabla html de inicio o fin con o sin borde y con color o sin
function mi_tabla($que_parte="i",$borde=0)
{
// Crea una tabla o la termina, dependiendo del parametro que_parte
// Puede tener borde o no
// Se puede definir con color (fondo teal con borde silver) o sin, incluyendo la palabra color en 
// el parametro que_parte.
	if(substr($que_parte,0,1)=="i")
	{
		if(strpos($que_parte,"color")>0)
		{
			echo("<table bgcolor='teal' align='center' border='$borde' bordercolor='silver'>");
		}else
		{
			echo("<table align='center' border='$borde'>");
		}	
	}else
	{
		echo("</table>");
	}
}


function lunes($hoy){
    $hoy_sis=a_fecha_sistema($hoy);
    $hoy_time=strtotime($hoy_sis);
    $dia_de_la_sem=date("N",$hoy_time);
    $lunes_time=$hoy_time-($dia_de_la_sem-1)*24*60*60;
    $lunes=date('d/m/Y',$lunes_time);
    return $lunes;    
}

function domingo($hoy){
    $hoy_sis=a_fecha_sistema($hoy);
    $hoy_time=strtotime($hoy_sis);
    $dia_de_la_sem=date("N",$hoy_time);
    $lunes_time=$hoy_time-($dia_de_la_sem-1)*24*60*60;
    $domingo_time=$lunes_time+6*24*60*60;
    $domingo=date('d/m/Y',$domingo_time);
    return $domingo;
}

// Fin de la libreria estandarizada

function botones_submit($variable,$rotulo,$cancela)
{
//	$variable=nombre de la variable Aceptar
//	$rotulo=leyenda del boton Aceptar
//	$cancela=URL destino al cancelar

/* Esta funcion fue modificada para que permita al cancelar
ir a una pantalla en particular pasando parametros.
La funcion normal al cancelar simplemente volvia al destino indicado
en $cancela, pero sin posibilidad de pasar datos.
*/
	echo("<table align=center border=0><tr valign='top'><td><input type=submit name='$variable' value='$rotulo' style='cursor:pointer;'>");
	mi_form("f");
	echo("</td><td>");	
	$div=strpos($cancela,"+");
	if($div>0)
	{
		$datos=explode("+",$cancela);
		$tam=count($datos);
		$volver=$datos[0];
		$panta_volver=$datos[1];
		$tam=count($datos);
		//trace($tam);
		//print_r($datos);
		if($tam>1)
		{
			$ocultos="<input type='hidden' name='$variable' value='$rotulo'>";
			for($i=2; $i<=$tam; $i=$i+2)
			{
				$campo=$datos[$i];
				$valor=$datos[$i+1];
				$ocultos.="<input type='hidden' name='$campo' value='$valor'>";
			}
		}
	}else
	{
		$volver=$cancela;
		$panta_volver="nada";
		$ocultos="";
	}
	echo("<form method='post' action='$volver'>");
	echo("<input type='hidden' name='panta' value='$panta_volver'>");
	echo($ocultos);
	echo("<input type=submit name=cancelar value='Cancelar' class='button'>");
	mi_form("f");
	echo("</td></tr></table>");
}

/*function un_boton($nombre="aceptar",$valor="Aceptar",$url="")
{
	if($url=="")
		$url=$_SERVER['PHP_SELF'];
	echo("<table align=center border=0><tr><td>");
	echo("<form action='$url' method='post'>");
	echo("<input type=submit name=$nombre value=$valor>");
	echo("</form></td></tr></table>");
}*/

function un_boton($nombre="aceptar",$valor="Aceptar",$url="",$ocu="",$que_valor="")
{
	if($url=="")
		$url=$_SERVER['PHP_SELF'];
	if($nombre=="")
		$nombre="aceptar";
	if($valor=="")
		$valor="Aceptar";
	echo("<table align=center border=0><tr><td>");
	echo("<form action='$url' method='post'>");
	$ocultos = explode(";",$ocu);
	$valores = explode(";",$que_valor);
	$tam=count($ocultos);
	$tam1=count($valores);
	if ($tam > 1){
		for ($i=0; $i < $tam; $i++)
			mi_oculto($ocultos[$i], $valores[$i]);
	}
	if($ocu<>"" && $tam <= 1)
	{
	    mi_oculto($ocu,$que_valor);
		//echo("<input type='hidden' name='$ocu' value='$que_valor'>");
	}
	echo("<input type=submit name='$nombre' value='$valor'>");
	echo("</form></td></tr></table>");
}	

function mi_box($rotulo,$campo,$ini,$check)
{
//	Agrega un campo check box a un formulario
//	Requiere una tabla abierta y un formulario abierto
//	Agrega una fila a la tabla
	echo("<tr><td><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td>");
	echo("<td><input type='checkbox' name='$campo' value='$ini'");
	if($check=="s")
	{
		echo("checked");
	}
	echo("></td></tr>");
}
function mi_text($rotulo,$campo,$ini,$largo)
{
//	Agrega un campo de texto a un formulario. 
//	Requiere que exista una tabla abierta y un formulario abierto
//	Agrega una fila a la tabla
	echo("<tr><td><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td>");
	echo("<td><input type='text' name='$campo' value='$ini' size='$largo'></td></tr>");
}

function mi_password($rotulo,$campo,$ini,$largo)
{
//	Agrega un campo de texto a un formulario. 
//	Requiere que exista una tabla abierta y un formulario abierto
//	Agrega una fila a la tabla
	echo("<tr><td><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td>");
	echo("<td><input type='password' name='$campo' value='$ini' size='$largo'></td></tr>");
}

function mi_oculto($campo,$valor)
{
//	Agrega un campo oculto en un formulario, para pasar valores ya definidos
	echo("<input type='hidden' name='$campo' value='$valor'>");
}
function mi_select_celda($campo,$sql,$columna)
{
// Funcion similar a la anterior pero sin el rotulo.
// En este caso no agrega una fila sino que usa la fila actual de la tabla
// y solamente agrega una celda para la lista desplegable
// Tampoco admite una columna para rotulos y otra para datos. Devuelve lo
// mismo que se muestra en la lista.
	echo("<td><select name='$campo'>");
	if($columna=="0")
	{
		$alista=explode(";",$sql);
		$total=count($alista);
		for( $i=0 ;$i<$total ; $i+2 )
		{
			$item=$alista[$i];
			echo("<option value='$item'>");
			$i++;
			$item=$alista[$i];
			echo("$item\n");
			$i++;
		}
	}else
	{		
		$lista=mysql_query($sql) or die("Error: no se pudo armar la lista $lista");
		echo("<option selected value='Elegir'>Elegir\n");
		while ($i=mysql_fetch_array($lista))
		{
			$dato=$i[$columna];
			echo("<option value='$dato'>$dato\n");
		}
	}	
	echo("</select></td>");
}	

function mi_select($campo, $rotulo, $sql, $columna, $default_rot="Elegir", $default_cod="Elegir", $tr=1) {
    if ($tr == 1) {
        echo("<tr><td><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td>");
        echo("<td><select name='$campo'>");
    } else {
        echo("<select name='$campo'>");
    }
    if ($columna == "0") {
        $alista = explode(";", $sql);
        $total = count($alista);
        for ($i = 0; $i < $total; $i + 2) {
            $item = $alista[$i];
            echo("<option value='$item'>");
            $i++;
            $item = $alista[$i];
            echo("$item\n");
            $i++;
        }
    } else {
        $lista = mysql_query($sql) or die("Error: no se pudo armar la lista $lista");
        $cols = explode(";", $columna);
        $total = count($cols);
        $mostrar = $cols[0];
        $resultado = $cols[1];
        if (strpos($columna, ";") === false) {
            $resultado = $mostrar;
        }
        echo("<option selected value='$default_cod'>$default_rot\n");
        while ($i = mysql_fetch_array($lista)) {
            $dat = $i[$resultado];
            $rot = $i[$mostrar];
            $rot = strtr($rot, chr(209), "N");
            echo("<option value='$dat'>$rot\n");
        }
    }
    echo("</select>");
    if ($tr == 1)
        echo("</td></tr>");
}



/*
function mi_select($campo,$rotulo,$sql,$columna,$default_rot="Elegir",$default_cod="Elegir")
{
// Esta funcion es igual a mi_select pero acepta definir un valor por default 
// para el caso de que no se despliegue la lista.
//Agrega un campo select a un formulario
// Agrega una fila a la tabla y dos celdas: rotulo y select
// Debe haber un formulario abierto y una tabla abierta
// Admite tomar valores de una consulta (sql) o recibir los valores 
// mediante una lista separada por punto y coma tambi?n en el parametro sql.
// Si es una consulta ($columna <>0), se puede especificar una columna para rotulos y una para datos
// mediante el parametro columna como lista separada por punto y coma.
// Ej. 1: mi_select("cliente","Razon Social","select codigo,razsoc from clientes","razsoc;codigo");
// Ej. 2: mi_select("tipo","Reporte","impreso;pantalla;xls;pdf;csv",0)
	echo("<tr><td><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td>");
	echo("<td><select name='$campo'>");
	if($columna=="0")
	{
		$alista=explode(";",$sql);
		$total=count($alista);
		for( $i=0 ;$i<$total ; $i+2 )
		{
			$item=$alista[$i];
			echo("<option value='$item'>");
			$i++;
			$item=$alista[$i];
			echo("$item\n");
			$i++;
		}
	}else
	{		
		$lista=mysql_query($sql) or die("Error: no se pudo armar la lista $lista");
		$cols=explode(";",$columna);
		$total=count($cols);
		$mostrar=$cols[0];
		$resultado=$cols[1];
		if(strpos($columna,";")=== false)
		{
			$resultado=$mostrar;
		}
		echo("<option selected value='$default_cod'>$default_rot\n");
		while ($i=mysql_fetch_array($lista))
		{
			$dat=$i[$resultado];
			//$dat=$dat*1;
			$rot=$i[$mostrar];
			echo("<option value='" . $dat . "'>$rot\n");
		}
	}	
	echo("</select></td></tr>");
}	
*/

function mi_textarea($rotulo,$campo,$ini='',$filas=4,$columnas=20)
{
//	Agrega un campo de text_area a un formulario.
//	Requiere que exista una tabla abierta y un formulario abierto
//	Agrega una fila a la tabla
	echo("<tr><td colspan=2><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td></tr>");
	echo("<tr><td colspan=2><TEXTAREA name='$campo' rows='$filas' cols='$columnas'>$ini</TEXTAREA></td></tr>");
}

function mi_titulo($texto)
{
	echo("<h3 align='center'>" . $texto . "<h3>");
}
	
function mi_panta($titulo,$campos,$submit,$ventana="")
{
//El parametro titulo es el texto del titulo de la pantalla
//El parametro campo es compuesto: Indica los elementos de entrada que compondran la pantalla
// y los parametros necesarios para cada uno.
// La sintaxis es la siguiente: 
// El tipo de campo: se indica con uno de los siguientes codigos: %SEL,%CHK,%TXT
// Los parametros correspondientes a dicho campo.
// Si es un select, los subparametros seran: nombre del campo, rotulo,sql y columna (Estos subpar?metros deber?n
// separarse por un signo + en lugar de punto y coma, que se usa para los par?metros principales.
// Si es un check box: los subparametros ser?n rotulo, campo, valor inicial y  marcado.
// Si es un text los subparametros seran: rotulo, campo, valor inicial y longitud.
// El par?metro "anterior" indica la direcci?n url a la cual se debe volver al pulsar cancelar.
// Ejemplo:
// mi_panta("Analisis de saldos Veinfar","cliente;%SEL-cliente-Cliente-caso uno+caso uno+caso dos+caso dos+caso tres+caso tres+caso cuatro+caso cuatro-0;dolarizado;%CHKdolarizado","tc_resumen.php");
	mi_titulo(strtr($titulo,"+",";"));
	$multipart=(!(strpos($campos,"%FIL")=== false)) ? "MULTI" : "";
	if($ventana<>"")
	{
		mi_form("w,$multipart");
	}else
	{
		mi_form("i,$multipart");
	}
	mi_tabla("i",0);
	$entrada=explode(";",$campos);
	foreach($entrada as $celda)
	{
		$largo=strlen($celda);
		// El campo es un select
		if(!(strpos($celda,"%SEL")=== false))
		{
			$param_sel=explode("-",$celda);
			$campo=$param_sel[1];
			$rotulo=$param_sel[2];
			$sql=$param_sel[3];
			$columna=$param_sel[4];
			if(count($param_sel)==5)
			{
				$def_rot="Elegir";
				$def_cod="Elegir";
			}else
			{
				$def_rot=$param_sel[5];
				$def_cod=$param_sel[6];
			}
			if($columna=="0")
			{
				$sql=strtr($sql,"+",";");
			}else
			{
				$columna=strtr($columna,"+",";");
			}
			//echo("<tr><td>$campo - $rotulo - $sql - $columna</td></tr>");
			mi_select($campo,$rotulo,$sql,$columna,$def_rot,$def_cod);
			//mi_tabla("i",0);
		}
		// El campo es un check box
		if(!(strpos($celda,"%CHK")=== false))
		{
			$param_chk=explode("-",$celda);
			$rotulo=$param_chk[1];
			$campo=$param_chk[2];
			$ini=$param_chk[3];
			$check=$param_chk[4];
			//echo("<tr><td>$rotulo - $cmapo - $ini - $check</td></tr>");
			mi_box($rotulo,$campo,$ini,$check);
			//mi_tabla("i",0);
		}
		// El campo es un texto
		if(!(strpos($celda,"%TXT")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=$param_txt[1];
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			$largo=$param_txt[4];
			mi_text($rotulo,$campo,$ini,$largo);
			//echo("<td><input type='text' name='$celda'></td></tr>");
		}
		if(!(strpos($celda,"%PWD")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=$param_txt[1];
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			$largo=$param_txt[4];
			mi_password($rotulo,$campo,$ini,$largo);
			//echo("<td><input type='text' name='$celda'></td></tr>");
		}
		// El campo es hidden
		if(!(strpos($celda,"%OCU")=== false))
		{
			$param_ocu=explode("-",$celda);
			$campo=$param_ocu[1];
			$valor=$param_ocu[2];
			mi_oculto($campo,$valor);
			// echo("<input type='hidden' name='$campo' value='$valor'>");
		}
		// El campo es solamente un rotulo
		if(!(strpos($celda,"%ROT")=== false))
		{
			$param_rot=explode("-",$celda);
			$rotulo=$param_rot[1];
			$rotulo=strtr($rotulo,"+",";");
			$rotulo=str_replace("@","&nbsp;",$rotulo);
			if(strpos($rotulo,"<tr>") === false)
				$rotulo="<tr><td><strong>" . $rotulo . "</strong></td></tr>";
			$rotulo=str_replace("##","</td><td>",$rotulo);
			echo($rotulo);
		}
		if(!(strpos($celda,"%ARE")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=$param_txt[1];
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			$filas=$param_txt[4];
			$columnas=$param_txt[5];
			mi_textarea($rotulo,$campo,$ini,$filas,$columnas);
			//echo("<td><input type='text' name='$celda'></td></tr>");
		}
		if(!(strpos($celda,"%FEC")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=strtoupper($param_txt[1]);
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			if($ini==0 or $ini=="")
				$ini=un_dato("select curdate()");
			
			if((strpos($ini,"/")==4 or strpos($ini,"-")==4) and (strpos($ini,"/",5)==7 or strpos($ini,"-",5)==7))
			{
				$ini=a_fecha_arg($ini);
			}
			mi_text($rotulo,$campo,$ini,10);
		}
		if(!(strpos($celda,"%FIL")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=strtoupper($param_txt[1]);
			mi_file($rotulo);
		}
	}
	mi_tabla("f",1);
	echo("<br>");
	if(!(strpos($submit,"-")=== false))
	{
		$param_submit=explode("-",$submit);
		$variable=$param_submit[0];
		$rotulo=$param_submit[1];
		$anterior=$param_submit[2];
		
	}else
	{
		$variable="primera";
		$anterior=$submit;
	}
	botones_submit($variable,$rotulo,$anterior);
}			
	
function trace($que_cosa)
{
	echo("<br>$que_cosa<br>");
}

function mensaje($que_cosa)
{
	echo("<hr><strong><center>$que_cosa</center></strong><hr>");
}

function tabla_existe($base, $table)
{
   $exists = mysql_query("SHOW TABLES LIKE '$table'");
  
   return mysql_num_rows($exists) == 1;
}

//setlocale(LC_MONETARY, 'en_US');
//echo money_format('%i', $number) . "\n";  
//------------------------------------------------------------------------------
function tabla_cons($titulo,$sql,$borde,$color,$cuerpo,$decimales="0",$tit_lnk="DETALLE",$btn_lnk="Entrar",$tgt="",$param_excel="")
/* Ej. tabla_cons("Este es el Titulo","select * from clientes",1,"cyan","gray",2);
 Agregado: posiblidad de inluir un link: dentro del parametro $sql se debe agregar ;campo_lnk:url_lnk
 Eso indica a la funcion que el ponga un boton de edicion pasando campo_lnk como parametro a url_lnk.
 Nuevos parametros: se puede ahora cambiar el titulo del campo link, y el rotulo del cartel
 con los parametros $tit_lnk y $btn_lnk
 Ademas se agrega la posibilidad de crear un pdf y un xls a partir de la tabla
 */

{
	//icaja($pos="relative",$izq=0,$arriba=0,$ancho=800,$alto=100,$fondo="gray",$tam=12);
	//icaja("relative",25,10,800,50,"blue",5);
	//echo $titulo;
	//setlocale(LC_MONETARY,'en_US');
	echo('<div id="tabla_cons" style="color: black">');
	$borde=$borde=="" ? 1 : $borde;
	$color=$color=="" ?"silver" : $color;
	//$color=$color=="" ?"silver" : $color;
	$cuerpo=$cuerpo=="" ?"#DAFDFF" : $cuerpo;
	//$cuerpo=$cuerpo=="" ?"teal" : $cuerpo;
	$alter=$cuerpo;
	// Verifico si en el parametro sql pase tambien links
	if(!strpos($sql,";")=== false)
	{
		$lnk=1;
		$subparam=explode(";",$sql);
		$sql=$subparam[0];
		$link=explode("+",$subparam[1]);
		$url_lnk=$link[0];
		$campo_lnk=$link[1];
		$tot_vars=count($link);
	}else{
		$lnk=0;
		$campo_lnk="";
	}
	// Veo si pase distintos decimales.
	$hay_dec=0;
	if(!strpos($decimales,";")=== false)
	{
		$hay_dec=1;
		$dec=explode(";",$decimales);
	}
	$resultado=mysql_query($sql) or die( mysql_error() );
	$cab=explode(";",strtoupper($titulo));
	// Parametros excel:
	//Titulo de la planilla
	//Nombre de la hoja (solapa)
	// Parte (Inicial i,Final f,Total t,Parcial p)
	if($param_excel<>"")
	{
		if(!strpos($param_excel,";")=== false)
		{
			$subxls=explode(";",$param_excel);
			$tit_xls=$subxls[0];
			$hoja=$subxls[1];
			$archivo=$subxls[2];
			$xls=excel($tit_xls,$titulo,$sql,$hoja,$archivo);
		}else
		{
			$xls=excel($tit_xls,$titulo,$sql);
		}
		excel_lnk($xls);
		//echo("<table><tr><td><form action='$xls'><input type='image' name='xls' value='xls' alt='xls' src='imagenes/excel.gif'></form></td><td></td></tr></table>");
	}
	//cabecera
	//echo("<p><font color='white'>");
	echo("<p><font color='black'>");
	//echo("<table align='center' border='$borde' bgcolor='$color' color='white'><tr align='center' bgcolor=$cuerpo>");
	echo("<table align='center' border='$borde' bgcolor='$color' color='white' font_color='black'><tr align='center' bgcolor=$cuerpo>");
	foreach($cab as $col){
		echo("<td align='center'><strong>$col</strong></td>");
	}
	if($lnk==1)
	{
		echo("<td align='center'><strong>$tit_lnk</strong></td>");
	}
	echo("</tr>");
	//cuerpo
	while($fila=mysql_fetch_array($resultado,MYSQL_ASSOC))
	{
		$alter=$alter==$cuerpo ? "" : $cuerpo;
		echo("<tr align='center' bgcolor=$alter>");
		$cuenta_campos=0;
		while(list($zclave,$valor)=each($fila))
		{
			$cuenta_campos++;
			if($hay_dec==1)
			{
				$decimales=$dec[$cuenta_campos-1];
			}
			if($zclave==$campo_lnk)
			{
				$vcampo=$valor;
			}
			if(is_numeric($valor)){
				if($zclave=="anio" or $zclave=="mes" or $zclave=="dia"){
					echo("<td align=center>" . $valor . "</td>");
				}else
				{
					echo("<td align=right>" . number_format($valor,$decimales,",",".") . "</td>");
				}
			}else{
				if($valor=="")
				{
					echo("<td align=center>&nbsp;</td>");
				}else
				{
					if(fecha_ok($valor))
					{
						echo("<td align=center>" . a_fecha_arg($valor) . "</td>");
					}else
					{
						echo("<td align=center>" . $valor . "</td>");
					}
				}
			}		
		}
		if($lnk==1)
		{
			if($url_lnk=="" or !isset($url_lnk))
			{
				$pagina=$_SERVER['PHP_SELF'];
			}else
			{
				$pagina=$url_lnk;
			}
			if($tgt=="")
				$tgt="_self";
			$string_lnk="<td align='center'><br><form method='post' action='$pagina' target='$tgt'><input type='hidden' name='clave' value='$campo_lnk'><input type='hidden' name='$campo_lnk' value='$vcampo'>";
			if($tot_vars>2)
			{
				for($x=2 ; $x < $tot_vars ; $x=$x+2)
				{
					$nombre_campo=$link[$x];
					$y=$x+1;
					$valor_nom=$link[$y];
					$string_lnk=$string_lnk . "<input type='hidden' name='$nombre_campo' value='$valor_nom'>";
				}
			}
			
			$string_lnk=$string_lnk . "<input type='submit' name='$btn_lnk' value='$btn_lnk' class='button'></form></td>";
			echo($string_lnk);
		}
		echo("</tr>");
	}
	echo("</table></font></p>");
	echo('</div>');
	//fcaja();
}



function excel($titulo,$campos,$sql,$hoja="copetin",$archivo="copetin")
{
	require_once 'Spreadsheet/Excel/Writer.php';
	$archivo="tmp/" . $archivo . ".xls";
	$resultado=mi_query($sql,"Error al obtener los datos para exportar a excel");
	$cab=explode(";",strtoupper($campos));
	// Veo si pase distintos decimales.
	//trace("Los decimales son: $decimales");
	$workbook = new Spreadsheet_Excel_Writer($archivo);
	//$workbook->send($archivo,"_blank");
	$worksheet=& $workbook->addWorksheet($hoja);
	$format_bold =& $workbook->addFormat();
	$format_bold->setBold();
	$format_titulo=& $workbook->addFormat();
	$format_titulo->setBold();
	$format_titulo->setColor("yellow");
	$format_titulo->setPattern(1);
	$format_titulo->setFgColor("blue");
	$format_titulo->setAlign("default");
	$j=0;
	$worksheet->write($j,0,$titulo,$format_titulo);
	$worksheet->write($j,1,"",$format_titulo);
	$worksheet->write($j,2,"",$format_titulo);
	$worksheet->write($j,3,"",$format_titulo);
	$worksheet->write($j,4,"",$format_titulo);
	$j++;
	$format_encabezado=& $workbook->addFormat();
	$format_encabezado->setAlign("center");
	$format_encabezado->setFgColor("magenta");
	$format_encabezado->setPattern(1);
	$format_encabezado->setColor("yellow");
	$format_encabezado->setBold();
	$format_parametros=& $workbook->addFormat();
	$j++;
	if(tabla_existe("cartuchos","param_tmp"))
	{
		$qry_prm=mi_query("select * from param_tmp","Error al buscar los parametros");
		$hay=un_dato("select count(*) from param_tmp where valor<>'Elegir' and valor<>'TODAS' and valor<>'TODOS' and valor<>''");
		if($hay)
		{
			//trace("Hay $hay parametros");
			$worksheet->write($j,0,"Parametros",$format_bold);
			$j++;
			while($datos=mysql_fetch_array($qry_prm))
			{
				$cartel=$datos["cartel"];
				$valor=$datos["valor"];
				$tipo=$datos["tipo"];
				if($valor=="S")
				{
					$valor="Si";
				}
				$color=$color=="" ?" color" : "";
				if($valor<>"TODAS" and $valor<>"TODOS" and $valor<>"Elegir" and $valor<>"")
				{
					$worksheet->write($j,0,$cartel,$format_parametros);
					$worksheet->write($j,1,$valor);
					$j++;
				}
			}
		}
	}
	$j++;
	$i=0;
	foreach($cab as $col){
		$worksheet->write($j,$i,$col,$format_encabezado);
		$i++;
	}
	$j++;
	while($fila=mysql_fetch_array($resultado,MYSQL_ASSOC))
	{
		$i=0;
		while(list($clave,$valor)=each($fila))
		{
			//$valor=htmlspecialchars($valor);
			if(!(strpos($clave,"fecha")=== false))
				$valor=a_fecha_arg($valor);
			$valor=limpiar($valor);
			/*
			$valor=strtr($valor,"'"," ");
			$valor=strtr($valor,"-"," ");
			$valor=strtr($valor,";"," ");
			$valor=strtr($valor,"\"," ");
			*/
			$valor=un_dato("select replace('$valor','<br>','')");
			$worksheet->write($j,$i,$valor);
			$i++;
		}
		$j++;
	}
	$workbook->close();
	return $archivo;
}


function excel_old2($titulo,$campos,$sql,$hoja="copetin",$archivo="copetin")
{
	require_once 'Spreadsheet/Excel/Writer.php';
	$archivo="tmp/" . $archivo . ".xls";
	$resultado=mi_query($sql,"Error al obtener los datos para exportar a excel");
	$cab=explode(";",strtoupper($campos));
	// Veo si pase distintos decimales.
	//trace("Los decimales son: $decimales");
	$workbook = new Spreadsheet_Excel_Writer($archivo);
	//$workbook->send($archivo,"_blank");
	$worksheet=& $workbook->addWorksheet($hoja);
	$format_bold =& $workbook->addFormat();
	$format_bold->setBold();
	$format_titulo=& $workbook->addFormat();
	$format_titulo->setBold();
	$format_titulo->setColor("yellow");
	$format_titulo->setPattern(1);
	$format_titulo->setFgColor("blue");
	$format_titulo->setAlign("merge");
	$worksheet->write(0,0,$titulo,$format_titulo);
	$worksheet->write(0,1,"",$format_titulo);
	$worksheet->write(0,2,"",$format_titulo);
	$worksheet->write(0,3,"",$format_titulo);
	$worksheet->write(0,4,"",$format_titulo);
	$format_titulo->setAlign("center");
	$i=0;
	foreach($cab as $col){
		$worksheet->write(1,$i,$col,$format_titulo);
		$i++;
	}
	$j=2;
	while($fila=mysql_fetch_array($resultado,MYSQL_ASSOC))
	{
		$i=0;
		while(list($clave,$valor)=each($fila))
		{
			//$valor=htmlspecialchars($valor);
			if(!(strpos($clave,"fecha")=== false))
				$valor=a_fecha_arg($valor);
			$valor=limpiar($valor);
			/*
			$caracter="&#039;";
			$valor=strtr($valor,$caracter," ");
			$valor=strtr($valor,"-"," ");
			$valor=strtr($valor,";"," ");
			$valor=strtr($valor,"\"," ");
			*/
			$valor=un_dato("select replace('$valor','<br>','')");
			$worksheet->write($j,$i,$valor);
			$i++;
		}
		$j++;
	}
	$workbook->close();
	return $archivo;
}


function excel_old($titulo,$campos,$sql)
{
	require_once 'Spreadsheet/Excel/Writer.php';
	$archivo="tmp/copetin.xls";
	//die($sql);
	$resultado=mi_query($sql,"Error al obtener los datos para exportar a excel");
	$cab=explode(";",strtoupper($campos));
	// Veo si pase distintos decimales.
	//trace("Los decimales son: $decimales");
	$workbook = new Spreadsheet_Excel_Writer($archivo);
	//$workbook->send($archivo,"_blank");
	$worksheet=& $workbook->addWorksheet('reporter');
	$format_bold =& $workbook->addFormat();
	$format_bold->setBold();
	$format_titulo=& $workbook->addFormat();
	$format_titulo->setBold();
	$format_titulo->setColor("yellow");
	$format_titulo->setPattern(1);
	$format_titulo->setFgColor("blue");
	$format_titulo->setAlign("merge");
	$worksheet->write(0,0,$titulo,$format_titulo);
	$worksheet->write(0,1,"",$format_titulo);
	$worksheet->write(0,2,"",$format_titulo);
	$worksheet->write(0,3,"",$format_titulo);
	$worksheet->write(0,4,"",$format_titulo);
	$worksheet->write(0,5,"",$format_titulo);
	$worksheet->write(0,6,"",$format_titulo);
	$format_titulo->setAlign("center");
	$i=0;
	foreach($cab as $col){
		$worksheet->write(1,$i,$col,$format_titulo);
		$i++;
	}
	$j=2;
	while($fila=mysql_fetch_array($resultado,MYSQL_ASSOC))
	{
		$i=0;
		while(list($clave,$valor)=each($fila))
		{
			//$valor=htmlspecialchars($valor);
			$valor=limpiar($valor);
			/*
			$valor=strtr($valor,"'"," ");
			$valor=strtr($valor,"-"," ");
			$valor=strtr($valor,";"," ");
			$valor=strtr($valor,"\"," ");
			*/
			$valor=un_dato("select replace('$valor','<br>','')");
			$worksheet->write($j,$i,$valor);
			$i++;
		}
		$j++;
	}
	$workbook->close();
	return $archivo;
}





function un_dato($sql,$cual=0)
{
	$rst=mi_query($sql,"Func. un_dato: imposible obtener el dato");
	if(mysql_num_rows($rst)>0)
	{
		return mysql_result($rst,0,$cual);
	}else
	{
		return 0;
	}
}	


function volver($uid)
{
	if($uid=="")
	{
		$donde=$_SERVER['PHP_SELF'];
	}else
	{
		$donde=un_dato("select p.url from perfiles p,usuarios u where p.prf=u.perfil and u.usuario='$uid'");
	}
	//trace("Volviendo a $donde...");
	un_boton("Aceptar","Aceptar",$donde);
}

function home($uid="")
{
	if($uid=="")
	{
		$donde=$_SERVER["PHP_SELF"];
	}else
	{
		$donde=un_dato("select p.url from perfiles p,usuarios u where p.prf=u.perfil and u.usuario='$uid'");
	}
	return $donde;
}


function aceptar($valor,$volver)
{
?>
<br>
<table align=center border=0><tr><td><form action="<? echo $_SERVER['PHP_SELF']; ?>" method='post'>
<input type=submit name="<? echo $valor;?>" value="Aceptar"></form></td>
<td><form action="<? echo $volver; ?>" method='post'>
<input type=submit name="cancelar" value="cancelar"></form></td>
</tr></table>
<br>
<?
}

function delay($url="self")
{	
	if($url=="self")
		$url=$_SERVER["PHP_SELF"];
	$delay = "3";
	echo '<meta http-equiv="refresh" content="'.$delay.';url='.$url.'">';
}

function mi_query($sql,$mensa="Cambie el programador y vuelva a intentar.")
{
	$mensa="Error: no fue posible completar la consulta. " . $mensa . "<br>";
	$q=mysql_query($sql) or die($mensa . mysql_error() . "<br>" .$sql);
	return $q;
}

function select_fecha()
{
	$lista_mes="0;Elegir;1;enero;2;febrero;3;marzo;4;abril;5;mayo;6;junio;7;julio;8;agosto;9;septiembre;10;octubre;11;noviembre;12;diciembre";
	$lista_ano="select distinct anio from periodo order by 1";
	mi_form("i");
	mi_tabla("i","0");
	echo("<tr><td><strong>Fecha</strong></td><td>&nbsp</td><td>&nbsp</td></tr><tr><td>");
	mi_tabla("i","0");
	mi_select("mdesde","desde",$lista_mes,0);
	mi_tabla("f","0");
	echo("</td><td>");
	mi_tabla("i","0");
	mi_select("adesde","",$lista_ano,"anio");
	mi_tabla("f","0");
	echo("</td></tr><tr><td>");
	mi_tabla("i","0");
	mi_select("mhasta","hasta",$lista_mes,0);
	mi_tabla("f","0");
	echo("</td><td>");
	mi_tabla("i","0");
	mi_select("ahasta","",$lista_ano,"anio");
	mi_tabla("f","0");
	echo("</td></tr>");
	mi_tabla("f","0");
}	

function mi_fila($que_parte,$datos)
{
//	Agrega una fila a una tabla con color de fondo verde o plateado
//  Permite especificar si se trata de una fila de titulos o de datos (que_parte: t=titulo)
//  El contenido se pasa mediante el parametro datos, que ser? una lista separada por punto y coma
//	conteniendo los valores de cada una de las celdas de la fila
	//$x=strpos($que_parte,"color");
	//trace("strpos vale $x");
	if(!strpos($que_parte,"color")=== false )
	{
		echo("<tr bgcolor='#f9e08c'>");
		//echo("<tr bgcolor='silver'>");
	}else
	{
		echo("<tr bgcolor='#f7e8aa '>");
		//echo("<tr bgcolor='#9b4f96'>");
	}		
	if(!strpos($que_parte,"t")=== false )
	{
		$cab=explode(";",strtoupper($datos));
		foreach($cab as $col)
		{
			echo("<td align='center'><strong>$col</strong></td>");
		}	
		echo("</tr>");
	}else
	{
		$cab=explode(";",$datos);
		foreach($cab as $col)
		{
			echo("<td>$col</td>");
		}	
		echo("</tr>");
	}
}


function mi_html($parte,$titulo)
{
	if($parte=="i")
	{
		echo("<html><head><title>$titulo</title></head>");
	}else
	{
		echo("</body></html>");
	}
}	

function borro_param_tmp()
{	
	if(tabla_existe("tablero","param_tmp"))
	{
		$borro=mi_query("drop table param_tmp","Error: no se puede borrar la tabla param_tmp");
	}
	$tabla_param=mi_query("create table param_tmp (nombre char(30),valor char(20))","Error: no fue posible crear la tabla temporal de parametros");
}

function a_fecha_arg($fecha_orig)
{
	if(fecha_ok($fecha_orig))
	{
		return date("d/m/Y",strtotime($fecha_orig));
	}else
	{
		return "00/00/0000";
	}
}
function a_fecha_sistema($fecha_orig)
{
	$dia=substr($fecha_orig,0,2);
	$mes=substr($fecha_orig,3,2);
	$anio=substr($fecha_orig,6,4);
	return $anio . "-" . $mes . "-" . $dia;
}
function fecha_ok($campo)
{
	if(strlen($campo)<>10)
		return false;
	$anio=substr($campo,0,4);
	$mes=substr($campo,5,2);
	$dia=substr($campo,8,2);
	return checkdate($mes*1,$dia*1,$anio*1);
}

function sumar_dias($fecha="", $dias="") {
    if ($fecha == "")
        $fecha = hoy();
    if ($dias == "")
        $dias = 1;
    return un_dato("select date_add('$fecha', interval $dias day)");
}

function sumar_anios($fecha="", $anios="") {
    if ($fecha == "")
        $fecha = hoy();
    if ($anios == "")
        $anios = 1;
    $fecha = a_fecha_sistema($fecha);
    $sql = "select date_add('$fecha', interval $anios year)";
    $resultado = un_dato($sql);
    return $resultado;
}

function sumar_meses($fecha="", $meses="") {
    if ($fecha == "")
        $fecha = hoy();
    if ($meses == "")
        $meses = 1;
    $fecha = a_fecha_sistema($fecha);
    return un_dato("select date_add('$fecha', interval $meses month)");
}


function imagen_prod($foto)
{
	echo("<table align='center' border='1'><tr><td><img src='$foto' alt='produccion'></td></tr></table>");
	echo("</layer></td></tr>");
}

function campos($la_tabla)
{
	$sql="desc $la_tabla";
	//trace($sql);
	$q=mi_query($sql,"codigo 101. Imposible obtener los campos de la tabla $la_tabla");
	$i=0;
	while($datos=mysql_fetch_array($q))
	{
		$cab[$i]=$datos["Field"];
		$nombre=$cab[$i];
		$i++;
	}
	return $cab;
}

function tipos($tabla)
{
	$sql="desc $tabla";
	$q=mi_query($sql,"codigo 101. Imposible obtener los tipos de la tabla $tabla");
	$i=0;
	while($datos=mysql_fetch_array($q))
	{
		$cab[$i]=$datos["Type"];
		$nombre=$cab[$i];
		$i++;
	}
	return $cab;
}
function que_tipo($tabla,$campo)
{
	$campos=campos($tabla);
	$tipos=tipos($tabla);
	for($i=0; $i<count($campos) ; $i++)
	{
		if($campos[$i]==$campo)
			return $tipos[$i];
	}
	return 0;
}
			
function que_tabla($base="cartuchos",$panta="",$tabla="tabla")
{
	mi_form("i");
	mi_tabla("i",0);
	mi_select($tabla,"tabla","show tables","Tables_in_" . $base);
	mi_tabla("f",0);
	echo("<br>");
	mi_tabla("i",0);
	echo("<tr><td><input type='hidden' name='panta' value='$panta'><input type=submit name=aceptar value=Aceptar></td></tr></table>");
	mi_form("f");
}
function abm_alta($parte=1,$tabla,$aceptar="Alta",$campo_tabla="tabla")
{
	//trace("y la tabla es $tabla");
	if($parte==1)
	{
		$campos=campos($tabla);
		$tipos=tipos($tabla);
		$parametros="";
		for($i=0;$i<count($campos);$i++)
		{
			$nom_campo=$campos[$i];
			$tipo=$tipos[$i];
			if(!(strpos($tipo,"(") === false))
			{
				$start=strpos($tipo,"(")+1;
				$end=strpos($tipo,")")-1;
				$longitud=substr($tipo,$start,$end-$start+1);
				if($longitud>50)
					$longitud=50;
			}else
			{
				if($tipo=="text")
				{
					$longitud=50;
				}else
				{
					$longitud=10;
				}
			}
			if(!(strpos($tipo,"int") === false))
			{
				$val_ini=0;
			}
			if(!(strpos($tipo,"float") === false))
			{
				$val_ini=0;
			}
			if(!(strpos($tipo,"char") === false))
			{
				$val_ini="";
			}
			if(!(strpos($tipo,"date") === false))
			{
				$val_ini="dd/mm/aaaa";
			}
			
			$parametros.=";%TXT-$nom_campo-$nom_campo-$val_ini-$longitud";
		}
		$parametros="%OCU-$campo_tabla-$tabla;%OCU-panta-alta2" . $parametros;
		$volver=$_SERVER["PHP_SELF"];
		mi_panta("",$parametros,"enviar-" . $aceptar . "-$volver");
	}else
	{
		//trace("estoy en la parte 2");
		$campos=campos($tabla);
		$tipos=tipos($tabla);
		$sql="insert into $tabla set ";
		for($i=0;$i<count($campos);$i++)
		{
			$nom_campo=$campos[$i];
			$tipo=$tipos[$i];
			$$nom_campo=$_POST[$nom_campo];
			$valor=$$nom_campo;
			if(!(strpos($tipo,"date") === false))
			{
				$valor=a_fecha_sistema($valor);
			}
			$sql.="$nom_campo='$valor',";
		}
		$sql=substr($sql,0,strlen($sql)-1);
		mi_query($sql,"codigo 85. Imposible dar de alta el registro en $tabla");
		mi_titulo("se actualiz&oacute; con &eacute;xito la tabla $tabla");
		echo("<hr>");

	}
}
function abm_modi($parte=1,$tabla,$cclave,$valor,$aceptar="Grabar",$campo_tabla="tabla")
{
	if($parte==1)
	{
		$campos=campos($tabla);
		$tipos=tipos($tabla);
		$sql_busco="select * from $tabla where $cclave='$valor' limit 1";
		//trace($sql_busco);
		$sql=mi_query($sql_busco,"Codigo 335. Imposible consultar la tabla $tabla");
		$datos=mysql_fetch_array($sql);
		$parametros="";
		for($i=0;$i<count($campos);$i++)
		{
			$nom_campo=$campos[$i];
			$tipo=$tipos[$i];
			$val_ini=$datos[$nom_campo];
			if(!(strpos($tipo,"(") === false))
			{
				$start=strpos($tipo,"(")+1;
				$end=strpos($tipo,")")-1;
				$longitud=substr($tipo,$start,$end-$start+1);
				if($longitud>50)
					$longitud=50;
			}else
			{
				$longitud=10;
			}
			if(!(strpos($tipo,"date") === false))
			{
				$val_ini=a_fecha_arg($val_ini);
			}
			//Ver si es un campo de clave y mostrar solo asteriscos
			if($nom_campo=="clave" or $nom_campo=="pwd" or $nom_campo=="password")
			{
				$parametros.=";%PWD-$nom_campo-$nom_campo-$val_ini-$longitud";
			}else
			{
				$parametros.=";%TXT-$nom_campo-$nom_campo-$val_ini-$longitud";
			}
		}
		$parametros="%OCU-$campo_tabla-$tabla;%OCU-panta-graba_modi;%OCU-cclave-$cclave;%OCU-valor-$valor" . $parametros;
		$volver=$_SERVER["PHP_SELF"];
		mi_panta("",$parametros,"submit-" . $aceptar . "-$volver");
		//mi_tabla("i",0);
		echo("<br><center><form action='$volver' method='post'><input type='hidden' name=$campo_tabla value='$tabla'><input type='hidden' name='cclave' value='$cclave'><input type='hidden' name='valor' value='$valor'><input type='hidden' name='panta' value='graba_modi'><input type='submit' name='baja' value='Borrar'></form></center>");
	}else
	{
		//trace("estoy en la parte 2 de la modi");
		$campos=campos($tabla);
		$tipos=tipos($tabla);
		$sql="update $tabla set ";
		for($i=0;$i<count($campos);$i++)
		{
			$nom_campo=$campos[$i];
			$$nom_campo=$_POST[$nom_campo];
			$valor_campo=$$nom_campo;
			$tipo=$tipos[$i];
			if(!(strpos($tipo,"date") === false))
			{
				$valor_campo=a_fecha_sistema($valor_campo);
			}
			if($nom_campo=="clave" or $nom_campo=="pwd" or $nom_campo=="password")
			{
				$sql.="$nom_campo=password('$valor_campo'),";
			}else
			{
				$sql.="$nom_campo='$valor_campo',";
			}
		}
		$sql=substr($sql,0,strlen($sql)-1) . " where $cclave='$valor'";
		//trace($sql);
		mi_query($sql,"codigo 85. Imposible modificar el registro $valor en $tabla");
		mi_titulo("se actualiz&oacute; con &eacute;xito la tabla $tabla");
	}
}

function abm($tabla,$cclave=1,$filtro="",$campo_tabla="tabla")
{
	$campos=campos($tabla);
	$lista_campos=implode(",",$campos);
	$encabezados=implode(";",$campos);
	$sql="";
	$sql="select $lista_campos from $tabla";
	$prueba_sql=mysql_query($sql);
	if($filtro<>"")
	{
		$sqlf=$sql . " where " . $filtro;
		$prueba_filtro=mysql_query($sqlf);
		if($prueba_sql and !$prueba_filtro)
		{
			mi_titulo("Hay un error en el filtro. La tabla no est&aacute; filtrada.");
		}else
		{
			$sql=$sqlf;
			mi_titulo("La tabla est&aacute; filtrada por: $filtro");
		}
	}
	if(is_integer($cclave))
	{
		$campo_clave=$campos[$cclave-1];
	}else
	{
		$campo_clave=$cclave;
	}
	$url=$_SERVER["PHP_SELF"];
	//$url="opprueba_abm.php";
	$sql.=";$url+$campo_clave+panta+detalle+$campo_tabla+$tabla";
	//trace("El sql es $sql");
	//tabla_cons($encabezados,$sql,1,"silver","#8EC99F",0);
	$titulo="$tabla;$tabla;$tabla";
	tabla_cons($encabezados,$sql,1,"silver","#8EC99F",0,"MODIFICAR","ENTRAR","",$titulo);
}
function abm_baja($tabla,$clave,$valor)
{
	mi_query("delete from $tabla where $clave='$valor'","Codigo 445. Imposible eliminar la clave $clave=$valor de la tabla $tabla");
}	
	
//	$sql="select d.partida,d.tam_partida,d.cant_lotes,d.tam_lote,g.grabado,d.observaciones,d.presentacion,d.vencimiento,p.descripcion,d.planta from ordendet d,ordencab c,grabado g,prioridad p where d.numero=$num_op and c.numero=$num_op and g.orden=d.grabado and d.prioridad=p.codigo order by d.partida;opgeneracion.php+partida+panta+detalle";
	//trace($sql);
//$titulos="partida;unidades;lotes;tam.lote;grabado;observaciones;presentacion;vencimiento;prioridad;planta";
//	tabla_cons($titulos,$sql,1,"silver","#8EC99F",0);
	
	
	



function filtro($tabla,$filtro="where",$boton="enviar",$campo_tabla="tabla")
{
	// Esta funcion crea una pantalla para generar una clausula where para usar de filtro en una tabla
	// Esta funcion genera un formulario para los campos: campo, operador, valor,conector y funcion
	
	$campo_campo="%SEL-campo-campo-desc $tabla-Field-Field-1";
	$campo_operador="%SEL-operador-operador-Elegir+Elegir+=+igual+>+mayor+<+menor+<=+menor o igual+>=+mayor o igual+instr+contiene-0";
	$val_ini="";
	$campo_valor="%TXT-valor-valor-$val_ini-10";
	$campo_funcion="%SEL-funcion-funcion-Elegir+Elegir+a_fecha_sistema(+fecha-0";
	
	$campo_conector="%SEL-conector-conector-Elegir+Elegir+and+Y+or+O-0";
	$campo_filtro="%TXT-filtro-filtro-$filtro-50";
	$campo_tabla="%OCU-$campo_tabla-$tabla";
	$los_campos=$campo_conector . ";%ROT-<tr><td><hr></td><td><hr></td></tr>;" . $campo_campo . ";" . $campo_tabla . ";" . $campo_operador . ";" . $campo_valor . ";" . $campo_funcion . ";" . ";" . $campo_filtro;
	$volver=$_SERVER['PHP_SELF'];
	//trace($los_campos);
	$submit="$boton-Agregar-$volver";
	//trace($submit);
	mi_panta("Filtro para la tabla $tabla",$los_campos,$submit);
}

function error($msg){
	?>
	<html>
	<head>
	<script language="JavaScript">
	<!--
		alert("<?=$msg?>");
		history.back();
	//-->
	</script>
	</head>
	</body>
	</html>
	<?
	exit;
}

function raya()
{
	echo("<hr>");
}


function parametros($accion="crear",$variable="",$valor="",$cartel="",$oper="=")
{
	switch($accion)
	{
		case "crear":
			if(tabla_existe("tablero","param_tmp"))
			{
				$borro=mi_query("drop table param_tmp","Error: no se puede borrar la tabla param_tmp");
			}
			$tabla_param=mi_query("create table param_tmp (tipo char(6),variable char(15),valor text,cartel varchar(30),oper varchar(15))","Error: no fue posible crear la tabla temporal de parametros");
			break;
		case "imprimir":
			$qry_prm=mi_query("select * from param_tmp","Error al buscar los parametros");
			mensaje("Par&aacute;metros");
			mi_tabla("i","1");
			while($datos=mysql_fetch_array($qry_prm))
			{
				$cartel=$datos["cartel"];
				$valor=$datos["valor"];
				$tipo=$datos["tipo"];
				if($valor=="S")
				{
					$valor="Si";
				}
				$color=$color=="" ?" color" : "";
				if($valor<>"TODAS" and $valor<>"TODOS" and $valor<>"Elegir" and $valor<>"")
				{
					mi_fila($color,"$cartel;$valor");
				}
			}
			mi_tabla("f");
			break;
		case "filtro":
			if($valor<>"Elegir")
				mi_query("insert into param_tmp set tipo='filtro',variable='$variable',valor='$valor',cartel='$cartel',oper='$oper'","Error: imposible agregar datos a la tabla param_tmp");
			break;
		case "agrup":
			if($valor=="S")
				mi_query("insert into param_tmp set tipo='agrup',variable='$variable',valor='$valor',cartel='$cartel'","Error: imposible agregar datos a la tabla param_tmp");
			break;
		case "mkfiltro":
			$qry_prm=mi_query("select * from param_tmp where tipo='filtro'","Error al obtener los parametros");
			$salida="";
			while($datos=mysql_fetch_array($qry_prm))
			{
				$variable=$datos["variable"];
				$valor=$datos["valor"];
				$oper=$datos["oper"];
				if($valor<>"TODAS" and $valor<>"TODOS" and $valor<>"Elegir" and $valor<>"" and $oper<>"")
				{
					switch($oper)
					{
						case "instr":
							$salida.=" and instr($variable,'$valor')";
							break;
						case "in":
							$valor=str_replace(",","','",$valor);
							$salida.=" and $variable in('" . $valor . "')";
							break;
						default:
							$salida.=" and $variable $oper '$valor'";
					}
				}
			}
			break;
		case "agrup_ini":
			$qry_prm=mi_query("select * from param_tmp where tipo='agrup'","Error al obtener los parametros");
			$salida="";
			while($datos=mysql_fetch_array($qry_prm))
			{
				$variable=$datos["variable"];
				$valor=$datos["valor"];
				$salida.="$variable,";
			}
			break;
		case "agrup_fin":
			$qry_prm=mi_query("select * from param_tmp where tipo='agrup'","Error al obtener los parametros");
			$salida="";
			while($datos=mysql_fetch_array($qry_prm))
			{
				$variable=$datos["variable"];
				$valor=$datos["valor"];
				$salida.=",$variable";
			}
			break;
		case "agrup_cab":
			$qry_prm=mi_query("select * from param_tmp where tipo='agrup'","Error al obtener los parametros");
			$salida="";
			while($datos=mysql_fetch_array($qry_prm))
			{
				$variable=$datos["variable"];
				$valor=$datos["valor"];
				$cartel=$datos["cartel"];
				$salida.="$cartel;";
			}
			break;
		default:
			if($valor<>"Elegir")
				mi_query("insert into param_tmp set variable='$variable',valor='$valor',cartel='$cartel',oper='$oper'","Error: imposible agregar datos a la tabla param_tmp");
			break;
	}
	return $salida;
}


function parametros_old($accion="crear",$variable="",$valor="",$cartel="",$oper="=")
{
	switch($accion)
	{
		case "crear":
			if(tabla_existe("cartuchos","param_tmp"))
			{
				$borro=mi_query("drop table param_tmp","Error: no se puede borrar la tabla param_tmp");
			}
			$tabla_param=mi_query("create table param_tmp (tipo char(6),variable char(15),valor varchar(100),cartel varchar(30),oper varchar(15))","Error: no fue posible crear la tabla temporal de parametros");
			break;
		case "imprimir":
			$qry_prm=mi_query("select * from param_tmp","Error al buscar los parametros");
			mensaje("Par&aacute;metros");
			mi_tabla("i");
			while($datos=mysql_fetch_array($qry_prm))
			{
				$cartel=$datos["cartel"];
				$valor=$datos["valor"];
				$tipo=$datos["tipo"];
				if($tipo=="agrup" and $valor=="S")
				{
					$valor="Si";
				}
				mi_fila("","$cartel;$valor");
			}
			mi_tabla("f");
			break;
		case "filtro":
			if($valor<>"Elegir")
				mi_query("insert into param_tmp set tipo='filtro',variable='$variable',valor='$valor',cartel='$cartel',oper='$oper'","Error: imposible agregar datos a la tabla param_tmp");
			break;
		case "agrup":
			if($valor=="S")
				mi_query("insert into param_tmp set tipo='agrup',variable='$variable',valor='$valor',cartel='$cartel'","Error: imposible agregar datos a la tabla param_tmp");
			break;
		case "mkfiltro":
			$qry_prm=mi_query("select * from param_tmp where tipo='filtro'","Error al obtener los parametros");
			$salida="";
			while($datos=mysql_fetch_array($qry_prm))
			{
				$variable=$datos["variable"];
				$valor=$datos["valor"];
				$oper=$datos["oper"];
				switch($oper)
				{
					case "instr":
						$salida.=" and instr($variable,'$valor')";
						break;
					default:
						$salida.=" and $variable $oper '$valor'";
				}
			}
			break;
		case "agrup_ini":
			$qry_prm=mi_query("select * from param_tmp where tipo='agrup'","Error al obtener los parametros");
			$salida="";
			while($datos=mysql_fetch_array($qry_prm))
			{
				$variable=$datos["variable"];
				$valor=$datos["valor"];
				$salida.="$variable,";
			}
			break;
		case "agrup_fin":
			$qry_prm=mi_query("select * from param_tmp where tipo='agrup'","Error al obtener los parametros");
			$salida="";
			while($datos=mysql_fetch_array($qry_prm))
			{
				$variable=$datos["variable"];
				$valor=$datos["valor"];
				$salida.=",$variable";
			}
			break;
		case "agrup_cab":
			$qry_prm=mi_query("select * from param_tmp where tipo='agrup'","Error al obtener los parametros");
			$salida="";
			while($datos=mysql_fetch_array($qry_prm))
			{
				$variable=$datos["variable"];
				$valor=$datos["valor"];
				$cartel=$datos["cartel"];
				$salida.="$cartel;";
			}
			break;
		default:
			if($valor<>"Elegir")
				mi_query("insert into param_tmp set variable='$variable',valor='$valor',cartel='$cartel',oper='$oper'","Error: imposible agregar datos a la tabla param_tmp");
			break;
	}
	return $salida;
}

function mkgrafico($tabla_datos,$filtro="",$clave="")
{
	//Hacer tabla grafico
	if(tabla_existe("copetin","grafico"))
		mi_query("drop table grafico","Error al borrar la tabla grafico");
	$mk_grafico="create table grafico (mes varchar(10),";
	//trace($clave);
	$qperiodos=mi_query("select distinct $clave from $tabla_datos where 1=1 $filtro","Error al obtener los periodos");
	while($datos=mysql_fetch_array($qperiodos))
	{
		//$periodo="Per_" . $datos["periodo"];
		$periodo="Per_" . $datos[$clave];
		$mk_grafico.="$periodo float(10,2),";
		$leyenda[]=$datos[$clave];
	}
	$mk_grafico=substr($mk_grafico,0,strlen($mk_grafico)-1) . ")";
	mi_query($mk_grafico,"Error al crear la tabla grafico");
	// Cargar valores en la tabla grafico
	$meses=array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
	for($i=0 ; $i<12; $i++)
	{
		$mes=$meses[$i];
		$sql_qry="select $clave,sum($mes) as $mes from $tabla_datos where 1=1 $filtro group by 1";
		//die($sql_qry);
		$qry=mi_query($sql_qry,"Error al procesar consulta");
		while($datos=mysql_fetch_array($qry))
		{
			$periodo="Per_" . $datos[$clave];
			$xvalor=$datos[$mes];
			$existe_fila=un_dato("select count(*) from grafico where mes='$mes'");
			if($existe_fila)
			{
				mi_query("update grafico set $periodo='$xvalor' where mes='$mes'","Error al actualizar la tabla de graficos");
			}else{
				mi_query("insert into grafico set $periodo='$xvalor',mes='$mes'","Error al insertar la tabla de graficos");
			}
		}
	}
	return $leyenda;
}


function mkgrafico_old($tabla_datos,$filtro)
{
	//Hacer tabla grafico
	if(tabla_existe("copetin","grafico"))
		mi_query("drop table grafico","Error al borrar la tabla grafico");
	$mk_grafico="create table grafico (mes varchar(10),";
	$qperiodos=mi_query("select distinct periodo from $tabla_datos where 1=1 $filtro","Error al obtener los periodos");
	while($datos=mysql_fetch_array($qperiodos))
	{
		$periodo="Per_" . $datos["periodo"];
		$mk_grafico.="$periodo float(10,2),";
		$leyenda[]=$datos["periodo"];
	}
	$mk_grafico=substr($mk_grafico,0,strlen($mk_grafico)-1) . ")";
	mi_query($mk_grafico,"Error al crear la tabla grafico");
	// Cargar valores en la tabla grafico
	$meses=array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
	for($i=0 ; $i<12; $i++)
	{
		$mes=$meses[$i];
		$sql_qry="select periodo,sum($mes) as $mes from $tabla_datos where 1=1 $filtro group by 1";
		//die($sql_qry);
		$qry=mi_query($sql_qry,"Error al procesar consulta");
		while($datos=mysql_fetch_array($qry))
		{
			$periodo="Per_" . $datos["periodo"];
			$xvalor=$datos[$mes];
			$existe_fila=un_dato("select count(*) from grafico where mes='$mes'");
			if($existe_fila)
			{
				mi_query("update grafico set $periodo='$xvalor' where mes='$mes'","Error al actualizar la tabla de graficos");
			}else{
				mi_query("insert into grafico set $periodo='$xvalor',mes='$mes'","Error al insertar la tabla de graficos");
			}
		}
	}
	return $leyenda;
}

function graficar($tit_graf,$tit_x,$tit_y,$alto,$ancho,$tipo,$leyenda)
{
	$sql2="select * from grafico";
	$qry_rst=mi_query($sql2,"Error al ejecutar la consulta para graficar");
	$par=1;
	while($datos=mysql_fetch_array($qry_rst))
	{
		foreach($datos as $zvalor)
		{
			if($par)
			{
				$zvector[]=$zvalor;
				$par=0;
				//trace($valor);
			}else
			{
				$par=1;
			}
		}
		//print_r($vector);
		$datos_grafico[]=$zvector;
		unset($zvector);
	}
	include_once('phplot/phplot.php');
	$grafico = new PHPlot($ancho,$alto);
	$grafico->SetDataValues($datos_grafico);
	$grafico->SetPrintImage(false);
	$grafico->SetFileFormat("jpg");
	$grafico->SetOutputFile("tmp/grafico.jpg");
	$grafico->SetIsInline(true);
	$grafico->SetDataType("text-data");
	$grafico->SetPlotType($tipo);
	$grafico->SetTitle($tit_graf);
	$grafico->SetXTitle($tit_x);
	$grafico->SetYTitle($tit_y);
	$grafico->SetPrecisionY(0);
	$grafico->SetLegend($leyenda);
	//$grafico->SetNewPlotAreaPixels(10,10,$ancho-30,$alto-10);
	//$grafico->SetXAxisPosition(0);
	$grafico->SetXDataLabelPos("plotdown");
	$grafico->DrawGraph();
	$grafico->PrintImage();
	mi_tabla("i color","1");
	mi_fila("m color","<img src='tmp/grafico.jpg'>");
	mi_tabla("f","1");
}

function grafico_barras($sql,$ejex="Titulo x",$ejey="Titulo y",$titulo_grafico="Titulo del grafico",$ticks_h=20,$alto=200,$ancho=800)
{
	$q=mi_query($sql,"Error al ejecutar la consulta");
	while($datos=mysql_fetch_array($q))
	{
		$datox=substr($datos[0],0,16);
		$datoy=$datos[1];
		$datos_a[]=array($datox,$datoy);
	}
	$datos_a=base64_encode(serialize($datos_a));
	//$titulo_grafico="Ranking de tipos de problema";
	mi_tabla("i color","1");
	mi_fila("m color","<img src='cografico.php?tipo=bars&datos=$datos_a&alto=$alto&ancho=$ancho&titulo=$titulo_grafico&eje_x=$ejex&eje_y=$ejey&ticks_h=$ticks_h'>");
	//mi_fila("m","hola");
	mi_tabla("f","1");
	echo("<br>");
}

function cierre()
{
	echo("</body>");
	echo("</html>");
}
function apertura($titulo)
{
	echo("<html>");
	echo("<head>");
	echo("<title>$titulo</title>");
	echo("</head>");
}