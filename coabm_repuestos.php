<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Stock de repuestos de Computacion");
require_once("cobody.php");
require_once("cocnx.php");
$home=home($uid);
$mismo=home();
$submit="aceptar-Aceptar-$mismo";
mi_titulo("Inventario de Repuestos de Computacion");
$panta=$_POST["panta"];
switch($panta)
{
	case "graba_tipo":
		$tipo=$_POST["tipo"];
		$descrip_tipo=$_POST["desc_cat"];
		$categoria=$_POST["categoria"];
		$marca=$_POST["marca"];
		$descripcion=$_POST["descripcion"];
		$caracteristicas=$_POST["caracteristicas"];
		mi_query("insert into tipo_repuestos set tipo='$tipo',descripcion='$desc_tipo'");
		mensaje("Se agrego un nuevo tipo: $tipo");
		$tit_cat="Continuar con alta de repuesto";
		$campos=";%OCU-categoria-$categoria";
		$campos.=";%OCU-tipo-$tipo";
		$campos.=";%OCU-marca-$marca";
		$campos.=";%OCU-descripcion-$descripcion";
		$campos.=";%OCU-caracteristicas-$caracteristicas";
		$campos.=";%OCU-panta-graba_alta";
		mi_panta($tit_cat,$campos,$submit);
		break;
	case "graba_cat":
		$categoria=$_POST["categoria"];
		$descrip_cat=$_POST["desc_cat"];
		$tipo=$_POST["tipo"];
		$marca=$_POST["marca"];
		$descripcion=$_POST["descripcion"];
		$caracteristicas=$_POST["caracteristicas"];
		mi_query("insert into cat_repuestos set categoria='$categoria',descripcion='$desc_cat'");
		mensaje("Se agrego una nueva categoria: $categoria");
		$tit_cat="Continuar con alta de repuesto";
		$campos=";%OCU-categoria-$categoria";
		$campos.=";%OCU-tipo-$tipo";
		$campos.=";%OCU-marca-$marca";
		$campos.=";%OCU-descripcion-$descripcion";
		$campos.=";%OCU-caracteristicas-$caracteristicas";
		$campos.=";%OCU-panta-graba_alta";
		mi_panta($tit_cat,$campos,$submit);
		break;
	case "graba_marca":
		$marca=$_POST["marca"];
		$descrip_marca=$_POST["desc_marca"];
		$categoria=$_POST["categoria"];
		$tipo=$_POST["tipo"];
		$descripcion=$_POST["descripcion"];
		$caracteristicas=$_POST["caracteristicas"];
		mi_query("insert into marca_repuestos set marca='$marca',descripcion='$descrip_marca'");
		mensaje("Se agrego una nueva marca: $marca");
		$tit_cat="Continuar con alta de repuesto";
		$campos=";%OCU-categoria-$categoria";
		$campos.=";%OCU-tipo-$tipo";
		$campos.=";%OCU-marca-$marca";
		$campos.=";%OCU-descripcion-$descripcion";
		$campos.=";%OCU-caracteristicas-$caracteristicas";
		$campos.=";%OCU-panta-graba_alta";
		mi_panta($tit_cat,$campos,$submit);
		break;
	case "graba_alta":
		$categoria=$_POST["categoria"];
		$tipo=$_POST["tipo"];
		$marca=$_POST["marca"];
		$descripcion=$_POST["descripcion"];
		$caracteristicas=$_POST["caracteristicas"];
		$atributo1=$_POST["atributo1"];
		$atributo2=$_POST["atributo2"];
		$atributo3=$_POST["atributo3"];
		if($categoria=="Elegir")
		{
			$tit_cat="Nueva Categoria";
			$campos=";%TXT-Categoria (4 letras)-categoria--4";
			$campos.=";%TXT-descripcion-desc_cat--40";
			$campos.=";%OCU-panta-graba_cat";
			$campos.=";%OCU-tipo-$tipo";
			$campos.=";%OCU-marca-$marca";
			$campos.=";%OCU-descripcion-$descripcion";
			$campos.=";%OCU-caracteristicas-$caracteristicas";
			mi_panta($tit_cat,$campos,$submit);
			break;	
		}
		if($tipo=="Elegir")
		{
			$tit_tipo="Nuevo tipo";
			$campos=";%TXT-Tipo (1 letra)-tipo--1";
			$campos.=";%TXT-descripcion-desc_tipo--8";
			$campos.=";%OCU-panta-graba_tipo";
			$campos.=";%OCU-categoria-$categoria";
			$campos.=";%OCU-marca-$marca";
			$campos.=";%OCU-descripcion-$descripcion";
			$campos.=";%OCU-caracteristicas-$caracteristicas";
			mi_panta($tit_tipo,$campos,$submit);
			break;
		}
		if($marca=="Elegir")
		{
			$tit_marca="Nueva marca";
			$campos=";%TXT-Marca (3 letras)-marca--3";
			$campos.=";%TXT-descripcion-desc_marca--30";
			$campos.=";%OCU-panta-graba_marca";
			$campos.=";%OCU-categoria-$categoria";
			$campos.=";%OCU-tipo-$tipo";
			$campos.=";%OCU-descripcion-$descripcion";
			$campos.=";%OCU-caracteristicas-$caracteristicas";
			mi_panta($tit_marca,$campos,$submit);
			break;
		}
		mi_query("insert into repuestos set categoria='$categoria',tipo='$tipo',marca='$marca',descripcion='$descripcion',caracteristicas='$caracteristicas',alta=curdate(),usado=false,descrip_uso='',atributo1='$atributo1',atributo2='$atributo2',atributo3='$atributo3'","Error al agregar una repuesto");
		$id_repuestos=mysql_insert_id();
		$codigo="0000$id_repuestos";
		$longitud=strlen($codigo);
		$inicio=$longitud-4;
		$codigo=substr($codigo,$inicio,4);
		$codigo=$categoria . $tipo . $marca . $codigo;
		mi_query("update repuestos set codigo='$codigo' where id_repuestos='$id_repuestos'");
		mensaje("Se agrego un nuevo repuesto con el codigo $codigo.");
		un_boton("Volver","Aceptar","coabm_repuestos.php");
		break;
	case "modi":
		$id_repuestos=$_POST["id_repuestos"];
		$cons=mi_query("select * from repuestos where id_repuestos='$id_repuestos'","Error al obtener el registro");
		$datos=mysql_fetch_array($cons);
		$categoria=$datos["categoria"];
		$descrip_cat=un_dato("select descripcion from cat_repuestos where categoria='$categoria'");
		$tipo=$datos["tipo"];
		$descrip_tipo=un_dato("select descripcion from tipo_repuestos where tipo='$tipo'");
		$marca=$datos["marca"];
		$descrip_marca=un_dato("select descripcion from marca_repuestos where marca='$marca'");
		$descripcion=$datos["descripcion"];
		$caracteristicas=$datos["caracteristicas"];
		$descrip_uso=$datos["descrip_uso"];
		$fecha_uso=a_fecha_arg($datos["fecha_uso"]);
		$atributo1=$datos["atributo1"];
		$atributo2=$datos["atributo2"];
		$atributo3=$datos["atributo3"];
		//trace($fecha_uso);
		$usado=$datos["usado"];
		$usar=($usado) ? "s":"n";
		$titulo="Modificacion de Repuestos de PCs";
		$tit_modi="MODIFICACION DE REPUESTOS";
		$campos=";%ROT-Id. registro</td><td><strong>$id_repuestos";
		$campos.=";%SEL-categoria-categoria-select categoria,descripcion from cat_repuestos order by 1-descripcion+categoria-$descrip_cat-$categoria";
		$campos.=";%SEL-tipo-tipo-select tipo,descripcion from tipo_repuestos order by 1-descripcion+tipo-$descrip_tipo-$tipo";
		$campos.=";%SEL-marca-marca-select marca,descripcion from marca_repuestos order by 1-descripcion+marca-$descrip_marca-$marca";
		$campos.=";%ARE-descripcion-descripcion-$descripcion-4-60";
		$campos.=";%ARE-caracteristicas-caracteristicas-$caracteristicas-4-60";
		$campos.=";%TXT-atributo 1-atributo1-$atributo1-10";
		$campos.=";%TXT-atributo 2-atributo2-$atributo2-10";
		$campos.=";%TXT-atributo 3-atributo3-$atributo3-10";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-id_repuestos-$id_repuestos";
		$campos.=";%OCU-fecha_uso-$fecha_uso";
		$campos.=";%OCU-descrip_uso-$descrip_uso";
		$campos.=";%CHK-usar-usar-s-$usar";
		//$campos.=";%CHK-borrar-borrar-s-N";
		mi_panta($tit_modi,$campos,$submit);
		break;
	case "graba_modi":
		$id_repuestos=$_POST["id_repuestos"];
		$categoria=$_POST["categoria"];
		$tipo=$_POST["tipo"];
		$marca=$_POST["marca"];
		$descripcion=$_POST["descripcion"];
		$caracteristicas=$_POST["caracteristicas"];
		$atributo1=$_POST["atributo1"];
		$atributo2=$_POST["atributo2"];
		$atributo3=$_POST["atributo3"];
		$codigo="0000$id_repuestos";
		$longitud=strlen($codigo);
		$inicio=$longitud-4;
		$codigo=substr($codigo,$inicio,4);
		$codigo=$categoria . $tipo . $marca . $codigo;
		$fecha_uso=$_POST["fecha_uso"];
		//trace($fecha_uso);
		$descrip_uso=$_POST["descrip_uso"];
		$usar=$_POST["usar"];
		if($usar=="s")
		{
			$tit_panta="Utilizacion de repuesto $codigo";
			$campos.=";%TXT-descripcion de uso-descrip_uso-$descrip_uso-40";
			$campos.=";%OCU-id_repuestos-$id_repuestos";
			$campos.=";%OCU-categoria-$categoria";
			$campos.=";%OCU-tipo-$tipo";
			$campos.=";%OCU-marca-$marca";
			$campos.=";%OCU-descripcion-$descripcion";
			$campos.=";%OCU-caracteristicas-$caracteristicas";
			$campos.=";%OCU-codigo-$codigo";
			$campos.=";%OCU-panta-utilizacion";
			$campos.=";%OCU-fecha_uso-$fecha_uso";
			mi_panta($tit_panta,$campos,$submit);
			break;
		}else
		{
			mi_query("update repuestos set categoria='$categoria',tipo='$tipo',marca='$marca',descripcion='$descripcion',caracteristicas='$caracteristicas',codigo='$codigo',usado=false,descrip_uso='',atributo1='$atributo1',atributo2='$atributo2',atributo3='$atributo3' where id_repuestos='$id_repuestos'","Error al modificar el registro de repuestos de pc");
			mensaje("Modificaci&oacute;n de $id_repuestos grabada");
		}
		delay();
		break;
	case "utilizacion":
		$id_repuestos=$_POST["id_repuestos"];
		$categoria=$_POST["categoria"];
		$tipo=$_POST["tipo"];
		$marca=$_POST["marca"];
		$descripcion=$_POST["descripcion"];
		$caracteristicas=$_POST["caracteristicas"];
		$codigo=$_POST["codigo"];
		$descrip_uso=$_POST["descrip_uso"];
		$fecha_uso=$_POST["fecha_uso"];
		$atributo1=$_POST["atributo1"];
		$atributo2=$_POST["atributo2"];
		$atributo3=$_POST["atributo3"];		
		//trace("Id $id_repuestos $descrip_uso $fecha_uso");
		if($fecha_uso=="00/00/0000")
		{
			$fecha_uso=a_fecha_sistema(hoy());
		}else
		{
			$fecha_uso=a_fecha_sistema($fecha_uso);
		}
		mi_query("update repuestos set usado=true,descrip_uso='$descrip_uso',fecha_uso='$fecha_uso',categoria='$categoria',tipo='$tipo',marca='$marca',descripcion='$descripcion',caracteristicas='$caracteristicas',codigo='$codigo',atributo1='$atributo1',atributo2='$atributo2',atributo3='$atributo3' where id_repuestos='$id_repuestos'");
		mensaje("Utilizacion de $codigo grabada");
		delay();
		break;
	case "filtrar":
		$tit_filtro="Busqueda filtrada";
		$campos.=";%SEL-categoria-categoria-select categoria,descripcion from cat_repuestos order by 1-descripcion+categoria";
		$campos.=";%SEL-marca-marca-select marca,descripcion from marca_repuestos order by 1-descripcion+marca";
		$campos.=";%TXT-caracteristica-caracteristica--10";
		$campos.=";%TXT-atributo 1-atributo1--10";
		$campos.=";%TXT-atributo 2-atributo2--10";
		$campos.=";%TXT-atributo 3-atributo3--10";
		$campos.=";%OCU-panta-procesa_filtro";
		mi_panta($tit_filtro,$campos,$submit);
		break;
	case "procesa_filtro":
		$categoria=$_POST["categoria"];
		$marca=$_POST["marca"];
		$caracteristica=$_POST["caracteristica"];
		$atributo1=$_POST["atributo1"];
		$atributo2=$_POST["atributo2"];
		$atributo3=$_POST["atributo3"];
		$filtro="where usado is not true";
		$desc_filtro="";
		if($categoria<>"Elegir")
		{
			$filtro.=" and categoria='$categoria'";
			$desc_cat=un_dato("select descripcion from cat_repuestos where categoria='$categoria'");
			$desc_filtro="categoria $desc_cat";
		}
		if($marca<>"Elegir")
		{
			$filtro.=" and marca='$marca'";
			$desc_marca=un_dato("select descripcion from marca_repuestos where marca='$marca'");
			$desc_filtro.=" marca $desc_marca";
		}
		if($caracteristica<>"")
		{
			$filtro.=" and instr(caracteristicas,'$caracteristica')";
			$desc_filtro.=" caracteristica $caracteristica";
		}
		if($atributo1<>"")
		{
			$filtro.=" and instr(atributo1,'$atributo1')";
			$desc_filtro.=" atributo 1 $atributo1";
		}
		if($atributo2<>"")
		{
			$filtro.=" and instr(atributo2,'$atributo2')";
			$desc_filtro.=" atributo 2 $atributo2";
		}
		if($atributo3<>"")
		{
			$filtro.=" and instr(atributo3,'$atributo3')";
			$desc_filtro.=" atributo 3 $atributo3";
		}

		$sql="select id_repuestos,codigo,categoria,tipo,marca,descripcion,caracteristicas,atributo1,atributo2,atributo3,alta from repuestos $filtro order by id_repuestos desc;coabm_repuestos.php+id_repuestos+panta+modi";
		mi_titulo("INVENTARIO filtrado por $desc_filtro");
		$titulos="id;codigo;categoria;tipo;marca;descripcion;caracteristicas;atributo 1;atributo 2;atributo 3;alta";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;0;2;2;0;0;0;0","ACTUALIZ.","MODIFICAR","","Repuestos de PCs;Repuestos de PCs;repuestos_pc");
		volver("");
		break;
	case "usados":
		$hay=un_dato("select count(*) from repuestos where usado is true");
		if($hay)
		{
			$sql="select id_repuestos,codigo,categoria,tipo,marca,descripcion,caracteristicas,atributo1,atributo2,atributo3,alta,descrip_uso,fecha_uso from repuestos where usado is true order by id_repuestos desc;coabm_repuestos.php+id_repuestos+panta+modi";
			$titulos="id;codigo;categoria;tipo;marca;descripcion;caracteristicas;atributo 1;atributo 2;atributo 3;alta;descripcion uso;fecha uso";
			mi_titulo("INVENTARIO");
			tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;0;2;2;0;0;0;0","ACTUALIZ.","MODIFICAR","","Repuestos de PCs;Repuestos de PCs;repuestos_pc");
		}else
		{
			mensaje("No hay datos para mostrar");
		}
		volver("");
		break;
	default:
		$submit="aceptar-Aceptar-$home";
		$tit_alta="ALTA DE REPUESTO";
		$campos.=";%SEL-categoria-categoria-select categoria,descripcion from cat_repuestos order by 2-descripcion+categoria";
		$campos.=";%SEL-tipo-tipo-select tipo,descripcion from tipo_repuestos order by 2-descripcion+tipo";
		$campos.=";%SEL-marca-marca-select marca,descripcion from marca_repuestos order by 2-descripcion+marca";
		$campos.=";%ARE-descripcion-descripcion--4-60";
		$campos.=";%ARE-caracteristicas-caracteristicas--4-60";
		$campos.=";%TXT-atributo 1-atributo1--10";
		$campos.=";%TXT-atributo 2-atributo2--10";
		$campos.=";%TXT-atributo 3-atributo3--10";
		$campos.=";%OCU-panta-graba_alta";
		mi_panta($tit_alta,$campos,$submit);
		$titulos="id;codigo;categoria;tipo;marca;descripcion;caracteristicas;atributo 1;atributo 2;atributo 3;alta";
		$hay=un_dato("select count(*) from repuestos where usado is not true");
		if($hay)
		{
			raya();
			un_boton("aceptar","Filtrar","","panta","filtrar");
			$sql="select id_repuestos,codigo,categoria,tipo,marca,descripcion,caracteristicas,atributo1,atributo2,atributo3,alta from repuestos where usado is not true order by id_repuestos desc;coabm_repuestos.php+id_repuestos+panta+modi";
			mi_titulo("INVENTARIO");
			tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;0;2;2;0;0;0;0","ACTUALIZ.","MODIFICAR","","Repuestos de PCs;Repuestos de PCs;repuestos_pc");
		}else
		{
			mensaje("No hay datos para mostrar");
		}
		un_boton("Volver","Volver",$home);
		raya();
		un_boton("aceptar","Ver usados","","panta","usados");
		break;
}

cierre();
?>