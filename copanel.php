<?
include_once('coacceso.php');
include_once('cofunciones_especificas.php');
include_once("cofunciones.php");
apertura("Panel de Control");
require_once("cobody.php");
require_once("cocnx.php");
actu_item();
mi_titulo("Panel de Control");

// Actualizacion de solicitudes que figuraban sin stock y ahora hay
$cual_sql="select s.numero from solicitudes s,stock k where s.estado='SIN STOCK' and k.cantidad>0 and k.cartucho=s.cartucho";
$cual_qry=mi_query($cual_sql,"copanel.php. Linea 29. Imposible obtener registros para actualizar");
while($datos=mysql_fetch_array($cual_qry))
{
	$numero=$datos["numero"];
	mi_query("update solicitudes set estado='PENDIENTE' where numero=$numero","copanel.php.Linea 33. Imposible elegir registros para actualizar.");
}

// Actualizacion de solicitudes que figuraban pendientes y ahora no tienen stock
$cual_sql="select s.numero from solicitudes s,stock k where s.estado='PENDIENTE' and k.cantidad=0 and k.cartucho=s.cartucho";
$cual_qry=mi_query($cual_sql,"copanel.php. Linea 29. Imposible obtener registros para actualizar");
while($datos=mysql_fetch_array($cual_qry))
{
	$numero=$datos["numero"];
	mi_query("update solicitudes set estado='SIN STOCK' where numero=$numero","copanel.php.Linea 33. Imposible elegir registros para actualizar.");
}

// MENSAJES NUEVOS
//trace("El usuario es $uid");
$msg_sql="select count(*) from foro where estado='VISIBLE' and (modo_usu='NO LEIDO' or modo_usu is null) and para='$uid'";
$hay_msg=un_dato($msg_sql);
if($hay_msg>0)
{

	if(ver_item("msg"))
	{
		$mensaje="Hay $hay_msg mensajes/es nuevos.";
		linea_menu("msg",$mensaje,0);
	}else
	{
		$mensaje="Ocultar mensajes";
		linea_menu("msg",$mensaje,1);
		$rotulos="id;fecha;de;para;tema;mensaje;rta. a msje.";
		$sql="select f.id_foro,f.fecha,f.usuario,f.para,c.tema,f.mensaje,f.respuesta from foro f,conversacion c where f.conversacion=c.id_conv and (f.estado<>'OCULTO' or f.estado is null) and (f.modo_usu<>'LEIDO' or f.modo_usu is null) and (f.vencimiento>=curdate() or f.vencimiento is null) and (f.para='$uid' or f.usuario='$uid') order by f.id_foro desc;coforo.php+id_foro+panta+ocultar+volver+copanel.php";
		tabla_cons($rotulos,$sql,"","","","0","ESTADO","ver/responder","");
		un_boton("aceptar","ver&nbsp;temas","coforo.php?volver=copanel.php");
	}	
}

// Agenda
if(ver_item("agenda"))
{
	$mensaje="Ver agenda";
	linea_menu("agenda",$mensaje,0);
}else
{
	$mensaje="Ocultar agenda";
	linea_menu("agenda",$mensaje,1);
	mensaje("Agenda");
	icaja("relative",0,0,$ancho=87,$alto=2,"#EACDE3");
	un_boton("aceptar","Modificar&#160;Agenda","coabm_agenda.php");
	fcaja();
	$qcuadrante=mi_query("select * from cuadrante");
	while($datos0=mysql_fetch_array($qcuadrante))
	{
		$id_cuad=$datos0["id_cuad"];
		$descrip_cuad=$datos0["descrip_cuad"];
		$tquad="create temporary table tquad$id_cuad (periodo char(10),";
		$qroles=mi_query("select id_rol,descrip_rol from rol","Error al buscar los roles");
		$rotulos="Periodo";
		while($datos=mysql_fetch_array($qroles))
		{
			$id_rol=$datos["id_rol"];
			$descrip_rol=$datos["descrip_rol"];
			$tquad.="$descrip_rol text,";
			$rotulos.=";$descrip_rol";
		}
		$tquad=substr($tquad,0,strlen($tquad)-1) . ")";
		//trace($tquad);
		mi_query($tquad);
		$qperiodos=mi_query("select * from periodo","Error al buscar los periodos");
		while($datos=mysql_fetch_array($qperiodos))
		{
			$id_per=$datos["id_per"];
			$descrip_per=$datos["descrip_per"];
			//trace("Estoy en periodo $descrip_per");
			$compara1=$datos["compara"];
			$compara2=str_replace("=","<=",$compara1);
			$muestra=substr($compara1,0,strpos($compara1,"="));
			//trace("$muestra");
			//die();
			//trace("Compara1 $compara1, Compara2 $compara2");
			//die();
			$sql_lleno="select rol,if($compara1,item,concat('<font color=red>',item,'</font>')) as item,$muestra as fecha from agenda where cuadrante=$id_cuad and periodo='$id_per' and estado=1 and ((caduca and $compara1) or ( not caduca and $compara2))";
			//trace($sql_lleno);
			$qlleno=mi_query($sql_lleno,"Error al obtener datos de la agenda");
			while($datos1=mysql_fetch_array($qlleno))
			{
				$id_rol=$datos1["rol"];
				//trace("El id_rol es $id_rol");
				$descrip_rol=un_dato("select descrip_rol from rol where id_rol='$id_rol'");
				//trace("Descrip rol es $descrip_rol");
				$item=$datos1["item"];
				$fecha=$datos1["fecha"];
				//trace("La fecha es: $fecha");
				$item="$fecha: $item";
				$existe=un_dato("select count(*) from tquad$id_cuad where periodo='$descrip_per'");
				if($existe)
				{
					$qry_insert="update tquad$id_cuad set $descrip_rol=concat(ifnull($descrip_rol,''),' ','$item') where periodo='$descrip_per'";
				}else
				{
					$qry_insert="insert into tquad$id_cuad set periodo='$descrip_per',$descrip_rol='$item'";
				}
				//trace($qry_insert);
				mi_query($qry_insert,"Error al cargar la tabla tquad$id_cuad");
			}
		}
		$llena=un_dato("select count(*) from tquad$id_cuad");
		if($llena)
		{
			$sql="select * from tquad$id_cuad;coactu_agenda.php+periodo+id_cuad+$id_cuad";
			mi_titulo("Cuadrante $id_cuad: $descrip_cuad");
			tabla_cons($rotulos,$sql,1,"silver","#8EC99F",0,"CAMBIAR","ENTRAR");
			//tabla_cons($rotulos,$sql,1,"silver","#FFD664",0,"CAMBIAR","ENTRAR");
		}
	}
}


// SOLICITUDES PENDIENTES
$solpend_sql="select count(*) from solicitudes where estado='PENDIENTE'";
$hay_sol_pen=un_dato($solpend_sql);
if($hay_sol_pen>0)
{

	if(ver_item("sol"))
	{
		$mensaje="Hay $hay_sol_pen solicitud/es de recambio pendientes";
		linea_menu("sol",$mensaje,0);
	}else
	{
		$mensaje="Ocultar solicitudes de recambio pendientes";
		linea_menu("sol",$mensaje,1);
		// Calculo y actualizo las fechas del último pedido anterior a cada pedido.
		$qry_sol=mi_query("select numero,usuario,impresora,cartucho from solicitudes where anterior is null order by 1","Error al buscar las solicitudes");
		while ($datos=mysql_fetch_array($qry_sol))
		{
			$snumero=$datos["numero"];
			$susuario=$datos["usuario"];
			$simpresora=$datos["impresora"];
			$scartucho=$datos["cartucho"];
			$anterior=un_dato("select a.fecha from solicitudes a where a.numero=(select max(b.numero) from solicitudes b where b.usuario='$susuario' and b.impresora='$simpresora' and b.cartucho='$scartucho' and b.numero<'$snumero')");
			mi_query("update solicitudes set anterior='$anterior' where numero='$snumero'","Error al actualizar la fecha anterir en la tabla de solicitudes");
		}
		$titulos="numero;fecha;usuario;puesto;impresora;cartucho;marca;color;observaciones;stock;ult.pedido";
		$sql="select s.numero,s.fecha,u.nombre,p.descripcion as puesto,i.modelo as impresora,c.codigo_orig as codint,c.marca,c.color,s.observaciones,k.cantidad as stock,s.anterior from solicitudes s,puestos p,impresoras i,cartuchos c,usuarios u,stock k where s.estado='PENDIENTE'";
		$sql.=" and s.usuario=u.usuario and s.puesto=p.codigo and s.impresora=i.codigo and s.cartucho=c.codigo_int and s.cartucho=k.cartucho order by s.numero;corecambio.php+numero";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F",0,"ACCION","PROCESAR");
	}	
}

// SOLICITUDES SIN STOCK
$solstk_sql="select count(*) from solicitudes where estado='SIN STOCK'";
$hay_sol_stk=un_dato($solstk_sql);
if($hay_sol_stk>0)
{

	if(ver_item("sol_stk"))
	{
		$mensaje="Hay $hay_sol_stk solicitud/es de recambio sin stock";
		linea_menu("sol_stk",$mensaje,0);
	}else
	{
		$mensaje="Ocultar solicitudes de recambio sin stock";
		linea_menu("sol_stk",$mensaje,1);
				$titulos="numero;fecha;usuario;puesto;impresora;cartucho;desc.cartucho;observaciones;stock";
		$sql="select s.numero,s.fecha,u.nombre,p.descripcion as puesto,i.modelo as impresora,c.codigo_orig as codint,c.descripcion as cartucho,s.observaciones,k.cantidad as stock from solicitudes s,puestos p,impresoras i,cartuchos c,usuarios u,stock k where s.estado='SIN STOCK' and s.usuario=u.usuario and s.puesto=p.codigo and s.impresora=i.codigo and s.cartucho=c.codigo_int and s.cartucho=k.cartucho order by s.numero";
		//echo($sql);
		tabla_cons($titulos,$sql,1,"silver","#8EC99F",0);
	}	
}


// PEDIDOS DE COMPRA POR RECIBIR
$pedrcb_sql="select count(*) from pedido_cab where estado='EMITIDO'";
$hay_ped_rcb=un_dato($pedrcb_sql);
if($hay_ped_rcb>0)
{
	if(ver_item("ped_rcb"))
	{
		$mensaje="Hay $hay_ped_rcb Pedido/s de Compra por recibir";
		linea_menu("ped_rcb",$mensaje,0);
	}else
	{
		$mensaje="Ocultar Pedidos de Compra por recibir";
		linea_menu("ped_rcb",$mensaje,1);
		$titulos="numero;fecha;usuario;proveedor;total";
		$sql="select c.numero,c.fecha,u.nombre,p.razon,c.total_conf+c.iva_conf from pedido_cab c,usuarios ";
		$sql.="u,proveedores p where c.estado='EMITIDO' and c.usuario=u.usuario and ";
		$sql.="c.proveedor=p.codigo order by numero;corecibecmp.php+numero";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;2");
	}	
}
// PEDIDOS DE COMPRA POR CONFIRMAR
$pedcnf_sql="select count(*) from pedido_cab where estado='GENERADO'";
$hay_ped_cnf=un_dato($pedcnf_sql);
if($hay_ped_cnf>0)
{
	if(ver_item("ped_cnf"))
	{
		$mensaje="Hay $hay_ped_cnf Pedido/s de Compra por Confirmar";
		linea_menu("ped_cnf",$mensaje,0);
	}else
	{
		$mensaje="Ocultar Pedidos de Compra por Confirmar";
		linea_menu("ped_cnf",$mensaje,1);
		$titulos="numero;fecha;usuario;proveedor;neto";
		$sql="select p.numero,p.fecha,u.nombre,r.razon,p.total_prev+p.iva_prev from pedido_cab p,proveedores r,usuarios u";
		$sql.=" where p.usuario=u.usuario and p.proveedor=r.codigo and estado='GENERADO' order by numero;coconficmp.php+numero";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;2");
	}	
}
// CARTUCHOS EN FALTA
/*
Conceptos
El campo pedir es el punto de reposicion normal, es la primera alarma
El campo minimo es la segunda alarma, es decir cuando ya es urgente pedir.
El campo compra es el stock maximo, lo que queda luego de recibir el pedido.
*/

$crtfal_sql="select count(*) from stock s,minimos m where s.cartucho=m.cartucho and s.cantidad<=m.pedir and m.compra-s.cantidad >0";
$crtmin_sql="select count(*) from stock s,minimos m where s.cartucho=m.cartucho and s.cantidad<=m.minimo";
$hay_crt_fal=un_dato($crtfal_sql);
$hay_crt_min=un_dato($crtmin_sql);
//trace("La cantidad de cartuchos faltantes es: $hay_crt_fal");
if($hay_crt_fal>0)
{
	$borrar_tbl=mysql_query("drop table faltan_tmp");
	$faltan_sql="create table faltan_tmp select c.codigo_int,c.codigo_orig,c.marca,c.color,c.tipo,c.descripcion,";
	$faltan_sql.="s.cantidad,m.compra-s.cantidad as faltan,000000 as ya_pedido from cartuchos c,stock s,minimos m";
	$faltan_sql.=" where s.cartucho=m.cartucho and s.cantidad<=m.pedir and m.compra-s.cantidad >0 and s.cartucho=c.codigo_int order by 1";
	
	mi_query($faltan_sql,"copanel.php. Imposible crear tabla temporal de faltantes");
	$borrar="select f.codigo_int,sum(i.activa) as activa from faltan_tmp f,cart_imp r,impresoras i where f.codigo_int=r.cod_cart and r.cod_imp=i.codigo group by 1 having sum(i.activa)=0";
	$borrar_qry=mi_query($borrar,"No se pudo consultar cartuchos inactivos");
	while($datos2=mysql_fetch_array($borrar_qry))
	{
		$codigo_int=$datos2["codigo_int"];
		mi_query("delete from faltan_tmp where codigo_int='$codigo_int'");
	}
	$yapedidos_sql="select d.cod_int,sum(cantidad) as pedido from pedido_cab c,pedido_det d where d.numero=c.numero and c.estado='GENERADO' group by 1";
	$yapedidos_qry=mi_query($yapedidos_sql,"copanel.php. Imposible obtener los pedidos.");
	while($datos=mysql_fetch_array($yapedidos_qry))
	{
		$cartucho=$datos["cod_int"];
		$pedido=$datos["pedido"];
		mi_query("update faltan_tmp set ya_pedido=$pedido where codigo_int=$cartucho","copanel.php. No se pudo actualizar tabla temporal de faltantes.");
	}
	if(ver_item("crt_fal"))
	{
		$ya_pedidos=un_dato("select count(*) from faltan_tmp where ya_pedido");
		$no_ped=$hay_crt_fal-$ya_pedidos;
		if($hay_crt_min>0): $mensaje="Hacer pedido de cartuchos urgente (faltan $no_ped tipos de cartucho)"; else: $mensaje="Hay $hay_crt_fal Cartucho/s en falta"; endif;
		linea_menu("crt_fal",$mensaje,0);
	}else
	{
		$mensaje="Ocultar cartuchos en falta";
		linea_menu("crt_fal",$mensaje,1);
		//linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,0,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
		$titulos="codigo Veinfar;codigo original;marca;color;tipo;descripcion;stock;pedir;ya pedido";
		$sql_t="select * from faltan_tmp  where !ya_pedido and tipo='Toner' order by 7";
		$sql_i="select * from faltan_tmp  where !ya_pedido and tipo<>'Toner' order by 7";
		$cuantos_t=un_dato("select count(*) from faltan_tmp where !ya_pedido and tipo='Toner'");
		$cuantos_i=un_dato("select count(*) from faltan_tmp where !ya_pedido and tipo<>'Toner'");
		if($cuantos_t)
		{
		    mi_titulo("Toner");
		    tabla_cons($titulos,$sql_t,1,"silver","#8EC99F",0);
		}
		if($cuantos_i)
		{
		    mi_titulo("Inkjet");
		    tabla_cons($titulos,$sql_i,1,"silver","#8EC99F",0);		    
		}
		$cuantos_faltan_sql="select count(*) from faltan_tmp where faltan>ya_pedido";
		$hay_que_pedir=un_dato($cuantos_faltan_sql);
		//trace("Pedir: $hay_que_pedir");
		if($hay_que_pedir>0)
			un_boton("aceptar","Hacer pedido","copedido.php");
		else
			mensaje("Ya estan todos pedidos");
	}	
}
// ORDENES DE TRABAJO SOLICITADAS
$hay_otsol=un_dato("select count(*) from soltrab where estado=1 and !instr(descripcion,'MP:')");
if($hay_otsol)
{
	if(ver_item("otsol"))
	{
		$mensaje="Hay $hay_otsol Ordenes de Trabajo solicitadas";
		linea_menu("otsol",$mensaje,0);
	}else
	{
		$mensaje="Ocultar Ordenes de Trabajo solicitadas";
		linea_menu("otsol",$mensaje,1);
		mi_titulo("Programacion de Ordenes de Trabajo");
		$rotulos="OT;USUARIO;PUESTO;UBICACION;FECHA SOL.;TIPO PROBLEMA;DISPOSITIVO;DESCRIPCION";
		$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_sol,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado=1  and !instr(s.descripcion,'MP:') and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo order by s.id_sol;coprogot.php+id_sol+panta+detalle+filtro+noprev";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="PROGRAMAR";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
	}
}

// ORDENES DE TRABAJO SOLICITADAS DE MANTENIMIENTO PREVENTIVO
$hay_otsolmp=un_dato("select count(*) from soltrab where estado=1 and instr(descripcion,'MP:')");
if($hay_otsolmp)
{
	if(ver_item("otsolmp"))
	{
		$mensaje="Hay $hay_otsolmp Ordenes de Trabajo solicitadas de Mant. Preventivo";
		linea_menu("otsolmp",$mensaje,0);
	}else
	{
		$mensaje="Ocultar Ordenes de Trabajo solicitadas de Mant. Preventivo";
		linea_menu("otsolmp",$mensaje,1);
		mi_titulo("Programacion de Ordenes de Trabajo de Mant. Preventivo");
		$rotulos="OT;USUARIO;PUESTO;UBICACION;FECHA SOL.;TIPO PROBLEMA;DISPOSITIVO;DESCRIPCION";
		$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_sol,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado=1  and instr(s.descripcion,'MP:') and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo order by s.id_sol;coprogot.php+id_sol+panta+detalle+filtro+prev";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="PROGRAMAR";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
	}
}


// ORDENES DE TRABAJO PROGRAMADAS
$hay_otprog=un_dato("select count(*) from soltrab where estado=2 and tecnico='$uid'");
if($hay_otprog)
{
	if(ver_item("otprog"))
	{
		$mensaje="Hay $hay_otprog Ordenes de Trabajo programadas para ud.";
		linea_menu("otprog",$mensaje,0);
	}else
	{
		$mensaje="Ocultar Ordenes de Trabajo programadas";
		linea_menu("otprog",$mensaje,1);
		mi_titulo("Ejecucion de Ordenes de Trabajo");
		$rotulos="OT;USUARIO;PUESTO;UBICACION;FECHA PROGRAMADA;TIPO PROBLEMA;DISPOSITIVO;DESCRIPCION";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="EJECUTAR";
		$para_hoy=un_dato("select count(*) from soltrab where estado=2 and tecnico='$uid' and fecha_prog=curdate()");
		if($para_hoy>0)
		{
			mi_titulo("Trabajos programados para hoy");
			$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_prog,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado=2 and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo and s.tecnico='$uid' and fecha_prog=curdate() order by s.id_sol;coejecot.php+id_sol+panta+detalle";
			tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		}
		$vencidos=un_dato("select count(*) from soltrab where estado=2 and tecnico='$uid' and fecha_prog<curdate()");
		if($vencidos>0)
		{
			mi_titulo("Trabajos programados vencidos");
			$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_prog,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado=2 and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo and s.tecnico='$uid' and fecha_prog<curdate() order by s.id_sol;coejecot.php+id_sol+panta+detalle";
			tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		}
		$futuros=un_dato("select count(*) from soltrab where estado=2 and tecnico='$uid' and fecha_prog>curdate()");
		if($futuros>0)
		{
			mi_titulo("Trabajos programados para despu&eacute;s");
			$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_prog,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado=2 and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo and s.tecnico='$uid' and fecha_prog>curdate() order by s.id_sol;coejecot.php+id_sol+panta+detalle";
			tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		}
	}
}
// Ordenes de trbajo vencidas
$hay_otvenc=un_dato("select count(*) from soltrab where estado in(2,3) and fecha_prog<curdate()");
if($hay_otvenc)
{
	if(ver_item("otvenc"))
	{
		$mensaje="Hay $hay_otvenc Ordenes de Trabajo vencidas";
		linea_menu("otvenc",$mensaje,0);
	}else
	{
		$mensaje="Ocultar Ordenes de Trabajo vencidas";
		linea_menu("otvenc",$mensaje,1);
		mi_titulo("Ordenes de Trabajo vencidas");
		$rotulos="OT;TECNICO;USUARIO;PUESTO;UBICACION;FECHA PROGRAMADA;TIPO PROBLEMA;DISPOSITIVO;DESCRIPCION";
		$sql="select s.id_sol,s.tecnico,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_prog,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado in(2,3) and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo and s.fecha_prog<curdate() order by s.id_sol";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,"","","","ORDENES DE TRABAJO VENCIDAS;otvenc;otvenc");	
	}
}

// Ordenes de trbajo en ejecucion
$hay_otejec=un_dato("select count(*) from soltrab where estado=3");
if($hay_otejec)
{
	if(ver_item("otejec"))
	{
		$mensaje="Hay $hay_otejec Ordenes de Trabajo en ejecuci&oacute;n";
		linea_menu("otejec",$mensaje,0);
	}else
	{
		$mensaje="Ocultar Ordenes de Trabajo en ejecuci&oacute;n";
		linea_menu("otejec",$mensaje,1);
		mi_titulo("Ordenes de Trabajo en ejecuci&oacute;n");
		$rotulos="OT;TECNICO;USUARIO;PUESTO;UBICACION;FECHA PROGRAMADA;TIPO PROBLEMA;DISPOSITIVO;DESCRIPCION";
		$sql="select s.id_sol,s.tecnico,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_prog,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado=3 and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo order by s.id_sol;coejecot.php+id_sol+panta+detalle";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales);
	}
}

// Ordenes de trbajo para aprobar
$hay_otapro=un_dato("select count(*) from soltrab where estado=4 and aprobado=0");
if($hay_otapro)
{
	if(ver_item("otapro"))
	{
		$mensaje="Hay $hay_otapro Ordenes de Trabajo para aprobar";
		linea_menu("otapro",$mensaje,0);
	}else
	{
		$mensaje="Ocultar Ordenes de Trabajo para aprobar";
		linea_menu("otapro",$mensaje,1);
		mi_titulo("Ordenes de Trabajo para aprobar");
		$rotulos="OT;TECNICO;USUARIO;PUESTO;FECHA PROGRAMADA;DISPOSITIVO;DESCRIPCION;RECOMENDACIONES;DIAGNOSTICO;TAREA";
		$sql="select s.id_sol,s.tecnico,u.nombre,p.descripcion as desc_puesto,s.fecha_prog,d.dispositivo,s.descripcion,s.obs_enc,s.obs_tec,s.tarea from soltrab s,puestos p,usuarios u,dispositivo d where s.estado=4 and aprobado=0 and s.usuario=u.usuario and s.puesto=p.codigo and d.id=s.dispositivo order by s.id_sol;coaproot.php+id_sol+panta+detalle";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="APROBAR";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
	}
}


// Desarrollos de software sin terminar vencido y con prioridad
$hay_desven=un_dato("select count(*) from desarrollo_software where estado<>'TERMINADO'  and fecha_prog<=curdate() and prioridad<>99");
if($hay_desven)
{
	if(ver_item("desven"))
	{
		$mensaje="Hay $hay_desven desarrollo/s de software vencido/s";
		linea_menu("desven",$mensaje,0);
	}else
	{
		$mensaje="Ocultar desarrollos de software vencidos";
		linea_menu("desven",$mensaje,1);
		mi_titulo("Desarrollos de software vencidos");
		$rotulos="id;prog;prioridad;categoria;descripcion;estado;avance;programador";
		$sql="select d.id_desa,d.fecha_prog,r.prioridad,d.categoria,d.descripcion,d.estado,d.avance,p.programador from desarrollo_software d,programador p,prioridad r where d.estado<>'TERMINADO' and d.fecha_prog<=curdate() and d.programador=p.id_prg and d.prioridad=r.id_prio and d.prioridad<>99 order by d.prioridad,2;coabm_desarrollo.php+id_desa+panta+modi";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="CAMBIAR";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
	}
}



raya();

// Otras funciones de cartuchos
if(ver_item("otros_car"))
{
	$mensaje="M&aacute;s funciones de cartuchos";
	linea_menu("otros_car",$mensaje,0);
}else
{
	$mensaje="Ocultar funciones de cartuchos";
	linea_menu("otros_car",$mensaje,1);
	echo("<ul>");
	echo("<ul><a href='coprecios.php'>Actualizar lista de precios</a></ul>");
	echo("<ul><a href='costock.php'>Ajuste de stock</a></ul>");
	echo("<ul><a href='cocart_imp.php'>Asignaci&oacute;n de cartuchos a impresoras</a></ul>");
	echo("<ul><a href='coabm_impresora.php'>ABM impresoras</a></ul>");
	echo("<ul><a href='coinfo_impresoras.php'>Informe de impresoras activas</a></ul>");
	echo("<ul><a href='copuesto_imp.php'>Asignaci&oacute;n de impresoras a puestos</a></ul>");
	echo("<ul><a href='cosolrec.php?volver=copanel.php''>Generaci&oacute;n de solicitud de recarga</a></ul>");
	echo("<ul><a href='coabm_minimos.php?volver=copanel.php''>Actualizaci&oacute;n de m&iacute;nimos</a></ul>");
	echo("<ul><a href='coestad_cartuchos.php?volver=copanel.php'>Estad&iacute;sticas</a></ul>");
	echo("<ul><a href='coabm_proveedor.php?volver=copanel.php'>Alta, baja y modi de proveedores</a></ul>");
	echo("<ul><a href='coconsped_compra.php?volver=copanel.php'>Consulta de Pedidos de Compra</a></ul>");
	echo("<ul><a href='coconsu_solrec.php?volver=copanel.php'>Consulta de Solicitudes de recarga</a></ul>");
	echo("</ul>");
}
	
// Otras funciones de soporte de sistemas
if(ver_item("otros_sop"))
{
	$mensaje="M&aacute;s funciones de soporte de sistemas";
	linea_menu("otros_sop",$mensaje,0);
}else
{
	$mensaje="Ocultar funciones de soporte de sistemas";
	linea_menu("otros_sop",$mensaje,1);
	echo("<ul>");
	echo("<ul><a href='cosolsoporte.php?volver=copanel.php'>Generaci&oacute;n de solicitud de soporte</a></ul>");
	echo("<ul><a href='coabm_ot.php'>Consulta y Modificacion de Ordenes de Trabajo</a></ul>");
	echo("<ul><a href='coconocimiento.php'>Base de conocimientos</a></ul>");
	echo("<ul><a href='coabm_inventario.php'>Inventario de PCs</a></ul>");
	echo("<ul><a href='coabm_repuestos.php'>Inventario de repuestos</a></ul>");
	echo("<ul><a href='cobitacora.php'>Bitacora de Problemas Resueltos</a></ul>");
	echo("<ul><a href='copreventivo.php'>Mantenimiento Preventivo</a></ul>");
	echo("<ul><a href='coforo.php?volver=copanel.php'>Mensajes</a></ul>");
	echo("<ul><a href='coestad_soporte.php?volver=copanel.php'>Estad&iacute;sticas</a></ul>");
	echo("</ul>");
}	

// Otras funciones de mantenimiento
if(ver_item("otros_man"))
{
	$mensaje="M&aacute;s funciones de mantenimiento";
	linea_menu("otros_man",$mensaje,0);
}else
{
	$mensaje="Ocultar funciones de mantenimiento";
	linea_menu("otros_man",$mensaje,1);
	echo("<ul>");
	echo("<ul><a href='cosolmant.php?volver=copanel.php'>Generaci&oacute;n de solicitud de mantenimiento</a></ul>");
	echo("<ul><a href='coabm_om.php'>Consulta y Modificacion de Ordenes de Trabajo</a></ul>");
	echo("</ul>");
}	
// Administracion de sistemas
if(ver_item("adm_sis"))
{
	$mensaje="Administracion de Sistemas";
	linea_menu("adm_sis",$mensaje,0);
}else
{
	$mensaje="Ocultar funciones de administracion";
	linea_menu("adm_sis",$mensaje,1);
	echo("<ul>");
	echo("<ul><a href='coabm_renovhard.php?volver=copanel.php'>Renovacion de Hardware</a></ul>");
	echo("<ul><a href='coabm_eventos.php?volver=copanel.php'>Registro de Eventos</a></ul>");
	echo("<ul><a href='coabm_desarrollo.php?volver=copanel.php'>Plan de desarrollo de Software</a></ul>");
	echo("<ul><a href='coabm_gastos.php?volver=copanel.php'>Ingreso de gastos</a></ul>");
	echo("<ul><a href='copresup_gastos.php'>Presupuesto de Sistemas</a></ul>");
	echo("<ul><a href='coevol_gasto.php'>Evolucion del Gasto</a></ul>");
	echo("<ul><a href='cofactu_abonos.php'>Liquidacion de Abonos</a></ul>");
	echo("<ul><a href='coabm_garantias.php'>Administracion de garantias</a></ul>");
	echo("<ul><a href='cotelcos.php'>Administracion de Telecomunicaciones</a></ul>");
	echo("<ul><a href='coabm_quemados.php'>Registro de dispositivos quemados</a></ul>");
	echo("</ul>");
}	

// Otras funciones de sistemas
if(ver_item("otros_sis"))
{
	$mensaje="M&aacute;s funciones de sistemas";
	linea_menu("otros_sis",$mensaje,0);
}else
{
	$mensaje="Ocultar funciones de sistemas";
	linea_menu("otros_sis",$mensaje,1);
	echo("<ul>");
	echo("<ul><a href='coabm_generico.php'>Abm Gen&eacute;rico</a></ul>");
	echo("<ul><a href='cousuarios.php'>Administraci&oacute;n de usuarios</a></ul>");
	echo("<ul><a href='cousu_puesto.php'>Asignaci&oacute;n de usuarios a puestos</a></ul>");
	echo("</ul>");
}	

cierre();
?>
