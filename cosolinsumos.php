<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Solicitud de Insumos</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$volver=$_GET["volver"];
if(!isset($volver))
{
	$volver="cocerrar.php";
}
$submit="aceptar-Confirmar-$volver";
$titulo="Solicitud de Insumos";
$panta=$_POST["panta"];
switch($panta)
{
	case "grabar":
		require_once("Mail.php");
		require_once("Mail/mime.php");
		mi_titulo($titulo);
		$fecha=$_POST["fecha"];
		$usuario=$_POST["usuario"];
		$descripcion=$_POST["descripcion"];
		$uid=$_POST["uid"];
		$puesto=$_POST["puesto"];
		$desc_puesto=$_POST["desc_puesto"];
		$cod_ins=$_POST["cod_ins"];
		$cantidad=$_POST["cantidad"];
		//$descripcion=$_POST["descripcion"];
		$observaciones=$_POST["observaciones"];
		$fecha_sis=a_fecha_sistema($fecha);
		$volver=$_POST["volver"];
		$urgente=$_POST["urgente"];
		$motivo=$_POST["motivo"];
		$urgentex=($urgente=='s') ? 1 : 0;
		//trace("El motivo es $motivo y urgente es $urgentex");
		if($volver=="")
			$volver="cocerrar.php";
		// Validacion
		$correcto=1;
		$error="";
		if($cod_insa=="0" and $descripcion=="")
		{
			$correcto=0;
			$error.="Falta definir el insumo.";
		}
		if($cantidad==0)
		{
			$correcto=0;
			$error.="\nFalta indicar la cantidad.";
		}
		if($urgentex and $motivo=="")
		{
			$correcto=0;
			$error.="\nFalta indicar el motivo de la urgencia.";
		}
		if(!$urgentex and $motivo<>"")
		{
			$correcto=0;
			$error.="\nNo se tildo urgente y sin embargo se puso un motivo.";
		}
		if($correcto)
		{
			if($cod_ins<>0)
				$descripcion=un_dato("select articulo from insumos where id_insumo='$cod_ins'");
			mi_query("insert into solins set usuario='$uid',puesto='$puesto',articulo='$descripcion',estado=1,fecha_sol=sysdate(),cod_ins='$cod_ins',cantidad='$cantidad',observaciones='$observaciones',urgente='$urgentex',motivo='$motivo'","Error al grabar la solicitud");
			$id_sol=mysql_insert_id();
			mensaje("Solicitud grabada con exito.");
			mi_tabla("i");
			echo("<tr><td>Solicitud: $id_sol</td></tr>");
			echo("<tr><td>Usuario: $nombre</td></tr>");
			echo("<tr><td>Fecha: $fecha</td></tr>");
			echo("<tr><td>Puesto: $desc_puesto</td></tr>");
			echo("<tr><td>Insumo: $cod_ins</td></tr>");
			echo("<tr><td>Descripcion: $descripcion</td></tr>");
			echo("<tr><td>Observaciones: $observaciones</td></tr>");
			echo("<tr><td>Urgente: $urgente</td></tr>");
			echo("<tr><td>Motivo: $motivo</td></tr>");
			mi_tabla("f");
			// Mail para el usuario
			$admin=un_dato("select usuario from usuarios where perfil=7");
			$asunto="Solicitud de Insumos";
			$texto="Hemos recibido su solicitud de insumos nro. $id_sol. Se le informar&aacute; en breve la fecha de env&iacute;o o de surgir alg&uacute;n inconveniente se le avisar&aacute; por esta misma via.";
			mandar_mail($usuario,"7",$asunto,$texto,$admin,"logo_copetin.jpeg",1);
			un_boton("Aceptar","Aceptar",$volver);
			break;
		}else
		{
			mensaje($error);
		}
	default:
		$fecha=a_fecha_arg(un_dato("select curdate()"));
		$usuario=un_dato("select nombre from usuarios where usuario='$uid'");
		$puesto=un_dato("select puesto from usu_puesto where usuario='$uid'");
		$perfil=un_dato("select perfil from usuarios where usuario='$uid'");
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		// validar
		if(isset($_POST["cod_ins"]) and $_POST["cod_ins"]<>0)
		{
			$cod_ins=$_POST["cod_ins"];
			$descrip_ins=un_dato("select articulo from insumos where id_insumo='$cod_ins'");
		}else
		{
			$cod_ins=0;
			$descrip_ins="Elegir";
		}
		if(isset($_POST["cantidad"]) and $_POST["cantidad"]<>0)
		{
			$cantidad=$_POST["cantidad"];
		}else
		{
			$cantidad=0;
		}
		if(isset($_POST["descripcion"]))
		{
			$descripcion=$_POST["descripcion"];
		}else
		{
			$descripcion="";
		}
		if(isset($_POST["observaciones"]))
		{
			$observaciones=$_POST["observaciones"];
		}else
		{
			$observaciones="";
		}
		if(isset($_POST["urgente"]))
		{
			$urgente=$_POST["urgente"];
		}else
		{
			$urgente="";
		}
		if(isset($_POST["motivo"]))
		{
			$motivo=$_POST["motivo"];
		}else
		{
			$motivo="";
		}
		$campos.="%ROT-<tr><td><strong>FECHA: </strong></td><td><strong>" . $fecha . "</strong></td></tr>";
		$campos.=";%OCU-fecha-$fecha";
		$campos.=";%ROT-<tr><td><strong>SOLICITANTE:</strong></td><td><strong>$usuario</strong></td></tr>";
		$campos.=";%ROT-<tr><td><strong>PUESTO:</strong></td><td><strong>$desc_puesto</strong></td></tr>";
		$campos.=";%OCU-uid-$uid";
		$campos.=";%OCU-usuario-$usuario";
		$campos.=";%OCU-puesto-$puesto";
		$campos.=";%OCU-desc_puesto-$desc_puesto";
		$campos.=";%SEL-cod_ins-insumo-select id_insumo,articulo from insumos order by 2-articulo+id_insumo-$descrip_ins-$cod_ins";
		$campos.=";%TXT-cantidad-cantidad-$cantidad-5";
		$campos.=";%ARE-descripcion-descripcion-$descripcion-5-80";
		$campos.=";%ARE-observaciones-observaciones-$observaciones-5-80";
		$campos.=";%CHK-urgente-urgente-s-$urgente";
		$campos.=";%TXT-motivo urg.-motivo-$motivo-50";
		$campos.=";%OCU-volver-$volver";
		$campos.=";%OCU-panta-grabar";
		mi_panta($titulo,$campos,$submit);
		break;
}
?>
</BODY>
</HTML>
