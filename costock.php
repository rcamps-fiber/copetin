<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Ajuste de stock</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("AJUSTES DE STOCK");
$panta=$_POST["panta"];
switch($panta)
{
	case "modi":
		$cod_int=$_POST["cartucho"];
		$cantidad=un_dato("select cantidad from stock where cartucho=$cod_int");
		$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int=$cod_int");
		$titulo="Actualizaci&oacute;n de Stock";
		$campos_pantalla="%OCU-cod_int-$cod_int;";
		$campos_pantalla.="%ROT-<tr><td><strong>MODELO</strong></td><td><strong>$cod_orig</strong></td></tr>;";
		$campos_pantalla.="%TXT-cantidad-cantidad-$cantidad-10;";
		$campos_pantalla.="%CHK-elimina-elimina-1-n;";
		$campos_pantalla.="%OCU-panta-graba_modi";
		$submit="aceptar-Modificar-costock.php";	
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
	case "graba_modi":
		$cod_int=$_POST["cod_int"];
		$cantidad=$_POST["cantidad"];
		un_dato("select cantidad from stock where cartucho=$cod_int");
		$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int=$cod_int");
		$elimina=$_POST["elimina"];
		if($elimina=="1")
		{
			mi_query("delete from stock where cartucho=$cod_int","costock.php Linea 39. Imposible eliminar el item de stock.");
			mensaje("Se elimin&oacute; del stock el $cod_orig.");
		}else
		{
			mi_query("update stock set cantidad=$cantidad where cartucho=$cod_int","costock.php Linea 43. Imposible actualizar el stock.");
			mensaje("Se actualiz&oacute; el stock para el item $cod_orig.");
		}
		un_boton();
		break;
	case "graba_alta":
		$cod_int=$_POST["cod_int"];
		$cantidad=$_POST["cantidad"];
		$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int=$cod_int");
		$no_existe=un_dato("select count(*) from stock where cartucho=$cod_int");
		if($no_existe==0)
		{
			mi_query("insert into stock set cartucho=$cod_int,cantidad=$cantidad","costock.php Linea 62. Imposible grabar alta de stock.");
			mensaje("Se agreg&oacute; a la lista de stock el $cod_orig.");
		}else
		{
			mensaje("El cartucho $cod_orig ya exist&iacute;a en el stock.");
		}
		un_boton();
		break;
	default:
		$titulo="Alta de Stock";
		$campos_pantalla="%SEL-cod_int-modelo-select codigo_orig,codigo_int from cartuchos-codigo_orig+codigo_int;";
		$campos_pantalla.="%TXT-cantidad-cantidad--3;";
		$campos_pantalla.="%OCU-panta-graba_alta";
		$submit="aceptar-Grabar-copanel.php";	
		mi_panta($titulo,$campos_pantalla,$submit);		
		break;
}
$titulos="cod.Veinfar;cod.orig;tipo;descripcion;cantidad";
$sql="select s.cartucho,c.codigo_orig,c.tipo,c.descripcion,s.cantidad";
$sql.=" from stock s,cartuchos c where s.cartucho=c.codigo_int order by 1;costock.php+cartucho+panta+modi";
mi_titulo("Lista de Stocks");
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Procesar","","Cartuchos;Cartuchos;cartuchos");
?>
</BODY>
</HTML>
