<?
include_once('coacceso.php');
include_once('cofunciones_especificas.php');
include_once("cofunciones.php");

?>
<HTML>

<HEAD>
<TITLE>Panel de Control</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
actu_item();
mi_titulo("Panel de Control");

// ORDENES DE MANTENIMIENTO SOLICITADAS
$hay_otmansol=un_dato("select count(*) from mantenimiento where estado=1");
if($hay_otmansol)
{
	if(ver_item("otmansol"))
	{
		$mensaje="Hay $hay_otmansol Ordenes de Mantenimiento solicitadas";
		linea_menu("otmansol",$mensaje,0,"copanel_mant.php");
	}else
	{
		$mensaje="Ocultar Ordenes de Mantenimiento solicitadas";
		linea_menu("otmansol",$mensaje,1,"copanel_mant.php");
		mi_titulo("Programacion de Ordenes de Mantenimiento");
		$rotulos="OM;FECHA;HORA;SOLICITANTE;EDIFICIO;SECTOR;RECLAMO;USUARIO;PUESTO";
		$sql="select s.id_mant,s.fecha_solicitud,s.hora_solicitud,s.solicitante,e.nombre as edificio,s.sector,s.reclamo,u.nombre,p.descripcion as desc_puesto from mantenimiento s,puestos p,usuarios u,edificio e where s.estado=1 and e.id=s.edificio and s.usuario=u.usuario and s.puesto=p.codigo order by s.id_mant;coprogom.php+id_mant+panta+detalle";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="PROGRAMAR";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
	}
}

// ORDENES DE MANTENIMIENTO PROGRAMADAS
$hay_otmanprog=un_dato("select count(*) from mantenimiento where estado=2");
if($hay_otmanprog)
{
	if(ver_item("otmanprog"))
	{
		$mensaje="Hay $hay_otmanprog Ordenes de Mantenimiento programadas";
		linea_menu("otmanprog",$mensaje,0,"copanel_mant.php");
	}else
	{
		$mensaje="Ocultar Ordenes de Mantenimiento programadas";
		linea_menu("otmanprog",$mensaje,1,"copanel_mant.php");
		mi_titulo("Ejecucion de Ordenes de Mantenimiento");
		$rotulos="OM;FECHA PROG.;SOLICITANTE;SECTOR;EDIFICIO;RECLAMO;OBSERVACIONES;USUARIO;TECNICO";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="EJECUTAR";
		$para_hoy=un_dato("select count(*) from mantenimiento where estado=2 and fecha_prog=curdate()");
		if($para_hoy>0)
		{
			mi_titulo("Trabajos programados para hoy");
			$sql="select s.id_mant,s.fecha_prog,s.solicitante,s.sector,e.nombre as edificio,s.reclamo,s.observaciones,u.nombre,s.tecnico from mantenimiento s,usuarios u,edificio e where s.estado=2 and s.usuario=u.usuario and s.edificio=e.id and fecha_prog=curdate() order by s.id_mant;coejecom.php+id_mant+panta+detalle";
			tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		}
		$vencidos=un_dato("select count(*) from mantenimiento where estado=2 and fecha_prog<curdate()");
		if($vencidos>0)
		{
			mi_titulo("Trabajos programados vencidos");
//			$rotulos="OM;FECHA PROG.;TECNICO;SOLICITANTE;EDIFICIO;SECTOR;RECLAMO;OBSERVACIONES;USUARIO";

			$sql="select s.id_mant,s.fecha_prog,s.solicitante,s.sector,e.nombre as edificio,s.reclamo,s.observaciones,u.nombre,s.tecnico from mantenimiento s,usuarios u,edificio e where s.estado=2 and s.usuario=u.usuario and s.edificio=e.id and fecha_prog<curdate() order by s.id_mant;coejecom.php+id_mant+panta+detalle";
			tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		}
		$futuros=un_dato("select count(*) from mantenimiento where estado=2 and fecha_prog>curdate()");
		if($futuros>0)
		{
			mi_titulo("Trabajos programados para despu&eacute;s");
			$sql="select s.id_mant,s.fecha_prog,s.solicitante,s.sector,e.nombre as edificio,s.reclamo,s.observaciones,u.nombre,s.tecnico from mantenimiento s,usuarios u,edificio e where s.estado=2 and s.usuario=u.usuario and s.edificio=e.id and fecha_prog>curdate() order by s.id_mant;coejecom.php+id_mant+panta+detalle";
			tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		}
	}
}
// Ordenes de mantenimiento vencidas
$hay_otmanvenc=un_dato("select count(*) from mantenimiento where estado in(2,3) and fecha_prog<curdate()");
if($hay_otmanvenc)
{
	if(ver_item("otmanvenc"))
	{
		$mensaje="Hay $hay_otmanvenc Ordenes de Mantenimiento vencidas";
		linea_menu("otmanvenc",$mensaje,0,"copanel_mant.php");
	}else
	{
		$mensaje="Ocultar Ordenes de Mantenimiento vencidas";
		linea_menu("otmanvenc",$mensaje,1,"copanel_mant.php");
		mi_titulo("Ordenes de Mantenimiento vencidas");
		$rotulos="OM;FECHA PROG.;TECNICO;SOLICITANTE;EDIFICIO;SECTOR;RECLAMO;OBSERVACIONES";
		$sql="select s.id_mant,s.fecha_prog,s.tecnico,s.solicitante,s.edificio,s.sector,s.reclamo,s.observaciones from mantenimiento s where s.estado in(2,3) and s.fecha_prog<curdate() order by s.id_mant";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales);
	}
}


// Otras funciones
if(ver_item("otrosman"))
{
	$mensaje="M&aacute;s funciones";
	linea_menu("otrosman",$mensaje,0,"copanel_mant.php");
}else
{
	$mensaje="Ocultar funciones";
	linea_menu("otrosman",$mensaje,1,"copanel_mant.php");
	echo("<ul>");
	echo("<ul><a href='cosolsoporte.php?volver=copanel_mant.php'>Generaci&oacute;n de solicitud de soporte</a</ul>");
	echo("<ul><a href='cosolrec.php?volver=copanel_mant.php''>Generaci&oacute;n de solicitud de recarga</a</ul>");
	echo("<ul><a href='coabm_om.php'>Consulta y Modificacion de Ordenes de Mantenimiento</a</ul>");
	echo("<ul><a href='comanprev.php'>Mantenimiento Preventivo</a</ul>");	
	echo("</ul>");
}	

?>
</body>
</html>
