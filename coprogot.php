<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Programacion de Ordenes de Trabajo</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Confirmar-coprogot.php";
$panta=$_POST["panta"];
//trace($_POST["filtro"]);
switch($panta)
{
	case "detalle":
		$id_sol=$_POST["id_sol"];
		$filtro=$_POST["filtro"];
		$fecha_prog=$_POST["fecha_prog"];
		$tecnico=$_POST["tecnico"];
		$prioridad=$_POST["prioridad"];
		$observaciones=$_POST["observaciones"];
		$sql="select s.fecha_sol,s.usuario,s.puesto,p.descripcion as desc_puesto,s.tipo_problema,t.problema,s.dispositivo,d.dispositivo as desc_dispo,s.descripcion as desc_prob ";
		$sql.="from soltrab s,tipo_problema t,puestos p,dispositivo d ";
		$sql.="where s.id_sol='$id_sol' and s.tipo_problema=t.id and s.dispositivo=d.id and s.puesto=p.codigo";
		$cns=mi_query($sql,"Error al obtener la ot");
		$datos=mysql_fetch_array($cns);
		$titulo="PROGRAMACION DE LA ORDEN DE TRABAJO Nro. $id_sol";
		$fecha_sol=$datos["fecha_sol"];
		if(!isset($fecha_prog))
		{
			$hoy=un_dato("select curdate()");
			$fecha_prog=a_fecha_arg($hoy);
		}else
		{
			$fecha_prog=a_fecha_arg($datos["fecha_prog"]);
		}
		$fecha_sol=a_fecha_arg($fecha_sol);
		if(!isset($prioridad))
			$prioridad="NORMAL";
		if(!isset($tecnico))
			$tecnico="Elegir";
		$usuario=$datos["usuario"];
		$puesto=$datos["puesto"];
		$desc_puesto=$datos["desc_puesto"];
		$descripcion=$datos["desc_prob"];
		$tipo_problema=$datos["tipo_problema"];
		$problema=$datos["problema"];
		$dispositivo=$datos["dispositivo"];
		$desc_dispo=$datos["desc_dispo"];
		$horas_est=un_dato("select horas from horas_estimadas h,dispositivo d where problema='$problema' and h.dispositivo=d.dispositivo and d.id='$dispositivo'");
		$campos="%ROT-FECHA SOL.</td><td>$fecha_sol";
		$campos.=";%ROT-SOLICITANTE</td><td>$usuario de $desc_puesto";
		$campos.=";%ROT-TIPO DE PROBLEMA</td><td>$problema";
		$campos.=";%ROT-DISPOSITIVO</td><td>$desc_dispo";
		$campos.=";%ROT-MOTIVO SOL.</td><td>$descripcion";
		$campos.=";%TXT-fecha prog.-fecha_prog-$fecha_prog-10";
		$campos.=";%ARE-observaciones-observaciones-$observaciones-5-80";
		$campos.=";%SEL-prioridad-prioridad-$prioridad+$prioridad+ALTA+ALTA+BAJA+BAJA+NORMAL+NORMAL-0";
		$campos.=";%SEL-tecnico-tecnico-select usuario,usuario from usuarios where perfil in(1,4)-usuario+usuario-$tecnico-$tecnico";
		$campos.=";%TXT-Horas estimadas-horas_est-$horas_est-10";
		$campos.=";%CHK-anular-anular-S-n";
		$campos.=";%OCU-id_sol-$id_sol";
		$campos.=";%OCU-panta-grabar";
		$campos.=";%OCU-fecha_sol-$fecha_sol";
		$campos.=";%OCU-usuario-$usuario";
		$campos.=";%OCU-desc_puesto-$desc_puesto";
		$campos.=";%OCU-problema-$problema";
		$campos.=";%OCU-dispositivo-$desc_dispo";
		$campos.=";%OCU-descripcion-$descripcion";
		$campos.=";%OCU-filtro-$filtro";
		mi_panta($titulo,$campos,$submit);
		break;
	case "grabar":
		require_once("Mail.php");
		require_once("Mail/mime.php");
		mi_titulo("Programacion de ordenes de trabajo");
		$id_sol=$_POST["id_sol"];
		$fecha_prog=a_fecha_sistema($_POST["fecha_prog"]);
		$tecnico=$_POST["tecnico"];
		$horas_est=$_POST["horas_est"];
		$prioridad=$_POST["prioridad"];
		$observaciones=$_POST["observaciones"];
		$fecha_sol=$_POST["fecha_sol"];
		$usuario=$_POST["usuario"];
		$desc_puesto=$_POST["desc_puesto"];
		$problema=$_POST["problema"];
		$dispositivo=$_POST["dispositivo"];
		$descripcion=$_POST["descripcion"];
		$filtro=$_POST["filtro"];
		$anular=$_POST["anular"];
		//trace("El tecnico es $tecnico");
		$nom_admin=un_dato("select nombre from usuarios where perfil=1");
		$admin=un_dato("select usuario from usuarios where perfil=1");			
		$email=un_dato("select email from usuarios where usuario='$usuario'");
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$email_admin=un_dato("select email from usuarios where usuario='$admin'");
		if($anular=="S")
		{
			mi_query("update soltrab set estado=5 where id_sol='$id_sol'","Error al anular la ot");
			mensaje("Solicitud anulada");
			if(!(strpos($descripcion,"MP:") === false))
			{
				$pos_ini=strpos($descripcion,"MP:");
				$pos_fin=strpos($descripcion,"*");
				$id_mant=substr($descripcion,$pos_ini+3,$pos_fin-($pos_ini+3));
				//trace("El id es $id_mant");
				$frecuencia=un_dato("select frecuencia from controlable where id='$id_mant'");
				mi_query("update controlable set estado=5,ultimo=curdate(),proximo=date_add(curdate(),interval $frecuencia day),ot=$id_sol where id='$id_mant'","Error: imposible actualizar la tabla de mantenimiento preventivo con el estado finalizado");
			}		
			// Mail  de anulacion para el usuario
			$mensaje="<html>";
			$mensaje.='<div id="logo"><img src="logo_copetin.jpeg" align="left" border=0></div>';
			$mensaje.='<div id="cuerpo"><h3>Sistema Copetin</h3>';
			$mensaje.="Estimado/a $nombre<br>En relaci&oacute;n a su solicitud nro. $id_sol por un problema de tipo $problema con el dispositivo $dispositivo, le informamos que la misma ha sido anulada.<br><br>Atte, $nom_admin.";
			$mensaje.="</div></html>";
			$version_texto="Estimado/a $nombre\nEn relacion a su solicitud nro. $id_sol por un problema de tipo $problema con el dispositivo $dispositivo, le informamos que la misma ha sido anulada.\nAtte, $nom_admin.";
			$destino = $email . "," . $email_admin;
			$encabezado['From']    = $email_admin;
			$encabezado['To']      = $email;
			$encabezado['Subject'] = 'Solicitud de soporte tecnico';
			$cuerpo = $mensaje;
			$params['sendmail_path'] = '/usr/lib/sendmail';
			// Crear el objeto correo con el metodo Mail::factory
			$imagen="imagenes/logo_copetin.jpeg";
			$mime= new Mail_mime("\n");
			$mime->setTXTBody($version_texto);
			$mime->setHTMLBody($mensaje);
			$mime->addHTMLImage($imagen," image/jpeg");
			$cuerpo=$mime->get();
			$hdrs=$mime->headers($encabezado);
			$mail=& Mail::factory('sendmail',$params);
			$mail->send($destino,$hdrs,$cuerpo);
			un_boton("Aceptar","Aceptar","coprogot.php","filtro",$filtro);
			break;
		}
		// Validacion
		$correcto=1;
		$error="";
		if(!fecha_ok($fecha_prog))
		{
			$correcto=0;
			$error.="Fecha programada invalida";
		}
		$hoy=un_dato("select curdate()");
		if($fecha_prog<$hoy)
		{
			$correcto=0;
			$error.="\nFecha programada ya vencida.";
		}
		if($tecnico=="Elegir")
		{
			$correcto=0;
			$error.="\nFalta especificar el tecnico.";
		}	
		if($correcto)
		{
			mi_query("update soltrab set fecha_prog='$fecha_prog',observaciones='$observaciones',tecnico='$tecnico',prioridad='$prioridad',estado=2 where id_sol='$id_sol'","Error al programar la ot");
			if(!(strpos($descripcion,"MP:") === false))
			{
				$pos_ini=strpos($descripcion,"MP:");
				$pos_fin=strpos($descripcion,"*");
				$id_mant=substr($descripcion,$pos_ini+3,$pos_fin-($pos_ini+3));
				//trace("El id es $id_mant");
				$frecuencia=un_dato("select frecuencia from controlable where id='$id_mant'");
				mi_query("update controlable set estado=2,ultimo=curdate(),proximo=date_add(curdate(),interval $frecuencia day),ot=$id_sol where id='$id_mant'","Error: imposible actualizar la tabla de mantenimiento preventivo con el estado finalizado");
			}		
			mensaje("Orden de Trabajo programada con exito.");
			$fecha_prog=a_fecha_arg($fecha_prog);
			mi_tabla("i");
			echo("<tr><td>Orden de trabajo: $id_sol</td></tr>");
			echo("<tr><td>Usuario: $usuario</td></tr>");
			echo("<tr><td>Fecha solicitud: $fecha_sol</td></tr>");
			echo("<tr><td>Problema: $problema</td></tr>");
			echo("<tr><td>Dispositivo: $dispositivo</td></tr>");
			echo("<tr><td>Descripcion: $descripcion</td></tr>");
			echo("<tr><td>Fecha: $fecha_prog</td></tr>");
			echo("<tr><td>Prioridad: $prioridad</td></tr>");
			echo("<tr><td>Tecnico asignado: $tecnico</td></tr>");
			echo("<tr><td>Observaciones: $observaciones</td></tr>");
			mi_tabla("f");
			// Mail para el usuario
			$mensaje="<html>";
			$mensaje.='<div id="logo"><img src="logo_copetin.jpeg" align="left" border=0></div>';
			$mensaje.='<div id="cuerpo"><h3>Sistema Copetin</h3>';
			$mensaje.="Estimado/a $nombre<br>En relaci&oacute;n a su solicitud nro. $id_sol por un problema de $problema con el dispositivo $dispositivo, hemos programado la Orden de Trabajo para el d&iacute;a $fecha_prog<br><br>Atte, $nom_admin.";
			$mensaje.="</div></html>";
			$version_texto="Estimado/a $nombre\nEn relacion a su solicitud nro. $id_sol por un problema de $problema con el dispositivo $dispositivo, hemos programado la Orden de Trabajo para el dia $fecha_prog\nAtte, $nom_admin.";
			$destino = $email . "," . $email_admin;
			$encabezado['From']    = $email_admin;
			$encabezado['To']      = $email;
			$encabezado['Subject'] = 'Solicitud de soporte tecnico';
			$cuerpo = $mensaje;
			$params['sendmail_path'] = '/usr/lib/sendmail';
			// Crear el objeto correo con el metodo Mail::factory
			$imagen="imagenes/logo_copetin.jpeg";
			$mime= new Mail_mime("\n");
			$mime->setTXTBody($version_texto);
			$mime->setHTMLBody($mensaje);
			$mime->addHTMLImage($imagen," image/jpeg");
			$cuerpo=$mime->get();
			$hdrs=$mime->headers($encabezado);
			$mail=& Mail::factory('sendmail',$params);
			$mail->send($destino,$hdrs,$cuerpo);
				
			// Mail para el tecnico
			$email=un_dato("select email from usuarios where usuario='$tecnico'");
			$nombre=un_dato("select nombre from usuarios where usuario='tecnico'");
			$encabezado['To']      = $email;
			$destino = $email . "," . $email_admin;
			$mensaje="<html>";
			$mensaje.='<div id="logo"><img src="logo_copetin.jpeg" align="left" border=0></div>';
			$mensaje.="<div id='cuerpo'><h3>Sistema Copetin - Asignaci&oacute;n de Orden de Trabajo Nro. $id_sol</h3><br><hr><br><br><br>";
			$mensaje.="T&eacute;cnico asignado: $tecnico<br>";
			$mensaje.="Fecha de Ejecuci&oacute;n: <strong>$fecha_prog</strong><br>";
			$mensaje.="Prioridad: $prioridad<br>";
			$mensaje.="Usuario solicitante: $usuario de $desc_puesto<br>";
			$mensaje.="Tipo de problema: $problema<br>";
			$mensaje.="Dispositivo: $dispositivo<br>";
			$mensaje.="Descripci&oacute;n: $descripcion<br>";
			$mensaje.="Observaciones: ";
			$mensaje.="<a href='http://www.veinfar.com.ar:666'>Por favor consultar el sistema</a><br>";
			$mensaje.="</div></html>";
			$version_texto="Sistema Copetin - Asignacion de Orden de Trabajo Nro. $id_sol\n";
			$version_texto.="Tecnico asignado: $tecnico\n";
			$version_texto.="Fecha de Ejecucion: $fecha_prog\n";
			$version_texto.="Prioridad: $prioridad\n";
			$version_texto.="Usuario solicitante: $usuario de $desc_puesto\n";
			$version_texto.="Tipo de problema: $problema\n";
			$version_texto.="Dispositivo: $dispositivo\n";
			$version_texto.="Descripcion: $descripcion\n";
			$version_texto.="Observaciones: $observaciones\n";
			
			$imagen="imagenes/logo_copetin.jpeg";
			$mime= new Mail_mime("\n");
			$mime->setTXTBody($version_texto);
			$mime->setHTMLBody($mensaje);
			$mime->addHTMLImage($imagen," image/jpeg");
			$cuerpo=$mime->get();
			$hdrs=$mime->headers($encabezado);
			$mail=& Mail::factory('sendmail',$params);
			$mail->send($destino,$hdrs,$cuerpo);
			un_boton("Aceptar","Aceptar","coprogot.php","filtro",$filtro);
			break;
		}else
		{
			mensaje($error);
			$campos="%OCU-panta-detalle";
			$campos.=";%OCU-id_sol-$id_sol";
			$campos.=";%OCU-fecha_prog-$fecha_prog";
			$campos.=";%OCU-tecnico-$tecnico";
			$campos.=";%OCU-prioridad-$prioridad";
			$campos.=";%OCU-observaciones-$observaciones";
			$campos.=";%OCU-filtro-$filtro";
			mi_panta("",$campos,$submit);
		}
		break;
	default:
		$filtro=(isset($_POST["filtro"]) and $_POST["filtro"]=="noprev") ? " and  !instr(s.descripcion,'MP:') " : "and 1=1";
		$filtro=(isset($_POST["filtro"]) and $_POST["filtro"]=="prev") ? " and  instr(s.descripcion,'MP:') " : $filtro;
		mi_titulo("Programacion de Ordenes de Trabajo");
		$rotulos="OT;USUARIO;PUESTO;UBICACION;FECHA SOL.;TIPO PROBLEMA;DISPOSITIVO;DESCRIPCION";
		$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_sol,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado=1 and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo $filtro order by s.id_sol;coprogot.php+id_sol+panta+detalle";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="PROGRAMAR";
		$casos=un_dato("select count(*) from soltrab s where estado=1 $filtro");
		if($casos)
		{
			tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		}else
		{
			mensaje("No hay ORDENES DE TRABAJO solicitadas pendientes de programaci&oacute;n.");
		}
		un_boton("Volver","Volver","copanel.php");
		break;
}
?>
</BODY>
</HTML>
