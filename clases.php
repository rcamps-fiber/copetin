<?
Class PANTALLA{


var $campos;
var $submit;
var $titulo;
var $ventana;
var $mostrar="Aceptar";
var $devolver="aceptar";
var $regresar;
var $tipos_validos="TXT,SEL,CHK,FIL,PWD,OCU,ROT,ARE,FEC";
var $SelRotIni="Elegir uno";
var $SelValIni=0;
var $FactorAltura=28;
var $IncrementoAltura=30;
var $recuadro=0;
var $ColorFondo="#8F8162";
var $ColorFrente="#D3C295";

function AgregarTXT($variable,$rotulo,$valor,$logitud)
{
	$this->campos.=";%TXT-$rotulo-$variable-$valor-$logitud";
}
function AgregarFEC($variable,$rotulo,$valor)
{
	$this->campos.=";%FEC-$rotulo-$variable-$valor";
}
function AgregarROT($rotulo)
{
	$this->campos.=";%ROT-$rotulo";
}
function AgregarPWD($variable,$rotulo,$valor,$logitud)
{
	$this->campos.=";%PWD-$rotulo-$variable-$valor-$logitud";
}
function AgregarARE($rotulo,$variable,$valor,$filas,$columnas)
{
	$this->campos.=";%ARE-$rotulo-$variable-$valor-$filas-$columnas";
}
function AgregarOCU($variable,$valor)
{
	$this->campos.=";%OCU-$variable-$valor";
}

function AgregarSEL($variable,$rotulo,$valor,$logitud,$una_tabla="")
{
	$this->campos.=";%SEL-$variable-$rotulo-select $variable,$rotulo from $una_tabla order by 2-$rotulo+$variable-$this->SelRotIni-$this->SelValIni";
}
function AgregarCHK($rotulo,$variable,$valor,$check)
{
	$this->campos.=";%CHK-$rotulo-$variable-$valor-$check";
}
function AgregarFIL($rotulo)
{
	$this->campos.=";%FIL-$rotulo";
}


function MostrarPantalla()
{
	if($this->recuadro)
	{
		$cuantos=substr_count($this->campos,"%");
		$cuantos=$cuantos+1;
		$espacio_titulo=(strlen($titulo)>0) ? 30 : 0;
		$altura=$cuantos*$this->FactorAltura+$this->IncrementoAltura+$espacio_titulo;
		$memos=substr_count($this->campos,"%ARE");
		//die();
		//$memos=5;
		//trace("Hay $memos memos");
		$altura_memo=0;
		for($i=0; $i<$memos; $i++)
		{
			//trace("valor de I $i");
			//die();
			// Hay campos memo. Hay que agrandar las cajas.
			$ultima_pos=strpos($this->campos,"%ARE")+5;
			//trace($this->campos);
			//trace("Ultima pos $ultima_pos");
			for($x=1;$x<4;$x++)
			{
				//trace("X=$x");
				$raya=strpos($this->campos,"-",$ultima_pos);
				//trace("Raya= $raya");
				$proxima=strpos($this->campos,"-",$raya+1);
				//trace("Prox. $proxima");
				$ultima_pos=$raya+1;
			}
			$posicion=$raya+1;
			$largo=$proxima-$posicion;
			$altura_memo=substr($this->campos,$posicion,$largo);
			//trace("El Memo es $altura_memo de algo");
			$agregar=$agregar+$altura_memo;
		}
		//trace("Agregar: $agregar");
		$altura=$altura+$altura_memo*25+25;
		//trace("La altura es $altura");
		icaja("relative",100,0,700,$altura,$this->ColorFondo);
		icaja("relative",-2,-2,700,$altura,$this->ColorFrente);
	}
	mi_titulo(strtr($this->titulo,"+",";"));
	$multipart=(!(strpos($this->campos,"%FIL")=== false)) ? "MULTI" : "";
	if($this->ventana<>"")
	{
		mi_form("w $multipart");
	}else
	{
		mi_form("i $multipart");
	}
	mi_tabla("i",0);
	$entrada=explode(";",$this->campos);
	foreach($entrada as $celda)
	{
		$largo=strlen($celda);
		// El campo es un select
		if(!(strpos($celda,"%SEL")=== false))
		{
			$param_sel=explode("-",$celda);
			$campo=$param_sel[1];
			$rotulo=$param_sel[2];
			$sql=$param_sel[3];
			$columna=$param_sel[4];
			if(count($param_sel)==5)
			{
				$def_rot="Elegir";
				$def_cod="Elegir";
			}else
			{
				$def_rot=$param_sel[5];
				$def_cod=$param_sel[6];
			}
			if($columna=="0")
			{
				$sql=strtr($sql,"+",";");
			}else
			{
				$columna=strtr($columna,"+",";");
			}
			mi_select($campo,$rotulo,$sql,$columna,$def_rot,$def_cod);
		}
		// El campo es un check box
		if(!(strpos($celda,"%CHK")=== false))
		{
			$param_chk=explode("-",$celda);
			$rotulo=$param_chk[1];
			$campo=$param_chk[2];
			$ini=$param_chk[3];
			$check=$param_chk[4];
			//echo("<tr><td>$rotulo - $cmapo - $ini - $check</td></tr>");
			mi_box($rotulo,$campo,$ini,$check);
			//mi_tabla("i",0);
		}
		// El campo es un texto
		if(!(strpos($celda,"%TXT")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=$param_txt[1];
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			$largo=$param_txt[4];
			mi_text($rotulo,$campo,$ini,$largo);
			//echo("<td><input type='text' name='$celda'></td></tr>");
		}
		if(!(strpos($celda,"%PWD")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=$param_txt[1];
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			$largo=$param_txt[4];
			mi_password($rotulo,$campo,$ini,$largo);
			//echo("<td><input type='text' name='$celda'></td></tr>");
		}
		// El campo es hidden
		if(!(strpos($celda,"%OCU")=== false))
		{
			$param_ocu=explode("-",$celda);
			$campo=$param_ocu[1];
			$valor=$param_ocu[2];
			mi_oculto($campo,$valor);
			// echo("<input type='hidden' name='$campo' value='$valor'>");
		}
		// El campo es solamente un rotulo
		if(!(strpos($celda,"%ROT")=== false))
		{
			$param_rot=explode("-",$celda);
			$rotulo=$param_rot[1];
			$rotulo=strtr($rotulo,"+",";");
			$rotulo=str_replace("@","&nbsp;",$rotulo);
			if(strpos($rotulo,"<tr>") === false)
				$rotulo="<tr><td><strong>" . $rotulo . "</strong></td></tr>";
			$rotulo=str_replace("##","</td><td>",$rotulo);
			echo($rotulo);
		}
		if(!(strpos($celda,"%ARE")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=$param_txt[1];
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			$filas=$param_txt[4];
			$columnas=$param_txt[5];
			mi_textarea($rotulo,$campo,$ini,$filas,$columnas);
			//echo("<td><input type='text' name='$celda'></td></tr>");
		}
		if(!(strpos($celda,"%FEC")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=strtoupper($param_txt[1]);
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			if($ini==0 or $ini=="")
				$ini=un_dato("select curdate()");
			
			if((strpos($ini,"/")==4 or strpos($ini,"-")==4) and (strpos($ini,"/",5)==7 or strpos($ini,"-",5)==7))
			{
				$ini=a_fecha_arg($ini);
			}
			mi_text($rotulo,$campo,$ini,10);
		}
		if(!(strpos($celda,"%FIL")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=strtoupper($param_txt[1]);
			mi_file($rotulo);
		}
	}
	mi_tabla("f",1);
	echo("<br>");
	botones_submit($this->devolver,$this->mostrar,$this->regresar);
}
}			



/* Clase encargada de gestionar las conexiones a la base de datos */
Class Db{

   private $servidor='localhost';
   private $usuario='cartuchos';
   private $password='kartuch05';
   private $base_datos='cartuchos';
   private $link;
   private $stmt;
   private $array;

   static $_instance;

   /*La función construct es privada para evitar que el objeto pueda ser creado mediante new*/
   private function __construct(){
      $this->conectar();
   }

   /*Evitamos el clonaje del objeto. Patrón Singleton*/
   private function __clone(){ }

   /*Función encargada de crear, si es necesario, el objeto. Esta es la función que debemos llamar desde fuera de la clase para instanciar el objeto, y así, poder utilizar sus métodos*/
   public static function getInstance(){
      if (!(self::$_instance instanceof self)){
         self::$_instance=new self();
      }
      return self::$_instance;
   }

   /*Realiza la conexión a la base de datos.*/
   private function conectar(){
      $this->link=mysql_connect($this->servidor, $this->usuario, $this->password);
      mysql_select_db($this->base_datos,$this->link);
      @mysql_query("SET NAMES 'utf8'");
   }

   /*Método para ejecutar una sentencia sql*/
   public function ejecutar($sql){
      $this->stmt=mysql_query($sql,$this->link);
      return $this->stmt;
   }

   /*Método para obtener una fila de resultados de la sentencia sql*/
   public function obtener_fila($stmt,$fila){
      if ($fila==0){
         $this->array=mysql_fetch_array($stmt);
      }else{
         mysql_data_seek($stmt,$fila);
         $this->array=mysql_fetch_array($stmt);
      }
      return $this->array;
   }

   //Devuelve el último id del insert introducido
   public function lastID(){
      return mysql_insert_id($this->link);
   }

}
?>
