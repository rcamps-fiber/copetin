<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Ejecucion de Ordenes de Mantenimiento</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Confirmar-coejecom.php";
$panta=$_POST["panta"];
switch($panta)
{
	case "detalle":
		$id_mant=$_POST["id_mant"];
		$tarea=$_POST["tarea"];
		$obs_tec=$_POST["obs_tec"];
		$estado_ot=$_POST["estado_ot"];
		if(!isset($estado_ot))
		{
			$estado="FINALIZADO";
			$id="4";
		}
		$sql="select s.fecha_prog,s.solicitante,e.nombre as edificio,s.sector,s.reclamo,s.tecnico,s.observaciones,s.usuario from mantenimiento s,edificio e where s.id_mant='$id_mant' and e.id=s.edificio";
		$cns=mi_query($sql,"Error al obtener la om");
		$datos=mysql_fetch_array($cns);
		$titulo="EJECUCION DE LA ORDEN DE MANTENIMIENTO Nro. $id_mant";
		$fecha_prog_sis=$datos["fecha_prog"];
		$fecha_prog=a_fecha_arg($fecha_prog_sis);
		$solicitante=$datos["solicitante"];
		$usuario=$datos["usuario"];
		$edificio=$datos["edificio"];
		$sector=$datos["sector"];
		$reclamo=$datos["reclamo"];
		$tecnico=$datos["tecnico"];
		$observaciones=$datos["observaciones"];
		$campos="%ROT-FECHA PROG.</td><td>$fecha_prog";
		$campos.=";%ROT-SOLICITANTE</td><td>$solicitante de $sector";
		$campos.=";%ROT-EDIFICIO</td><td>$edificio";
		$campos.=";%ROT-RECLAMO</td><td>$reclamo";
		$campos.=";%ROT-TECNICO</td><td>$tecnico";
		$campos.=";%ROT-OBSERVACIONES</td><td>$observaciones";
		$campos.=";%TXT-tarea realizada-tarea-$tarea-50";
		$campos.=";%TXT-Observ. tecnico-obs_tec-$obs_tec-50";
		$campos.=";%SEL-estado_ot-estado-$id+$estado+4+FINALIZADO+3+EN EJECUCION-0";
		$campos.=";%OCU-id_mant-$id_mant";
		$campos.=";%OCU-fecha_prog-$fecha_prog";
		$campos.=";%OCU-solicitante-$solicitante";
		$campos.=";%OCU-usuario-$usuario";
		$campos.=";%OCU-reclamo-$reclamo";
		$campos.=";%OCU-tecnico-$tecnico";
		$campos.=";%OCU-panta-grabar";
		$campos.=";%OCU-observaciones-$observaciones";
		mi_panta($titulo,$campos,$submit);
		break;
	case "grabar":
		mi_titulo("Ejecucion de ordenes de Mantenimiento");
		$id_mant=$_POST["id_mant"];
		$tarea=$_POST["tarea"];
		$solicitante=$_POST["solicitante"];
		$usuario=$_POST["usuario"];
		$reclamo=$_POST["reclamo"];
		$fecha_prog=$_POST["fecha_prog"];
		$obs_tec=$_POST["obs_tec"];
		$estado_ot=$_POST["estado_ot"];
		$estado_desc=un_dato("select estado from estado_ot where id='$estado_ot'");
		$fecha_prog=$_POST["fecha_prog"];
		$tecnico=$_POST["tecnico"];
		$observaciones=$_POST["observaciones"];
		// Validacion
		$correcto=1;
		$error="";
		if($tarea=="")
		{
			$correcto=0;
			$error.="No se completo la tarea realizada";
		}
		if($correcto)
		{
			//trace("Estoy en correcto");
			mi_query("update mantenimiento set fecha_mod=curdate(),tarea='$tarea',obs_tec='$obs_tec',estado='$estado_ot' where id_mant='$id_mant'","Error al ejecutar la OM");
			if($estado_ot==4)
			{
				//trace("Estoy en estado 4");
				mi_query("update mantenimiento set fin=curdate() where id_mant='$id_mant'","Error al ejecutar la OM");
				mensaje("Orden de Mantenimiento Ejecutada con exito.");
			}else
			{
				mensaje("Orden de Mantenimiento actualizada con exito.");			
			}
			$fecha_mod=a_fecha_arg(un_dato("select curdate()"));
			mi_tabla("i");
			echo("<tr><td>Orden de trabajo: $id_mant</td></tr>");
			echo("<tr><td>Solicitante: $solicitante</td></tr>");
			echo("<tr><td>Fecha programada: $fecha_prog</td></tr>");
			echo("<tr><td>Reclamo: $reclamo</td></tr>");
			echo("<tr><td>Fecha: $fecha_mod</td></tr>");
			echo("<tr><td>Tarea realizada: $tarea</td></tr>");
			echo("<tr><td>Observ. tecnico: $obs_tec</td></tr>");
			echo("<tr><td>Estado: $estado_desc</td></tr>");
			mi_tabla("f");
			
			// Mail para el usuario 
			$admin=un_dato("select usuario from usuarios where perfil=5");
			$asunto="Solicitud de Mantenimiento";
			$texto="En relaci&oacute;n a la solicitud nro. $id_mant solicitada por $solicitante por un reclamo de $reclamo, hemos realizado la siguiente tarea: $tarea, quedando la orden en estado $estado_desc.";
			//trace("El usuario es $usuario");
			mandar_mail($usuario,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
			un_boton("Aceptar","Aceptar","coejecom.php");
			break;
		}else
		{
			mensaje($error);
			$campos="%OCU-panta-detalle";
			$campos.=";%OCU-id_mant-$id_mant";
			$campos.=";%OCU-tarea-$tarea";
			$campos.=";%OCU-obs_tec-$obs_tec";
			$campos.=";%OCU-estado_ot-$estado_ot";
			mi_panta("",$campos,$submit);
		}
		break;
	default:
		mi_titulo("Ejecucion de Ordenes de Mantenimiento");
		$rotulos="OM;FECHA PROG.;RECLAMO;EDIFICIO;SECTOR;SOLICITANTE;TECNICO;OBSERVACIONES;PRIORIDAD";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="EJECUTAR";
		$casos=un_dato("select count(*) from soltrab where estado=2");
		if($casos)
		{
			$vencidas=un_dato("select count(*) from soltrab where estado=2 and fecha_prog<curdate()");
			if($vencidas>=0)
			{
				mi_titulo("Ordenes de Mantenimiento vencidas");
				$sql="select s.id_mant,fecha_prog,s.reclamo,e.nombre as edificio,s.sector,s.solicitante,s.tecnico,s.observaciones,s.prioridad from mantenimiento s,edificio e  where s.estado=2 and e.id=s.edificio and s.fecha_prog<curdate() order by s.id_mant;coejecom.php+id_mant+panta+detalle";
				tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
			}
			$hoy=un_dato("select count(*) from mantenimiento where estado=2 and fecha_prog=curdate()");
			if($hoy>=0)
			{
				mi_titulo("Ordenes programadas para hoy");
				$sql="select s.id_mant,fecha_prog,s.reclamo,e.nombre as edificio,s.sector,s.solicitante,s.tecnico,s.observaciones,s.prioridad from mantenimiento s,edificio e  where s.estado=2 and e.id=s.edificio and s.fecha_prog=curdate() order by s.id_mant;coejecom.php+id_mant+panta+detalle";
				tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
			}
			$futuras=un_dato("select count(*) from soltrab where estado=2 and fecha_prog>curdate()");
			if($vencidas>=0)
			{
				mi_titulo("Ordenes programadas para m&aacute;s adelante");
				$sql="select s.id_mant,fecha_prog,s.reclamo,e.nombre as edificio,s.sector,s.solicitante,s.tecnico,s.observaciones,s.prioridad from mantenimiento s,edificio e  where s.estado=2 and e.id=s.edificio and s.fecha_prog>curdate() order by s.id_mant;coejecom.php+id_mant+panta+detalle";
				tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
			}
		}else
		{
			mensaje("No hay ORDENES DE MANTENIMIENTO programadas");
		}
		$prf=un_dato("select perfil from usuarios where usuario='$uid' and clave= PASSWORD('$pwd')");
		if($prf==1)
		{
			$volver="copanel_mant.php";
		}else
		{
			$volver="cocerrar.php";
		}
		un_boton("Volver","Volver",$volver);
		break;
}
?>
</BODY>
</HTML>
