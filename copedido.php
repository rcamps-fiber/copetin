<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Pedido de compra cartuchos</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("Pedido de Compra");
$volver=home($uid);
$submit="aceptar-Confirmar-$volver";
$panta=$_POST["panta"];
switch($panta)
{
	case "detalle":
		$proveedor=$_POST["proveedor"];
	    //trace("Prov. $proveedor");
		$tipo=$_POST["tipo"];
		if($proveedor=="Elegir" or $tipo=="Elegir")
		{
			mensaje("Faltan datos para hacer el pedido. Elija proveedor y tipo de cartucho");
			delay();
			break;
		}
		$razon=un_dato("select razon from proveedores where codigo='$proveedor'");
		//trace("El proveedor es $proveedor - $razon");
		$cab_qry="insert into pedido_cab set fecha=sysdate(),usuario='$uid',proveedor='$proveedor',tipo='$tipo',iva_prev=0,total_prev=0,iva_conf=0,total_conf=0,estado='TEMPORAL'";
		mi_query($cab_qry,"copedido.php. Imposible grabar cabecera de pedido");
		$pedido=un_dato("select max(numero) from pedido_cab");
		$auto=$_POST["auto"];
		if($auto=="S"){
		    // Busco faltantes y armo el detalle del pedido
		    //$faltan_sql="select s.cartucho from stock s,minimos m where s.cartucho=m.cartucho and s.cantidad<=m.pedir and m.compra-s.cantidad >0";
		    $faltan_sql="select distinct s.cartucho,c.codigo_orig from stock s left join minimos m on s.cartucho=m.cartucho left join cartuchos c on s.cartucho=c.codigo_int left join cart_imp r on s.cartucho=r.cod_cart left join impresoras i on r.cod_imp=i.codigo where s.cantidad<=m.pedir and m.compra-s.cantidad >0 and i.activa and c.tipo='$tipo'";
		    $faltan_qry=mi_query($faltan_sql,"copedido.php.Linea 27. Imposible obtener los faltantes.");
		    $item=0;
		    while($datos=mysql_fetch_array($faltan_qry))
		    {
			    $cartucho=$datos["cartucho"];
			    $item++;
			    $stock=un_dato("select cantidad from stock where cartucho='$cartucho'");
			    $cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int='$cartucho'");
			    $precio=un_dato("select ifnull(min(precio),0) from precios where cod_int='$cartucho' and proveedor='$proveedor'");
			    //trace("El precio del cartucho $cartucho es $precio");
			    $menor_prec=un_dato("select ifnull(min(precio),0) from precios where cod_int='$cartucho' and proveedor in (select codigo from proveedores where id_clase=1)");
			    //trace("El menor precio es $menor_prec");
			    $menor_prov=un_dato("select proveedor from precios where cod_int='$cartucho' and precio=$menor_prec");
			    //trace("El menor prov es $menor_prov");
			    $desc_prv=un_dato("select razon from proveedores where codigo='$menor_prov' and id_clase=1");
			    //trace("La razon del proveedor es $desc_prv");
			    $obs="";
			    if($menor_prec>0 and $menor_prec<>$precio)
				    $obs="Prov. $desc_prv: $menor_prec";
			    $cod_prv=un_dato("select cod_prv from precios where cod_int='$cartucho' and proveedor='$proveedor' and round(precio,2)='$precio'");
			    $cantidad=un_dato("select if(m.compra-s.cantidad<0,0,m.compra-s.cantidad) as cant from stock s,minimos m where s.cartucho='$cartucho' and m.cartucho=s.cartucho");
			    $tipo_cart=un_dato("select upper(tipo) from cartuchos where codigo_int='$cartucho'");
			    $item_sql="insert into pedido_det set numero='$pedido',item='$item',cod_orig='$cod_orig',cod_int='$cartucho',cod_prv='$cod_prv',cantidad='$cantidad',stock='$stock',cant_pendiente='$cantidad',precio_prev='$precio',obs='$obs'";
			    mi_query($item_sql,"copedido.php. Imposible agregar un item al pedido.");
//			    if($tipo==$tipo_cart or $tipo=="TODOS")
//			    {
//				    $item_sql="insert into pedido_det set numero='$pedido',item='$item',cod_orig='$cod_orig',cod_int='$cartucho',cod_prv='$cod_prv',cantidad='$cantidad',cant_pendiente='$cantidad',precio_prev='$precio',obs='$obs'";
//				    mi_query($item_sql,"copedido.php. Imposible agregar un item al pedido.");
//			    }
		    }
		}
	case "seguir":
		if(isset($_POST["senia"]))
		{
			//trace("Estoy en seguir");
			$pedido=$_POST["pedido"];
			$codigo_int=$_POST["codigo_int"];
			$cantidad=$_POST["cantidad"];
			$precio=$_POST["precio"];
			$proveedor=$_POST["proveedor"];
			$razon=un_dato("select razon from proveedores where codigo='$proveedor'");
			$item=un_dato("select max(item)+1 from pedido_det where numero='$pedido'");
			$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int='$codigo_int'");
			$cod_prv=un_dato("select cod_prv from precios where cod_int='$codigo_int' and proveedor='$proveedor'");
			$tipo=$_POST["tipo"];
			$obs=$_POST["obs"];
			//trace("Las observ son $obs");
			$item_sql="insert into pedido_det set numero='$pedido',item='$item',cod_orig='$cod_orig',cod_int='$codigo_int',cod_prv='$cod_prv',cantidad='$cantidad',cant_pendiente='$cantidad',precio_prev='$precio',obs='$obs'";
			mi_query($item_sql,"copedido.php. Imposible agregar un item al pedido.");
		}
		mostrarPedido($pedido);
		break;
	case "confirma":
		require_once('Mail.php');
		require_once("Mail/mime.php");	
		$pedido=$_POST["pedido"];
		$observaciones=$_POST["observaciones"];
		mi_query("update pedido_cab set estado='GENERADO',observaciones='$observaciones' where numero='$pedido'","copedido.php. Linea 64. Imposible actualizar el estado del pedido $pedido");
		mensaje("Pedido de compra $pedido guardado con estado GENERADO");
		$nom_admin=un_dato("select nombre from usuarios where perfil=7");
		$admin=un_dato("select usuario from usuarios where perfil=7");			
		$email=un_dato("select email from usuarios where usuario='$uid'");
		$nombre=un_dato("select nombre from usuarios where usuario='$uid'");
		$email_admin=un_dato("select email from usuarios where usuario='$admin'");
		$cab_qry=mi_query("select c.fecha,p.razon,c.iva_prev,c.total_prev from pedido_cab c,proveedores p where numero='$pedido' and c.proveedor=p.codigo","Error al buscar el pedido");
		$datos=mysql_fetch_array($cab_qry);
		$fecha=a_fecha_arg($datos["fecha"]);
		$proveedor=$datos["razon"];
		$iva_prev=$datos["iva_prev"];
		$total_prev=$datos["total_prev"];
		$mensaje="<html>";
		$mensaje.='<div id="logo"><img src="logo_copetin.jpeg" align="left" border=0></div>';
		$mensaje.='<div id="cuerpo"><h3>Sistema Copetin</h3>';
		$mensaje.="<br>Pedido generado (sin confirmar)";
		$mensaje.="<br>Estimado/a $nom_admin<br>Solicitamos la compra de los siguientes cartuchos:";
		$mensaje.="<br>Pedido $pedido - fecha: $fecha - Prov. $proveedor";
		$mensaje.="<br><table border='1'><tr><td>item</td><td>cod.orig</td><td>cod.int.</td><td>cod.prov.</td><td>marca</td><td>color</td><td>tipo</td><td>cantidad</td><td>precio</td><td>observaciones</td></tr>";
		$version_texto="Estimado/a $nom_admin\nSolicitamos la compra de los siguientes cartuchos:\nPedido $pedido - fecha: $fecha - Prov. $proveedor\n";
		$version_texto.="item - cod.orig -  - cod.int. - cod.prov. - marca - color - tipo ";
		$version_texto.="- cantidad - precio - observaciones";
		$det_qry=mi_query("select p.item,p.cod_orig,p.cod_int,p.cod_prv,c.marca,c.color,c.tipo,p.cantidad,p.precio_prev,p.obs from pedido_det p,cartuchos c where numero='$pedido' and p.cod_int=c.codigo_int","Error al obtener el detalle del pedido $pedido");
		while($datos=mysql_fetch_array($det_qry))
		{
			$item=$datos["item"];
			$cod_orig=$datos["cod_orig"];
			$cod_int=$datos["cod_int"];
			$cod_prv=$datos["cod_prv"];
			$marca=$datos["marca"];
			$color=$datos["color"];
			$tipo=$datos["tipo"];
			$cantidad=$datos["cantidad"];
			$precio_prev=$datos["precio_prev"];
			$obs=$datos["obs"];
			$mensaje.="<tr><td>$item</td><td>$cod_orig</td><td>$cod_int</td><td>$cod_prv</td>";
			$mensaje.="<td>$marca</td><td>$color</td><td>$tipo</td><td>$cantidad</td>";
			$mensaje.="<td>$precio_prev</td><td>$obs</td></tr>";
			$version_texto.="$item - $cod_orig - $cod_int - $cod_prv - $marca - $color - $tipo -";
			$version_texto.="$cantidad - $precio_prev - $obs\n";
		}
		$mensaje.="</table>";
		$mensaje.="<br>Importe bruto: " . number_format($total_prev,2,",",".");
		$mensaje.="<br>Iva: " . number_format($iva_prev,2,",",".");
		$total=$total_prev+$iva_prev;
		$observaciones=un_dato("select observaciones from pedido_cab where numero='$pedido'");
		$mensaje.="<br>Neto: " . number_format($total,2,",",".");
		$mensaje.="<br>Notas: $observaciones";
		$mensaje.="<br><br>Atentamente.";
		$mensaje.="</div></html>";
		
		$version_texto.="\nImporte bruto: " . number_format($total_prev,2,",",".");
		$version_texto.="\nIva: " . number_format($iva_prev,2,",",".");
		$version_texto.="\nNeto: " . number_format($total,2,",",".");
		$version_texto.="\n\nNotas: $observaciones";
		$version_texto.="\n\nAtentamente.";
		//$destino = $email;
		// Ahora tambien le llega a compras el pedido generado (no emitido)
		//$destino = $email . "," . $email_admin;
		$destino = $email;
		$encabezado['From']    = $email;
		$encabezado['To']      = $email;
		$encabezado['Subject'] = 'Generacion de Orden de Compra de Cartuchos inkjet/toner';
		$cuerpo = $mensaje;
		$params['sendmail_path'] = '/usr/lib/sendmail';
		// Crear el objeto correo con el metodo Mail::factory
		$imagen="imagenes/logo_copetin.jpeg";
		$mime= new Mail_mime("\n");
		$mime->setTXTBody($version_texto);
		$mime->setHTMLBody($mensaje);
		$mime->addHTMLImage($imagen," image/jpeg");
		$cuerpo=$mime->get();
		$hdrs=$mime->headers($encabezado);
		$mail=& Mail::factory('sendmail',$params);
		$mail->send($destino,$hdrs,$cuerpo);	
		//mensaje("Se envio mail a $destino");
		//echo($mensaje);
		un_boton("aceptar","Aceptar","copanel.php");
		break;
	case "item":
		$item=$_POST["item"];
		$pedido=$_POST["pedido"];
		$cantidad=un_dato("select cantidad from pedido_det where numero=$pedido and item=$item");
		$precio=un_dato("select precio_prev from pedido_det where numero=$pedido and item=$item");
		$obs=un_dato("select obs from pedido_det where numero='$pedido' and item='$item'");
		$titulo="Modificaci&oacute;n del Item $item del pedido $pedido";
		$cod_int=un_dato("select cod_int from pedido_det where numero='$pedido' and item='$item'");
		$cod_orig=un_dato("select cod_orig from pedido_det where numero='$pedido' and item='$item'");
		$marca=un_dato("select marca from cartuchos where codigo_int='$cod_int'");
		$color=un_dato("select color from cartuchos where codigo_int='$cod_int'");
		$campo="%ROT-Cartucho $marca $cod_orig $color;";
		$campo_item="%OCU-item-$item;";
		$campo_anula="%CHK-anula-anula-0-n;";
		$campo_cantidad="%TXT-cantidad-cantidad-$cantidad-5;";
		$campo_precio="%TXT-precio-precio-$precio-10;";
		$campo_obs="%ARE-observaciones-obs-$obs-4-50;";
		$cod_int=un_dato("select cod_int from pedido_det where item='$item' and numero='$pedido'");
		$campo_pedido="%OCU-pedido-$pedido;";
		$campo_panta="%OCU-panta-graba_item";
		$submit="aceptar-Confirmar-copedido.php+graba_item+pedido+$pedido+cantidad+$cantidad+item+$item+anula+1+precio+$precio+obs+$obs";
		$campos_pantalla=$campo . $campo_item . $campo_anula . $campo_cantidad . $campo_precio . $campo_obs . $campo_pedido . $campo_panta;
		mi_panta($titulo,$campos_pantalla,$submit);
		$sql="select r.codigo,r.razon,p.precio from precios p,proveedores r where p.proveedor=r.codigo and r.id_clase=1 and p.cod_int='$cod_int'";
		$titulos="codigo;razon;precio";
		mi_titulo("Precios alternativos");
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;2");
		break;
	case "graba_item":
		$pedido=$_POST["pedido"];
		$cantidad=$_POST["cantidad"];
		$precio=$_POST["precio"];
		$anula=$_POST["anula"];
		$item=$_POST["item"];
		$obs=$_POST["obs"];
		if($anula<>"0")
		{
			mi_query("update pedido_det set cantidad='$cantidad',cant_pendiente='$cantidad',precio_prev='$precio',obs='$obs' where numero=$pedido and item=$item","copedido.php. Linea 86. Imposible actualizar item $item");
			// Si cambio el precio lo actualizo en la tabla de precios
			$cod_int=un_dato("select cod_int from pedido_det where numero='$pedido' and item='$item'");
			$proveedor=un_dato("select proveedor from pedido_cab where numero='$pedido'");
			mi_query("update precios set precio='$precio' where cod_int='$cod_int' and proveedor='$proveedor' and precio<>'$precio'","Error al actualizar el precio de $cod_int para el proveedor $proveedor");
			mensaje("Se actualiz&oacute; el item $item del pedido $pedido.");
		}else
		{
			mi_query("delete from pedido_det where numero=$pedido and item=$item","copedido.php.Linea 98. Imposible borrar item $item del pedido $pedido");
			mensaje("Se borr&oacute; el item $item del pedido $pedido.");
		}
		mostrarPedido($pedido);
		break;
	default:
		//Borrando temporales
		$cuantos=un_dato("select count(*) from pedido_cab where estado='TEMPORAL'");
		if($cuantos>0)
		{
			$tmp_sql="select numero from pedido_cab where estado='TEMPORAL'";
			$tmp_qry=mi_query($tmp_sql,"copedido.php. Linea 72. Imposible obtener temporales");
			while($datos=mysql_fetch_array($tmp_qry))
			{
				$borrar=$datos["numero"];
				mi_query("delete from pedido_det where numero=$borrar","copedido.php.Linea 76. Imposible borrar item temporal");
			}
			mi_query("delete from pedido_cab where numero=$borrar","copedido.php. Linea 78. Imposible borrar cabecera temporal");
		}		
		$titulo="Cabecera del Pedido";
		$fecha=getdate();
		$que_fecha=$fecha["mday"] . "/" . $fecha["mon"] . "/" . $fecha["year"];
		$campos="%ROT-<tr><td><strong>Fecha: " . $que_fecha . "</strong></td></tr>";
		$campos.=";%ROT-<tr><td><strong>Solicitante:</strong></td><td><strong>$uid</strong></td></tr>;";
		$campos.=";%SEL-proveedor-proveedor-select codigo,razon from proveedores where id_clase=1 order by 1-razon+codigo";
		$campos.=";%OCU-panta-detalle";
		$campos.=";%SEL-tipo-tipo cartucho-Elegir+Elegir+INKJET+INKJET+TONER+TONER-0";
		$campos.=";%CHK-autocompletar-auto-S-s";
		$submit="aceptar-Confirmar-copanel.php";
		mi_panta($titulo,$campos,$submit);
	break;
}
cierre();
?>

