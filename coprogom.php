<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Programacion de Ordenes de Mantenimiento</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Confirmar-coprogom.php";
$panta=$_POST["panta"];
switch($panta)
{
	case "detalle":
		$id_mant=$_POST["id_mant"];
		$fecha_prog=$_POST["fecha_prog"];
		$tecnico=$_POST["tecnico"];
		$prioridad=$_POST["prioridad"];
		$observaciones=$_POST["observaciones"];
		$msg_usu=$_POST["msg_usu"];
		$sql="select s.fecha_solicitud,s.hora_solicitud,s.solicitante,e.nombre as edificio,s.sector,s.usuario,s.puesto,p.descripcion as desc_puesto,s.reclamo ";
		$sql.="from mantenimiento s,puestos p,edificio e ";
		$sql.="where s.id_mant='$id_mant' and s.edificio=e.id and s.puesto=p.codigo";
		$cns=mi_query($sql,"Error al obtener la om");
		$datos=mysql_fetch_array($cns);
		$titulo="PROGRAMACION DE LA ORDEN DE MANTENIMIENTO Nro. $id_mant";
		$fecha_solicitud=$datos["fecha_solicitud"];
		//trace("La fecha es $fecha_solicitud");
		if(!isset($fecha_prog))
		{
			$hoy=un_dato("select curdate()");
			$fecha_prog=a_fecha_arg($hoy);
		}else
		{
			$fecha_prog=a_fecha_arg($datos["fecha_prog"]);
		}
		$fecha_solicitud=a_fecha_arg($fecha_solicitud);
		if(!isset($prioridad))
			$prioridad="NORMAL";
		if(!isset($tecnico))
			$tecnico="Elegir";
		$usuario=$datos["usuario"];
		$puesto=$datos["puesto"];
		$desc_puesto=$datos["desc_puesto"];
		$reclamo=$datos["reclamo"];
		$hora_solicitud=$datos["hora_solicitud"];
		$solicitante=$datos["solicitante"];
		$edificio=$datos["edificio"];
		$sector=$datos["sector"];
		$campos="%ROT-FECHA SOL.</td><td>$fecha_solicitud";
		$campos.=";%ROT-HORA SOL.</td><td>$hora_solicitud";
		$campos.=";%ROT-SOLICITANTE</td><td>$solicitante de $sector";
		$campos.=";%ROT-EDIFICIO</td><td>$edificio";
		$campos.=";%ROT-USUARIO</td><td>$usuario de $desc_puesto";
		$campos.=";%ROT-RECLAMO</td><td>$reclamo";
		$campos.=";%TXT-fecha prog.-fecha_prog-$fecha_prog-10";
		$campos.=";%ARE-mensaje al usuario-msg_usu-$msg_usu-4-70";
		$campos.=";%TXT-observaciones-observaciones-$observaciones-50";
		$campos.=";%SEL-prioridad-prioridad-$prioridad+$prioridad+ALTA+ALTA+BAJA+BAJA+NORMAL+NORMAL-0";
		$campos.=";%SEL-tecnico-tecnico-select usuario,usuario from usuarios where perfil in(5,6)-usuario+usuario-$tecnico-$tecnico";
		$campos.=";%CHK-anular-anular-S-n";
		$campos.=";%OCU-id_mant-$id_mant";
		$campos.=";%OCU-panta-grabar";
		$campos.=";%OCU-fecha_solicitud-$fecha_solicitud";
		$campos.=";%OCU-hora_solicitud-$hora_solicitud";
		$campos.=";%OCU-usuario-$usuario";
		$campos.=";%OCU-desc_puesto-$desc_puesto";
		$campos.=";%OCU-reclamo-$reclamo";
		$campos.=";%OCU-solicitante-$solicitante";
		$campos.=";%OCU-sector-$sector";
		$campos.=";%OCU-edificio-$edificio";
		mi_panta($titulo,$campos,$submit);
		break;
	case "grabar":
		mi_titulo("Programacion de ordenes de mantenimiento");
		$id_mant=$_POST["id_mant"];
		$fecha_prog=a_fecha_sistema($_POST["fecha_prog"]);
		$tecnico=$_POST["tecnico"];
		$prioridad=$_POST["prioridad"];
		$observaciones=$_POST["observaciones"];
		$fecha_solicitud=$_POST["fecha_solicitud"];
		$usuario=$_POST["usuario"];
		$desc_puesto=$_POST["desc_puesto"];
		$reclamo=$_POST["reclamo"];
		$anular=$_POST["anular"];
		$solicitante=$_POST["solicitante"];
		$sector=$_POST["sector"];
		$edificio=$_POST["edificio"];
		$msg_usu=$_POST["msg_usu"];
		
		if($anular=="S")
		{
			mi_query("update mantenimiento set estado=5 where id_mant='$id_mant'","Error al anular la ot");
			mensaje("Solicitud anulada");
			// Mail  de anulacion para el usuario
			$admin=un_dato("select usuario from usuarios where perfil=5");
			$asunto="Anulacion de solicitud de Mantenimiento";
			$texto="En relaci&oacute;n a la solicitud nro. $id_mant solicitada por $solicitante por el reclamo: $reclamo, le informamos que la misma ha sido anulada.";
			//trace("El usuario es $usuario");
			mandar_mail($usuario,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
			un_boton("Aceptar","Aceptar","coprogom.php");
			break;
		}
		// Validacion
		$correcto=1;
		$error="";
		if(!fecha_ok($fecha_prog))
		{
			$correcto=0;
			$error.="Fecha programada invalida";
		}
		$hoy=un_dato("select curdate()");
		if($fecha_prog<$hoy)
		{
			$correcto=0;
			$error.="\nFecha programada ya vencida.";
		}
		if($tecnico=="Elegir")
		{
			$correcto=0;
			$error.="\nFalta especificar el tecnico.";
		}	
		if($correcto)
		{
			mi_query("update mantenimiento set fecha_prog='$fecha_prog',observaciones='$observaciones',tecnico='$tecnico',prioridad='$prioridad',msg_usu='$msg_usu',estado=2 where id_mant='$id_mant'","Error al programar la OM");
			mensaje("Orden de Mantenimiento programada con exito.");
			$fecha_prog=a_fecha_arg($fecha_prog);
			mi_tabla("i");
			echo("<tr><td>Orden de mantenimiento: $id_mant</td></tr>");
			echo("<tr><td>Usuario: $usuario</td></tr>");
			echo("<tr><td>Fecha solicitud: $fecha_solicitud</td></tr>");
			echo("<tr><td>Solicitante: $solicitante</td></tr>");
			echo("<tr><td>Sector: $sector</td></tr>");
			echo("<tr><td>Edificio: $edificio</td></tr>");
			echo("<tr><td>Reclamo: $reclamo</td></tr>");
			echo("<tr><td>Fecha programada: $fecha_prog</td></tr>");
			echo("<tr><td>Prioridad: $prioridad</td></tr>");
			echo("<tr><td>Tecnico asignado: $tecnico</td></tr>");
			echo("<tr><td>Observaciones: $observaciones</td></tr>");
			echo("<tr><td>Mensaje al usuario: $msg_usu</td></tr>");
			mi_tabla("f");
			// Mail para el usuario
			$admin=un_dato("select usuario from usuarios where perfil=5");
			$asunto="Solicitud de Mantenimiento";
			$texto="En relaci&oacute;n a la solicitud nro. $id_mant solicitada por $solicitante por el reclamo: $reclamo, hemos programado la tarea para el dia $fecha_prog.<br>$msg_usu";
			//trace("El usuario es $usuario");
			mandar_mail($usuario,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
				
			// Mail para el tecnico
			$admin=un_dato("select usuario from usuarios where perfil=5");
			$asunto="Asignacion de tecnico a Solicitud de Mantenimiento";
			$texto="T&eacute;cnico asignado: $tecnico<br>";
			$texto.="Fecha de Ejecuci&oacute;n: <strong>$fecha_prog</strong><br>";
			$texto.="Prioridad: $prioridad<br>";
			$texto.="Solicitante: $solicitante<br>";
			$texto.="Sector: $sector<br>";
			$texto.="Edificio: $edificio<br>";
			$texto.="Usuario: $usuario de $desc_puesto<br>";
			$texto.="Reclamo: $reclamo<br>";
			$texto.="Observaciones: $observaciones<br>";
			$texto.="</div></html>";
			mandar_mail($tecnico,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
			un_boton("Aceptar","Aceptar","coprogom.php");
			break;
		}else
		{
			mensaje($error);
			$campos="%OCU-panta-detalle";
			$campos.=";%OCU-id_mant-$id_mant";
			$campos.=";%OCU-fecha_prog-$fecha_prog";
			$campos.=";%OCU-tecnico-$tecnico";
			$campos.=";%OCU-prioridad-$prioridad";
			$campos.=";%OCU-observaciones-$observaciones";
			mi_panta("",$campos,$submit);
		}
		break;
	default:
		mi_titulo("Programacion de Ordenes de Mantenimiento");
		$rotulos="OT;FECHA SOLICITUD;HORA;SOLICITANTE;EDIFICIO;SECTOR;USUARIO;PUESTO;DESCRIPCION RECLAMO";
		
		$sql="select s.id_mant,s.fecha_solicitud,s.hora_solicitud,s.solicitante,e.nombre as edificio,s.sector,s.usuario,p.descripcion as desc_puesto,s.reclamo ";
		$sql.="from mantenimiento s,puestos p,edificio e ";
		$sql.="where s.edificio=e.id and s.puesto=p.codigo and estado=1 ";
		$sql.="order by s.id_mant;coprogom.php+id_mant+panta+detalle";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="PROGRAMAR";
		$casos=un_dato("select count(*) from mantenimiento where estado=1");
		if($casos)
		{
			tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		}else
		{
			mensaje("No hay ORDENES DE MANTENIMIENTO solicitadas pendientes de programaci&oacute;n.");
		}
		un_boton("Volver","Volver","copanel_mant.php");
		break;
}
?>
</BODY>
</HTML>
