<?
include_once('coacceso.php');
include_once('cofunciones_especificas.php');
include_once("cofunciones.php");

apertura("Modulo de Insumos");
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Confirmar-coinsumos.php";
if(isset($_GET["tipo"]))
{
	$tipo=$_GET["tipo"];
}else
{
	$tipo=$_POST["tipo"];
}
$panta=$_POST["panta"];
switch($panta)
{
	case "detalle":
		$id_oc=$_POST["id_oc"];
		$tipo=$_POST["tipo"];
		$proveedor=un_dato("select proveedor from orden_cab where id_oc='$id_oc'");
		mi_titulo("Recepci&oacute;n de Orden de Compra de Insumos Nro. $id_oc");
		$razon=un_dato("select razon from proveedores where codigo='$proveedor'");
		mi_titulo("Proveedor: $razon");
		$tit_panta="Agregar un item";
		if(isset($tipo))
			$and=" and tipo='$tipo'";
		$campos="%SEL-id_insumo-insumo-select id_insumo,articulo from insumos where id_insumo not in (select insumo from orden_det where id_oc='$id_oc') $and-articulo+id_insumo";
		$campos.=";%OCU-panta-nuevo_item";
		$campos.=";%OCU-id_oc-$id_oc";
		$campos.=";%OCU-proveedor-$proveedor";
		$submit2="aceptar-Agregar item-coinsumos.php";
		mi_panta($tit_panta,$campos,$submit2);
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales="0;0;0;0;2;2;2;0";
		$tit_lnk="ACCION";
		$btn_lnk="MODIFICAR";
		$sql="select renglon,insumo,descripcion,cod_prv,cantidad,precio,total,observaciones from orden_det where id_oc='$id_oc' and estado=0 order by renglon;corecep_ins.php+renglon+id_oc+$id_oc+panta+renglon+tipo+$tipo";
		$rotulos="reng;item;descripcion;cod.prov.;cant.;p.unit;total;observaciones";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		$subtotal=un_dato("select sum(total) from orden_det where id_oc='$id_oc' and estado=0");
		$iva_21=un_dato("select sum(total*.21) from orden_det where id_oc='$id_oc' and estado=0 and iva=21");
		$iva_105=un_dato("select sum(total*.105) from orden_det where id_oc='$id_oc' and estado=0 and iva=10.5");
		$total=$subtotal+$iva_21+$iva_105;
		mi_query("update orden_cab set subtotal='$subtotal',iva_21='$iva_21',iva_105='$iva_105',total='$total' where id_oc='$id'");
		$subtot_prt=number_format($subtotal,2,",",".");
		$total_prt=number_format($total,2,",",".");
		$iva_105_prt=number_format($iva_105,2,",",".");
		$iva_21_prt=number_format($iva_21,2,",",".");
		mi_tabla("i");
		mi_fila("d","Sub total: $;$subtot_prt");
		mi_fila("d","IVA 21%: $;$iva_21_prt");
		mi_fila("d","IVA 10.5%: $;$iva_105_prt");
		mi_fila("d","Total: $;$total_prt");
		mi_tabla("f");
		$tit_panta="";
		$campos="%OCU-id_oc-$id_oc";
		$campos.=";%CHK-anular-anular-S-n";
		$campos.=";%OCU-panta-confirmar";
		mi_panta($tit_panta,$campos,$submit);
		break;
	case "confirmar":
		$id_oc=$_POST["id_oc"];
		$estado=un_dato("select estado from orden_cab where id_oc='$id_oc'");
		if($estado<>0)
		{
			mensaje("La Orden de Compra $id_oc no esta pendiente de recepci&oacute;n");
			delay();
			break;
		}
		$anular=$_POST["anular"];
		if($anular=="S")
		{
			mi_query("update orden_cab set estado=3 where id_oc='$id_oc'","Error al anular la orden $id_oc");
			mensaje("Orden nro. $id_oc anulada.");
			delay();
			break;
		}
		// Aca es donde le pongo el estado 0 a orden_cab
		// Borro ordenes incompletas que pudieran haber quedado (no deberia haber)
		orden_inc();
		// Aca tengo que dibujar la orden en un formato que se pueda imprimir
		// Ademas tengo que mandarla por mail al administrador de compras
		
		$proveedor=un_dato("select proveedor from orden_cab where id_oc='$id_oc'");
		mi_titulo("Finalizacion de la Orden de Compra de Insumos Nro. $id_oc");
		$razon=un_dato("select razon from proveedores where codigo='$proveedor'");
		mi_titulo("Proveedor: $razon");
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales="0;0;0;0;2;2;2;0";
		$sql="select renglon,insumo,descripcion,cod_prv,cantidad,precio,total,observaciones from orden_det where id_oc='$id_oc' and estado=0 order by renglon";
		$rotulos="reng;item;descripcion;cod.prov.;cant.;p.unit;total;observaciones";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		$subtotal=un_dato("select sum(total) from orden_det where id_oc='$id_oc' and estado=0");
		$iva_21=un_dato("select sum(total*.21) from orden_det where id_oc='$id_oc' and estado=0 and iva=21");
		$iva_105=un_dato("select sum(total*.105) from orden_det where id_oc='$id_oc' and estado=0 and iva=10.5");
		$total=$subtotal+$iva_21+$iva_105;
		$subtot_prt=number_format($subtotal,2,",",".");
		$total_prt=number_format($total,2,",",".");
		$iva_105_prt=number_format($iva_105,2,",",".");
		$iva_21_prt=number_format($iva_21,2,",",".");
		mi_tabla("i");
		mi_fila("d","Sub total: $;$subtot_prt");
		mi_fila("d","IVA 21%: $;$iva_21_prt");
		mi_fila("d","IVA 10.5%: $;$iva_105_prt");
		mi_fila("d","Total: $;$total_prt");
		mi_tabla("f");
		imprimir();
		$admin=un_dato("select usuario from usuarios where perfil=1");
		$asunto="Orden de Compra de Insumos Nro. $id_oc";
		$texto="<h3>Orden de Compra de Insumos Nro. $id_oc</h3>";
		$texto.="<p><strong>PROVEEDOR: $razon</strong></p>";
		$texto.="<br><hr><br><table border=1 align=left><tr>";
		$texto.="<td><strong><center>RENG.</td><td><strong><center>ITEM</td>";
		$texto.="<td><strong><center>DESCRIPCION</td><td><strong><center>COD.PROV.</td>";
		$texto.="<td><strong><center>CANT.</td><td><strong><center>P.UNIT.</td>";
		$texto.="<td><strong><center>TOTAL</td><td><strong><center>OBSERVACIONES</td></tr><tr>";
		$qry=mi_query($sql,"Error al obtener los items de la oc $id_oc");
		while($datos=mysql_fetch_array($qry))
		{
			$renglon=$datos["renglon"];
			$insumo=$datos["insumo"];
			$descripcion=$datos["descripcion"];
			$cod_prv=$datos["cod_prv"];
			$cantidad=$datos["cantidad"];
			$precio=$datos["precio"];
			$total=$datos["total"];
			$observaciones=$datos["observaciones"];
			$texto.="<td>$renglon</td><td>$insumo</td><td>$descripcion</td>";
			$texto.="<td>$cod_prv</td><td>$cantidad</td><td>$precio</td>";
			$texto.="<td>$total</td><td>$observaciones</td></tr>";
			mi_query("update insumos set stock=stock+'$cantidad' where id_insumo='$insumo'","Error al actualizar el stock para el insumo $insumo");
		}
		mi_query("update orden_cab set estado=1 where id_oc='$id_oc'","Error al actualizar estado de la orden $id_cab");
		//tablita con los totales
		$texto.="</table><br><hr><br><table border=1 align=right><tr>";
		$texto.="<td align=left><strong>Sub Total: $</td><td>$subtot_prg</td>";
		$texto.="<td align=left><strong>IVA 21%: $</td><td>$iva_21_prt</td>";
		$texto.="<td align=left><strong>IVA 10.5%: $</td><td>$iva_105_prt</td>";
		$texto.="<td align=left><strong>TOTAL: $</td><td>$total_prt</td>";
		$texto.="</tr></table>";
		mandar_mail($admin,$uid,$asunto,$texto,$uid,"logo_copetin.jpeg",1);
		un_boton();
		break;
	case "renglon":
		$id_oc=$_POST["id_oc"];
		$proveedor=un_dato("select proveedor from orden_cab where id_oc='$id_oc'");
		$razon=un_dato("select razon from proveedores where codigo='$proveedor'");
		$renglon=$_POST["renglon"];
		$tipo=$_POST["tipo"];
		$rng=mi_query("select * from orden_det where id_oc='$id_oc' and renglon='$renglon' and estado=0");
		$datos=mysql_fetch_array($rng);
		$insumo=$datos["insumo"];
		$descripcion=$datos["descripcion"];
		$cod_prv=$datos["cod_prv"];
		$cantidad=$datos["cantidad"];
		$precio=$datos["precio"];
		$iva=$datos["iva"];
		$total=$datos["total"];
		$observaciones=$datos["observaciones"];
		$campos.=";%ROT-ITEM: ##$insumo";
		$campos.=";%ROT-Descripcion: ##$descripcion";
		$campos.=";%TXT-cantidad-cantidad-$cantidad-5";
		$campos.=";%TXT-precio-precio-$precio-5";
		$campos.=";%TXT-observaciones-observaciones-$observaciones-50";
		$campos.=";%TXT-iva-iva-$iva-5";
		$campos.=";%CHK-Eliminar-anular-S-n";
		$campos.=";%OCU-panta-graba_reng";
		$campos.=";%OCU-id_oc-$id_oc";
		$campos.=";%OCU-renglon-$renglon";
		$campos.=";%OCU-tipo-$tipo";
		$tit_panta="Orden de Compra Nro. $id_oc Prov.: $razon";
		mi_panta($tit_panta,$campos,$submit);
		break;
	case "graba_reng":
		$id_oc=$_POST["id_oc"];
		$renglon=$_POST["renglon"];
		$cantidad=$_POST["cantidad"];
		$precio=$_POST["precio"];
		$iva=$_POST["iva"];
		$observaciones=$_POST["observaciones"];
		$anular=$_POST["anular"];
		$tipo=$_POST["tipo"];
		$total=$cantidad*$precio;
		$estado=($anular=="S") ? 1 : 0;
		mi_query("update orden_det set cantidad='$cantidad',precio='$precio',iva='$iva',total='$total',observaciones='$observaciones',estado='$estado' where id_oc='$id_oc' and renglon='$renglon'","Error al actualizar el item $renglon de la orden $id_oc");
		mensaje("Se modific&oacute; el rengl&oacute;n $renglon de la orden $id_oc");
		$campos.=";%OCU-id_oc-$id_oc";
		$campos.=";%OCU-tipo-$tipo";
		$campos.=";%OCU-panta-detalle";
		$tit_panta="";
		mi_panta($tit_panta,$campos,$submit);
		break;
	case "nuevo_item":
		$id_oc=$_POST["id_oc"];
		$id_insumo=$_POST["id_insumo"];
		//trace("Estoy en nuevo item con id_oc $id_oc y insumo $id_insumo");
		$sql="select * from insumos where id_insumo='$id_insumo'";
		$qry_sql=mi_query($sql,"Error al obtener los datos del insumo");
		$renglon=un_dato("select max(renglon) from orden_det where id_oc='$id_oc'");
		$renglon++;
		$datos=mysql_fetch_array($qry_sql);
		$descripcion=$datos["articulo"];
		$cod_prv=un_dato("select cod_prv from precios where cod_int='$insumo' and proveedor='$proveedor'");
		$lote_compra=$datos["lote_compra"];
		$stock=$datos["stock"];
		$cantidad=$lote_compra-$stock;
		$precio=un_dato("select precio from precios where cod_int='$insumo' and proveedor='$proveedor'");
		$iva=un_dato("select iva from precios where cod_int='$insumo' and proveedor='$proveedor'");
		$total=$cantidad*$precio;
		$observaciones="";
		$estado=0;
		mi_query("insert into orden_det set id_oc='$id_oc',renglon='$renglon',insumo='$id_insumo',descripcion='$descripcion',cod_prv='$cod_prv',cantidad='$cantidad',precio='$precio',iva='$iva',total='$total',observaciones='$observaciones',estado='$estado'");
		$campos.=";%OCU-id_oc-$id_oc";
		$campos.=";%OCU-panta-detalle";
		$campos.=";%OCU-tipo-$tipo";
		$tit_panta="Se agrego el item $renglon $descripcion al pedido $id_oc";
		mi_panta($tit_panta,$campos,$submit);
		break;
	default:
		mi_titulo("Ordenes de Compra pendientes");
		$titulos="numero;fecha;proveedor;total";
		$sql=" select o.id_oc,o.fecha_conf,p.razon,o.total from orden_cab o,proveedores p where o.estado=0 and o.proveedor=p.codigo order by fecha_conf;corecep_ins.php+id_oc+panta+detalle";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;2");
		un_boton("Aceptar","Acpetar","coinsumos.php");
}
cierre();
?>