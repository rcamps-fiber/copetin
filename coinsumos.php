<?
// Mostrar en el panel los faltantes de stock para hacer el pedido de compra.
// Ver diagrama de la orden de compra en block borrador.

/*
Estados de la solicitud:
1 = pendiente
2 = sin stock
3 = entregado
4 =
5= Anulado
*/
include_once('coacceso.php');
include_once('cofunciones_especificas.php');
include_once("cofunciones.php");

apertura("Modulo de Insumos");
require_once("cobody.php");
require_once("cocnx.php");
actu_item();
$submit="aceptar-Aceptar-coinsumos.php";
mi_titulo("Modulo de Insumos");
// Actualizacion de stocks nulos
mi_query("update insumos set stock=0 where stock is null","Error al actualizar los stocks nulos");

// Actualizacion de solicitudes que no tienen stock
mi_query("update solins s,insumos i set s.estado=2 where s.estado=1 and s.cod_ins=i.id_insumo and i.stock=0");

// Actualizacion de solicitudes que ya tienen stock
mi_query("update solins s,insumos i set s.estado=1 where s.cod_ins=i.id_insumo and s.estado=2 and i.stock>0");

// SOLICITUDES PENDIENTES
$solinpend_sql="select count(*) from solins where estado=1 and cod_ins<>0";
$hay_solin_pen=un_dato($solinpend_sql);
if($hay_solin_pen>0)
{

	if(ver_item("solins"))
	{
		$mensaje="Hay $hay_solin_pen solicitud/es de insumo/s pendiente/s";
		linea_menu("solins",$mensaje,0,"coinsumos.php");
	}else
	{
		$mensaje="Ocultar solicitudes de insumos pendientes";
		linea_menu("solins",$mensaje,1,"coinsumos.php");
		//filtrar por tipo
		$filtro_tipo="";
		$desc_filtro_tipo="";
		if(isset($_POST["tipo"]))
		{
			$tipo=$_POST["tipo"];
			if($tipo<>"Elegir")
			{
				$filtro_tipo=" and i.tipo='$tipo'";
				//$desc_filtro_tipo.=" El tipo es $tipo.";
				mi_titulo("Filtrado por tipo $tipo");
			}
		}
		$campos="%SEL-tipo-tipo-select distinct tipo from insumos order by 1-tipo-Elegir-Elegir";
		mi_panta("Filtro de busqueda",$campos,$submit);
		$titulos="numero;fecha;usuario;puesto;articulo;descripcion;cantidad;observaciones;urgente";
		$sql="select s.id_sol,s.fecha_sol,s.usuario,p.ubicacion,s.cod_ins,s.articulo,s.cantidad,s.observaciones,if(urgente,'SI','NO') as urgente from solins s,puestos p,insumos i where s.estado=1";
		$sql.=" and s.puesto=p.codigo and s.cod_ins=i.id_insumo $filtro_tipo order by s.urgente desc,s.fecha_sol desc;corespuesta_ins.php+id_sol";
		//trace($sql);
		tabla_cons($titulos,$sql,1,"silver","#8EC99F",0,"ACCION","PROCESAR","","solicitudes pendientes;solicitudes;pendientes");
	}	
}

// SOLICITUDES SIN STOCK
$solstk_sql="select count(*) from solins where estado=2 and cod_ins<>0";
$hay_sol_stk=un_dato($solstk_sql);
if($hay_sol_stk>0)
{

	if(ver_item("solin_stk"))
	{
		$mensaje="Hay $hay_sol_stk solicitud/es de insumo/s con faltante de stock";
		linea_menu("solin_stk",$mensaje,0,"coinsumos.php");
	}else
	{
		$mensaje="Ocultar solicitudes de insumos con faltante de stock";
		linea_menu("solin_stk",$mensaje,1,"coinsumos.php");
		//filtrar por tipo
		$filtro_tipo="";
		$desc_filtro_tipo="";
		if(isset($_POST["tipo"]))
		{
			$tipo=$_POST["tipo"];
			if($tipo<>"Elegir")
			{
				$filtro_tipo=" and i.tipo='$tipo'";
				mi_titulo("Filtrado por tipo $tipo.");
			}
		}
		$campos="%SEL-tipo-tipo-select distinct tipo from insumos order by 1-tipo-Elegir-Elegir";
		mi_panta("Filtro de busqueda",$campos,$submit);
		$titulos="numero;fecha;usuario;puesto;articulo;descripcion;cantidad;observaciones;urgente";
		$sql="select s.id_sol,s.fecha_sol,s.usuario,p.ubicacion,s.cod_ins,s.articulo,s.cantidad,s.observaciones,if(urgente,'SI','NO') as urgente from solins s,puestos p,insumos i where s.estado=2 and s.cod_ins<>0";
		$sql.=" and s.puesto=p.codigo  and s.cod_ins=i.id_insumo $filtro_tipo order by s.urgente desc,s.fecha_sol desc;corespuesta_ins.php+id_sol";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F",0,"ACCION","PROCESAR","","solicitudes sin stock;solicitudes;sin_stock");
	}	
}

// SOLICITUDES NO CLASIFICADOS
//$solnc_sql="select count(*) from solins where cod_ins=0 and estado<>3 and estado<>5";
$solnc_sql="select count(*) from solins where cod_ins=0 and estado=1";
$hay_sol_nc=un_dato($solnc_sql);
if($hay_sol_nc>0)
{

	if(ver_item("solin_nc"))
	{
		$mensaje="Hay $hay_sol_nc solicitud/es de insumo/s no clasificado/s";
		linea_menu("solin_nc",$mensaje,0,"coinsumos.php");
	}else
	{
		$mensaje="Ocultar solicitudes de insumos no clasificados";
		linea_menu("solin_nc",$mensaje,1,"coinsumos.php");
		$titulos="numero;fecha;usuario;puesto;descripcion;cantidad;observaciones;urgente";
		$sql="select s.id_sol,s.fecha_sol,s.usuario,p.ubicacion,s.articulo,s.cantidad,s.observaciones,if(urgente,'SI','NO') as urgente from solins s,puestos p where s.estado=1 and s.cod_ins=0";
		$sql.=" and s.puesto=p.codigo order by s.urgente desc,s.fecha_sol desc;corespuesta_ins.php+id_sol";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F",0,"ACCION","PROCESAR","","solicitudes sin clasificar;solicitudes;sin_clasificar");
	}	
}

// Faltantes de stock
$falstk_sql="select count(*) from insumos where stock<=punto_pedido";
$hay_falstk=un_dato($falstk_sql);
if($hay_falstk)
{
	if(ver_item("falins"))
	{
		$mensaje="Hacer pedido de insumos faltantes (faltan $hay_falstk insumos)";
		linea_menu("falins",$mensaje,0,"coinsumos.php");
	}else
	{
		$mensaje="Ocultar insumos faltantes";
		linea_menu("falins",$mensaje,1,"coinsumos.php");
		$tipos_sql="select distinct tipo from insumos order by 1";
		$rtipos=mi_query($tipos_sql,"Error al buscar los tipos de insumo");
		while($datos=mysql_fetch_array($rtipos))
		{
			$tipo=$datos["tipo"];
			$sql="select id_insumo,articulo,unidad,stock,lote_compra from insumos where tipo='$tipo' and stock<=punto_pedido";
			mi_titulo("Tipo: $tipo");
			$titulos="id;articulo;unidad;stock;comprar";
			tabla_cons($titulos,$sql,1,"silver","#8EC99F",0);
			un_boton("aceptar",'Hacer pedido',"copedido_ins.php?tipo=$tipo");
		}
	}
}


// PEDIDOS DE COMPRA POR RECIBIR
$pedrcb_sql="select count(*) from orden_cab where estado=0";
$hay_ped_rcb=un_dato($pedrcb_sql);
if($hay_ped_rcb>0)
{
	if(ver_item("pedin_rcp"))
	{
		$mensaje="Hay $hay_ped_rcb Pedido/s de Compra por recibir";
		linea_menu("pedin_rcp",$mensaje,0,"coinsumos.php");
	}else
	{
		$mensaje="Ocultar Pedidos de Compra por recibir";
		linea_menu("pedin_rcp",$mensaje,1,"coinsumos.php");
		$titulos="numero;fecha;proveedor;total";
		$sql=" select o.id_oc,o.fecha_conf,p.razon,o.total from orden_cab o,proveedores p where o.estado=0 and o.proveedor=p.codigo order by fecha_conf;corecep_ins.php+id_oc+panta+detalle";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;2");
	}	
}


// PEDIDOS DE COMPRA DE TINTA Y TONER POR CONFIRMAR
$pedcnf_sql="select count(*) from pedido_cab where estado='GENERADO'";
$hay_ped_cnf=un_dato($pedcnf_sql);
if($hay_ped_cnf>0)
{
	raya();

	if(ver_item("ped_cnf"))
	{
		$mensaje="Hay $hay_ped_cnf Pedido/s de Compra de Tinta y Toner por Confirmar";
		linea_menu("ped_cnf",$mensaje,0,"coinsumos.php");
	}else
	{
		$mensaje="Ocultar Pedidos de Compra de Tinta y Toner por Confirmar";
		linea_menu("ped_cnf",$mensaje,1,"coinsumos.php");
		$titulos="numero;fecha;usuario;proveedor;neto";
		$sql="select p.numero,p.fecha,u.nombre,r.razon,p.total_prev+p.iva_prev from pedido_cab p,proveedores r,usuarios u";
		$sql.=" where p.usuario=u.usuario and p.proveedor=r.codigo and estado='GENERADO' order by numero;coconficmp.php+numero";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;2");
	}	
}

raya();

// Solicitudes finalizadas
if(ver_item("solfin"))
{
	$mensaje="Solicitudes finalizadas";
	linea_menu("solfin",$mensaje,0,"coinsumos.php");
}else
{
	$mensaje="Ocultar solicitudes finalizadas";
	linea_menu("solfin",$mensaje,1,"coinsumos.php");
	$filtro="";
	$desc_filtro="";
	if(isset($_POST["usuario"]))
	{
		$usuario=$_POST["usuario"];
		if($usuario<>"Elegir")
		{
			$filtro=" and s.usuario='$usuario'";
			$desc_filtro.=" El usuario es $usuario.";
		}
	}
	if(isset($_POST["urgente"]))
	{
		$urgente=$_POST["urgente"];
		if($urgente<>"Elegir")
		{
			$filtro.=" and s.urgente='$urgente'";
			$desc_filtro.=($urgente) ? " Es urgente." : " No es urgente.";
		}
	}
	if(isset($_POST["cod_ins"]))
	{
		$cod_ins=$_POST["cod_ins"];
		if($cod_ins<>"Elegir")
		{
			$filtro.=" and s.cod_ins='$cod_ins'";
			$articulo=un_dato("select articulo from insumos where id_insumo='$cod_ins'");
			$desc_filtro.=" El art. es $cod_ins $articulo.";
		}
	}
	if(isset($_POST["desde"]))
	{
		$desde=a_fecha_sistema($_POST["desde"]);
		if($desde<>"1900-01-01")
		{
			$filtro.=" and s.fecha_sol>='$desde'";
			$desc_filtro.=" Desde el $desde.";
		}
	}
	if(isset($_POST["hasta"]))
	{
		$hasta=a_fecha_sistema($_POST["hasta"]);
		if($hasta<>"1900-01-01")
		{
			$filtro.=" and s.fecha_sol>='$hasta'";
			$desc_filtro.=" Hasta el $hasta.";
		}
	}
	
	$campos="%SEL-usuario-usuario-select distinct usuario from solins order by 1-usuario-Elegir-Elegir";
	$campos.=";%SEL-urgente-urgente-Elegir+Elegir+1+SI+0+NO-0";
	$campos.=";%SEL-cod_ins-articulo-select distinct cod_ins,concat(cod_ins,' ',articulo) as articulo from solins order by 2-articulo+cod_ins-Elegir-Elegir";
	$campos.=";%FEC-desde-desde-01/01/1900-10";
	$campos.=";%FEC-hasta-hasta-01/01/1900-10";
	mi_panta("Filtro de busqueda",$campos,$submit);
	$titulos="numero;fecha;usuario;puesto;articulo;descripcion;cantidad;observaciones;urgente";
	$sql="select s.id_sol,s.fecha_sol,s.usuario,p.ubicacion,s.cod_ins,s.articulo,s.cantidad,s.observaciones,if(urgente,'SI','NO') as urgente from solins s,puestos p where s.estado=3 and s.puesto=p.codigo $filtro order by s.fecha_sol desc";
	if($desc_filtro<>"")
	mensaje("Criterio de filtrado: $desc_filtro");
	tabla_cons($titulos,$sql,1,"silver","#8EC99F",0,"","");
}	

raya();
// Otras funciones 
if(ver_item("otras_ins"))
{
	$mensaje="M&aacute;s funciones de administraci&#243;n de insumos";
	linea_menu("otras_ins",$mensaje,0,"coinsumos.php");
}else
{
	$mensaje="Ocultar otras funciones de administraci&#243;n de insumos";
	linea_menu("otras_ins",$mensaje,1,"coinsumos.php");
	echo("<ul>");
	//echo("<ul><a href='coconsulta_oc.php'>Consulta de Ordenes de Compra</a></ul>");
	echo("<ul><a href='comodi_precios.php'>Actualizar lista de precios (nuevo)</a></ul>");
	echo("<ul><a href='coabm_insumos.php''>Alta, baja y modi de insumos</a></ul>");
	echo("<ul><a href='coabm_proveedor.php'>Alta, baja y modi de proveedores</a></ul>");
	echo("<ul><a href='coprecios.php'>ABM de precios</a></ul>");
	echo("<ul><a href='coalta_ordcomp.php'>Generar Orden de Compra</a></ul>");
	echo("<ul><a href='cocontroloc.php'>Administrar Ordenes de Compra</a></ul>");
	echo("</ul>");
}

cerrar();

?>