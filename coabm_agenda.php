<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Abm de agenda");
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Aceptar-copanel.php";
mi_titulo("Abm de la Agenda");
$panta=$_POST["panta"];
switch($panta)
{
	case "graba_alta":
		//trace("Estoy en graba alta");
		$fecha=a_fecha_sistema($_POST["fecha"]);
		$id_per=$_POST["id_per"];
		$compara=un_dato("select compara from periodo where id_per='$id_per'");
		$compara=str_replace($compara,"curdate()","$fecha");
		//trace("La comparacion es $compara");
		$id_rol=$_POST["id_rol"];
		$id_cuad=$_POST["id_cuad"];
		$item=$_POST["item"];
		$caduca=($_POST["caduca"]=='s') ? 1 : 0;
		//trace("Caduca $caduca");
		//die();
		if($id_per=="Elegir" or $id_rol=="Elegir" or $id_cuad=="Elegir")
		{
			mensaje("Faltan elegir algunos parametros");
		}else
		{
			$sql="select count(*) from agenda where periodo='$id_per' and rol='$id_rol' and cuadrante='$id_cuad' and $compara";
			//trace("La consulta es $sql");
			$existe=un_dato($sql);
			if($existe)
				$item="<br>$item";
			mi_query("insert into agenda set fecha='$fecha',alta=curdate(),periodo='$id_per',cuadrante='$id_cuad',rol='$id_rol',item='$item',estado=1,caduca='$caduca'","Error al insertar un nuevo evento");
			mensaje("Se agrego un evento a la agenda.");
		}
		delay();
		break;
	case "modi":
		$id_agenda=$_POST["id_agenda"];
		$cons=mi_query("select * from agenda where id_agenda='$id_agenda'","Error al obtener el registro");
		$datos=mysql_fetch_array($cons);
		$fecha=a_fecha_arg($datos["fecha"]);
		$alta=a_fecha_arg($datos["alta"]);
		$cuadrante=$datos["cuadrante"];
		$rol=$datos["rol"];
		$periodo=$datos["periodo"];
		$item=$datos["item"];
		$caduca=($datos["caduca"]==1) ? "s" : "";
		//$caduca="s";
		//trace("Caduca vale $caduca");
		$titulo="Modificacion de evento de la Agenda";
		$tit_modi="MODIFICACION DE EVENTO";
		$campos=";%ROT-Id. ITEM</td><td><strong>$id_agenda";
		$campos.=";%FEC-fecha-fecha-$fecha-10";
		$descrip_per=un_dato("select descrip_per from periodo where id_per='$periodo'");
		$campos.=";%SEL-id_per-periodo-select id_per,descrip_per from periodo order by 1-descrip_per+id_per-$descrip_per-$periodo";
		$descrip_rol=un_dato("select descrip_rol from rol where id_rol='$rol'");
		$campos.=";%SEL-id_rol-rol-select id_rol,descrip_rol from rol order by 1-descrip_rol+id_rol-$descrip_rol-$rol";
		$descrip_cuad=un_dato("select descrip_cuad from cuadrante where id_cuad='$cuadrante'");
		$campos.=";%SEL-id_cuad-cuadrante-select id_cuad,descrip_cuad from cuadrante order by 1-descrip_cuad+id_cuad-$descrip_cuad-$cuadrante";
		$campos.=";%ARE-evento-item-$item-5-55";
		$campos.=";%CHK-caduca-caduca-s-$caduca";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-id_agenda-$id_agenda";
		$campos.=";%CHK-borrar-borrar-s-N";
		mi_panta($tit_modi,$campos,$submit);
		break;
	case "graba_modi":
		$fecha=a_fecha_sistema($_POST["fecha"]);
		$id_per=$_POST["id_per"];
		$id_rol=$_POST["id_rol"];
		$id_cuad=$_POST["id_cuad"];
		$item=$_POST["item"];
		$id_agenda=$_POST["id_agenda"];
		$borrar=$_POST["borrar"];
		$caduca=($_POST["caduca"]=='s') ? 1 : 0;
		//trace("Caduca $caduca");
		//die();
		if($borrar=="s")
		{
			mi_query("update agenda set estado=3 where id_agenda='$id_agenda'","Error al anular el evento");
			mensaje("Se anulo el evento $id_agenda");
		}else
		{
			mi_query("update agenda set fecha='$fecha',periodo='$id_per',cuadrante='$id_cuad',rol='$id_rol',item='$item',caduca='$caduca' where id_agenda='$id_agenda'","Error al modificar un evento de la agenda");
			mensaje("Modificaci&oacute;n de $id_agenda grabada");
		}
		delay();
		break;
	default:
		$tit_alta="NUEVO EVENTO";
		$campos="%FEC-fecha-fecha--10";
		$campos.=";%SEL-id_per-periodo-select id_per,descrip_per from periodo order by 1-descrip_per+id_per";
		$campos.=";%SEL-id_rol-rol-select id_rol,descrip_rol from rol order by 1-descrip_rol+id_rol";
		$campos.=";%SEL-id_cuad-cuadrante-select id_cuad,descrip_cuad from cuadrante order by 1-descrip_cuad+id_cuad";
		$campos.=";%ARE-evento-item--5-55";
		$campos.=";%CHK-caduca-caduca-s-n";
		$campos.=";%OCU-panta-graba_alta";
		mi_panta($tit_alta,$campos,$submit);
		$hay=un_dato("select count(*) from agenda");
		if($hay)
		{
			raya();
			$titulos="id;alta;fecha;periodo;cuadrante;rol;evento;caduca";
			$sql="select a.id_agenda,a.alta,a.fecha,p.descrip_per,c.descrip_cuad,r.descrip_rol,a.item,if(a.caduca,'S','N') as caduca from agenda a,periodo p,cuadrante c, rol r where a.estado=1 and a.periodo=p.id_per and a.cuadrante=c.id_cuad and a.rol=r.id_rol order by a.id_agenda;coabm_agenda.php+id_agenda+panta+modi";
			//trace($sql);
			mi_titulo("EVENTOS AGENDADOS");
			tabla_cons($titulos,$sql,1,"silver","#8EC99F",0,"ACTUALIZ.","MODIFICAR","","Eventos Agendados;Eventos Agendados;eventos_agendados");
		}else
		{
			mensaje("No hay eventos agendados para mostrar");
		}
		un_boton("Volver","Volver","copanel.php");
		break;
}

cierre();
?>