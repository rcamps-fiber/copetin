<?
include_once('coacceso.php');
include_once('cofunciones_especificas.php');
include_once("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Mantenimiento Preventivo</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Grabar-copanel.php";	
mi_titulo("Mantenimiento Preventivo");
$panta=$_POST["panta"];
switch($panta)
{
	case "modi":
		$id=$_POST["id"];
		if(isset($_POST["hayerror"]))
		{
			$item=$_POST["item"];
			$ubicacion=$_POST["ubicacion"];
			$tarea=$_POST["tarea"];
			$frecuencia=$_POST["frecuencia"];
			$ultimo_a=$_POST["ultimo"];
			//trace("Al ingresar a modi ultimo a es $ultimo_a");
			$ultimo_sis=a_fecha_sistema($ultimo_a);
			//trace("y ultimo sis es $ultimo_sis");
			$proximo_a=$_POST["proximo"];
			//trace("Al ingresar a modi proximo a es $proximo_a");
			$proximo_sis=a_fecha_sistema($proximo_a);
			//trace("y proximo sis es $proximo_sis");
			$tecnico=$_POST["tecnico"];
			$estado=$_POST["estado"];
			$desc_estado=un_dato("select estado from estado_ot where id='$estado'");
			$ot=$_POST["ot"];
			$dispositivo=$_POST["dispositivo"];
			$desc_dispo=un_dato("select dispositivo from dispositivo where id='$dispositivo'");
		}else
		{
			$sql="select * from controlable where id='$id'";
			$rst=mi_query($sql,"Error al obtener los datos de la tarea");
			$datos=mysql_fetch_array($rst);
			$item=$datos["item"];
			$ubicacion=$datos["ubicacion"];
			$tarea=$datos["tarea"];
			$frecuencia=$datos["frecuencia"];
			$ultimo_sis=$datos["ultimo"];
			$ultimo_a=a_fecha_arg($ultimo_sis);
			$proximo_sis=$datos["proximo"];
			$proximo_a=a_fecha_arg($proximo_sis);
			$tecnico=$datos["tecnico"];
			$estado=$datos["estado"];
			$desc_estado=un_dato("select estado from estado_ot where id='$estado'");
			$ot=$datos["ot"];
			$dispositivo=$datos["dispositivo"];
			$desc_dispo=un_dato("select dispositivo from dispositivo where id='$dispositivo'");
		}
		$titulo="Modificacion de Tarea Programada";
		$campos="%ROT-TAREA</td><td>$id";
		$campos.=";%TXT-item-item-$item-30";
		$campos.=";%SEL-ubicacion-ubicacion-$ubicacion+$ubicacion+Elegir+Elegir+PIEDRABUENA 1P+PIEDRABUENA 1P+PIEDRABUENA PB+PIEDRABUENA PB+RODO 1P+RODO 1P+RODO 2P+RODO 2P+FASANDAMA+FASANDAMA+JANER+JANER-0";
		$campos.=";%TXT-tarea-tarea-$tarea-50";
		$campos.=";%TXT-frecuencia (dias)-frecuencia-$frecuencia-3";
		$campos.=";%TXT-ultimo-ultimo-$ultimo_a-10";
		$campos.=";%TXT-proximo-proximo-$proximo_a-10";
		$campos.=";%SEL-tecnico-tecnico-$tecnico+$tecnico+Elegir+Elegir+alejandro+alejandro+rcamps+rcamps-0";
		$campos.=";%SEL-estado-estado-$estado+$desc_estado+0+Elegir+2+SOLICITADO+4+FINALIZADO-0";
		$campos.=";%TXT-ot-ot-$ot-5";
		$campos.=";%SEL-dispositivo-dispositivo-select id,dispositivo from dispositivo order by freq desc-dispositivo+id-$desc_dispo-$dispositivo";		
		$campos.=";%OCU-id-$id";
		$campos.=";%CHK-elimina-elimina-1-n";
		$campos.=";%OCU-panta-graba_modi";
		$submit="aceptar-Modificar-copanel.php";	
		mi_panta($titulo,$campos,$submit);
		break;
	case "graba_modi":
		$id=$_POST["id"];		
		if($elimina=="1")
		{
			mi_query("delete from controlable where id='$id'","Error al eliminar una tarea.");
			mensaje("Se elimin&oacute; la tarea exitosamente");
			un_boton();
		}else
		{
			$ubicacion=$_POST["ubicacion"];
			$tarea=$_POST["tarea"];
			$frecuencia=$_POST["frecuencia"];
			$ultimo_a=$_POST["ultimo"];
			//trace("ultima fecha $ultimo_a");
			$proximo_a=$_POST["proximo"];
			//trace("proxima fecha $proximo_a");
			$tecnico=$_POST["tecnico"];
			$estado=$_POST["estado"];
			$ot=$_POST["ot"];
			$dispositivo=$_POST["dispositivo"];
			$mensaje="";
			if($ubicacion=="Elegir")
				$mensaje="Falta completar la ubicacion.";
			if($tarea=="")
				$mensaje.=" - Falta completar la tarea a realizar.";
			if($frecuencia=="")
				$mensaje.=" - Falta indicar la frecuencia en dias.";
			if($ultimo_a=="")
				$mensaje.=" - Falta indicar la fecha de la &uacute;ltima ejecuci&oacute;n.";
			$ultimo_sis=a_fecha_sistema($ultimo_a);
			//trace("El ultimo sis es $ultimo_sis");
			if(!fecha_ok($ultimo_sis))
				$mensaje.=" - Fecha de &uacute;ltima ejecuci&oacute;n inv&aacute;lida.";
			if($proximo_a=="")
				$mensaje.=" - Falta indicar la fecha de la pr&oacute;xima ejecuci&oacute;n.";
			$proximo_sis=a_fecha_sistema($proximo_a);
			//trace("El proximo sis es $proximo_sis");
			if(!fecha_ok($proximo_sis))
				$mensaje.=" - Fecha de proxima ejecuci&oacute;n inv&aacute;lida.";
			if($tecnico=="Elegir")
				$mensaje.=" - No se indic&oacute; el t&eacute;cnico.";
			if($estado=="Elegir")
				$mensaje.=" - No se indic&oacute; el estado.";
			if($dispositivo=="Elegir")
				$mensaje.=" - Falta seleccionar un dispositivo.";
			if($mensaje<>"")
			{
				mensaje($mensaje);
				$titulo="";
				$campos="%OCU-id-$id";
				$campos.=";%OCU-item-$item";
				$campos.=";%OCU-ubicacion-$ubicacion";
				$campos.=";%OCU-tarea-$tarea";
				$campos.=";%OCU-frecuencia-$frecuencia";
				$campos.=";%OCU-proximo-$proximo_a";
				$campos.=";%OCU-ultimo-$ultimo_a";
				$campos.=";%OCU-tecnico-$tecnico";
				$campos.=";%OCU-estado-$estado";
				$campos.=";%OCU-ot-$ot";
				$campos.=";%OCU-dispositivo-$dispositivo";
				$campos.=";%OCU-panta-modi";
				$campos.=";%OCU-hayerror-1";
				$submit="aceptar-Corregir-copanel.php";	
				mi_panta($titulo,$campos,$submit);
			}else
			{
				mi_query("update controlable set item='$item',ubicacion='$ubicacion',tarea='$tarea',frecuencia='$frecuencia',ultimo='$ultimo_sis',proximo='$proximo_sis',tecnico='$tecnico',estado='$estado',dispositivo='$dispositivo',ot='$ot' where id='$id'","Error al actualizar una tarea");
				mensaje("Se actualiz&oacute; correctamente la tarea");
				un_boton();
			}
		}
		break;
	case "graba_alta":
		$ubicacion=$_POST["ubicacion"];
		$tarea=$_POST["tarea"];
		$frecuencia=$_POST["frecuencia"];
		$proximo=$_POST["proximo"];
		$tecnico=$_POST["tecnico"];
		$dispositivo=$_POST["dispositivo"];
		$mensaje="";
		if($ubicacion=="Elegir")
			$mensaje="Falta completar la ubicacion.";
		if($tarea=="")
			$mensaje.=" - Falta completar la tarea a realizar.";
		if($frecuencia=="")
			$mensaje.=" - Falta indicar la frecuencia en dias.";
		if($proximo=="")
			$mensaje.=" - Falta indicar la fecha de la pr&oacute;xima ejecuci&oacute;n.";
		$proximo_sis=a_fecha_sistema($proximo);
		if(!fecha_ok($proximo_sis))
			$mensaje.=" - Fecha de proxima ejecuci&oacute;n inv&aacute;lida.";
		if($tecnico=="Elegir")
			$mensaje.=" - No se indic&oacute; el t&eacute;cnico.";
		if($dispositivo=="Elegir")
			$mensaje.=" - Falta seleccionar un dispositivo.";
		if($mensaje<>"")
		{
			mensaje($mensaje);
			$titulo="";
			$campos="%OCU-ubicacion-$ubicacion";
			$campos.=";%OCU-tarea-$tarea";
			$campos.=";%OCU-frecuencia-$frecuencia";
			$campos.=";%OCU-proximo-$proximo";
			$campos.=";%OCU-tecnico-$tecnico";
			$campos.=";%OCU-dispositivo-$dispositivo";
			mi_panta($titulo,$campos,$submit);
		}else
		{
			mi_query("insert into controlable set item='$item',ubicacion='$ubicacion',tarea='$tarea',frecuencia='$frecuencia',proximo='$proximo_sis',tecnico='$tecnico',estado=2,dispositivo='$dispositivo'","Error al agregar una tarea");
			mensaje("Se agrego correctamente la nueva tarea");
			un_boton();
		}
		break;
	default:
		$ubicacion=$_POST["ubicacion"];
		$tarea=$_POST["tarea"];
		$frecuencia=$_POST["frecuencia"];
		$proximo=$_POST["proximo"];
		$tecnico=$_POST["tecnico"];
		$dispositivo=$_POST["dispositivo"];
		if($ubicacion=="")
			$ubicacion="Elegir";
		if($tecnico=="")
			$tecnico="Elegir";
		if($dispositivo=="")
		{
			$desc_dispo="Elegir";
			$dispositivo=0;
		}
		$titulo="Agregar Tarea Programada";
		$campos="%TXT-item-item-$item-30";
		$campos.=";%SEL-ubicacion-ubicacion-$ubicacion+$ubicacion+Elegir+Elegir+PIEDRABUENA 1P+PIEDRABUENA 1P+PIEDRABUENA PB+PIEDRABUENA PB+RODO 1P+RODO 1P+RODO 2P+RODO 2P+FASANDAMA+FASANDAMA+JANER+JANER-0";
		$campos.=";%TXT-tarea-tarea-$tarea-50";
		$campos.=";%TXT-frecuencia (dias)-frecuencia-$frecuencia-3";
		$campos.=";%TXT-proximo-proximo-$proximo-10";
		$campos.=";%SEL-tecnico-tecnico-$tecnico+$tecnico+Elegir+Elegir+alejandro+alejandro+rcamps+rcamps-0";
		$campos.=";%SEL-dispositivo-dispositivo-select id,dispositivo from dispositivo order by freq desc-dispositivo+id-$desc_dispo-$dispositivo";		
		$campos.=";%OCU-panta-graba_alta";
		mi_panta($titulo,$campos,$submit);
		break;
}
$titulos="id;item;ubicacion;tarea;frec.;ultimo;proximo;tecnico;estado;ot;dispositivo";
if(isset($_POST["hay_filtro"]))
{
		$filtro="";
		$ftecnico=$_POST["ftecnico"];
		if($ftecnico<>"Elegir")
			$filtro.=" and c.tecnico='$ftecnico'";
		$fubicacion=$_POST["fubicacion"];
		if($fubicacion<>"Elegir")
			$filtro.=" and c.ubicacion='$fubicacion'";
		$ffrecuencia=$_POST["ffrecuencia"];
		if($ffrecuencia<>"")
			$filtro.=" and c.frecuencia='$ffrecuencia'";
		$fdispositivo=$_POST["fdispositivo"];
		if($fdispositivo<>"Elegir")
		{
			$filtro.=" and c.dispositivo='$fdispositivo'";
			$fdesc_dispo=un_dato("select dispositivo from dispositivo where id='$fdispositivo'");
		}
}
echo("<hr>");
if($ftecnico=="")
	$ftecnico="Elegir";
if($fubicacion=="")
	$fubicacion="Elegir";
if($fdispositivo=="")
{
	$fdispositivo="Elegir";
	$fdesc_dispo="Elegir";
}
	$titulo="Filtrar tareas programadas";
$xcampos.="%SEL-ftecnico-tecnico-$ftecnico+$ftecnico+Elegir+Elegir+alejandro+alejandro+rcamps+rcamps-0";
	$xcampos.=";%SEL-fubicacion-ubicacion-$fubicacion+$fubicacion+Elegir+Elegir+PIEDRABUENA 1P+PIEDRABUENA 1P+PIEDRABUENA PB+PIEDRABUENA PB+RODO 1P+RODO 1P+RODO 2P+RODO 2P+FASANDAMA+FASANDAMA+JANER+JANER-0";
	$xcampos.=";%TXT-frecuencia (dias)-ffrecuencia-$ffrecuencia-3";
	$xcampos.=";%SEL-fdispositivo-dispositivo-select 'Elegir' as id,'Elegir' as dispositivo union select id,dispositivo from dispositivo-dispositivo+id-$fdesc_dispo-$fdispositivo";
	$xcampos.=";%OCU-hay_filtro-s";
	$submit="aceptar-Filtrar-copreventivo.php";	
	mi_panta($titulo,$xcampos,$submit);
	

$sql="select c.id,c.item,c.ubicacion,c.tarea,c.frecuencia,c.ultimo,c.proximo,c.tecnico,e.estado,c.ot,d.dispositivo from controlable c,estado_ot e,dispositivo d where c.estado=e.id and c.dispositivo=d.id $filtro order by 1;copreventivo.php+id+panta+modi";


mi_titulo("Tareas Programadas");
if($filtro<>"")
	mensaje("La tabla est&aacute; filtrada");
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Editar");

?>
</BODY>
</HTML>
