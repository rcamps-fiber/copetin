<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Relacion de usuario y puestos</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("RELACION DE USUARIOS Y PUESTOS");
$panta=$_POST["panta"];
switch($panta)
{
	case "modi":
		$orden=$_POST["orden"];
		$usuario=un_dato("select usuario from usu_puesto where orden='$orden'");
		$puesto=un_dato("select puesto from usu_puesto where orden='$orden'");
		$descripcion=un_dato("select descripcion from puestos where codigo='$puesto'");
		
		$titulo="Modificaci&oacute;n de relacion entre usuarios y puestos";
		$campos_pantalla="%SEL-usuario-usuario-select usuario from usuarios where perfil=2 order by 1-usuario+usuario-$usuario-$usuario";
		$campos_pantalla.=";%SEL-puesto-puesto-select codigo,descripcion from puestos order by 2-descripcion+codigo-$descripcion-$puesto";
		$campos_pantalla.=";%CHK-elimina-elimina-1-n";
		$campos_pantalla.=";%OCU-orden-$orden";
		$campos_pantalla.=";%OCU-panta-graba_modi";
		$submit="aceptar-Grabar-copanel.php";	
		$submit="aceptar-Modificar-cousu_puesto.php";	
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
	case "graba_modi":
		$orden=$_POST["orden"];
		$usuario=$_POST["usuario"];
		$puesto=$_POST["puesto"];
		$descripcion=un_dato("select descripcion from puestos where codigo='$puesto'");
		$elimina=$_POST["elimina"];
		if($elimina=="1")
		{
			mi_query("delete from usu_puesto where orden='$orden'","Imposible eliminar la relacion $usuario - $descripcion.");
			mensaje("Se elimin&oacute; la relaci&oacute;n $usuario - $descripcion");
		}else
		{
			mi_query("update usu_puesto set usuario='$usuario',puesto='$puesto' where orden='$orden'","Imposible actualizar la relacion $usuario - $descripcion.");
			mensaje("Se actualiz&oacute; la relaci&oacute;n $usuario - $descripcion.");
		}
		un_boton();
		break;
	case "graba_alta":
		$usuario=$_POST["usuario"];
		$puesto=$_POST["puesto"];
		$descripcion=un_dato("select descripcion from puestos where codigo='$puesto'");
		$no_existe=un_dato("select count(*) from usu_puesto where usuario='$usuario' and puesto='$puesto'");
		if($no_existe==0)
		{
			mi_query("insert into usu_puesto set usuario='$usuario',puesto='$puesto'","Error al grabar el alta de la rel. usuario-puesto");
			mensaje("Se agreg&oacute; la relaci&oacute;n $usuario - $descripcion a la tabla usu_puesto.");
		}else
		{
			mensaje("La relaci&oacute;n $usuario-$descripcion ya exist&iacute;a en el sistema.");
		}
		un_boton();
		break;
	default:
		$titulo="Alta de relacion entre usuarios y puestos";
		$campos_pantalla="%SEL-usuario-usuario-select usuario from usuarios where perfil=2 order by 1-usuario+usuario-Elegir-Elegir";
		$campos_pantalla.=";%SEL-puesto-puesto-select codigo,descripcion from puestos order by 2-descripcion+codigo-Elegir-Elegir";
		$campos_pantalla.=";%OCU-panta-graba_alta";
		$submit="aceptar-Grabar-copanel.php";	
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
}
$titulos="id;usuario;nombre;puesto;observaciones";
$sql="select x.orden as orden,u.usuario,u.nombre,p.descripcion,p.observaciones";
$sql.=" from usuarios u,puestos p,usu_puesto x";
$sql.=" where u.usuario=x.usuario and p.codigo=x.puesto order by 1;cousu_puesto.php+orden+panta+modi";
mi_titulo("Lista de usuarios/puestos");
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Procesar");
?>
</BODY>
</HTML>
