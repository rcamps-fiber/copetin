<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Respuesta al pedido de insumos");
require_once("cobody.php");
require_once("cocnx.php");
require_once 'Mail.php';
require_once("Mail/mime.php");
$submit="aceptar-Confirmar-coinsumos.php";
mi_titulo("Respuesta al pedido de insumos");
$anular=$_POST["anular"];
if($anular=="S")
{
	$id_sol=$_POST["id_sol"];
	mi_query("update solins set estado=5 where id_sol='$id_sol'","Error al borrar la solicitud $id_sol");
	mensaje("Se elimin&#243; la solicitud $id_sol");
	delay("coinsumos.php");
	die();
}

$panta=$_POST["panta"];
switch($panta)
{
	case "clasificar":
		$id_sol=$_POST["id_sol"];
		$qry_sol=mi_query("select * from solins where id_sol='$id_sol'","Error al obtener datos de la solicitud de insumos");
		$datos=mysql_fetch_array($qry_sol);
		$cantidad=$datos["cantidad"];
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$articulo=$datos["articulo"];
		$observaciones=$datos["observaciones"];
		$admin=un_dato("select usuario from usuarios where perfil=7");
		$asunto="Solicitud de Insumos";
		// Pantalla de clasificacion
		$tit_panta="Clasificacion de Insumos";
		$campos.=";%ROT-<strong>Descripcion:</td><td>$articulo";
		$campos.=";%ROT-<strong>Observaciones:</td><td>$observaciones";
		$campos.=";%ARE-articulo-articulo-$articulo-4-80";
		$campos.=";%SEL-tipo-tipo-LB+LB+L+L+P+P-0";
		$campos.=";%TXT-unidad-unidad--5";
		$campos.=";%TXT-minimo-minimo--5";
		$campos.=";%TXT-maximo-maximo--5";
		$campos.=";%TXT-plazo-plazo--5";
		$campos.=";%TXT-punto pedido-punto_pedido--5";
		$campos.=";%TXT-Lote de compra-lote_compra--5";
		$campos.=";%TXT-consumo-consumo--5";
		$campos.=";%TXT-stock-stock--5";
		$campos.=";%OCU-id_sol-$id_sol";
		$campos.=";%OCU-panta-graba_clasif";
		mi_panta($tit_panta,$campos,$submit);
		break;
	case "graba_clasif":
		$id_sol=$_POST["id_sol"];
		$articulo=$_POST["articulo"];
		$tipo=$_POST["tipo"];
		$unidad=$_POST["unidad"];
		$minimo=$_POST["minimo"];
		$maximo=$_POST["maximo"];
		$plazo=$_POST["plazo"];
		$punto_pedido=$_POST["punto_pedido"];
		$lote=$_POST["lote_compra"];
		$consumo=$_POST["consumo"];
		$stock=$_POST["stock"];
		$qry=mi_query("insert into insumos set articulo='$articulo',tipo='$tipo',unidad='$unidad',minimo='$minimo',maximo='$maximo',plazo='$plazo',punto_pedido='$punto_pedido',lote_compra='$lote',consumo='$consumo',stock='$stock'","Error al ingresar el insumo $insumo");
		$clave=mysql_insert_id();
		//trace("La nueva clave es $clave");
		//die();
		mi_query("update solins set cod_ins='$clave' where id_sol='$id_sol'","Error al actualizar el codigo de articulo en la solicitud");
		mensaje("Se agreg&oacute; el insumo correctamente y se actualizo la solicitud");
		delay("coinsumos.php");
		break;
	case "responder":
		$id_sol=$_POST["id_sol"];
		$qry_sol=mi_query("select * from solins where id_sol='$id_sol'","Error al obtener datos de la solicitud de insumos");
		$datos=mysql_fetch_array($qry_sol);
		$articulo=$datos["articulo"];
		$cantidad=$datos["cantidad"];
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$observaciones=$datos["observaciones"];
		$texto="Con respecto a su pedido de insumos nro. $id_sol, le informamos que se programo la compra de $articulo. Le informaremos cuando recibamos el producto";
		$tit_panta="Respuesta al pedido de insumos no clasificados nro. $id_sol";
		$campos.=";%ROT-<strong>ARTICULO</td><td>$articulo";
		$campos.=";%ROT-<strong>CANTIDAD</td><td>$cantidad";
		$campos.=";%ROT-<strong>USUARIO</td><td>$nombre";
		$campos.=";%ROT-<strong>PUESTO</td><td>$desc_puesto";
		$campos.=";%ROT-<strong>OBSERVACIONES</td><td>$observaciones";
		$campos.=";%ARE-texto-texto-$texto-4-80";
		$campos.=";%OCU-panta-mandar_respuesta";
		$campos.=";%OCU-id_sol-$id_sol";
		$campos.=";%OCU-articulo-$articulo";
		mi_panta($tit_panta,$campos,$submit);
		break;
	case "mandar_respuesta":
		$texto=$_POST["texto"];
		$id_sol=$_POST["id_sol"];
		$qry_sol=mi_query("select * from solins where id_sol='$id_sol'","Error al obtener datos de la solicitud de insumos");
		$datos=mysql_fetch_array($qry_sol);
		$articulo=$datos["articulo"];
		$cantidad=$datos["cantidad"];
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$observaciones=$datos["observaciones"];
		$admin=un_dato("select usuario from usuarios where perfil=7");
		$asunto="Solicitud de Insumos";
		mandar_mail($usuario,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
		mensaje("Mail enviado a $usuario");
		mensaje("Mail enviado a $admin");
		delay("coinsumos.php");
		break;
	case "finalizar":
		$id_sol=$_POST["id_sol"];
		$qry_sol=mi_query("select * from solins where id_sol='$id_sol'","Error al obtener datos de la solicitud de insumos");
		$datos=mysql_fetch_array($qry_sol);
		$articulo=$datos["articulo"];
		$cantidad=$datos["cantidad"];
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$observaciones=$datos["observaciones"];
		$texto="Con respecto a su pedido de insumos nro. $id_sol, le informamos que ya disponemos de $articulo y le estaremos enviando el pedido a la brevedad.";
		$tit_panta="Finalizacion del pedido de insumos no clasificados nro. $id_sol";
		$campos.=";%ROT-<strong>ARTICULO</td><td>$articulo";
		$campos.=";%ROT-<strong>CANTIDAD</td><td>$cantidad";
		$campos.=";%ROT-<strong>USUARIO</td><td>$nombre";
		$campos.=";%ROT-<strong>PUESTO</td><td>$desc_puesto";
		$campos.=";%ROT-<strong>OBSERVACIONES</td><td>$observaciones";
		$campos.=";%TXT-entregar-entregar-$cantidad-5";
		$campos.=";%ARE-texto-texto-$texto-4-80";
		$campos.=";%OCU-panta-grabar_final";
		$campos.=";%OCU-id_sol-$id_sol";
		$campos.=";%OCU-articulo-$articulo";
		mi_panta($tit_panta,$campos,$submit);
		break;
	case "grabar_final":
		$texto=$_POST["texto"];
		$id_sol=$_POST["id_sol"];
		//trace("Estoy en grabar final con $id_sol");
		$entregar=$_POST["entregar"];
		$qry_sol=mi_query("select * from solins where id_sol='$id_sol'","Error al obtener datos de la solicitud de insumos");
		$datos=mysql_fetch_array($qry_sol);
		$articulo=$datos["articulo"];
		$cantidad=$datos["cantidad"];
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$observaciones=$datos["observaciones"];
		//trace("Id sol sigue siendo $id_sol");
		$qry="update solins set estado=3,entregado='$entregar' where id_sol='$id_sol'";
		//trace($qry);
		mi_query($qry,"Error al finalizar la solicitud de insumos $id_sol");
		mensaje("Solicitud de insumos $id_sol finalizada correctamente");
		$admin=un_dato("select usuario from usuarios where perfil=7");
		$asunto="Solicitud de Insumos";
		mandar_mail($usuario,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
		mensaje("Mail enviado a $usuario");
		mensaje("Mail enviado a $admin");
		delay("coinsumos.php");
		break;
	case "ejecucion":
		$id_sol=$_POST["id_sol"];
		$entregar=$_POST["entregar"];
		$respuesta=$_POST["respuesta"];
		$qry_sol=mi_query("select * from solins where id_sol='$id_sol'","Error al obtener datos de la solicitud de insumos");
		$datos=mysql_fetch_array($qry_sol);
		$cod_ins=$datos["cod_ins"];
		$cantidad=$datos["cantidad"];
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$articulo=$datos["articulo"];
		$observaciones=$datos["observaciones"];
		$admin=un_dato("select usuario from usuarios where perfil=7");
		$asunto="Solicitud de Insumos";
		if($entregar)
		{
				// Mail para el usuario
				if($entregar==1)
				{
					$texto="Con respecto a su pedido de insumos nro. $id_sol le estaremos entregando a la brevedad una unidad del articulo $articulo";

				}else
				{
					$texto="Con respecto a su pedido de insumos nro. $id_sol le estaremos entregando a la brevedad $entregar unidades del articulo $articulo";
				}
				if($entregar<$cantidad)
				{
					$texto.=", de las $cantidad solicitadas por ud.";
				}else
				{
					$texto.=", completando asi su orden.";
				}
				if($respuesta<>"")
					$texto.=" Observaciones: $respuesta";
				$stock_anterior=un_dato("select stock from insumos where id_insumo='$cod_ins'");
				if($entregar>$stock_anterior)
				{
					mensaje("Imposible entregar $entregar. Hay $stock_anterior y se entrega esa cantidad");
					$entregar=$stock_anterior;
				}
				mi_query("update insumos set stock=stock-'$entregar' where id_insumo='$cod_ins'","Error al actualizar el stock del insumo $articulo");
				$punto_pedido=un_dato("select punto_pedido from insumos where id_insumo='$cod_ins'");
				$stock_nuevo=$stock_anterior-$entregar;
				if($stock_nuevo<=$punto_pedido)
				{
					$asunto2="Stock de insumos";
					$texto2="El stock del articulo codigo $cod_ins, $articulo ha llegado a su punto de pedido";
					mandar_mail($admin,$uid,$asunto2,$texto2,$admin,"logo_copetin.jpg",1);
				}
				mi_query("update solins set estado=3,entregado='$entregar' where id_sol='$id_sol'","Error al actualizar el estado de la solicitud $id_sol");
				mandar_mail($usuario,$uid,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
				mensaje("Mail enviado a $usuario");
				mensaje("Mail enviado a $admin");
				delay("coinsumos.php");
				die();	
		}else
		{
			mensaje("No se entrega ningun articulo.");
			if($respuesta<>"")
			{
				$texto="Con respecto a su pedido de insumos nro. $id_sol por $cantidad unidades del articulo $articulo, no podemos entregar su orden por el siguiente motivo: $respuesta";
				mandar_mail($usuario,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
				mensaje("Mail enviado a $usuario");
				mensaje("Mail enviado a $admin");
				delay("coinsumos.php");
				die();	
				
			}
		}
		un_boton();
		break;
	case "sin_stock":
		$id_sol=$_POST["id_sol"];
		$qry_sol=mi_query("select * from solins where id_sol='$id_sol'","Error al obtener datos de la solicitud de insumos");
		$datos=mysql_fetch_array($qry_sol);
		$cod_ins=$datos["cod_ins"];
		$cantidad=$datos["cantidad"];
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$articulo=$datos["articulo"];
		$observaciones=$datos["observaciones"];
		// Mail para el usuario
		$admin=un_dato("select usuario from usuarios where perfil=7");
		$asunto="Solicitud de Insumos";
		$texto="Con respecto a su pedido $id_sol le informamos que al momento no hay stock de $articulo. En cuanto recibamos la mercaderia completaremos la orden.";
		if($respuesta<>"")
			$texto.=" $respuesta";
		mandar_mail($usuario,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
		mensaje("Mail enviado a $usuario");
		mensaje("Mail enviado a $admin");
		delay("coinsumos.php");
		break;
	default:
		$id_sol=$_POST["id_sol"];
		$qry_sol=mi_query("select * from solins where id_sol='$id_sol'","Error al obtener datos de la solicitud de insumos");
		$datos=mysql_fetch_array($qry_sol);
		$cod_ins=$datos["cod_ins"];
		$cantidad=$datos["cantidad"];
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$articulo=$datos["articulo"];
		$observaciones=$datos["observaciones"];
		if($cod_ins==0)
		{
			// Pedido no clasificado
			$tit_panta="Pedido No Clasificado";
			$campos.=";%ROT-<strong>Descripcion:</td><td>$articulo";
			$campos.=";%ROT-<strong>Observaciones:</td><td>$observaciones";
			$campos.=";%OCU-id_sol-$id_sol";
			$campos.=";%SEL-panta-accion-clasificar+CLASIFICAR+responder+RESPONDER+finalizar+FINALIZAR-0";
			$campos.=";%CHK-anular-anular-S-n";
			mi_panta($tit_panta,$campos,$submit);
		}else
		{
			// Pedido clasificado
			$stock=un_dato("select stock from insumos where id_insumo='$cod_ins'");
			if($stock>$cantidad)
			{
				$entregar=$cantidad;
			}else
			{
				$entregar=$stock;
			}
			if($entregar>0)
			{
				// Hay stock.
				$tit_panta="Ejecucion del Pedido de Insumos Nro. $id_sol.";
				$campos.=";%OCU-panta-ejecucion";
				$campos.=";%OCU-id_sol-$id_sol";
				$campos.=";%ROT-<strong>USUARIO:</td><td>$nombre";
				$campos.=";%ROT-<strong>PUESTO:</td><td>$desc_puesto";
				$campos.=";%ROT-<strong>CODIGO</td><td>$cod_ins";
				$campos.=";%ROT-<strong>ARTICULO</td><td>$articulo";
				$campos.=";%ROT-<strong>OBSERVACIONES</td><td>$observaciones";
				$campos.=";%ROT-<strong>CANT. SOLICITADA</td><td>$cantidad";
				$campos.=";%TXT-entregar-entregar-$entregar-5";
				$campos.=";%ARE-respuesta-respuesta--8-50";
				$campos.=";%CHK-anular-anular-S-n";
				mi_panta($tit_panta,$campos,$submit);
			}else
			{
				// Poner con estado sin stock
				mi_query("update solins set estado=2 where id_sol='$id_sol'");
				$tit_panta="Pedido de Insumos Nro. $id_sol sin stock";
				$campos.=";%OCU-panta-sin_stock";
				$campos.=";%OCU-id_sol-$id_sol";
				$campos.=";%ROT-<strong>USUARIO:</td><td>$nombre";
				$campos.=";%ROT-<strong>PUESTO:</td><td>$desc_puesto";
				$campos.=";%ROT-<strong>CODIGO</td><td>$cod_ins";
				$campos.=";%ROT-<strong>ARTICULO</td><td>$articulo";
				$campos.=";%ROT-<strong>OBSERVACIONES</td><td>$observaciones";
				$campos.=";%ROT-<strong>solicitado</td><td>$cantidad";
				$campos.=";%ARE-respuesta-respuesta--8-50";
				$campos.=";%CHK-anular-anular-S-n";
				mi_panta($tit_panta,$campos,$submit);
			}
		}
	break;
}
cierre();
?>
