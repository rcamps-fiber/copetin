<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Administracion de usuarios</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("ADMINISTRACION DE USUARIOS");
$panta=$_POST["panta"];
switch($panta)
{
	case "graba_asigna":
		$usuario=$_POST["usuario"];
		$puesto=$_POST["puesto"];
		mi_query("insert into usu_puesto set usuario='$usuario',puesto=$puesto","cousuarios.php. Linea 31. Imposible agregar un puesto para $usuario.");
		mensaje("Se agreg&oacute; el puesto $puesto para $usuario");
	case "asigna":
		if(!isset($usuario))
			$usuario=$_POST["usuario"];
		$campos_pantalla="%ROT-<tr><td><strong>USUARIO</strong></td><td><strong>$usuario</strong></td></tr>;";
		$campos_pantalla.="%OCU-usuario-$usuario;";
		$campos_pantalla.="%SEL-puesto-puesto-select codigo,concat(descripcion,' Ubic.: ',ubicacion) as descrip from puestos-descrip+codigo;";
		$campos_pantalla.="%OCU-panta-graba_asigna";
		$titulo="Asignaci&oacute;n de puestos";
		$submit="aceptar-Asignar-cousuarios.php";	
		mi_panta($titulo,$campos_pantalla,$submit);		
		break;
	case "modi":
		$usuario=$_POST["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$perfil=un_dato("select perfil from usuarios where usuario='$usuario'");
		$email=un_dato("select email from usuarios where usuario='$usuario'");
		$campos_pantalla="%OCU-usuario-$usuario;";
		$campos_pantalla.="%TXT-nombre-nombre-$nombre-30;";
		$campos_pantalla.="%TXT-email-email-$email-25;";
		$prf_def=un_dato("select descripcion from perfiles where prf=$perfil");
		$campos_pantalla.="%SEL-perfil-perfil-select prf,descripcion from perfiles order by 1-descripcion+prf-$prf_def-$perfil;";
		$campos_pantalla.="%CHK-Borra puestos-saca_puestos-1-n;";
		$campos_pantalla.="%CHK-elimina-elimina-1-n;";
		$campos_pantalla.="%OCU-panta-graba_modi";
		$titulo="Modificaci&oacute;n";
		$submit="aceptar-Modificar-cousuarios.php";	
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
	case "graba_modi":
		$usuario=$_POST["usuario"];
		$nombre=$_POST["nombre"];
		$perfil=$_POST["perfil"];
		$email=$_POST["email"];
		$elimina=$_POST["elimina"];
		if($saca_puestos=="1")
		{
			mi_query("delete from usu_puesto where usuario='$usuario'","cousuarios.php. Linea 59. Imposible eliminar los puestos del usuario $usuario.");
			mensaje("Se eliminaron los puestos del usuario $usuario.");
		}
		if($elimina=="1")
		{
			//mi_query("delete from usuarios where usuario='$usuario'","cousuarios.php Linea 42. Imposible eliminar el usuario.");
			mi_query("update usuarios set perfil=8 where usuario='$usuario'");
			//mi_query("delete from usu_puesto where usuario='$usuario'","cousuarios.php. Linea 44. Imposible eliminar el usuario de la tabla usu_puesto");
			mi_query("update usu_puesto set puesto=48 where usuario='$usuario'");
			mensaje("Se deshabilito el usuario $usuario.");
		}else
		{
			mi_query("update usuarios set nombre='$nombre',perfil=$perfil,email='$email' where usuario='$usuario'","cousuarios.php Linea 50. Imposible actualizar el usuario.");
			mensaje("Se actualiz&oacute; el usuario $usuario.");
		}
		un_boton();
		break;
	case "graba_alta":
		$usuario=$_POST["usuario"];
		$nombre=$_POST["nombre_u"];
		$clave=$_POST["clave"];
		$perfil=$_POST["perfil"];
		$email=$_POST["email"];
		$no_existe=un_dato("select count(*) from usuarios where usuario='$usuario'");
		if($no_existe==0)
		{
			mi_query("insert into usuarios set usuario='$usuario',nombre='$nombre',clave=password('$clave'),perfil=$perfil,email='$email'","cousuarios.php Linea 61. Imposible grabar alta de usuario");
			mensaje("Se agreg&oacute; a $usuario a los usuarios de Copetin.");
		}else
		{
			mensaje("El usuario $usuario ya exist&iacute;a en el sistema.");
		}
		un_boton();
		break;
	default:
		$titulo="Alta de usuarios";
		$campos_pantalla="%TXT-usuario-usuario--10;";
		$campos_pantalla.="%TXT-nombre-nombre_u--30;";
		$campos_pantalla.="%PWD-clave-clave--16;";
		$prf_def=un_dato("select descripcion from perfiles where prf=2");
		$campos_pantalla.="%SEL-perfil-perfil-select prf,descripcion from perfiles order by 1-descripcion+prf-$prf_def-2;";
		$campos_pantalla.="%TXT-email-email-usuario@veinfar.com.ar-25;";
		$campos_pantalla.="%OCU-panta-graba_alta";
		$submit="aceptar-Grabar-copanel.php";	
		mi_panta($titulo,$campos_pantalla,$submit);		
		mensaje("Si no es un perfil de administrador, deber&aacute; asignarle al menos un puesto.");
		break;
}
$si=un_dato("select count(*) from usuarios u left join usu_puesto p on u.usuario=p.usuario where p.usuario is null and perfil=2");
if($si>0)
{
	$titulos="usuario;nombre;email";
	$sql="select u.usuario,u.nombre,u.email";
	$sql.=" from usuarios u left join usu_puesto p on u.usuario=p.usuario where p.usuario is null and perfil=2;cousuarios.php+usuario+panta+asigna";
	mi_titulo("Usuarios sin puesto");
	tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Asignar");
}
$si=un_dato("select count(*) from usuarios u,puestos p,usu_puesto r where u.perfil=2 and u.usuario=r.usuario and r.puesto=p.codigo");
if($si>0)
{
	$titulos="usuario;nombre;email;ubicacion;descripcion";
	$sql="select u.usuario,u.nombre,u.email,p.ubicacion,p.descripcion";
	$sql.=" from usuarios u,puestos p,usu_puesto r where u.perfil=2 and u.usuario=r.usuario and r.puesto=p.codigo and p.codigo<>48 order by 1;cousuarios.php+usuario+panta+modi";
	mi_titulo("Lista de Usuarios activos");
	tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Procesar");
}
$si=un_dato("select count(*) from usuarios u,perfiles x where u.perfil=x.prf and u.perfil<>2");
if($si>0)
{
	$titulos="usuario;nombre;perfil;email";
	$sql="select u.usuario,u.nombre,x.descripcion as prf,u.email";
	$sql.=" from usuarios u,perfiles x where u.perfil=x.prf and u.perfil<>2 order by 1;cousuarios.php+usuario+panta+modi";
	mi_titulo("Lista de Administradores");
	tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Procesar");
}
?>
</BODY>
</HTML>
