<?

Class ORDENCOMPRA {

    var $idOc = 0;
    var $numeroOc = 0;
    var $idProveedor = 0;
    var $fechaGeneracion = "";
    var $idFormaPago = 0;
    var $subtotal = 0;
    var $iva = 0;
    var $total = 0;
    var $idPlazoEntrega = 0;
    var $idEstadoAutoriz = 1;
    var $idNivelAutoriz = 0;
    var $idMoneda = 0;
    var $tipoCambio = 0;
    var $idSectorSolic = 0;
    var $solicitante = "";
    var $presupuesto = "";
    var $idImputacion = 0;
    var $idRubro = 0;
    var $idTipoGasto = 0;
    var $idEstadoPago = 1;
    var $numerosOp = "";
    var $idEstadoOc = 1;
    var $msg = "";
    var $hayErrores = 0;

    function generacion() {
	//Validacion
	$this->numeroOc=$_POST["numeroOc"]; 
	$this->idProveedor=$_POST["codigo"];
	$this->idFormaPago=$_POST["idFormaPago"];
	$this->idPlazoEntrega=$_POST["idPlazoEntrega"];
	$this->idNivelAutoriz=$_POST["idNivelAutoriz"];
	$this->idMoneda=$_POST["idMoneda"];
	$this->tipoCambio=$_POST["tipoCambio"];
	$this->idSectorSolic=$_POST["idSectorSolic"];
	$this->solicitante=$_POST["solicitante"];
	$this->presupuesto=$_POST["presupuesto"];
	$this->idImputacion=$_POST["idImputacion"];
	$this->idRubro=$_POST["idRubro"];
	$this->idTipoGasto=$_POST["idTipoGasto"];
	$this->msg.=($this->numeroOc==0) ? "Numero de OC invalido." : "";
	$this->msg.=($this->idProveedor==0) ? "<br>Proveedor invalido." : "";
	$this->msg.=($this->idFormaPago==0) ? "<br>Forma de pago invalida." : "";
	$this->msg.=($this->idPlazoEntrega==0) ? "<br>Plazo de entrega invalido." : "";
	$this->msg.=($this->idNivelAutoriz==0) ? "<br>Nivel de autorizacion invalido." : "";
	$this->msg.=($this->idMoneda==0) ? "<br>Moneda invalida." : "";
	$this->msg.=($this->idMoneda<>1 and $this->tipoCambio==0) ? "<br>Tipo de cambio invalido." : "";
	$this->msg.=($this->idMoneda==1 and $this->tipoCambio<>0) ? "<br>Tipo de cambio invalido." : "";
	$this->msg.=($this->idSectorSolic==0) ? "<br>Sector solicitante invalido." : "";
	$this->msg.=($this->solicitante=="") ? "<br>Solicitante invalido." : "";
	$numeroOcDuplicado=un_dato("select count(*) from ocCabecera where numeroOc='$this->numeroOc'");
	$this->msg.=($numeroOcDuplicado>0) ? "<br>Numero de Orden duplicado" : "";
	if($this->msg==""){
	    $sql_insert="insert into ocCabecera set numeroOc='$this->numeroOc',idProveedor='$this->idProveedor',fechaGeneracion=curdate(),idFormaPago='$this->idFormaPago',idPlazoEntrega='$this->idPlazoEntrega',idEstadoAutoriz='$this->idEstadoAutoriz',idNivelAutoriz='$this->idNivelAutoriz',idMoneda='$this->idMoneda',tipoCambio='$this->tipoCambio',idSectorSolic='$this->idSectorSolic',solicitante='$this->solicitante',presupuesto='$this->presupuesto',idEstadoPago='$this->idEstadoPago',numerosOp='$this->numerosOp',idEstadoOc='$this->idEstadoOc',idImputacion='$this->idImputacion',idRubro='$this->idRubro',idTipoGasto='$this->idTipoGasto'";
	    mi_query($sql_insert);
	    $this->idOc=  mysql_insert_id();	    
	    $this->mostrarCabecera();
	    $this->altaItems();
	}else{
	    mensaje("Hay errores:<br>$this->msg");
	    $this->hayErrores=1;
	    $this->msg="";
	    $this->altaCabecera();
	}
	return;
    }
    
    function procesarItems()
    {
	$grabados=0;
	for($i=1; $i<=20 ; $i++){
	    $idOc=$_POST["idOc"];
	    //trace("Id OC vale $idOc");
	    $descripcion=$_POST["descripcion_$i"];
	    $cantidad=$_POST["cantidad_$i"];
	    $unitario=$_POST["unitario_$i"];
	    $total=$_POST["total_$i"];
	    $idRubro=$_POST["idRubro_$i"];
	    $idTipoGasto=$_POST["idTipoGasto_$i"];
	    $idImputacion=$_POST["idImputacion_$i"];	 
	    //trace("Total: $total, Unit.: $unitario, cantidad: $cantidad");
	    if($descripcion=="")
		break;
	    if($total==0 and $unitario<>0 and $cantidad<>0)
		    $total=$cantidad*$unitario;
	    if($total==0 and $unitario<>0 and $cantidad==0){
		    $total=$unitario;
		    $cantidad=1;
	    }	   
	    if($total<>0 and $unitario==0 and $cantidad<>0)
		    $unitario=$total/$cantidad;
	    if($total<>0 and $unitario<>0 and $unitario*$cantidad<>$total)
		$this->msg.="<br>Hay error en los importes";
	    $repetido=un_dato("select count(*) from ocDetalle where idOc='$idOc' and item='$i'");
	    if($repetido==0){
		$grabaItems="insert into ocDetalle set idOc='$idOc',item='$i',descripcion='$descripcion',unitario='$unitario',cantidad='$cantidad',total='$total',idRubro='$idRubro',idTipoGasto='$idTipoGasto',idImputacion='$idImputacion'";
		mi_query($grabaItems);
		$grabados++;	    
	    }else{
		$this->msg.=" Se intenta grabar un registro repetido. idOc: $idOc - item $item. ";
	    }
	}
	$this->msg.="<br>Se grabaron $grabados items";
	if($grabados>=20){
	    $this->mostrarCabecera();
	    $this->altaItems ();
	}
	$this->idOc=$idOc;
	$this->cargarDatosOc($idOc);	
	$this->mostrarCabecera();
	$this->mostrarItems();
	$this->cargaTotales();
	//un_boton();
    }

    function grabaTotales(){
	$this->idOc=$_POST["idOc"];
	$this->cargarDatosOc();
	$this->subtotal=$_POST["subtotal"];
	$this->iva=$_POST["iva"];
	$this->total=$_POST["total"];
	$sqlTotales="update ocCabecera set subtotal='$this->subtotal',iva='$this->iva',total='$this->total' where idOc='$this->idOc'";
	mi_query($sqlTotales);
    }
    
    function impresionOc(){
	require_once('tcpdf/config/lang/spa.php');
	require_once('tcpdf/tcpdf.php');
	$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Rodrigo Camps');
	$pdf->SetTitle('ORDEN DE COMPRA');
	$pdf->SetSubject('VEINFAR');
	$pdf->SetKeywords('TCPDF, PDF, orden de compra, veinfar, copetin');
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	$pdf->setLanguageArray($l);
	$pdf->SetFont('helvetica', 'B', 10);
	$pdf->AddPage();
	$pdf->Setx(12);
	$pdf->SetY(35);
	$pdf->Setx(12);
	$pdf->Write(0, 'JOSE ENRIQUE RODO 5685 - C.A.B.A.', '', 0, 'L', true, 0, false, false, 0);
	$pdf->Setx(12);
	$pdf->Write(0, 'PLANTA RODO 4139-5891 / 92 / 93', '', 0, 'L', true, 0, false, false, 0);
	$pdf->Setx(12);
	$pdf->Write(0, 'PLANTA PIEDRABUENA: 4601-6910', '', 0, 'L', true, 0, false, false, 0);
        $pdf->SetY(35);
	$pdf->Setx(12);
	$pdf->Write(0, 'FECHA: ' . $this->fechaGeneracion, '', 0, 'R', true, 0, false, false, 0);
	$pdf->Setx(12);
	$pdf->Write(0, 'O.C. Nro.: ' . $this->numeroOc, '', 0, 'R', true, 0, false, false, 0);
	$pdf->Setx(12);
	$pdf->Write(0, 'PRESUPUESTO: ' . $this->presupuesto, '', 0, 'R', true, 0, false, false, 0);
	$pdf->SetY(60);
	$pdf->Setx(13);
	$pdf->SetFont('helvetica', '', 10);
	// -----------------------------------------------------------------------------
	$tbl1 = <<<EOD
	<table cellspacing="0" cellpadding="1" border="0" style="background-color:#7A378B">
	    <tr>
		<td bgcolor='#660066' width="15%"><strong>Señores</strong></td>
	    </tr>
	</table>
EOD;

	$pdf->writeHTML($tbl1, true, false, false, false, '');
	$pdf->SetY(65);
	$proveedor=un_dato("select razon from proveedores where codigo='$this->idProveedor'");
	$domicilio=un_dato("select domicilio from proveedores where codigo='$this->idProveedor'");
	$telefono=un_dato("select telefono from proveedores where codigo='$this->idProveedor'");
	$fax=un_dato("select fax from proveedores where codigo='$this->idProveedor'");
	$formaPago=un_dato("select forma_de_pago from proveedores where codigo='$this->idProveedor'");
	$pdf->Setx(12);
	$pdf->SetFont('helvetica', 'B', 10);
	$pdf->Write(0, $proveedor, '', 0, 'L', true, 0, false, false, 0);
	$pdf->Setx(12);
	$pdf->SetFont('helvetica', '', 8);
	$pdf->Write(0, "DIRECCION: $domicilio", '', 0, 'L', true, 0, false, false, 0);
	$pdf->Setx(12);
	$pdf->Write(0,"TELEFONO: $telefono", '', 0, 'L', true, 0, false, false, 0);
	$pdf->Setx(12);
	$pdf->Write(0,"FAX: $fax", '', 0, 'L', true, 0, false, false, 0);
	$pdf->Setx(12);
	$pdf->Write(0,"FORMA DE PAGO: $formaPago", '', 0, 'L', true, 0, false, false, 0);
	// -----------------------------------------------------------------------------

	$pdf->SetY(95);
	$moneda=un_dato("select moneda from moneda where idMoneda='$this->idMoneda'");
	$items="select item,descripcion,cantidad,unitario,total from ocDetalle where idOc='$this->idOc'";
	$encabezados="item;descripcion;cantidad;$moneda;total $moneda";
	$fondo="#7A378B";
	$tablaItems=tabla_cons_var($encabezados,$items,$fondo,$frente);
	$pdf->writeHTML($tablaItems, true, false, false, false, '');

	$pdf->Setx(12);
	$pdf->Write(0,"__________________________________________________", '', 0, 'R', true, 0, false, false, 0);
	$pdf->Write(0,"SUBTOTAL: $subtotal $moneda", '', 0, 'R', true, 0, false, false, 0);
	$pdf->Write(0,"IVA: $this->iva $moneda", '', 0, 'R', true, 0, false, false, 0);
	$pdf->Write(0,"TOTAL: $this->total $moneda", '', 0, 'R', true, 0, false, false, 0);

	$pdf->Setx(50);


	$pdf->SetY($pdf->GetY()-2);

	$tbl = <<<EOD
	<table cellspacing="0" cellpadding="1" border="0" style="background-color:#7A378B">
	    <tr>
		<td bgcolor='#7A378B' width="15%"><strong>OBSERVACIONES</strong></td>
	    </tr>
	</table>
EOD;

	$pdf->SetX(12);
	$pdf->writeHTML($tbl, true, false, false, false, '');
	$pdf->SetY($pdf->GetY()-2);
	$plazoEntrega=un_dato("select plazoEntrega from plazoEntrega where idPlazoEntrega='$idPlazoEntrega'");
	$pdf->Setx(11);
	$pdf->Write(0,"PLAZO DE ENTREGA: $plazoEntrega", '', 0, 'L', true, 0, false, false, 0);
	$pdf->SetY($pdf->GetY()+2);
	$pdf->Ln();

	$tbl = <<<EOD
	<table cellspacing="0" cellpadding="1" border="0" style="background-color:#7A378B">
	    <tr>
		<td bgcolor='#CC00CC' width="100%"><strong>AUTORIZADO POR</strong></td>
	    </tr>
	</table>
EOD;

	$pdf->SetX(160);
	$pdf->writeHTML($tbl, true, false, false, false, '');


	$pdf->Sety($pdf->GetY()-2);
	$sql="select * from autorizOc where idOc='$idOc'";
	$resultado=mi_query($sql);
	while($datos=mysql_fetch_array($resultado)){
	    $idAutoriz=$datos["idAutoriz"];		
		

	    $usuario=$datos["usuario"];
	    $fecha=$datos["fecha"];
	    $firma="$fecha | $usuario";
	    $pdf->Setx(160);
	    $pdf->Write(0,$firma, '', 0, 'L', true, 0, false, false, 0);
	}

	$pdf->Ln();
	$pdf->Ln();
	$pdf->Ln();

	$tbl = <<<EOD
	<table cellspacing="0" cellpadding="1" border="0" style="background-color:#7A378B">
	    <tr>
		<td bgcolor='#CC00CC' width="100%" align="center"><strong>ATENCION</strong></td>
	    </tr>
	</table>
EOD;
	$pdf->Setx($pdf->Getx()+5);
	$pdf->SetX(10);
	$pdf->writeHTML($tbl, true, false, false, false, '');

	$pdf->Sety($pdf->GetY()-2);
	$pdf->SetFont('helvetica', '', 6);
	$texto="EL HORARIO DE ENTREGA ES EXCLUSIVAMENTE DE 7 A 12 Y DE 13 A 15 HS. LA MERCADERIA DEBERA ENTREGARSE UNICAMENTE EN JOSE ENRIQUE RODO 5685 - C.A.B.A.";
	$pdf->SetX(10);
	$pdf->Write(0,$texto, '', 0, 'L', true, 0, false, false, 0);
	$texto="LOS REACTIVOS, COLUMNAS O MATERIAL QUE REQUIERA SER ENTREGADO CON CERTIFICADO DEBERA VENIR JUNTO CON EL PEDIDO, DE LO CONTRARIO NO SE ACEPTARA ";
	$pdf->SetX(10);
	$pdf->Write(0,$texto, '', 0, 'L', true, 0, false, false, 0);
	$texto="LA MERCADERIA. NO SERAN RECONOCIDAS LAS MERCADERIAS Y FACTURAS ENTREGADAS FUERA DEL HORARIO Y LUGAR INDICADOS.";
	$pdf->SetX(10);
	$pdf->Write(0,$texto, '', 0, 'L', true, 0, false, false, 0);
	$pdf->SetX(10);


	$pdf->Output('ordenCompraVeinfar.pdf', 'I');
    }
    
    
    function cargaTotales(){
	$moneda=un_dato("select moneda from moneda where idMoneda='$this->idMoneda'");
	$subtotal=un_dato("select sum(total) from ocDetalle where idOc='$this->idOc'");
	$iva=$subtotal*0.21;
	$total=$subtotal+$iva;
	$pantaCargaTotales=new PANTALLA();
	$pantaCargaTotales->titulo="";	
	$pantaCargaTotales->AgregarROT("SUBTOTAL $moneda##$subtotal");
	$pantaCargaTotales->AgregarTXT("iva $moneda","iva",$iva,10);
	$pantaCargaTotales->AgregarTXT("total $moneda","total",$total,10);
	$pantaCargaTotales->AgregarOCU("subtotal","$subtotal");
	$pantaCargaTotales->AgregarOCU("panta","grabaTotales");
	$pantaCargaTotales->AgregarOCU("idOc",$this->idOc);
	$pantaCargaTotales->regresar=$_SERVER['PHP_SELF'];
	$pantaCargaTotales->FactorAltura=30;
	$pantaCargaTotales->IncrementoAltura=30;
	$pantaCargaTotales->recuadro=1;
	//$pantaAltaItem->izquierda=-360;
	$pantaCargaTotales->izquierda=945;
	//$pantaAltaItem->tope=0;
	//$pantaCargaTotales->tope=-173;
	$pantaCargaTotales->tope=-500;
	$pantaCargaTotales->alto=850;
	$pantaCargaTotales->ancho=700;
	$pantaCargaTotales->ColorFrente="#f7e8aa";
	$pantaCargaTotales->ColorFondo="#f7e8aa";
	$pantaCargaTotales->MostrarPantalla();
	
    }
    
    function cargarDatosOc(){
	$sqlOc="select * from ocCabecera where idOc='$this->idOc'";
	$qry=mi_query($sqlOc);
	$datos= mysql_fetch_array($qry);
	//$this->idOc=$datos["idOc"];
	$this->numeroOc=$datos["numeroOc"];
	$this->idProveedor=$datos["idProveedor"];
	$this->fechaGeneracion=$datos["fechaGeneracion"];
	$this->idFormaPago=$datos["idFormaPago"];
	$this->subtotal=$datos["subtotal"];
	$this->iva=$datos["iva"];
	$this->total=$datos["total"];
	$this->idPlazoEntrega=$datos["idPlazoEntrega"];
	$this->idEstadoAutoriz=$datos["idEstadoAutoriz"];
	$this->idNivelAutoriz=$datos["idNivelAutoriz"];
	$this->idMoneda=$datos["idMoneda"];
	$this->tipoCambio=$datos["tipoCambio"];
	$this->idSectorSolic=$datos["idSectorSolic"];
	$this->solicitante=$datos["solicitante"];
	$this->idEstadoPago=$datos["idEstadoPago"];
	$this->numerosOp=$datos["numerosOp"];
	$this->idEstadoOc=$datos["idEstadoOc"];
	$this->presupuesto=$datos["presupuesto"];
	$this->idImputacion=$datos["idImputacion"];
	$this->idRubro=$datos["idRubro"];
	$this->idTipoGasto=$datos["idTipoGasto"];	
    }
    
    function altaItems()
    {
	$pantaAltaItem=new PANTALLA();
	$pantaAltaItem->titulo="Carga de items";	
	$pantaAltaItem->AgregarTXT("descripcion","descripcion","",30);
	$pantaAltaItem->AgregarTXT("cantidad","cantidad",0,10);
	$pantaAltaItem->AgregarTXT("precio","unitario",0,10);
	$pantaAltaItem->AgregarTXT("total","total",0,10);		
	$rubro=($this->idRubro==0) ? "Elegir" : un_dato("select rubro from rubroImputacion where idRubro='$this->idRubro'");
	$pantaAltaItem->AgregarSEL("Rubro","idRubro","rubro",$this->idRubro,$rubro,"rubroImputacion");
	$tipoGasto=($this->idTipoGasto==0) ? "Elegir" : un_dato("select tipoGasto from tipoGasto where idTipoGasto='$this->idTipoGasto'");
	$pantaAltaItem->AgregarSEL("Tipo Gasto","idTipoGasto","tipoGasto",$this->idTipoGasto,$tipoGasto,"tipoGasto");
	$voz=($this->idImputacion==0) ? "Elegir" : un_dato("select voz from imputacion where idImputacion='$this->idImputacion'");
	$pantaAltaItem->AgregarSEL("Imputacion","idImputacion","voz",$this->idImputacion,$voz,"imputacion");
	$pantaAltaItem->AgregarOCU("panta","procesarItems");
	$pantaAltaItem->AgregarOCU("idOc",$this->idOc);
	$pantaAltaItem->regresar=$_SERVER['PHP_SELF'];
	$pantaAltaItem->FactorAltura=30;
	$pantaAltaItem->IncrementoAltura=30;
	$pantaAltaItem->recuadro=1;
	//$pantaAltaItem->izquierda=-360;
	$pantaAltaItem->izquierda=10;
	//$pantaAltaItem->tope=-70;
	//$pantaAltaItem->tope=-173;
	$pantaAltaItem->tope=-200;
	$pantaAltaItem->alto=850;
	//$pantaAltaItem->ancho=1650;
	$pantaAltaItem->ancho=1850;
	$pantaAltaItem->ColorFrente="#f7e8aa";
	$pantaAltaItem->ColorFondo="gray";
	$pantaAltaItem->MostrarPlanilla();
	
    }
    function mostrarCabecera()
    {
	$hoy=hoy();
	icaja("relative",0,0,1850,1060,"gray");
	    icaja("relative",5,5,1840,50,"#f7e8aa");
		echo("<h1 align=center>ORDEN DE COMPRA</h1>");
	    fcaja();
	    //icaja("relative",0,-10,800,100,"#f7e8aa");
	    icaja("relative",5,-5,1840,88,"#f7e8aa");
		echo("<div style='font-size: 14px;line-height: 18px; font-weight: bold;font-family: arial;'>");
		echo("<p><p>Av. Piedrabuena 4190 - CABA </p>");
		echo("<div style='font-size: 14px;line-height: 8px;'>");
		echo("Tel. 4601-6910</p>");
		echo("</div>");
	   icaja("relative",1200,-46,440,80,"#f7e8aa");
		//echo("<div style='font-size: 11px;line-height: 12px;'>");
		echo("<div style='font-size: 14px;line-height: 8px; font-weight: bold;font-family: arial;'>");
		echo("<p><p align=right>Fecha $hoy</p>");
		echo("</div>");
		//echo("<div style='font-size: 11px;line-height: 10px;'>");
		echo("<div style='font-size: 14px;line-height: 8px;'>");
		echo("<p align=right>O.C. # $this->numeroOc</p>");
		$contacto=un_dato("select contacto from proveedores where codigo='$this->idProveedor'");
		echo("<p align=right>Atencion: $contacto</p>");
		echo("<p align=right>Presupuesto: $this->presupuesto</p>");
		echo("</div>");
	    fcaja();
	    icaja("relative",0,-54,1000,110,"#f7e8aa");
		$razon=un_dato("select razon from proveedores where codigo='$this->idProveedor'");
		//icaja("relative",0,0,400,25,"#f7e8aa");
		echo("<div style='font-size: 14px;line-height: 14px; font-weight: bold;font-family: arial;'>");
		echo("<p>Se&ntilde;ores: $razon</p>");
		echo("</div>");	
		$domic=un_dato("select domicilio from proveedores where codigo='$this->idProveedor'");
		echo("<div style='font-size: 14px;line-height: 6px;'>");
		echo("Direccion: $domic");	
		$telefono=un_dato("select telefono from proveedores where codigo='$this->idProveedor'");
		echo("<p>Tel: $telefono</p>");
		$fax=un_dato("select fax from proveedores where codigo='$this->idProveedor'");
		echo("<p>Fax: $fax</p>");
		$formaPago=un_dato("select formaPago from formaPago where idFormaPago='$this->idFormaPago'");
		echo("<p>Forma de pago: $formaPago</p>");
		echo("</div>");		
		//fcaja();		
	    fcaja();
	    
	    icaja("relative",1000,-178,840,110,"#f7e8aa");
		$rubro=un_dato("select rubro from rubroImputacion where idRubro='$this->idRubro'");
		$tipoGasto=un_dato("select tipoGasto from tipoGasto where idTipoGasto='$this->idTipoGasto'");
		$voz=un_dato("select voz from imputacion where idImputacion='$this->idImputacion'");
		echo("<div style='font-size: 14px;line-height: 20px; font-weight: bold;font-family: arial'>");
		echo("<p><p align=right>Rubro: $rubro");
		echo("</div>");
		echo("<div style='font-size: 14px;line-height: 8px;'>");
		echo("<p><p align=right>Tipo de Gasto: $tipoGasto");
		echo("<p><p align=right>Imputacion: $voz");
		echo("</div>");
	    fcaja();
	fcaja();
    }
    
    function mostrarItems(){
	//trace("Estoy en mostrar items con id $this->idOc");
	// Me fijo en la imputacion general. Si el item tiene la misma imputacion que la cabecera
	// entonces no la muestro 
	$sqlCab="select idMoneda,idImputacion,idRubro,idTipoGasto from ocCabecera where idOc='$this->idOc'";
	$qryCab=mi_query($sqlCab);
	$datosCab=mysql_fetch_array($qryCab);
	$idMoneda=$datosCab["idMoneda"];
	$moneda=un_dato("select moneda from moneda where idMoneda='$idMoneda'");
	$idImputacionCab=$datosCab["idImputacion"];
	$idRubroCab=$datosCab["idRubro"];
	$idTipoGastoCab=$datosCab["idTipoGasto"];
	$distintaImputacion=un_dato("select count(*) from ocDetalle where idOc='$this->idOc' and (idImputacion<>'$idImputacionCab' or idRubro<>'$idRubroCab' or idTipoGasto<>'$idTipoGastoCab')");
	$titulos="ITEM;DESCRIPCION;CANTIDAD;PRECIO;TOTAL;MONEDA";
	if($distintaImputacion)
		$titulos.=";RUBRO;TIPO;IMPUTACION";
	$sqlItems="select * from ocDetalle where idOc='$this->idOc'";
	$qry=mi_query($sqlItems);
	icaja("relative",1,-170,1840,790,"#f7e8aa");
	mi_tabla("i",0);
	$tono="xcolor";
	mi_fila("t",$titulos);
	while($datos=mysql_fetch_array($qry))
	{
	    $idOcDet=$datos["idOcDet"];
	    $item=$datos["item"];
	    $descripcion=$datos["descripcion"];
	    $cantidad=$datos["cantidad"];
	    $unitario=$datos["unitario"];
	    $total=$datos["total"];
	    $idRubro=$datos["idRubro"];
	    $idTipoGasto=$datos["idTipoGasto"];
	    $idImputacion=$datos["idImputacion"];
	    $rubro=un_dato("select rubro from rubroImputacion where idRubro='$idRubro'");
	    $tipoGasto=un_dato("select tipoGasto from tipoGasto where idTipoGasto='$idTipoGasto'");
	    $imputacion=un_dato("select voz from imputacion where idImputacion='$idImputacion'");
	    if($idImputacion==$idImputacionCab and $idRubro==$idRubroCab and $idTipoGasto==$idTipoGastoCab){
		$renglon="$item;$descripcion;$cantidad;$unitario;$total;$moneda";
	    }else{
		$renglon="$item;$descripcion;$cantidad;$unitario;$total;$moneda;$rubro;$tipoGasto;$imputacion";
	    }
	    
	    mi_fila($tono,$renglon);
	    $tono=($tono=="x") ? "xcolor" : "x";
	}
	mi_tabla("f");
	fcaja();
    }

    
    function mostrarTotales(){
	$moneda=un_dato("select moneda from moneda where idMoneda='$this->idMoneda'");
	$subtotal=un_dato("select sum(total) from ocDetalle where idOc='$this->idOc'");
	$iva=$subtotal*0.21;
	$total=$subtotal+$iva;
	//icaja("relative",945,-500,600,200,"white");
	icaja("relative",945,-500,600,200,"#f7e8aa");
	mi_tabla("i",0);
	//echo('<table style="background-color: #f7e8aa; font: bold;">');
	echo("<tr><td><strong>________________</td><td><strong>___________________</td><td><strong>______________</td></tr>");
	echo("<tr><td><strong>SUBTOTAL</td><td align=right> " . number_format($subtotal,2,",",".") . "</td><td>$moneda</td></tr>");
	echo("<tr><td><strong>IVA</td><td align=right>" . number_format($iva,2,",",".") . "</td><td>$moneda</td></tr>");
	echo("<tr><td><strong>TOTAL</td><td align=right>" . number_format($total,2,",",".") . "</td><td>$moneda</td></tr>");
	mi_tabla("f");
	fcaja();
    }
    
    function mostrarOrden(){
	$this->cargarDatosOc();
	$this->mostrarCabecera();
	$this->mostrarItems();
	$this->mostrarTotales();	
    }
    
    function creacionTablas() {
	$sqlCrearCab = "create table if not exists ocCabecera (idOc int not null auto_increment,primary key(idOc),numeroOc int,idProveedor int not null,fechaGeneracion date,idFormaPago int not null,subtotal float,iva float,total float,idPlazoEntrega int not null,idEstadoAutoriz int not null,idNivelAutoriz int not null,idMoneda int not null,tipoCambio float,idSectorSolic int not null,solicitante varchar(10),idEstadoPago int not null,numerosOp varchar(10),idEstadoOc int not null,presupuesto varchar(30),idImputacion int not null,idRubro int not null,idTipoGasto int not null)";
	mi_query($sqlCrearCab);
	$sqlCrearDet = "Create table if not exists ocDetalle (idOcDet int not null auto_increment,primary key(idOcDet),idOc int not null,item int,descripcion varchar(100),cantidad int,unitario float,idImputacion int not null,idRubro int not null,idTipoGasto int not null)";
	mi_query($sqlCrearDet);
	return;	
    }
    
    function altaCabecera()
    {    
	$panta_alta=new PANTALLA();
	$panta_alta->titulo="Generacion de Orden de Compra";
	$panta_alta->AgregarROT("Datos de   Cabecera");
	$this->numeroOc=($this->numeroOc==0) ? un_dato("select max(numeroOc)+1 from ocCabecera") : $this->numeroOc ; 
	$panta_alta->AgregarTXT("numero Oc","numeroOc",$this->numeroOc,8);
	$razon=($this->idProveedor==0) ? "Elegir" : un_dato("select razon from proveedores where codigo='$this->idProveedor'");
	$panta_alta->AgregarSEL("Proveedor","codigo","razon",$this->idProveedor,$razon,"proveedores");
	$formaPago=($this->idFormaPago==0) ? "Elegir" : un_dato("select formaPago from formaPago where idFormaPago='$this->idFormaPago'");
	$panta_alta->AgregarSEL("Forma de pago","idFormaPago","formaPago",$this->idFormaPago,$formaPago,"formaPago");
	$plazoEntrega=($this->idPlazoEntrega==0) ? "Elegir" : un_dato("select plazoEntrega from plazoEntrega where idPlazoEntrega='$this->idPlazoEntrega'");
	$panta_alta->AgregarSEL("Plazo de entrega","idPlazoEntrega","plazoEntrega",$this->idPlazoEntrega,$plazoEntrega,"plazoEntrega");
	$nivelAutoriz=($this->idNivelAutoriz==0) ? "Elegir" : un_dato("select nivelAutoriz from nivelAutoriz where idNivelAutoriz='$this->idNivelAutoriz'");
	$panta_alta->AgregarSEL("Nivel de autorizacion","idNivelAutoriz","nivelAutoriz",$this->idNivelAutoriz,$nivelAutoriz,"nivelAutoriz");
	$moneda=($this->idMoneda==0) ? "Elegir" : un_dato("select moneda from moneda where idMoneda='$this->idMoneda'");
	$panta_alta->AgregarSEL("Moneda","idMoneda","moneda",$this->idMoneda,$moneda,"moneda");
	$panta_alta->AgregarTXT("tipo de cambio","tipoCambio",$this->tipoCambio,4);
	$sectorSolic=($this->idSectorSolic==0) ? "Elegir" : un_dato("select sectorSolic from sectorSolic where idSectorSolic='$this->idSectorSolic'");
	$panta_alta->AgregarSEL("Sector Solicitante","idSectorSolic","sectorSolic",$this->idSectorSolic,$sectorSolic,"sectorSolic");
	//$panta_alta->AgregarTXT("solicitante","solicitante",$this->solicitante,8);
	$panta_alta->AgregarSELCustom3("solicitante","solicitante","nombre","usuario",0,"Elegir","select usuario,nombre from usuarios where perfil<>8");
	//AgregarSELCustom3($rotulo,$variable, $descripcion,$codigo,$valor_ini, $rot_ini, $misql)
	//$panta_alta->AgregarSELCustom($rotulo, $variable, $descripcion, $valor_ini, $rot_ini, $misql)
	$panta_alta->AgregarTXT("presupuesto","presupuesto",$this->presupuesto,8);
	$rubro=($this->idRubro==0) ? "Elegir" : un_dato("select rubro from rubroImputacion where idRubro='$this->idRubro'");
	$panta_alta->AgregarSEL("Rubro","idRubro","rubro",$this->idRubro,$rubro,"rubroImputacion");
	$tipoGasto=($this->idTipoGasto==0) ? "Elegir" : un_dato("select tipoGasto from tipoGasto where idTipoGasto='$this->idTipoGasto'");
	$panta_alta->AgregarSEL("Tipo de Gasto","idTipoGasto","tipoGasto",$this->idTipoGasto,$tipoGasto,"tipoGasto");
	$voz=($this->idImputacion==0) ? "Elegir" : un_dato("select voz from imputacion where idImputacion='$this->idImputacion'");
	$panta_alta->AgregarSEL("Imputacion","idImputacion","voz",$this->idImputacion,$voz,"imputacion");
	$panta_alta->AgregarOCU("panta","procesar");
	//$panta_alta->regresar=$_SERVER['PHP_SELF'];
	$panta_alta->regresar="coinsumos.php";
	$panta_alta->FactorAltura=30;
	$panta_alta->IncrementoAltura=30;
	$panta_alta->recuadro=1;
	$panta_alta->ColorFrente="#f7e8aa";
	$panta_alta->ColorFondo="gray";		
	//$panta_alta->ColorFrente="#efc6d3";
	//$panta_alta->ColorFondo="#083D06";
	$panta_alta->MostrarPantalla();
    }
    
    function aprobar(){
	//icaja("relative",1,-460,1840,300,"#f7e8aa");
	icaja("relative",1,-700,600,150,"#f9e08c");
	$panta_apro=new PANTALLA();
 	$panta_apro->titulo="Aprobacion";
	$panta_apro->AgregarSEL("Dictamen","idEstadoAutoriz","estadoAutoriz",0,"Elegir","estadoAutoriz");
	$panta_apro->AgregarOCU("panta","procesar_apro");
	$panta_apro->AgregarOCU("idOc",$this->idOc);
	$panta_apro->regresar=$_SERVER['PHP_SELF'];
	$panta_apro->FactorAltura=30;
	$panta_apro->IncrementoAltura=30;
	$panta_apro->recuadro=0;
	$panta_apro->ColorFrente="#f9e08c";
	$panta_apro->ColorFondo="gray";	
	$panta_apro->MostrarPantalla();
	fcaja();
   }
   
   function grabaAprobar(){
       //mensaje("Estoy en graba aprobar");
       $sql="update ocCabecera set idEstadoOc='$this->idEstadoOc',idEstadoAutoriz='$this->idEstadoAutoriz' where idOc='$this->idOc'";
       //trace($sql);
       mi_query($sql);
       mensaje("Se actualizo la Oc $this->numeroOc");
   }
   function recibir(){
	//icaja("relative",1,-460,1840,300,"#f7e8aa");
	icaja("relative",1,-700,600,150,"#f9e08c");
	$panta_recep=new PANTALLA();
 	$panta_recep->titulo="Recepcion de la Orden de Compra";
	//$panta_recep->AgregarSEL("Dictamen","idEstadoOc","estadoOc",0,"Elegir","estadoOc");
	$panta_recep->AgregarSELCustom2("Dictamen","idEstadoOc","estadoOc+idEstadoOc","select idEstadoOc,estadoOc from estadoOc where idEstadoOc in (7,8)");
	//gregarSELCustom2($rotulo,$variable, $descripcion, $misql) {
	$panta_recep->AgregarOCU("panta","procesar_recepcion");
	$panta_recep->AgregarOCU("idOc",$this->idOc);
	$panta_recep->regresar=$_SERVER['PHP_SELF'];
	$panta_recep->FactorAltura=30;
	$panta_recep->IncrementoAltura=30;
	$panta_recep->recuadro=0;
	$panta_recep->ColorFrente="#f9e08c";
	$panta_recep->ColorFondo="gray";	
	$panta_recep->MostrarPantalla();
	fcaja();
   }
   
   function grabaRecibir(){
       //mensaje("Estoy en graba recibir");
       $sql="update ocCabecera set idEstadoOc='$this->idEstadoOc' where idOc='$this->idOc'";
       //trace($sql);
       mi_query($sql);
       mensaje("Se actualizo la Oc $this->numeroOc");
   }

   function enviar(){
	//icaja("relative",1,-460,1840,300,"#f7e8aa");
	icaja("relative",1,-700,600,150,"#f9e08c");
	$panta_apro=new PANTALLA();
 	$panta_apro->titulo="Envio al Proveedor";
	//$panta_apro->AgregarSEL("Dictamen","idEstadoAutoriz","estadoAutoriz",0,"Elegir","estadoAutoriz");
	$panta_apro->AgregarOCU("panta","procesar_envio");
	$panta_apro->AgregarOCU("idEstadoOc",6);
	$panta_apro->AgregarOCU("idOc",$this->idOc);
	$panta_apro->regresar=$_SERVER['PHP_SELF'];
	$panta_apro->FactorAltura=30;
	$panta_apro->IncrementoAltura=30;
	$panta_apro->recuadro=0;
	$panta_apro->ColorFrente="#f9e08c";
	$panta_apro->ColorFondo="gray";	
	$panta_apro->MostrarPantalla();
	fcaja();
   }
   
   function grabaEnviar(){
       //mensaje("Estoy en graba aprobar");
       $sql="update ocCabecera set idEstadoOc='$this->idEstadoOc' where idOc='$this->idOc'";
       //trace($sql);
       mi_query($sql);
       mensaje("Se actualizo la Oc $this->numeroOc");
   }
   
      function emitirCheque(){
	//icaja("relative",1,-460,1840,300,"#f7e8aa");
	icaja("relative",1,-700,600,150,"#f9e08c");
	$panta_apro=new PANTALLA();
 	$panta_apro->titulo="Emision del cheque";
	//$panta_apro->AgregarSEL("Dictamen","idEstadoAutoriz","estadoAutoriz",0,"Elegir","estadoAutoriz");
	$panta_apro->AgregarOCU("panta","procesar_emision_cheque");
	$panta_apro->AgregarOCU("idEstadoOc",9);
	$panta_apro->AgregarOCU("idOc",$this->idOc);
	$panta_apro->regresar=$_SERVER['PHP_SELF'];
	$panta_apro->FactorAltura=30;
	$panta_apro->IncrementoAltura=30;
	$panta_apro->recuadro=0;
	$panta_apro->ColorFrente="#f9e08c";
	$panta_apro->ColorFondo="gray";	
	$panta_apro->MostrarPantalla();
	fcaja();
   }

   
   function grabaEmitirCheque(){
       //mensaje("Estoy en grabaEmitirCheque");
       $sql="update ocCabecera set idEstadoOc='$this->idEstadoOc' where idOc='$this->idOc'";
       mi_query($sql);
       mensaje("Se actualizo la Oc $this->numeroOc");
   }

      function pagar(){
	//icaja("relative",1,-460,1840,300,"#f7e8aa");
	icaja("relative",1,-700,600,150,"#f9e08c");
	$panta_apro=new PANTALLA();
 	$panta_apro->titulo="PAGO DE LA ORDEN DE COMPRA";
	//$panta_apro->AgregarSEL("Dictamen","idEstadoAutoriz","estadoAutoriz",0,"Elegir","estadoAutoriz");
	$panta_apro->AgregarOCU("panta","procesar_pagar");
	$panta_apro->AgregarOCU("idEstadoOc",10);
	$panta_apro->AgregarOCU("idOc",$this->idOc);
	$panta_apro->regresar=$_SERVER['PHP_SELF'];
	$panta_apro->FactorAltura=30;
	$panta_apro->IncrementoAltura=30;
	$panta_apro->recuadro=0;
	$panta_apro->ColorFrente="#f9e08c";
	$panta_apro->ColorFondo="gray";	
	$panta_apro->MostrarPantalla();
	fcaja();
   }

   
   function grabaPagar(){
       //mensaje("Estoy en grabaEmitirCheque");
       $sql="update ocCabecera set idEstadoOc='$this->idEstadoOc' where idOc='$this->idOc'";
       mi_query($sql);
       mensaje("Se actualizo la Oc $this->numeroOc");
   }
   
   
}  


Class PANTALLA {

    var $campos;
    var $submit;
    var $titulo;
    var $ventana;
    var $mostrar = "Aceptar";
    var $devolver = "Aceptar";
    var $regresar;
    var $tipos_validos = "TXT,SEL,CHK,FIL,PWD,OCU,ROT,ARE,FEC";
    var $SelRotIni = "Elegir uno";
    var $SelValIni = 0;
    var $FactorAltura = 28;
    var $IncrementoAltura = 30;
    var $recuadro = 0;
    var $ColorFondo = "#8F8262";
    var $ColorFrente = "#efc6d3";
    var $izquierda = 100;
    var $tope = 0;
    var $alto = 0;
    var $ancho = 700;

    function AgregarTXT($rotulo, $variable, $valor, $logitud) {
	$this->campos.=";%TXT-$rotulo-$variable-$valor-$logitud";
    }

    function AgregarFEC($rotulo, $variable, $valor) {
	$this->campos.=";%FEC-$rotulo-$variable-$valor";
    }

    function AgregarROT($rotulo) {
	$this->campos.=";%ROT-$rotulo";
    }

    function AgregarPWD($rotulo, $variable, $valor, $logitud) {
	$this->campos.=";%PWD-$rotulo-$variable-$valor-$logitud";
    }

    function AgregarARE($rotulo, $variable, $valor, $filas, $columnas) {
	$this->campos.=";%ARE-$rotulo-$variable-$valor-$filas-$columnas";
    }

    function AgregarOCU($variable, $valor) {
	$this->campos.=";%OCU-$variable-$valor";
    }

    function AgregarSEL($rotulo, $variable, $descripcion, $valor_ini, $rot_ini, $una_tabla = "") {
	$this->campos.=";%SEL-$variable-$rotulo-select distinct $variable,$descripcion from $una_tabla order by 2-$descripcion+$variable-$rot_ini-$valor_ini";
    }

    function AgregarSELCustom($rotulo, $variable, $descripcion, $valor_ini, $rot_ini, $misql) {
	$this->campos.=";%SEL-$variable-$rotulo-$misql-$descripcion+$variable-$rot_ini-$valor_ini";
    }
    function AgregarOPT($rotulo,$variable,$descripcion) {
        $this->campos.=";%SEL-$variable-$rotulo-$descripcion-0";
    }

    function AgregarSELCustom2($rotulo,$variable, $descripcion, $misql) {
        $this->campos.=";%SEL-$variable-$rotulo-$misql-$descripcion";
    }
     function AgregarSELCustom3($rotulo,$variable, $descripcion,$codigo,$valor_ini, $rot_ini, $misql) {
        $this->campos.=";%SEL-$variable-$rotulo-$misql-$descripcion+$codigo-$rot_ini-$valor_ini";
    }

    function AgregarCHK($rotulo, $variable, $valor, $check) {
	$this->campos.=";%CHK-$rotulo-$variable-$valor-$check";
    }

    function AgregarFIL($rotulo) {
	$this->campos.=";%FIL-$rotulo";
    }

    function MostrarPantalla() {
	if ($this->recuadro) {
	    $cuantos = substr_count($this->campos, "%");
	    $cuantos = $cuantos + 1;
	    $espacio_titulo = (strlen($titulo) > 0) ? 30 : 0;
	    $altura = $cuantos * $this->FactorAltura + $this->IncrementoAltura + $espacio_titulo;
	    $memos = substr_count($this->campos, "%ARE");
	    //die();
	    //$memos=5;
	    //trace("Hay $memos memos");
	    $altura_memo = 0;
	    for ($i = 0; $i < $memos; $i++) {
		//trace("valor de I $i");
		//die();
		// Hay campos memo. Hay que agrandar las cajas.
		$ultima_pos = strpos($this->campos, "%ARE") + 5;
		//trace($this->campos);
		//trace("Ultima pos $ultima_pos");
		for ($x = 1; $x < 4; $x++) {
		    //trace("X=$x");
		    $raya = strpos($this->campos, "-", $ultima_pos);
		    //trace("Raya= $raya");
		    $proxima = strpos($this->campos, "-", $raya + 1);
		    //trace("Prox. $proxima");
		    $ultima_pos = $raya + 1;
		}
		$posicion = $raya + 1;
		$largo = $proxima - $posicion;
		$altura_memo = substr($this->campos, $posicion, $largo);
		//trace("El Memo es $altura_memo de algo");
		$agregar = $agregar + $altura_memo;
	    }
	    //trace("Agregar: $agregar");
	    $altura = $altura + $altura_memo * 25 + 25;
	    //trace("La altura es $altura");
	    icaja("relative", $this->izquierda, $this->tope, $this->ancho, $altura, $this->ColorFondo);
	    icaja("relative", -2, -2, $this->ancho, $altura, $this->ColorFrente);
	}
	mi_titulo(strtr($this->titulo, "+", ";"));
	$multipart = (!(strpos($this->campos, "%FIL") === false)) ? "MULTI" : "";
	if ($this->ventana <> "") {
	    mi_form("w $multipart");
	} else {
	    mi_form("i $multipart");
	}
	mi_tabla("i", 0);
	$entrada = explode(";", $this->campos);
	foreach ($entrada as $celda) {
	    $largo = strlen($celda);
	    // El campo es un select
	    if (!(strpos($celda, "%SEL") === false)) {
		$param_sel = explode("-", $celda);
		$campo = $param_sel[1];
		$rotulo = $param_sel[2];
		$sql = $param_sel[3];
		$columna = $param_sel[4];
		if (count($param_sel) == 5) {
		    $def_rot = "Elegir";
		    $def_cod = "Elegir";
		} else {
		    $def_rot = $param_sel[5];
		    $def_cod = $param_sel[6];
		}
		if ($columna == "0") {
		    $sql = strtr($sql, "+", ";");
		} else {
		    $columna = strtr($columna, "+", ";");
		}
		mi_select($campo, $rotulo, $sql, $columna, $def_rot, $def_cod);
	    }
	    // El campo es un check box
	    if (!(strpos($celda, "%CHK") === false)) {
		$param_chk = explode("-", $celda);
		$rotulo = $param_chk[1];
		$campo = $param_chk[2];
		$ini = $param_chk[3];
		$check = $param_chk[4];
		//echo("<tr><td>$rotulo - $cmapo - $ini - $check</td></tr>");
		mi_box($rotulo, $campo, $ini, $check);
		//mi_tabla("i",0);
	    }
	    // El campo es un texto
	    if (!(strpos($celda, "%TXT") === false)) {
		$param_txt = explode("-", $celda);
		$rotulo = $param_txt[1];
		$campo = $param_txt[2];
		$ini = $param_txt[3];
		$largo = $param_txt[4];
		mi_text($rotulo, $campo, $ini, $largo);
		//echo("<td><input type='text' name='$celda'></td></tr>");
	    }
	    if (!(strpos($celda, "%PWD") === false)) {
		$param_txt = explode("-", $celda);
		$rotulo = $param_txt[1];
		$campo = $param_txt[2];
		$ini = $param_txt[3];
		$largo = $param_txt[4];
		mi_password($rotulo, $campo, $ini, $largo);
		//echo("<td><input type='text' name='$celda'></td></tr>");
	    }
	    // El campo es hidden
	    if (!(strpos($celda, "%OCU") === false)) {
		$param_ocu = explode("-", $celda);
		$campo = $param_ocu[1];
		$valor = $param_ocu[2];
		mi_oculto($campo, $valor);
		// echo("<input type='hidden' name='$campo' value='$valor'>");
	    }
	    // El campo es solamente un rotulo
	    if (!(strpos($celda, "%ROT") === false)) {
		$param_rot = explode("-", $celda);
		$rotulo = $param_rot[1];
		$rotulo = strtr($rotulo, "+", ";");
		$rotulo = str_replace("@", "&nbsp;", $rotulo);
		//if (strpos($rotulo, "<str>") !== false)
		  //  $rotulo = "<tr><td><strong>" . strtoupper(str_replace('<strong>','',$rotulo)) . "</strong></td></tr>";
		$rotulo = str_replace("##", "</td><td bgcolor='white'>", $rotulo);
		if (strpos($rotulo, "<2>") !== false)
		    $rotulo = "<td colspan=2 valign=top>" . str_replace('<2>','',$rotulo);
		if (strpos($rotulo, "<tr>") === false)
		    $rotulo = "<tr>" . $rotulo;
		if (strpos($rotulo, "</td>") === false)
		    $rotulo.="</td>";
		if (strpos($rotulo, "</tr>") === false)
		    $rotulo.="</tr>";
		echo($rotulo);
	    }
	    if (!(strpos($celda, "%ARE") === false)) {
		$param_txt = explode("-", $celda);
		$rotulo = $param_txt[1];
		$campo = $param_txt[2];
		$ini = $param_txt[3];
		$filas = $param_txt[4];
		$columnas = $param_txt[5];
		mi_textarea($rotulo, $campo, $ini, $filas, $columnas);
		//echo("<td><input type='text' name='$celda'></td></tr>");
	    }
	    if (!(strpos($celda, "%FEC") === false)) {
		$param_txt = explode("-", $celda);
		$rotulo = strtoupper($param_txt[1]);
		$campo = $param_txt[2];
		$ini = $param_txt[3];
		if ($ini == 0 or $ini == "")
		    $ini = un_dato("select curdate()");

		if ((strpos($ini, "/") == 4 or strpos($ini, "-") == 4) and (strpos($ini, "/", 5) == 7 or strpos($ini, "-", 5) == 7)) {
		    $ini = a_fecha_arg($ini);
		}
		mi_text($rotulo, $campo, $ini, 10);
	    }
	    if (!(strpos($celda, "%FIL") === false)) {
		$param_txt = explode("-", $celda);
		$rotulo = strtoupper($param_txt[1]);
		mi_file($rotulo);
	    }
	}
	mi_tabla("f", 1);
	echo("<br>");
	botones_submit($this->devolver, $this->mostrar, $this->regresar);
    }

    
    function MostrarPlanilla()
    {
	// Pantalla de ingreso de datos en forma de planilla, con multiples renglones.
	icaja("relative",$this->izquierda-15,$this->tope,$this->ancho,$this->alto,"$this->ColorFondo");
	icaja("relative",5,5,$this->ancho-10,$this->alto-10,"$this->ColorFrente");
	mi_titulo(strtr($this->titulo,"+",";"));
	$que_form="mi_plani" . rand();
	mi_form("i",$que_form);
	mi_tabla("i",0);
	$fila_titulos="";
	$entrada_orig=explode(";",$this->campos);
	for($i=1; $i<=20; $i++)
	{
		$entrada=$entrada_orig;
		if($i==1)
		{
			echo("<tr>");
			foreach($entrada as $celda)
			{
				if(!(strpos($celda,"%SEL")=== false))
				{
					$param_sel=explode("-",$celda);
					$rotulo=strtoupper($param_sel[2]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%CHK")=== false))
				{
					$param_chk=explode("-",$celda);
					$rotulo=strtoupper($param_chk[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%TXT")=== false))
				{
					$param_txt=explode("-",$celda);
					$rotulo=strtoupper($param_txt[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%ARE")=== false))
				{
					$param_txt=explode("-",$celda);
					$rotulo=strtoupper($param_txt[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%FEC")=== false))
				{
					$param_txt=explode("-",$celda);
					$rotulo=strtoupper($param_txt[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%PWD")=== false))
				{
					$param_txt=explode("-",$celda);
					$rotulo=strtoupper($param_txt[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
				if(!(strpos($celda,"%ROT")=== false))
				{
					$param_txt=explode("-",$celda);
					$rotulo=strtoupper($param_txt[1]);
					echo("<td><strong>$rotulo</strong></td>");
				}
			}
			echo("</tr>");
		}
		echo("<tr>");
		foreach($entrada as $celda)
		{
			$largo=strlen($celda);
			// El campo es un select
			if(!(strpos($celda,"%SEL")=== false))
			{
				$param_sel=explode("-",$celda);
				$campo=$param_sel[1] . "_$i";
				$rotulo=$param_sel[2];
				$sql=$param_sel[3];
				$columna=$param_sel[4];
				if(count($param_sel)==5)
				{
					$def_rot="Elegir";
					$def_cod="Elegir";
				}else
				{
					$def_rot=$param_sel[5];
					$def_cod=$param_sel[6];
				}
				if($columna=="0")
				{
					$sql=strtr($sql,"+",";");
				}else
				{
					$columna=strtr($columna,"+",";");
				}
				mi_select_plan($campo,$sql,$columna,$def_rot,$def_cod);
			}
			// El campo es un check box
			if(!(strpos($celda,"%CHK")=== false))
			{
				$param_chk=explode("-",$celda);
				$rotulo=$param_chk[1];
				$campo=$param_chk[2] . "_$i";
				$ini=$param_chk[3];
				$check=$param_chk[4];
				mi_box_plan($campo,$ini,$check);
			}
			// El campo es un texto
			if(!(strpos($celda,"%TXT")=== false))
			{
				$param_txt=explode("-",$celda);
				$rotulo=$param_txt[1];
				$campo=$param_txt[2] . "_$i";
				$ini=$param_txt[3];
				$largo=$param_txt[4];
				mi_text_plan($campo,$ini,$largo);
			}
			if(!(strpos($celda,"%ARE")=== false))
			{
				$param_txt=explode("-",$celda);
				$rotulo=$param_txt[1];
				$campo=$param_txt[2] . "_$i";
				$ini=$param_txt[3];
				$filas=$param_txt[4];
				$columnas=$param_txt[5];
				mi_textarea_plan($campo,$ini,$filas,$columnas);
			}
			if(!(strpos($celda,"%FEC")=== false))
			{
				$param_txt=explode("-",$celda);
				$rotulo=strtoupper($param_txt[1]);
				$campo=$param_txt[2] . "_$i";
				$ini=$param_txt[3];
				if($ini==0 or $ini=="")
					$ini=un_dato("select curdate()");
				
				if((strpos($ini,"/")==4 or strpos($ini,"-")==4) and (strpos($ini,"/",5)==7 or strpos($ini,"-",5)==7))
				{
					$ini=a_fecha_arg($ini);
				}
				mi_text_plan($campo,$ini,10);
			}
			if(!(strpos($celda,"%PWD")=== false))
			{
				$param_txt=explode("-",$celda);
				$rotulo=$param_txt[1];
				$campo=$param_txt[2] . "_$i";
				$ini=$param_txt[3];
				$largo=$param_txt[4];
				mi_password_plan($campo,$ini,$largo);
			}
			// El campo es hidden
			if(!(strpos($celda,"%OCU")=== false))
			{
				$param_ocu=explode("-",$celda);
				$campo=$param_ocu[1];
				$valor=$param_ocu[2];
				mi_oculto($campo,$valor);
			}
			// El campo es solamente un rotulo
			if(!(strpos($celda,"%ROT")=== false))
			{
				$param_rot=explode("-",$celda);
				$rotulo=$param_rot[1];
				if(strpos($rotulo,"+"))
				{
					if(strpos($rotulo,"<td>") === false)
						$rotulo="<td valign='top'><strong>" . $rotulo . "</td>";
					$rotulo=str_replace("+","</td><td valign='top'>",$rotulo);
				}
				$rotulo=str_replace("@","&nbsp;",$rotulo);
				if(strpos($rotulo,"<td>") === false)
					$rotulo="<td valign='top'><strong>" . $rotulo;
				if(strpos($rotulo,"</td>") === false)
					$rotulo.="</td>";
				echo($rotulo);
			}
		}
		echo("</tr>");
	}
	mi_tabla("f",1);
	if(!(strpos($this->submit,"-")=== false))
	{
		$param_submit=explode("-",$this->submit);
		$variable=$param_submit[0];
		$rotulo=$param_submit[1];
		$anterior=$param_submit[2];
		
	}else
	{
		$variable="primera";
		$anterior=$this->submit;
	}
	botones_submit($this>mostrar,$this->devolver,$this->regresar);
	fcaja();
	fcaja();
    }			


}


require_once('tcpdf/config/lang/spa.php');
require_once('tcpdf/tcpdf.php');


// Extension de la clase TCPDF que personaliza el encabezado y pie de pagina en los pdf
class MYPDF extends TCPDF {

	//Page header
	public function Header() {
		// Logo
		//$image_file = K_PATH_IMAGES.'logo_example.jpg';
	        $image_file = 'imagenes/open.jpg';
		//$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
		$this->Image($image_file, 10, 5, 70, '', 'JPG', '', 'T', false, 500, '', false, false, 0, false, false, false);
		// Set font
		$this->SetFont('helvetica', 'B', 20);
		//$this->SetTextColor(153,0,153);
		$this->SetTextColor(122,55,139);
		//330033
		// Title
		$this->SetY(22);
		$this->Cell(0, 15, ' ORDEN DE COMPRA ', 0, false, 'R', 0, '', 0, false, 'M', 'M');
		// print an ending header line
		$this->SetLineStyle(array('width' => 15 / $this->k, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(122,55,139)));
		//$this->SetLineStyle(array('width' => 0.85 / $this->k, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $headerdata['line_color']));
		//$this->SetY((2.835 / $this->k) + max($imgy, $this->y));
		$this->SetY(29);
		
		if ($this->rtl) {
			$this->SetX($this->original_rMargin);
		} else {
			$this->SetX($this->original_lMargin);
		}
		$this->Cell(($this->w - $this->original_lMargin - $this->original_rMargin), 0, '', 'T', 0, 'C');
	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Pag '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}





?>