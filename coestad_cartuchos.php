<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Estadisticas de soporte</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("Estad&iacute;sticas de Soporte T&eacute;cnico");
$sql="select concat(t.codigo_orig,' ',t.marca,' ',t.tipo) as cartucho,count(*) as cambios from cambios c,cartuchos t where c.cod_int=t.codigo_int and c.fecha >= date_sub(curdate(),interval 12 month) group by 1 order by 2 desc limit 10";
$titulos="cartuchos;cambios";
$titulo="Ranking top ten de cambios por tipo de cartuchos ultimos 12 meses";
mi_titulo($titulo);
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","MODIFICAR","","Listado");
grafico_barras($sql,"Cartuchos","Cambios",$titulo);
un_boton("Volver","Volver","copanel.php");

raya();
$sql="select i.modelo,count(*) as cambios from cambios c,impresoras i where c.impresora=i.codigo and c.fecha >= date_sub(curdate(),interval 12 month) group by 1 order by 2 desc limit 10";
$titulo="Ranking top ten de cambios por impresora ultimos 12 meses";
mi_titulo($titulo);
$titulos="impresoras;cambios";
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","MODIFICAR","","Listado");
grafico_barras($sql,"Impresoras","Cambios",$titulo);
un_boton("Volver","Volver","copanel.php");

raya();
$sql="select usuario_sol,count(*) as cambios from cambios where fecha >= date_sub(curdate(),interval 12 month) group by 1 order by 2 desc limit 10";
$titulo="Ranking top ten de cambios por usuarios ultimos 12 meses";
mi_titulo($titulo);
$titulos="usuarios;cambios";
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","MODIFICAR","","Listado");
grafico_barras($sql,"Usuarios","Cambios",$titulo);
un_boton("Volver","Volver","copanel.php");

raya();
$sql="select left(fecha,7) as fecha,sum(total_conf) as importe from pedido_cab where estado='FINALIZADO' and fecha>=date_sub(curdate(),interval 12 month) group by 1 order by 1";
$titulo="Compra Mensual de tinta";
mi_titulo($titulo);
$titulos="fecha;importe";
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","MODIFICAR","","Listado");
grafico_barras($sql,"Fecha","importe",$titulo);
un_boton("Volver","Volver","copanel.php");

?>
</BODY>
</HTML>
