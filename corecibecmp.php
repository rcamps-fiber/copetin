<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Recepci&#243;n de Pedido de compra</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("Recepci&oacute;n de pedidos de Compra");
$panta=$_POST["panta"];
switch($panta)
{
	case "confirma":
		$pedido=$_POST["pedido"];
		$estado=un_dato("select estado from pedido_cab where numero='$pedido'");
		if($estado<>"EMITIDO")
		{
			mensaje("Error: el pedido $pedido ya fue recibido anteriormente.");
			un_boton("volver","Volver","copanel.php");
			die();
		}
		//trace("Aca tengo que actualizar el stock con los nuevos cartuchos ingresados");
		$actu_stk_sql="select item,cod_int,cod_orig,cantidad,cant_recibida,cant_pendiente,cant_a_recibir from pedido_det where numero=$pedido and cant_pendiente>0";
		$actu_stk_qry=mi_query($actu_stk_sql,"Imposible buscar cartuchos para actualizar stock.");
		while($datos=mysql_fetch_array($actu_stk_qry))
		{
			$item=$datos["item"];
			$cod_int=$datos["cod_int"];
			$cod_orig=$datos["cod_orig"];
			$cantidad=$datos["cantidad"];
			$cant_recibida=$datos["cant_recibida"];
			$cant_pendiente=$datos["cant_pendiente"];
			$cant_a_recibir=$datos["cant_a_recibir"];
			if(is_null($cant_a_recibir))
				$cant_a_recibir=$cant_pendiente;
			$stock_ant=un_dato("select cantidad from stock where cartucho='$cod_int'");
			$updtstk_sql="update stock set cantidad=cantidad+'$cant_a_recibir' where cartucho=$cod_int";
			$updtstk_qry=mi_query($updtstk_sql,"Imposible actualizar el stock");
			$stock_nvo=un_dato("select cantidad from stock where cartucho='$cod_int'");
			$cant_pendiente=$cant_pendiente-$cant_a_recibir;
			$cant_recibida=$cant_recibida+$cant_a_recibir;
			mi_query("update pedido_det set cant_pendiente='$cant_pendiente',cant_recibida='$cant_recibida',cant_a_recibir=null where numero='$pedido' and item='$item'");
			mensaje("Cartucho $cod_orig: Stock anterior: $stock_ant + $cant_a_recibir = $stock_nvo unidad/es.");
		}
		$pendientes=un_dato("select sum(cant_pendiente) from pedido_det where numero='$pedido'");
		//die("Pendientes vale $pendientes");
		if($pendientes==0)
		{
			mi_query("update pedido_cab set estado='FINALIZADO' where numero=$pedido","coconficmp.php. Linea 64. Imposible actualizar el estado del pedido $pedido");
			// Actualizo la tabla de gastos
			$fecha_ing=un_dato("select fecha from pedido_cab where numero='$pedido'");
			$fecha_imput=$fecha_ing;
			$rubro="insumos";
			$proveedor=un_dato("select p.razon from proveedores p,pedido_cab c where c.numero='$pedido' and c.proveedor=p.codigo");
			$fac_numero="Pedido de tinta nro. $pedido";
			$qry_det=mi_query("select * from pedido_det where numero='$pedido'","Error al obtener el detalle del pedido");
			while($datos=mysql_fetch_array($qry_det))
			{
				$cantidad=$datos["cantidad"];
				$cod_orig=$datos["cod_orig"];
				$cod_int=$datos["cod_int"];
				$marca=un_dato("select marca from cartuchos where codigo_int='$cod_int'");
				$concepto="$cantidad cartuchos de tinta codigo $cod_orig $marca";
				$precio_conf=$datos["precio_conf"];
				$importe=$precio_conf*$cantidad/1.21;
				$observaciones=$datos["obs"];
				$importe_iva=$precio_conf*$cantidad*0.21;
				$importe_total=$precio_conf*$cantidad;
				mi_query("insert into gastos set fecha_ing='$fecha_ing',fecha_imput='$fecha_imput',rubro='$rubro',concepto='$concepto',importe='$importe',fac_numero='$fac_numero',proveedor='$proveedor',observaciones='$observaciones',importe_iva='$importe_iva',importe_total='$importe_total'","Error al agregar el item a la tabla de gastos");
			}
			
			mensaje("Pedido de compra $pedido est&aacute; FINALIZADO");
		}
		$usus_sql="select t1.cartucho,t1.usuario from solicitudes t1,pedido_det t2 where t1.estado='SIN STOCK' and t1.cartucho=t2.cod_int and t2.numero=$pedido";
		$usus_qry=mi_query($usus_sql,"coconficmp.php.Linea 23. Imposible obtener los usuarios sin stock");
		while($datos=mysql_fetch_array($usus_qry))
		{
			$usuario=$datos["usuario"];
			$cartucho=$datos["cartucho"];
			$dir_mail=un_dato("select email from usuarios where usuario='$usuario'");
			$nombre=un_dato("select nombre from usuarios where usuario='$uid'");
			$mensaje="El sistema COPETIN le informa que se ha recibido la compra de cartuchos";
			$mensaje.=" de tinta para impresoras inkjet, entre los cuales se incluye el que ud. ha solicitado.\n";
			$mensaje.="En breve se le va a instalar el cartucho nuevo.\nAtentamente, $nombre";
			mail($dir_mail,"Compra de cartuchos",$mensaje);
		}
		un_boton("aceptar","Aceptar","copanel.php");
		break;
	case "item":
		$item=$_POST["item"];
		$pedido=$_POST["pedido"];
		$cantidad=un_dato("select cantidad from pedido_det where numero='$pedido' and item=$item");
		$cant_pendiente=un_dato("select cant_pendiente from pedido_det where numero='$pedido' and item=$item");
		$cant_a_recibir=un_dato("select cant_a_recibir from pedido_det where numero='$pedido' and item='$item'");
		if(is_null($cant_a_recibir))
			$cant_a_recibir=$cant_pendiente;
		$cant_recibida=un_dato("select cant_recibida from pedido_det where numero='$pedido' and item='$item'");
		$precio=un_dato("select precio_conf from pedido_det where numero='$pedido' and item='$item'");
		$descripcion=un_dato("select c.descripcion from cartuchos c,pedido_det p where p.numero='$pedido' and p.item='$item' and p.cod_int=c.codigo_int");
		$titulo="Modificaci&oacute;n del Item $item: $descripcion del pedido $pedido";
		$campos="%OCU-item-$item";
		$campos.=";%CHK-anula-anula-0-n";
		$campos.=";%TXT-cantidad-cantidad-$cantidad-5";
		$campos.=";%TXT-pendiente-cant_pendiente-$cant_pendiente-5";
		$campos.=";%TXT-recibida-cant_recibida-$cant_recibida-5";
		$campos.=";%TXT-precio-precio-$precio-10";
		$campos.=";%OCU-pedido-$pedido";
		$campos.=";%OCU-panta-graba_item";
		$submit="aceptar-Confirmar-copanel.php";
		mi_panta($titulo,$campos,$submit);
		if($cantidad>0)
		{
			$titulo="Ingreso parcial del Item $item: $descripcion del pedido $pedido";
			$submit="aceptar-Confirmar-copanel.php";
			$campos="%OCU-item-$item";
			$campos.=";%TXT-cantidad-cant_a_recibir-$cant_a_recibir-5";
			$campos.=";%OCU-pedido-$pedido";
			$campos.=";%OCU-panta-parcial";
			mi_panta($titulo,$campos,$submit);
		}
		break;
	case "parcial":
		$pedido=$_POST["pedido"];
		//$cantidad=$_POST["cantidad"];
		$precio=$_POST["precio"];
		$cant_a_recibir=$_POST["cant_a_recibir"];
		$item=$_POST["item"];
		$descripcion=un_dato("select c.descripcion from cartuchos c,pedido_det p where p.item='$item' and p.cod_int=c.codigo_int");
		mi_query("update pedido_det set cant_a_recibir='$cant_a_recibir' where numero='$pedido' and item='$item'","Imposible actualizar item $item: $descripcion");
		mensaje("Se actualiz&oacute; el item $item: $descripcion del pedido $pedido.");
		un_boton("aceptar","Volver","","pedido","$pedido");
		break;
	case "graba_item":
		$pedido=$_POST["pedido"];
		$cantidad=$_POST["cantidad"];
		$precio=$_POST["precio"];
		$anula=$_POST["anula"];
		$item=$_POST["item"];
		$proveedor=un_dato("select proveedor from pedido_cab where numero='$pedido'");
		$cod_int=un_dato("select cod_int from pedido_det where numero='$pedido' and item='$item'");
		$cod_orig=un_dato("select cod_orig from pedido_det where numero='$pedido' and item='$item'");
		$descripcion=un_dato("select c.descripcion from cartuchos c,pedido_det p where p.item='$item' and p.cod_int=c.codigo_int");
		if($anula<>"0")
		{
			mi_query("update pedido_det set cantidad='$cantidad',cant_pendiente='$cant_pendiente',precio_conf='$precio' where numero='$pedido' and item='$item'","coconficmp.php. Linea 48. Imposible actualizar item $item");
			$existe=un_dato("select count(*) from precios where proveedor='$proveedor' and cod_int='$cod_int'");
			if($existe)
			{
				$cambio=un_dato("select count(*) from precios where proveedor='$proveedor' and cod_int='$cod_int' and precio<>'$precio'");
				if($cambio)
				{
					mi_query("update precios set precio='$precio' where cod_int='$cod_int' and proveedor='$proveedor' and precio<>'$precio'","Error al actualizar el precio de $cod_int para el proveedor $proveedor");
					mensaje("Se actualizo el precio para el prov. $proveedor y cart. $cod_orig");
				}
			}else
			{
				$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int='$cod_int'");
				mi_query("insert into precios set precio='$precio',cod_int='$cod_int',cod_prv='$cod_orig',proveedor='$proveedor',iva='$precio'*0.21","Error al agregar un precio");
				mensaje("Se agrego un registro de precio para el prov. $proveedor y cart. $cod_orig");
			}
			mensaje("Se actualiz&oacute; el item $item: $descripcion del pedido $pedido.");
		}else
		{
			mi_query("delete from pedido_det where numero='$pedido' and item='$item'","Imposible borrar item $item: $descripcion del pedido $pedido");
			mensaje("Se borr&oacute; el item $item: $descripcion del pedido $pedido.");
		}
	default:
		if(isset($_POST["numero"])){
		    $pedido=$_POST["numero"];
		}
		if(isset($_POST["pedido"]))
		{
			$pedido=$_POST["pedido"];
			//trace("El pedido es $pedido");
		}
		$titulos="item;cod.Veinfar;cod.orig;cod.prov.;marca;color;tipo;pendiente;a recibir;precio";
		$sql="select p.item,p.cod_int,p.cod_orig,p.cod_prv,c.marca,c.color,c.tipo,p.cant_pendiente,if(isnull(p.cant_a_recibir),p.cant_pendiente,p.cant_a_recibir) as a_recibir,p.precio_conf";
		$sql.=" from pedido_det p,cartuchos c where p.cod_int=c.codigo_int and p.numero='$pedido' and p.cant_pendiente>0 order by p.item;corecibecmp.php+item+pedido+$pedido+panta+item";
		$fecha=getdate();
		$que_fecha=$fecha["mday"] . "-" . $fecha["mon"] . "-" . $fecha["year"];
		$razon=un_dato("select p.razon from proveedores p,pedido_cab c where p.codigo=c.proveedor and c.numero='$pedido'");
		mi_titulo("Pedido Nro. $pedido<br><hr align='center' width='100px'>Fecha: $que_fecha - Proveedor $razon");
		//trace($sql);
		$hay=un_dato("select count(*) from pedido_det where numero='$pedido' and cant_pendiente>0");
		if($hay)
		{
			//trace($sql);
			tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;0;0;0;0;0;2");
			$total_conf=un_dato("select sum(precio_conf*cantidad) from pedido_det where numero=$pedido");
			if($total_conf=="")
				$total_conf=0;
			$iva_conf=$total_conf*0.105;
			$total=$total_conf+$iva_conf;
			mi_query("update pedido_cab set total_conf=$total_conf,iva_conf=$iva_conf where numero=$pedido","coconficmp.php. Linea 69. Imposible actualizar el total del pedido");
			echo("<table width='20%' align='right' border='1'>");
			echo("<tr><td><strong>Importe bruto</strong></td><td>" . number_format($total_conf,2,",",".") . "</td></tr>");
			echo("<tr><td><strong>IVA 10.5</strong></td><td><u>" . number_format($iva_conf,2,",","."). "</u></td></tr>");
			echo("<tr><td><strong>Total neto</strong></td><td>" . number_format($total,2,",",".") . "</td></tr>");
			mi_tabla("f");
			echo("<hr width='80%' align='left'><br><br><br><br>");		
		}else
		{
			mensaje("No hay items para mostrar");
		}
		$observaciones=un_dato("select observaciones from pedido_cab where numero='$pedido'");
		$titulo="";
		$campos="%ROT-Notas: ##$observaciones";
		$campos.=";%OCU-pedido-$pedido;%OCU-panta-confirma";
		$submit="aceptar-Confirmar-copanel.php";	
		mi_panta($titulo,$campos,$submit);
		break;
}
?>
</BODY>
</HTML>
