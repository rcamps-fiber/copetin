<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>ABM de impresora</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Aceptar-coabm_impresora.php";
$salir="aceptar-Aceptar-copanel.php";
if(isset($_GET["panta"]))
{
	$panta=$_GET["panta"];
}else
{
$panta=$_POST["panta"];
}
switch($panta)
{
	case "graba_alta":
		//trace("Estoy en graba alta");
		$marca=$_POST["marca"];
		$modelo=$_POST["modelo"];
		$descripcion=$_POST["descripcion"];
		$tipo=$_POST["tipo"];
		$activa=$_POST["activa"];
		trace($activa);
		$activa=($activa=="s") ? 1 : 0;
		trace($activa);
		mi_query("insert into impresoras set marca='$marca',modelo='$modelo',descripcion='$descripcion',tipo='$tipo',activa='$activa'","Error al agregar una impresora");
		mensaje("Se agrego una nueva impresora.");
		$codigo=mysql_insert_id();
		$puesto=$_POST["puesto"];
		if($puesto<>"Elegir")
		{
			mi_query("insert into puesto_imp set impresora='$codigo',puesto='$puesto'","Error al grabar el alta de la rel. impresora-puesto");
		}
	case "cartucho":
		if(isset($_GET["impresora"]))
		{
			$codigo=$_GET["codigo"];
			$marca=$_GET["marca"];
			$modelo=$_GET["modelo"];
			$tipo=$_GET["tipo"];
		}
		$titulo="Asignacion de cartuchos para la imp. $marca $modelo";
		$campos=";%SEL-cartucho-cartucho-select codigo_int,concat(c.codigo_orig,' ',c.marca,' ',c.color,' ',c.tipo,' ',c.descripcion) as descart from cartuchos c where c.marca='$marca' and c.tipo='$tipo' order by 2-descart+codigo_int-Elegir-Elegir";
		$campos.=";%OCU-codigo-$codigo";
		$campos.=";%OCU-marca-$marca";
		$campos.=";%OCU-modelo-$modelo";
		$campos.=";%OCU-tipo-$tipo";
		$campos.=";%OCU-panta-graba_cartucho";
		$submit="aceptar-Aceptar-copanel.php";
		mi_panta($titulo,$campos,$submit);
		$titulos="id;cartucho;descrip.cartucho";
		$sql="select x.cod_rel as cod_rel,c.codigo_orig,concat(c.marca,' ',c.color,' ',c.tipo,' ',c.descripcion) as descart";
		$sql.=" from impresoras i,cartuchos c,cart_imp x";
		$sql.=" where x.cod_cart=c.codigo_int and x.cod_imp=i.codigo and i.codigo='$codigo' order by 1;cocart_imp.php+cod_rel+panta+modi";
		mi_titulo("Lista de cartuchos/impresoras");
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Procesar");
		break;
	case "graba_cartucho":
		$codigo=$_POST["codigo"];
		$cartucho=$_POST["cartucho"];
		$marca=$_POST["marca"];
		$modelo=$_POST["modelo"];
		$tipo=$_POST["tipo"];
		$desccart=un_dato("select concat(marca,' ',color,' ',tipo,' ',descripcion) from cartuchos where codigo_int='$cartucho'");
		//$marca=un_dato("select concat(marca,' ',modelo,' ',descripcion) from impresoras where codigo='$codigo'");
		//trace($cartucho);
		if($cartucho<>"Elegir")
		{
			$no_existe=un_dato("select count(*) from cart_imp where cod_imp='$codigo' and cod_cart='$cartucho'");
			if($no_existe==0)
			{
				mi_query("insert into cart_imp set cod_imp='$codigo',cod_cart='$cartucho'","Error al grabar el alta de la rel. impresora-puesto");
				mensaje("Se agreg&oacute; la relaci&oacute;n $marca - $desccart a la tabla cart_imp.");
			}else
			{
				mensaje("La relaci&oacute;n $marca-$desccart ya exist&iacute;a en el sistema.");
			}
			delay("coabm_impresora.php?codigo=$codigo&panta=cartucho&marca=$marca&modelo=$modelo&tipo=$tipo");
		}else
		{
			delay();
		}
		break;
	case "modi":
		$codigo=$_POST["codigo"];
		$cons=mi_query("select * from impresoras where codigo='$codigo'","Error al obtener el registro");
		$datos=mysql_fetch_array($cons);
		$marca=$datos["marca"];
		$modelo=$datos["modelo"];
		$descripcion=$datos["descripcion"];
		$tipo=$datos["tipo"];
		$activa=$datos["activa"];
		$activa=($activa==1) ? 's' : 'n';
		$titulo="Modificacion de Impresora";
		$tit_modi="MODIFICACION DE IMPRESORA";
		$campos=";%ROT-Codigo##$codigo";
		$campos="%SEL-marca-marca-select nombre from marcas order by 1-nombre+nombre-$marca-$marca";
		$campos.=";%TXT-modelo-modelo-$modelo-50";
		$campos.=";%TXT-descripcion-descripcion-$descripcion-50";
		$campos.=";%SEL-tipo-tipo-$tipo+$tipo+TONER+TONER+INKJET+INKJET-0";
		$campos.=";%CHK-activa-activa-s-$activa";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-codigo-$codigo";
		$campos.=";%CHK-borrar-borrar-s-N";
		mi_panta($tit_modi,$campos,$submit);
		$hay=un_dato("select count(*) from cart_imp where cod_imp='$codigo'");
		if($hay)
		{
			$titulos="id;cartucho;descrip.cartucho";
			$sql="select x.cod_rel as cod_rel,c.codigo_orig,concat(c.marca,' ',c.color,' ',c.tipo,' ',c.descripcion) as descart";
			$sql.=" from impresoras i,cartuchos c,cart_imp x";
			$sql.=" where x.cod_cart=c.codigo_int and x.cod_imp=i.codigo and i.codigo='$codigo' order by 1;cocart_imp.php+cod_rel+panta+modi";
			mi_titulo("Lista de cartuchos/impresoras");
		}
		$hay=un_dato("select count(*) from puesto_imp where impresora='$codigo'");
		if($hay)
		{
			tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Procesar");
			$titulos="id;puesto";
			$sql="select x.cod as cod,p.descripcion";
			$sql.=" from impresoras i,puestos p,puesto_imp x";
			$sql.=" where x.puesto=p.codigo and x.impresora=i.codigo and i.codigo='$codigo' order by 1;copuesto_imp.php+cod+panta+modi";
			mi_titulo("Lista de impresoras/puestos");
			tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Procesar");
		}
		break;
	case "graba_modi":
		$codigo=$_POST["codigo"];
		$marca=$_POST["marca"];
		$modelo=$_POST["modelo"];
		$descripcion=$_POST["descripcion"];
		$tipo=$_POST["tipo"];
		$activa=$_POST["activa"];
		$activa=($activa=='s') ? 1 : 0;
		$borrar=$_POST["borrar"];
		if($borrar=="s")
		{
			mi_query("delete from impresoras where codigo='$codigo'","Error al borrar la impresora $marca $modelo");
			mensaje("Se borro el registro $codigo de la impresora $marca $modelo");
		}else
		{
			mi_query("update impresoras set  marca='$marca',modelo='$modelo',descripcion='$descripcion',tipo='$tipo',activa='$activa' where codigo='$codigo'","Error al modificar el registro de impresora $marca $modelo");
			mensaje("Modificaci&oacute;n de $marca $modelo grabada");
		}
		delay();
		break;
	default:
		mi_query("drop table if exists informe_impresoras");
		mi_query("create table informe_impresoras select codigo,marca,modelo,tipo from impresoras where activa");
		mi_query("alter table informe_impresoras add cartuchos text");
		mi_query("alter table informe_impresoras add usuarios text");
		mi_query("alter table informe_impresoras add recambio date");
		mi_query("alter table informe_impresoras add puesto text");
		$qry=mi_query("select * from cart_imp");
		while ($datos=mysql_fetch_array($qry))
		{
			$cod_rel=$datos["cod_rel"];
			$cod_imp=$datos["cod_imp"];
			$cod_cart=$datos["cod_cart"];
			//trace("$cod_rel $cod_imp $cod_cart");
			$codigo_cartucho=un_dato("select concat(codigo_orig,'/',codigo_corto) as codigo_cartucho from cartuchos where codigo_int='$cod_cart'");
			$color=un_dato("select color from cartuchos where codigo_int='$cod_cart'");
			$cartuchos="$codigo_cartucho $color";
			//trace("Actualizando $cod_imp con $cartuchos");
			mi_query("update informe_impresoras set cartuchos=concat(if(isnull(cartuchos),' ',concat(cartuchos,', ')),' $cartuchos') where codigo='$cod_imp'");
		}
		$qry=mi_query("select x.impresora,x.puesto,r.usuario from puesto_imp x,usu_puesto r,usuarios u where x.puesto=r.puesto and r.usuario=u.usuario and u.perfil<>8");
		while ($datos=mysql_fetch_array($qry))
		{
			$impresora=$datos["impresora"];
			$puesto=$datos["puesto"];
			$puesto_desc=un_dato("select descripcion from puestos where codigo='$puesto'");
			$usuario=$datos["usuario"];
			$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
			//trace("$impresora $puesto $usuario");
			mi_query("update informe_impresoras set usuarios=concat(if(isnull(usuarios),' ',concat(usuarios,', ')),' $nombre'),puesto='$puesto_desc' where codigo='$impresora'");
		}	
		$qry=mi_query("select codigo from informe_impresoras");
		while ($datos=mysql_fetch_array($qry))
		{
			$impresora=$datos["codigo"];
			$recambio=un_dato("select max(fecha) from solicitudes where impresora='$impresora' and estado='FINALIZADA'");
			mi_query("update informe_impresoras set recambio='$recambio' where codigo='$impresora'");
		}	
		$sql="select * from informe_impresoras";
		$titulos="codigo;marca;modelo;tipo;cartuchos;usuarios;ultimo recambio;puesto";
		mi_titulo("INFORME DE IMPRESORAS ACTIVAS");
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0",".","","","Impresoras;Impresoras;impresoras");
		un_boton("Volver","Volver","copanel.php");
		break;
}

?>
</BODY>
</HTML>
