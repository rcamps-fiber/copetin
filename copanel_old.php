<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");

?>
<HTML>

<HEAD>
<TITLE>Panel de Control</TITLE>
</HEAD>
<?
function linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje)
{
	echo("<ul><a href='copanel.php?ver_sol=$ver_sol&ver_sol_stk=$ver_sol_stk&ver_ped_rcb=$ver_ped_rcb&ver_ped_cnf=$ver_ped_cnf&ver_crt_fal=$ver_crt_fal&ver_otsol=$ver_otsol&ver_otprog=$ver_otprog&ver_otvenc=$ver_otvenc&ver_otros=$ver_otros'>$mensaje</a></ul>");
}

require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("Panel de Control");
if(isset($_GET["ver_sol"])): $ver_sol=$_GET["ver_sol"];else: $ver_sol=0;endif;
if(isset($_GET["ver_sol_stk"])): $ver_sol_stk=$_GET["ver_sol_stk"];else: $ver_sol_stk=0;endif;
if(isset($_GET["ver_ped_rcb"])): $ver_ped_rcb=$_GET["ver_ped_rcb"];else: $ver_ped_rcb=0;endif;
if(isset($_GET["ver_ped_cnf"])): $ver_ped_cnf=$_GET["ver_ped_cnf"];else: $ver_ped_cnf=0;endif;
if(isset($_GET["ver_crt_fal"])): $ver_crt_fal=$_GET["ver_crt_fal"];else: $ver_crt_fal=0;endif;
if(isset($_GET["ver_otsol"])): $ver_otsol=$_GET["ver_otsol"];else: $ver_otsol=0;endif;
if(isset($_GET["ver_otprog"])): $ver_otprog=$_GET["ver_otprog"];else: $ver_otprog=0;endif;
if(isset($_GET["ver_otros"])): $ver_otros=$_GET["ver_otros"];else: $ver_otros=0;endif;

// Actualizacion de solicitudes que figuraban sin stock y ahora hay
$cual_sql="select s.numero from solicitudes s,stock k where s.estado='SIN STOCK' and k.cantidad>0 and k.cartucho=s.cartucho";
$cual_qry=mi_query($cual_sql,"copanel.php. Linea 29. Imposible obtener registros para actualizar");
while($datos=mysql_fetch_array($cual_qry))
{
	$numero=$datos["numero"];
	mi_query("update solicitudes set estado='PENDIENTE' where numero=$numero","copanel.php.Linea 33. Imposible elegir registros para actualizar.");
}
//$solstk_sql="update solicitudes s,stock k set s.estado='PENDIENTE' where s.estado='SIN STOCK' and k.cantidad>0 and k.cartucho=s.cartucho";
//mi_query($solstk_sql,"copanel.php. Linea 27. Imposible actualizar estado de solicutudes sin stock.");

// Actualizacion de solicitudes que figuraban pendientes y ahora no tienen stock
$cual_sql="select s.numero from solicitudes s,stock k where s.estado='PENDIENTE' and k.cantidad=0 and k.cartucho=s.cartucho";
$cual_qry=mi_query($cual_sql,"copanel.php. Linea 29. Imposible obtener registros para actualizar");
while($datos=mysql_fetch_array($cual_qry))
{
	$numero=$datos["numero"];
	mi_query("update solicitudes set estado='SIN STOCK' where numero=$numero","copanel.php.Linea 33. Imposible elegir registros para actualizar.");
}
//$solstk_sql="update solicitudes s,stock k set s.estado='SIN STOCK' where s.estado='PENDIENTE' and k.cantidad=0 and k.cartucho=s.cartucho";
//mi_query($solstk_sql,"copanel.php. Linea 32. Imposible actualizar estado de solicutudes pendientes.");

// SOLICITUDES PENDIENTES
$solpend_sql="select count(*) from solicitudes where estado='PENDIENTE'";
$hay_sol_pen=un_dato($solpend_sql);
if($hay_sol_pen>0)
{

	if($ver_sol==0)
	{
		$mensaje="Hay $hay_sol_pen solicitud/es de recambio pendientes";
		linea_menu(1,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
	}else
	{
		$mensaje="Ocultar solicitudes de recambio pendientes";
		linea_menu(0,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
		$titulos="numero;fecha;usuario;puesto;impresora;cartucho;marca;color;observaciones;stock";
		$sql="select s.numero,s.fecha,u.nombre,p.descripcion as puesto,i.modelo as impresora,c.codigo_orig as codint,c.marca,c.color,s.observaciones,k.cantidad as stock from solicitudes s,puestos p,impresoras i,cartuchos c,usuarios u,stock k where s.estado='PENDIENTE'";
		$sql.=" and s.usuario=u.usuario and s.puesto=p.codigo and s.impresora=i.codigo and s.cartucho=c.codigo_int and s.cartucho=k.cartucho order by s.numero;corecambio.php+numero";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F",0,"ACCION","PROCESAR");
	}	
}

// SOLICITUDES SIN STOCK
$solstk_sql="select count(*) from solicitudes where estado='SIN STOCK'";
$hay_sol_stk=un_dato($solstk_sql);
if($hay_sol_stk>0)
{

	if($ver_sol_stk==0)
	{
		$mensaje="Hay $hay_sol_stk solicitud/es de recambio sin stock";
		linea_menu($ver_sol,1,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
	}else
	{
		$mensaje="Ocultar solicitudes de recambio sin stock";
		linea_menu($ver_sol,0,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
		$titulos="numero;fecha;usuario;puesto;impresora;cartucho;desc.cartucho;observaciones;stock";
		$sql="select s.numero,s.fecha,u.nombre,p.descripcion as puesto,i.modelo as impresora,c.codigo_orig as codint,c.descripcion as cartucho,s.observaciones,k.cantidad as stock from solicitudes s,puestos p,impresoras i,cartuchos c,usuarios u,stock k where s.estado='SIN STOCK'";
		$sql.=" and s.usuario=u.usuario and s.puesto=p.codigo and s.impresora=i.codigo and s.cartucho=c.codigo_int and s.cartucho=k.cartucho order by s.numero";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F",0);
	}	
}

// PEDIDOS DE COMPRA POR RECIBIR
$pedrcb_sql="select count(*) from pedido_cab where estado='EMITIDO'";
$hay_ped_rcb=un_dato($pedrcb_sql);
if($hay_ped_rcb>0)
{
	if($ver_ped_rcb==0)
	{
		$mensaje="Hay $hay_ped_rcb Pedido/s de Compra por recibir";
		linea_menu($ver_sol,$ver_sol_stk,1,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
	}else
	{
		$mensaje="Ocultar Pedidos de Compra por recibir";
		linea_menu($ver_sol,$ver_sol_stk,0,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
		$titulos="numero;fecha;usuario;proveedor;total_conf";
		$sql="select c.numero,c.fecha,u.nombre,p.razon,c.total_conf+c.iva_conf from pedido_cab c,usuarios ";
		$sql.="u,proveedores p where c.estado='EMITIDO' and c.usuario=u.usuario and ";
		$sql.="c.proveedor=p.codigo order by numero;corecibecmp.php+numero";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;2");
	}	
}
// PEDIDOS DE COMPRA POR CONFIRMAR
$pedcnf_sql="select count(*) from pedido_cab where estado='GENERADO'";
$hay_ped_cnf=un_dato($pedcnf_sql);
if($hay_ped_cnf>0)
{
	if($ver_ped_cnf==0)
	{
		$mensaje="Hay $hay_ped_cnf Pedido/s de Compra por Confirmar";
		linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,1,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
	}else
	{
		$mensaje="Ocultar Pedidos de Compra por Confirmar";
		linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,0,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
		$titulos="numero;fecha;usuario;proveedor;neto";
		$sql="select p.numero,p.fecha,u.nombre,r.razon,p.total_prev+p.iva_prev from pedido_cab p,proveedores r,usuarios u";
		$sql.=" where p.usuario=u.usuario and p.proveedor=r.codigo and estado='GENERADO' order by numero;coconficmp.php+numero";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;2");
	}	
}
// CARTUCHOS EN FALTA
/*
Conceptos
El campo pedir es el punto de reposici?n normal, es la primera alarma
El campo minimo es la segunda alarma, es decir cuando ya es urgente pedir.
El campo compra es el stock maximo, lo que queda luego de recibir el pedido.
*/

$crtfal_sql="select count(*) from stock s,minimos m where s.cartucho=m.cartucho and s.cantidad<=m.pedir";
$crtmin_sql="select count(*) from stock s,minimos m where s.cartucho=m.cartucho and s.cantidad<=m.minimo";
$hay_crt_fal=un_dato($crtfal_sql);
$hay_crt_min=un_dato($crtmin_sql);
//trace("La cantidad de cartuchos faltantes es: $hay_crt_fal");
if($hay_crt_fal>0)
{
	$borrar_tbl=mysql_query("drop table faltan_tmp");
	$faltan_sql="create table faltan_tmp select c.codigo_int,c.codigo_orig,c.marca,c.color,c.tipo,c.descripcion,";
	$faltan_sql.="s.cantidad,m.compra-s.cantidad as faltan,000000 as ya_pedido from cartuchos c,stock s,minimos m";
	$faltan_sql.=" where s.cartucho=m.cartucho and s.cantidad<=m.pedir and m.compra-s.cantidad >0 and s.cartucho=c.codigo_int order by 1";
	
	mi_query($faltan_sql,"copanel.php.Linea 148. Imposible crear tabla temporal de faltantes");
	$yapedidos_sql="select d.cod_int,sum(cantidad) as pedido from pedido_cab c,pedido_det d where d.numero=c.numero and c.estado='EMITIDO' group by 1";
	$yapedidos_qry=mi_query($yapedidos_sql,"copanel.php.Linea 151. Imposible obtener los pedidos.");
	while($datos=mysql_fetch_array($yapedidos_qry))
	{
		$cartucho=$datos["cod_int"];
		$pedido=$datos["pedido"];
		mi_query("update faltan_tmp set ya_pedido=$pedido where codigo_int=$cartucho","copanel.php.Linea 156. No se pudo actualizar tabla temporal de faltantes.");
	}
	if($ver_crt_fal==0)
	{
		if($hay_crt_min>0): $mensaje="Hacer pedido de cartuchos urgente (faltan $hay_crt_fal tipos de cartucho)"; else: $mensaje="Hay $hay_crt_fal Cartucho/s en falta"; endif;
		linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,1,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
	}else
	{
		$mensaje="Ocultar cartuchos en falta";
		linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,0,$ver_otsol,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
		$titulos="codigo Veinfar;codigo original;marca;color;tipo;descripcion;stock;pedir;ya pedido";
		$sql="select * from faltan_tmp order by 1";
		tabla_cons($titulos,$sql,1,"silver","#8EC99F",0);
		$cuantos_faltan_sql="select count(*) from faltan_tmp where faltan>ya_pedido";
		$hay_que_pedir=un_dato($cuantos_faltan_sql);
		//trace("Pedir: $hay_que_pedir");
		if($hay_que_pedir>0)
			un_boton("aceptar","'Hacer pedido'","copedido.php");
	}	
}
// ORDENES DE TRABAJO SOLICITADAS
$hay_otsol=un_dato("select count(*) from soltrab where estado=1");
if($hay_otsol)
{
	if($ver_otsol==0)
	{
		$mensaje="Hay $hay_otsol Ordenes de Trabajo solicitadas";
		linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,1,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
	}else
	{
		$mensaje="Ocultar Ordenes de Trabajo solicitadas";
		linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,0,$ver_otprog,$ver_otvenc,$ver_otros,$mensaje);
		mi_titulo("Programacion de Ordenes de Trabajo");
		$rotulos="OT;USUARIO;PUESTO;UBICACION;FECHA SOL.;TIPO PROBLEMA;DISPOSITIVO;DESCRIPCION";
		$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_sol,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado=1 and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo order by s.id_sol;coprogot.php+id_sol+panta+detalle";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="PROGRAMAR";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
	}
}

// ORDENES DE TRABAJO PROGRAMADAS
$hay_otprog=un_dato("select count(*) from soltrab where estado=2 and tecnico='$uid'");
if($hay_otprog)
{
	if($ver_otprog==0)
	{
		$mensaje="Hay $hay_otprog Ordenes de Trabajo programadas";
		linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,1,$ver_otvenc,$ver_otros,$mensaje);
	}else
	{
		$mensaje="Ocultar Ordenes de Trabajo programadas";
		linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,0,$ver_otvenc,$ver_otros,$mensaje);
		mi_titulo("Ejecucion de Ordenes de Trabajo");
		$rotulos="OT;USUARIO;PUESTO;UBICACION;FECHA PROGRAMADA;TIPO PROBLEMA;DISPOSITIVO;DESCRIPCION";
		$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_prog,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado=2 and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo and s.tecnico='$uid' order by s.id_sol;coejecot.php+id_sol+panta+detalle";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="MODIFICAR";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
	}
}
// Ordenes de trbajo vencidas
$hay_otvenc=un_dato("select count(*) from soltrab where estado in(2,3) and fecha_prog<curdate()");
if($hay_otvenc)
{
	if($ver_otvenc==0)
	{
		$mensaje="Hay $hay_otvenc Ordenes de Trabajo vencidas";
		linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,1,$ver_otros,$mensaje);
	}else
	{
		$mensaje="Ocultar Ordenes de Trabajo vencidas";
		linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,0,$ver_otros,$mensaje);
		mi_titulo("Ordenes de Trabajo vencidas");
		$rotulos="OT;USUARIO;PUESTO;UBICACION;FECHA PROGRAMADA;TIPO PROBLEMA;DISPOSITIVO;DESCRIPCION";
		$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_prog,t.problema,d.dispositivo,s.descripcion from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado in(2,3) and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo and s.fecha_prog<curdate() order by s.id_sol";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales);
	}
}


// Otras funciones
if($ver_otros==0)
{
	$mensaje="M&aacute;s funciones";
	linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,1,$mensaje);
}else
{
	$mensaje="Ocultar funciones";
	linea_menu($ver_sol,$ver_sol_stk,$ver_ped_rcb,$ver_ped_cnf,$ver_crt_fal,$ver_otsol,$ver_otprog,$ver_otvenc,0,$mensaje);
	echo("<ul><ul><a href='coprecios.php'>Actualizar lista de precios</a></ul>");
	echo("<ul><a href='costock.php'>Ajuste de stock</a></ul>");
	echo("<ul><a href='cousuarios.php'>Administraci&oacute;n de usuarios</a></ul>");
	echo("<ul><a href='cousu_puesto.php'>Asignaci&oacute;n de usuarios a puestos</a></ul>");
	echo("<ul><a href='cocart_imp.php'>Asignaci&oacute;n de cartuchos a impresoras</a></ul>");
	echo("<ul><a href='copuesto_imp.php'>Asignaci&oacute;n de impresoras a puestos</a</ul>");
	echo("<ul><a href='coabm_ot.php'>Consulta y Modificacion de Ordenes de Trabajo</a</ul>");
	echo("<ul><a href='coabm_generico.php'>Abm Gen&eacute;rico</a</ul></ul></ul>");
}	

?>
</body>
</html>
