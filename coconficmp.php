<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Confirmacion de Pedido de compra</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("Confirmaci&oacute;n de pedidos de Compra");
$volver=home($uid);
$submit="aceptar-Confirmar-$volver";
$panta=$_POST["panta"];
switch($panta)
{
	case "anula":
		$pedido=$_POST["pedido"];
		$anula=$_POST["anula"];
		if($anula=="1")
		{
			mi_query("update pedido_cab set estado='ANULADO' where numero='$pedido'","coconficmp.php. Imposible anular el pedido $pedido");
			mensaje("Pedido de compra $pedido est&aacute; ANULADO");
			un_boton("aceptar","Aceptar",$volver);
		}else
		{
			mensaje("No se hace ningun cambio");
			un_boton("Volver","Volver","coconficmp.php","panta;pedido","nada;$pedido");
		}
		break;
	case "confirma":
		require_once 'Mail.php';
		require_once("Mail/mime.php");	
		$pedido=$_POST["pedido"];
		$observaciones=$_POST["observaciones"];
		mi_query("update pedido_cab set estado='EMITIDO',observaciones='$observaciones' where numero='$pedido'","coconficmp.php. Imposible actualizar el estado del pedido $pedido");
		mensaje("Pedido de compra $pedido est&aacute; EMITIDO");
		$usus_sql="select t1.cartucho,t1.usuario from solicitudes t1,pedido_det t2 where t1.estado='SIN STOCK' and t1.cartucho=t2.cod_int and t2.numero='$pedido'";
		$usus_qry=mi_query($usus_sql,"coconficmp.php. Imposible obtener los usuarios sin stock");
		while($datos=mysql_fetch_array($usus_qry))
		{
			$usuario=$datos["usuario"];
			$cartucho=$datos["cartucho"];
			$dir_mail=un_dato("select email from usuarios where usuario='$usuario'");
			$nombre=un_dato("select nombre from usuarios where usuario='$uid'");
			$mensaje="El sistema COPETIN le informa que se ha encargado la compra de cartuchos";
			$mensaje.=" de impresion, entre los cuales esta el que ud. ha solicitado.\n";
			$mensaje.="En cuanto se reciba el pedido recibira otro aviso.\nAtentamente, $nombre";
			mail($dir_mail,"Compra de cartuchos",$mensaje);
		}
		$proveedor=un_dato("select proveedor from pedido_cab where numero='$pedido'");
		$razon=un_dato("select p.razon from proveedores p,pedido_cab c where p.codigo=c.proveedor and c.numero='$pedido'");
		// Codigo para el envio de mail a compras
		$nom_admin=un_dato("select nombre from usuarios where perfil=7");
		$admin=un_dato("select usuario from usuarios where perfil=7");			
		$email=un_dato("select email from usuarios where usuario='$uid'");
		$nombre=un_dato("select nombre from usuarios where usuario='$uid'");
		$email_admin=un_dato("select email from usuarios where usuario='$admin'");
		$cab_qry=mi_query("select c.fecha,p.razon,c.iva_conf,c.total_conf from pedido_cab c,proveedores p where numero='$pedido' and c.proveedor=p.codigo","Error al buscar el pedido");
		$datos=mysql_fetch_array($cab_qry);
		$fecha=a_fecha_arg($datos["fecha"]);
		$proveedor=$datos["razon"];
		$iva_conf=$datos["iva_conf"];
		$total_conf=$datos["total_conf"];
		$mensaje="<html>";
		$mensaje.='<div id="logo"><img src="imagenes/logo_copetin.jpeg" align="left" border=0></div>';
		$mensaje.='<div id="cuerpo"><h3>Sistema Copetin</h3>';
		$mensaje.="Estimado/a $nom_admin<br>Solicitamos la compra de los siguientes cartuchos:";
		$mensaje.="<br>Pedido $pedido - fecha: $fecha - Prov. $proveedor";
		$mensaje.="<br><table border='1'><tr><td>item</td><td>cod.orig</td><td>cod.int.</td><td>cod.prov.</td><td>marca</td><td>color</td><td>tipo</td><td>cantidad</td><td>precio</td><td>observaciones</td></tr>";
		$version_texto="Estimado/a $nom_admin\nSolicitamos la compra de los siguientes cartuchos:\nPedido $pedido - fecha: $fecha - Prov. $proveedor\n";
		$version_texto.="item - cod.orig -  - cod.int. - cod.prov. - marca - color - tipo ";
		$version_texto.="- cantidad - precio - observaciones";
		$det_qry=mi_query("select p.item,p.cod_orig,p.cod_int,p.cod_prv,c.marca,c.color,c.tipo,p.cantidad,p.precio_conf,p.obs from pedido_det p,cartuchos c where numero='$pedido' and p.cod_int=c.codigo_int","Error al obtener el detalle del pedido $pedido");
		while($datos=mysql_fetch_array($det_qry))
		{
			$item=$datos["item"];
			$cod_orig=$datos["cod_orig"];
			$cod_int=$datos["cod_int"];
			$cod_prv=$datos["cod_prv"];
			$marca=$datos["marca"];
			$color=$datos["color"];
			$tipo=$datos["tipo"];
			$cantidad=$datos["cantidad"];
			$precio_conf=$datos["precio_conf"];
			$obs=$datos["obs"];
			$mensaje.="<tr><td>$item</td><td>$cod_orig</td><td>$cod_int</td><td>$cod_prv</td>";
			$mensaje.="<td>$marca</td><td>$color</td><td>$tipo</td><td>$cantidad</td>";
			$mensaje.="<td>$precio_conf</td><td>$obs</td></tr>";
			$version_texto.="$item - $cod_orig - $cod_int - $cod_prv - $marca - $color - $tipo -";
			$version_texto.="$cantidad - $precio - $obs\n";
		}
		$mensaje.="</table>";
		$mensaje.="<br>Importe bruto: " . number_format($total_conf,2,",",".");
		$mensaje.="<br>Iva: " . number_format($iva_conf,2,",",".");
		$total=$total_conf+$iva_conf;
		$mensaje.="<br>Neto: " . number_format($total,2,",",".");
		$observaciones=un_dato("select observaciones from pedido_cab where numero='$pedido'");
		$mensaje.="<br>Notas: $observaciones";
		$mensaje.="<br><br>Atentamente.";
		$mensaje.="</div></html>";
		
		$version_texto.="\nImporte bruto: " . number_format($total_conf,2,",",".");
		$version_texto.="\nIva: " . number_format($iva_conf,2,",",".");
		$version_texto.="\nNeto: " . number_format($total,2,",",".");
		$version_texto.="\n\nNotas: $observaciones";
		$version_texto.="\n\nAtentamente.";
		//$destino = $email . "," . $email_admin;
		$destino = $email;
		//$encabezado['From']    = $email_admin;
		$encabezado['From']    = $email;
		$encabezado['To']      = $email;
		$encabezado['Subject'] = 'Orden de Compra de Cartuchos inkjet/toner';
		$cuerpo = $mensaje;
		$params['sendmail_path'] = '/usr/lib/sendmail';
		// Crear el objeto correo con el metodo Mail::factory
		$imagen="imagenes/logo_copetin.jpeg";
		$mime= new Mail_mime("\n");
		$mime->setTXTBody($version_texto);
		$mime->setHTMLBody($mensaje);
		$mime->addHTMLImage($imagen," image/jpeg");
		$cuerpo=$mime->get();
		$hdrs=$mime->headers($encabezado);
		$mail=& Mail::factory('sendmail',$params);
		$mail->send($destino,$hdrs,$cuerpo);	
		//mensaje("Se envio mail a $destino");
		//echo($mensaje);
		// Fin codigo para mail a compras
		delay($volver);
		//un_boton("aceptar","Aceptar",$volver);
		break;
	case "seguir":
		if(isset($_POST["senia"]))
		{
			//trace("Estoy en seguir");
			$pedido=$_POST["pedido"];
			$codigo_int=$_POST["codigo_int"];
			$cantidad=$_POST["cantidad"];
			$precio=$_POST["precio"];
			$proveedor=$_POST["proveedor"];
			$razon=un_dato("select razon from proveedores where codigo='$proveedor'");
			$item=un_dato("select max(item)+1 from pedido_det where numero='$pedido'");
			$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int='$codigo_int'");
			$cod_prv=un_dato("select cod_prv from precios where cod_int='$codigo_int' and proveedor='$proveedor'");
			$tipo=$_POST["tipo"];
			$obs=$_POST["obs"];
			//trace("Las observ son $obs");
			$item_sql="insert into pedido_det set numero='$pedido',item='$item',cod_orig='$cod_orig',cod_int='$codigo_int',cod_prv='$cod_prv',cantidad='$cantidad',cant_pendiente='$cantidad',precio_conf='$precio',obs='$obs'";
			trace($item_sql);
			mi_query($item_sql,"copedido.php. Imposible agregar un item al pedido.");
		}
		mostrarPedido($pedido);
		break;
	case "item":
		$item=$_POST["item"];
		$pedido=$_POST["pedido"];
		$cantidad=un_dato("select cantidad from pedido_det where numero='$pedido' and item='$item'");
		$precio=un_dato("select precio_conf from pedido_det where numero='$pedido' and item='$item'");
		$obs=un_dato("select obs from pedido_det where numero='$pedido' and item='$item'");
		$cod_orig=un_dato("select cod_orig from pedido_det where numero='$pedido' and item='$item'");
		$marca=un_dato("select marca from cartuchos where codigo_int='$cod_int'");
		$color=un_dato("select color from cartuchos where codigo_int='$cod_int'");
		$titulo="Modificaci&oacute;n del Item $item del pedido $pedido";
		$campos="%ROT-Cartucho $marca $cod_orig $color;";
		$campos.=";%OCU-item-$item";
		$campos.=";%CHK-anula-anula-0-n";
		$campos.=";%TXT-cantidad-cantidad-$cantidad-5";
		$campos.=";%TXT-precio-precio-$precio-10";
		$campos.=";%ARE-observaciones-obs-$obs-4-50";
		$campos.=";%OCU-pedido-$pedido";
		$campos.=";%OCU-panta-graba_item";
		mi_panta($titulo,$campos,$submit);
		break;
	case "graba_item":
		$pedido=$_POST["pedido"];
		$cantidad=$_POST["cantidad"];
		$precio=$_POST["precio"];
		$obs=$_POST["obs"];
		$anula=$_POST["anula"];
		$item=$_POST["item"];
		if($anula<>"0")
		{
			mi_query("update pedido_det set cantidad='$cantidad',precio_conf='$precio',obs='$obs' where numero='$pedido' and item='$item'","coconficmp.php. Imposible actualizar item ''$item''");
			// Si cambio el precio lo actualizo en la tabla de precios
			$cod_int=un_dato("select cod_int from pedido_det where numero='$pedido' and item='$item'");
			$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int='$cod_int'");
			$proveedor=un_dato("select proveedor from pedido_cab where numero='$pedido'");
			$existe=un_dato("select count(*) from precios where proveedor='$proveedor' and cod_int='$cod_int'");
			if($existe)
			{
				mi_query("update precios set precio='$precio' where cod_int='$cod_int' and proveedor='$proveedor' and precio<>'$precio'","Error al actualizar el precio de $cod_int para el proveedor $proveedor");
				mensaje("Se actualizo el precio para el prov. $proveedor y cart. $cod_orig");
			}else
			{
				$cod_orig=un_dato("select codigo_orig from cartuchos where codigo_int='$cod_int'");
				mi_query("insert into precios set precio='$precio',cod_int='$cod_int',cod_prv='$cod_orig',proveedor='$proveedor'","Error al agregar un precio");
				mensaje("Se agrego un registro de precio para el prov. $proveedor y cart. $cod_orig");
			}
			mensaje("Se actualiz&oacute; el item $item del pedido $pedido.");
		}else
		{
			mi_query("delete from pedido_det where numero='$pedido' and item='$item'","coconficmp.php. Imposible borrar item $item del pedido $pedido");
			mensaje("Se borr&oacute; el item $item del pedido $pedido.");
		}
		mostrarPedido($pedido);
		break;	
	default:
		if(!isset($pedido))
		{
			$pedido=$_POST["numero"];
			mi_query("update pedido_det set precio_conf=precio_prev where numero='$pedido'","coconficmp.php. Imposible actualizar precio confirmado");
		}
		mostrarPedido($pedido);
		/*
		$titulos="item;cod.Veinfar;cod.orig;cod.prov.;marca;color;tipo;cant.;precio;observ.";
		$sql="select p.item,p.cod_int,p.cod_orig,p.cod_prv,c.marca,c.color,c.tipo,p.cantidad,p.precio_conf,p.obs";
		$sql.=" from pedido_det p,cartuchos c where p.cod_int=c.codigo_int and p.numero='$pedido' order by p.item;coconficmp.php+item+pedido+$pedido+panta+item";
		$fecha=getdate();
		$que_fecha=$fecha["mday"] . "-" . $fecha["mon"] . "-" . $fecha["year"];
		$razon=un_dato("select p.razon from proveedores p,pedido_cab c where p.codigo=c.proveedor and c.numero='$pedido'");
		mi_titulo("Pedido Nro. $pedido<br><hr align='center' width='100px'>Fecha: $que_fecha - Proveedor $razon");
		//trace($sql);
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;0;0;0;0;2");
		$total_conf=un_dato("select sum(precio_conf*cantidad) from pedido_det where numero='$pedido'");
		if($total_conf=="")
			$total_conf=0;
		$iva_conf=$total_conf*0.105;
		$total=$total_conf+$iva_conf;
		mi_query("update pedido_cab set total_conf=$total_conf,iva_conf=$iva_conf where numero='$pedido'","coconficmp.php. Imposible actualizar el total del pedido");
		$observaciones=un_dato("select observaciones from pedido_cab where numero='$pedido'");
		echo("<table width='20%' align='right' border='1'>");
		echo("<tr><td><strong>Importe bruto</strong></td><td>" . number_format($total_conf,2,",",".") . "</td></tr>");
		echo("<tr><td><strong>IVA 10.5</strong></td><td><u>" . number_format($iva_conf,2,",","."). "</u></td></tr>");
		echo("<tr><td><strong>Total neto</strong></td><td>" . number_format($total,2,",",".") . "</td></tr>");
		mi_tabla("f");
		echo("<hr width='80%' align='left'><br><br><br><br>");
		$titulo="";
		$campos="%ARE-notas complementarias-observaciones-$observaciones-2-50";
		//$campos="%ROT-Nota: $observaciones";
		$campos.=";%OCU-pedido-$pedido;%OCU-panta-confirma";
		mi_panta($titulo,$campos,$submit);
		echo("<hr>");
		$titulo="Anulaci&oacute;n de Pedido Nro. $pedido";
		$campos=";%CHK-anula-anula-1-n";
		$campos.=";%OCU-pedido-$pedido;%OCU-panta-anula";
		mi_panta($titulo,$campos,$submit);
		 */
		break;
}
?>
</BODY>
</HTML>
