<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Estadisticas de soporte</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$anio=a_fecha_sistema(hoy());
$anio=substr($anio,0,4);

mi_titulo("Estad&iacute;sticas de Soporte T&eacute;cnico del a&ntilde;o $anio");
$casos=un_dato("select count(*) from soltrab where left(fin,4)>=left(curdate(),4)");
mi_titulo("Total de Casos del a&ntilde;o: $casos");

$sql="select t.problema as problema,count(*) as casos from soltrab s,tipo_problema t where s.tipo_problema=t.id and t.id<>8 and left(s.fin,4)>=left(curdate(),4) group by 1 order by 2 desc limit 10";
$titulos="Problema;Casos";
mi_titulo("Ranking top ten de tipos de problema");
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","MODIFICAR","","Listado");
grafico_barras($sql,"Tipos de problema","casos","Ranking de tipos de problema");
un_boton("Volver","Volver","copanel.php");

raya();
mi_titulo("Ranking top ten de usuarios");
$sql="select usuario,count(*) as casos from soltrab where tipo_problema<>8 and left(fin,4)>=left(curdate(),4) group by 1 order by 2 desc limit 10";
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","MODIFICAR","","Listado");
grafico_barras($sql,"Usuarios","solicitudes","Ranking de usuarios");
un_boton("Volver","Volver","copanel.php");

raya();
mi_titulo("Ranking top ten de dispositivos");
$sql="select d.dispositivo,count(*) as casos from soltrab s,dispositivo d where s.dispositivo=d.id and s.tipo_problema<>8 and left(s.fin,4)>=left(curdate(),4) group by 1 order by 2 desc limit 10";
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","MODIFICAR","","Listado");
grafico_barras($sql,"Dispositivo","casos","Ranking de dispositivos");
un_boton("Volver","Volver","copanel.php");
$sql="select avg(datediff(fecha_mod,fecha_sol)) from soltrab where estado=4";
$rta0=un_dato($sql);
$rta0f=number_format($rta0,"1",",",".");

raya();
mi_titulo("En promedio las solicitudes se resuelven en:");

$sql="select tecnico,avg(datediff(fecha_mod,fecha_sol)) from soltrab where estado=4 and tecnico in('alejandro','rcamps') and left(fin,4)>=left(curdate(),4) group by 1 union select 'TODOS' as tecnico,avg(datediff(fecha_mod,fecha_sol)) from soltrab where estado=4";
$titulos="tecnico;dias";
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;1","ACTUALIZ.","MODIFICAR","","Listado");
grafico_barras($sql,"Tecnico","dias","Tiempo de resolucion de trabajos",2,200,300);
un_boton("Volver","Volver","copanel.php");

?>
</BODY>
</HTML>
