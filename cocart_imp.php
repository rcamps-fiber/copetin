<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Relacion de impresoras y cartuchos</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("RELACION DE IMPRESORAS Y CARTUCHOS");
$panta=$_POST["panta"];
switch($panta)
{
	case "modi":
		$cod_rel=$_POST["cod_rel"];
		$impresora=un_dato("select cod_imp from cart_imp where cod_rel='$cod_rel'");
		$cartucho=un_dato("select cod_cart from cart_imp where cod_rel='$cod_rel'");
		$desccart=un_dato("select concat(marca,' ',color,' ',tipo,' ',descripcion) from cartuchos where codigo_int='$cartucho'");
		$marca=un_dato("select concat(marca,' ',modelo,' ',descripcion) from impresoras where codigo='$impresora'");		
		$titulo="Modificaci&oacute;n de relacion entre impresoras y cartuchos";
		$campos_pantalla="%SEL-impresora-impresora-select codigo,concat(marca,' ',modelo,' ',descripcion) as descrip from impresoras where activa order by 1-descrip+codigo-$marca-$impresora";
		$campos_pantalla.=";%SEL-cartucho-cartucho-select codigo_int,concat(c.marca,' ',c.color,' ',c.tipo,' ',c.descripcion) as descart from cartuchos c order by 2-descart+codigo_int-$desccart-$cartucho";
		$campos_pantalla.=";%CHK-elimina-elimina-1-n";
		$campos_pantalla.=";%OCU-cod_rel-$cod_rel";
		$campos_pantalla.=";%OCU-panta-graba_modi";
		$submit="aceptar-Grabar-copanel.php";	
		$submit="aceptar-Modificar-cocart_imp.php";	
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
	case "graba_modi":
		$cod_rel=$_POST["cod_rel"];
		$impresora=$_POST["impresora"];
		$cartucho=$_POST["cartucho"];
		$desccart=un_dato("select concat(marca,' ',color,' ',tipo,' ',descripcion) from cartuchos where codigo_int='$cartucho'");
		$marca=un_dato("select concat(marca,' ',modelo,' ',descripcion) from impresoras where codigo='$impresora'");		
		$elimina=$_POST["elimina"];
		if($elimina=="1")
		{
			mi_query("delete from cart_imp where cod_rel='$cod_rel'","Imposible eliminar la relacion $marca - $desccart.");
			mensaje("Se elimin&oacute; la relaci&oacute;n $marca - $desccart");
		}else
		{
			mi_query("update cart_imp set cod_imp='$impresora',cod_cart='$cartucho' where cod_rel='$cod_rel'","Imposible actualizar la relacion $marca - $desccart.");
			mensaje("Se actualiz&oacute; la relaci&oacute;n $marca - $desccart.");
		}
		un_boton();
		break;
	case "graba_alta":
		$impresora=$_POST["impresora"];
		$cartucho=$_POST["cartucho"];
		$desccart=un_dato("select concat(marca,' ',color,' ',tipo,' ',descripcion) from cartuchos where codigo_int='$cartucho'");
		$marca=un_dato("select concat(marca,' ',modelo,' ',descripcion) from impresoras where codigo='$impresora'");
		$no_existe=un_dato("select count(*) from cart_imp where cod_imp='$impresora' and cod_cart='$cartucho'");
		if($no_existe==0)
		{
			mi_query("insert into cart_imp set cod_imp='$impresora',cod_cart='$cartucho'","Error al grabar el alta de la rel. impresora-puesto");
			mensaje("Se agreg&oacute; la relaci&oacute;n $marca - $desccart a la tabla cart_imp.");
		}else
		{
			mensaje("La relaci&oacute;n $marca-$desccart ya exist&iacute;a en el sistema.");
		}
		un_boton();
		break;
	default:
		$titulo="Alta de relacion entre impresoras y cartuchos";
		$campos_pantalla="%SEL-impresora-impresora-select codigo,concat(marca,' ',modelo,' ',descripcion) as descrip from impresoras where activa order by 1-descrip+codigo-Elegir-Elegir";
		$campos_pantalla.=";%SEL-cartucho-cartucho-select codigo_int,concat(c.codigo_orig,' ',c.marca,' ',c.color,' ',c.tipo,' ',c.descripcion) as descart from cartuchos c order by 2-descart+codigo_int-Elegir-Elegir";
		$campos_pantalla.=";%OCU-panta-graba_alta";
		$submit="aceptar-Grabar-copanel.php";	
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
}
$titulos="id;cartucho;descrip.cartucho;impresora";
$sql="select x.cod_rel as cod_rel,c.codigo_orig,concat(c.marca,' ',c.color,' ',c.tipo,' ',c.descripcion) as descart,concat(i.marca,' ',i.modelo,' ',i.descripcion) as impresora";
$sql.=" from impresoras i,cartuchos c,cart_imp x";
$sql.=" where i.activa and x.cod_cart=c.codigo_int and x.cod_imp=i.codigo order by 1;cocart_imp.php+cod_rel+panta+modi";
mi_titulo("Lista de cartuchos/impresoras");
tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","Procesar");
?>
</BODY>
</HTML>
