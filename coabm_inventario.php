
<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Inventario de PC");
require_once("cobody.php");
require_once("cocnx.php");
$home=home($uid);
$mismo=home();
$submit="aceptar-Aceptar-$mismo";
mi_titulo("Inventario de PC y equipamiento informatico");
$panta=$_POST["panta"];
switch($panta)
{
	case "graba_alta":
		$responsable=$_POST["responsable"];
		$ip=$_POST["ip"];
		trace("La nueva ip es $ip");
		$edificio=$_POST["edificio"];
		$oficina=$_POST["oficina"];
		$puesto=$_POST["puesto"];
		$procesador=$_POST["procesador"];
		$memoria=$_POST["memoria"];
		$disco_rigido=$_POST["disco_rigido"];
		$sistema_operativo=$_POST["sistema_operativo"];
		$cdrom=$_POST["cdrom"];
		$monitor=$_POST["monitor"];
		$impresora=$_POST["impresora"];
		$otros=$_POST["otros"];
		$nombre_red=$_POST["nombre_red"];
		$software=$_POST["software"];
		$servidor=$_POST["servidor"];
		$servidor=($servidor=="s") ? 1 : 0;
		if($edificio=="Elegir")
			$edificio=1;
		if($sistema_operativo=="Elegir")
			$sistema_operativo=10;
		if($puesto=="Elegir")
			$puesto=16;		
		$ya_esta=un_dato("select count(*) from inventario_pc where responsable='$responsable' and ip='$ip' and edificio='$edificio' and oficina='$oficina' and puesto='$puesto' and procesador='$procesador' and memoria='$memoria' and disco_rigido='$disco_rigido' and sistema_operativo='$sistema_operativo' and cdrom='$cdrom' and monitor='$monitor' and impresora='$impresora' and otros='$otros'");
		if($ya_esta)
		{
			mensaje("Registro ya grabado");
			delay();
		}else
		{
			mi_query("insert into inventario_pc set responsable='$responsable',ip='$ip',edificio='$edificio',oficina='$oficina',puesto='$puesto',procesador='$procesador',memoria='$memoria',disco_rigido='$disco_rigido',sistema_operativo='$sistema_operativo',cdrom='$cdrom',monitor='$monitor',impresora='$impresora',otros='$otros',nombre_red='$nombre_red',fecha_mod=curdate(),software='$software',servidor='$servidor'","Error al agregar una PC");
			mensaje("Se agrego una nueva PC.");
			delay();
		}
		break;
	case "modi":
		$id_pc=$_POST["id_pc"];
		$cons=mi_query("select * from inventario_pc where id_pc='$id_pc'","Error al obtener el registro");
		$datos=mysql_fetch_array($cons);
		$responsable=$datos["responsable"];
		$ip=$datos["ip"];
		$edificio=$datos["edificio"];
		$desc_edificio=un_dato("select nombre from edificio where id='$edificio'");
		$oficina=$datos["oficina"];
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$procesador=$datos["procesador"];
		$memoria=$datos["memoria"];
		$disco_rigido=$datos["disco_rigido"];
		$sistema_operativo=$datos["sistema_operativo"];
		$desc_sisop=un_dato("select sistema from sistema_operativo where id_sis='$sistema_operativo'");
		$cdrom=$datos["cdrom"];
		$monitor=$datos["monitor"];
		$impresora=$datos["impresora"];
		$desc_impre=un_dato("select modelo from impresoras where codigo='$impresora'");
		$otros=$datos["otros"];
		$nombre_red=$datos["nombre_red"];
		$nombre_red=strtr($nombre_red,"-","_");
		$fecha_mod=a_fecha_arg(($datos["fecha_mod"]));
		$servidor=$datos["servidor"];
		//trace("El servidor tenia valor $servidor");
		$servidor=($servidor==0) ? "n" : "s";
		//trace("Ahora le puse $servidor");
		$software=$datos["software"];
		$titulo="Modificacion de Inventario de PCs";
		$tit_modi="MODIFICACION DE INVENTARIO";
		$campos=";%ROT-Id. registro</td><td><strong>$id_pc";
		$campos.=";%TXT-nombre de red-nombre_red-$nombre_red-20";
		$campos.=";%TXT-responsable-responsable-$responsable-30";
		$campos.=";%TXT-ip-ip-$ip-15";
		$campos.=";%SEL-edificio-edificio-select id,nombre from edificio order by 1-nombre+id-$desc_edificio-$edificio";
		//$campos.=";%TXT-edificio-edificio-$edificio-20";
		$campos.=";%TXT-oficina-oficina-$oficina-30";
		$campos.=";%SEL-puesto-puesto-select codigo,descripcion from puestos order by 1-descripcion+codigo-$desc_puesto-$puesto";
		//$campos.=";%TXT-puesto-puesto-$puesto-40";
		$campos.=";%TXT-procesador-procesador-$procesador-15";
		$campos.=";%TXT-memoria-memoria-$memoria-15";
		$campos.=";%TXT-disco rigido-disco_rigido-$disco_rigido-15";
		$campos.=";%SEL-sistema_operativo-sistema operativo-select id_sis,sistema from sistema_operativo order by 1-sistema+id_sis-$desc_sisop-$sistema_operativo";
		//$campos.=";%TXT-sistema operativo-sistema_operativo-$sistema_operativo-20";
		$campos.=";%TXT-cdrom-cdrom-$cdrom-20";
		$campos.=";%TXT-monitor-monitor-$monitor-15";
		//$campos.=";%TXT-impresora-impresora-$impresora-30";
		$campos.=";%SEL-impresora-impresora-select codigo,modelo from impresoras order by 1-modelo+codigo-$desc_impre-$impresora";
		$campos.=";%ARE-lista de software-software-$software-5-80";
		$campos.=";%ARE-otros-otros-$otros-5-80";
		$campos.=";%CHK-es un servidor-servidor-s-$servidor";
		$campos.=";%ROT-Ultima modificacion: $fecha_mod";
		//$campos.=";%ARE-descripcion-descripcion-$descripcion-4-50";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-id_pc-$id_pc";
		$campos.=";%CHK-borrar-borrar-s-N";
		mi_panta($tit_modi,$campos,$submit);
		break;
	case "graba_modi":
		$id_pc=$_POST["id_pc"];
		$responsable=$_POST["responsable"];
		$ip=$_POST["ip"];
		$edificio=$_POST["edificio"];
		$oficina=$_POST["oficina"];
		$puesto=$_POST["puesto"];
		$procesador=$_POST["procesador"];
		$memoria=$_POST["memoria"];
		$disco_rigido=$_POST["disco_rigido"];
		$sistema_operativo=$_POST["sistema_operativo"];
		$cdrom=$_POST["cdrom"];
		$monitor=$_POST["monitor"];
		$impresora=$_POST["impresora"];
		$otros=$_POST["otros"];
		$nombre_red=$_POST["nombre_red"];
		$nombre_red=strtr($nombre_red,"_","-");
		$servidor=$_POST["servidor"];
		//trace("Servidor: $servidor");
		$servidor=($servidor=="s") ? 1 : 0;
		//trace("ahora Servidor: $servidor");
		//die();
		$software=$_POST["software"];
		$borrar=$_POST["borrar"];
		if($borrar=="s")
		{
			mi_query("delete from inventario_pc where id_pc='$id_pc'","Error al borrar el registro");
			mensaje("Se borro el registro $id_pc");
		}else
		{
			//trace("ip $ip. Edif. $edificio. Nombre de red $nombre_red");
			$ya_esta_ip=un_dato("select count(*) from inventario_pc where ip='$ip' and id_pc<>'$id_pc'");
			//trace($ya_esta);
			//die();
			if($ya_esta_ip)
			{
				mensaje("Ip $ip ya ocupada por otro equipo con id. $id_pc");
				delay();
			}else
			{
				$ip_ant=un_dato("select ip from inventario_pc where id_pc='$id_pc'");
				if($ip<>$ip_ant)
				{
					$admin=un_dato("select usuario from usuarios where perfil=1");
					$asunto="Modificacion de IP";
					$texto="El sistema Copetin le informa que el tecnico $uid ha modificado la ip de la pc $nombre_red de $ip_ant a $ip.";
					$aquien=$admin;
					 mandar_mail($aquien,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
				}
				$nombre_red=strtr($nombre_red,"_","-");
				mi_query("update inventario_pc set responsable='$responsable',ip='$ip',edificio='$edificio',oficina='$oficina',puesto='$puesto',procesador='$procesador',memoria='$memoria',disco_rigido='$disco_rigido',sistema_operativo='$sistema_operativo',cdrom='$cdrom',monitor='$monitor',impresora='$impresora',otros='$otros',nombre_red='$nombre_red',fecha_mod=curdate(),software='$software',servidor='$servidor' where id_pc='$id_pc'","Error al modificar el registro de inventario de pc");
				mensaje("Modificaci&oacute;n de $id_pc grabada");
			}
		}
		delay();
		break;
	case "alta":
		$submit="aceptar-Aceptar-$home";
		// Armo la lista de ips disponibles por edificio
		$elegir_ip=";%SEL-ip-ip-";
		$qry_edif=mi_query("select id,nombre from edificio","Error al consultar los edificios");
		while($datos=mysql_fetch_array($qry_edif))
		{
			$id_edif=$datos["id"];
			$nombre_edif=$datos["nombre"];
			for($i=1;$i<255;$i++)
			{
				$ip=($id_edif==2) ? "192.168.2." . $i :  "192.168.1." . $i;
				$nova=un_dato("select count(*) from inventario_pc where concat(ip,edificio)=concat('$ip','$id_edif')");
				if(!$nova)
					$elegir_ip.="$ip" . "+" . "$ip . $nombre_edif+";
			}
		}
		$elegir_ip.="-0-elegir+elegir";
		//mensaje($elegir_ip);
		$tit_alta="ALTA DE EQUIPO";
		$campos.=";%TXT-nombre de red-nombre_red--20";
		$campos.=";%TXT-responsable-responsable--30";
		$campos.=$elegir_ip;
		//$campos.=";%TXT-ip-ip--15";
		$campos.=";%SEL-edificio-edificio-select id,nombre from edificio order by 1-nombre+id";
		//$campos.=";%TXT-edificio-edificio--20";
		$campos.=";%TXT-oficina-oficina--30";
		$campos.=";%SEL-puesto-puesto-select codigo,descripcion from puestos order by 1-descripcion+codigo";		//$campos.=";%TXT-puesto-puesto--40";
		$campos.=";%TXT-procesador-procesador--15";
		$campos.=";%TXT-memoria-memoria--15";
		$campos.=";%TXT-disco rigido-disco_rigido--15";
		$campos.=";%SEL-sistema_operativo-sistema operativo-select id_sis,sistema from sistema_operativo order by 1-sistema+id_sis";
		//$campos.=";%TXT-sistema operativo-sistema_operativo--20";
		$campos.=";%TXT-cdrom-cdrom--20";
		$campos.=";%TXT-monitor-monitor--15";
		//$campos.=";%TXT-impresora-impresora--30";
		$campos.=";%SEL-impresora-impresora-select codigo,modelo from impresoras order by 1-modelo+codigo";
		$campos.=";%ARE-lista de software-software--5-80";
		$campos.=";%ARE-otros-otros--5-80";
		$campos.=";%CHK-es un servidor-servidor-s-N";
		$campos.=";%OCU-panta-graba_alta";
		mi_panta($tit_alta,$campos,$submit);
		break;
	case "consulta":
		$titulo="Filtro de consulta";
		$campos.=";%SEL-ip-ip-select distinct ip from inventario_pc order by 1-ip";
		$campos.=";%SEL-nombre_red-nombre_de_red-select distinct nombre_red from inventario_pc order by 1-nombre_red";
		$campos.=";%SEL-responsable-responsable-select distinct responsable from inventario_pc order by 1-responsable";
		$campos.=";%SEL-edificio-edificio-select id,nombre from edificio order by 1-nombre+id";
		$campos.=";%SEL-oficina-oficina-select distinct oficina from inventario_pc order by 1-oficina";
		$campos.=";%SEL-puesto-puesto-select codigo,descripcion from puestos order by 1-descripcion+codigo";
		$campos.=";%SEL-sistema operativo-sistema_operativo-select id_sis,sistema from sistema_operativo order by 1-sistema+id_sis";
		$campos.=";%TXT-software-software--15";
		$campos.=";%TXT-otros-otros--15";
		$campos.=";%SEL-servidor-funcion-Elegir+Elegir+0+PC Escritorio+1+Servidor-0";
		$campos.=";%OCU-panta-resultado";
		mi_panta($titulo,$campos,$submit);
		break;	
	case "resultado":
		$ip=$_POST["ip"];
		$nombre_red=$_POST["nombre_red"];
		$responsable=$_POST["responsable"];
		$edificio=$_POST["edificio"];
		$oficina=$_POST["oficina"];
		$puesto=$_POST["puesto"];
		$sistema_operativo=$_POST["sistema_operativo"];
		$software=$_POST["software"];
		$otros=$_POST["otros"];
		$servidor=$_POST["servidor"];
		//trace("Servidor es $servidor");
		$filtro="";
		$descrip_filtro="";
		if($ip<>"Elegir")
		{
			$filtro=" and ip='$ip'";
			$descrip_filtro="- Ip: $ip. ";
		}
		if($nombre_red<>"Elegir")
		{
			$filtro.=" and nombre_red='$nombre_red'";
			$descrip_filtro.="- Nombre de red: $nombre_red";
		}
		if($responsable<>"Elegir")
		{
			$filtro.=" and responsable='$responsable'";
			$descrip_filtro.="- Responsable: $responsable";
		}
		if($edificio<>"Elegir")
		{
			$filtro.=" and edificio='$edificio'";
			$desc_edificio=un_dato("select nombre from edificio where id='$edificio'");
			$descrip_filtro.="- Edificio: $desc_edificio";
		}	
		if($oficina<>"Elegir")
		{
			$filtro.=" and oficina='$oficina'";
			$descrip_filtro.="- Oficina: $oficina";
		}
		if($puesto<>"Elegir")
		{
			$filtro.=" and puesto='$puesto'";
			$descrip_filtro.="- Puesto: $puesto";
		}
		if($sistema_operativo<>"Elegir")
		{
			$filtro.=" and sistema_operativo='$sistema_operativo'";
			$descrip_filtro.="- Sistema Operativo: $sistema_operativo";
		}
		if($software<>"")
		{
			$filtro.=" and instr(software,'$software')";
			$descrip_filtro.="- Incluyendo el software $software";
		}
		if($otros<>"")
		{
			$filtro.=" and instr(otros,'$otros')";
			$descrip_filtro.="- Incluyendo $otros";
		}
		switch($servidor)
		{
			case "Elegir":
				break;
			case "0":
				$filtro.=" and servidor<>1";
				$descrip_filtro.="- Equipos de escritorio";
				break;
			case "1":
				$filtro.=" and servidor=1";
				$descrip_filtro.="- Servidores";
				break;
			default:
				$filtro.=" and servidor<>1";
				$descrip_filtro.="- Equipos de escritorio";
				break;
		}
		if($descrip_filtro=="")
			$descrip_filtro="Listado Completo";
		$titulos="id;nombre de red;responsable;ip;edificio;oficina;puesto;actualizado";
		$hay=un_dato("select count(*) from inventario_pc");
		if($hay)
		{
			raya();
			mi_query("update inventario_pc set edificio=1 where edificio=0");
			mi_query("update inventario_pc set puesto=16 where puesto=0");
			mi_query("update inventario_pc set sistema_operativo=10 where sistema_operativo=0");
			$sql="select i.id_pc,i.nombre_red,i.responsable,i.ip,e.nombre,oficina,p.descripcion,i.fecha_mod from inventario_pc i,puestos p,edificio e,sistema_operativo s where i.puesto=p.codigo and s.id_sis=i.sistema_operativo and e.id=i.edificio $filtro order by i.id_pc;coabm_inventario.php+id_pc+panta+modi";
			mi_titulo("Mostrar $descrip_filtro");
			tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;0;2;2;0","ACTUALIZ.","MODIFICAR","","Inventario de PCs;Inventario de PCs;inventario_pc");
		}else
		{
			mensaje("No hay datos para mostrar");
		}
		un_boton("Volver","Volver",$mismo);
		break;
	default:
		$submit="aceptar-Aceptar-$home";
		$campos.=";%SEL-panta-modo-consulta+consulta+alta+alta-0";
		mi_panta($titulo,$campos,$submit);
}

cierre();
?>