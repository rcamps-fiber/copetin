<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Registro de dispositivos quemados");
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Aceptar-copanel.php";
mi_titulo("Registro de dispositivos quemados");
$panta=$_POST["panta"];
switch($panta)
{
	case "graba_alta":
		$equipo=$_POST["equipo"];
		$observaciones=$_POST["observaciones"];
		$fecha_incidente=a_fecha_sistema($_POST["fecha_incidente"]);
		$fecha_ingreso=a_fecha_sistema($_POST["fecha_ingreso"]);
		$responsable=$_POST["responsable"];
		$edificio=$_POST["edificio"];
		mi_query("insert into quemados set equipo='$equipo',observaciones='$observaciones',fecha_incidente='$fecha_incidente',fecha_ingreso='$fecha_ingreso',responsable='$responsable',edificio='$edificio'");
		mensaje("Se agrego un nuevo dispositivo quemado.");
		delay();
		break;
	case "modi":
		$id_quemado=$_POST["id_quemado"];
		$cons=mi_query("select * from quemados where id_quemado='$id_quemado'","Error al obtener el registro");
		$datos=mysql_fetch_array($cons);
		$equipo=$datos["equipo"];
		$descripcion=un_dato("select descripcion from equipo where id_equipo='$equipo'");
		$observaciones=$datos["observaciones"];
		$fecha_incidente=a_fecha_arg($datos["fecha_incidente"]);
		$fecha_ingreso=a_fecha_arg($datos["fecha_ingreso"]);
		$usuario=$datos["responsable"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$id=$datos["edificio"];
		$edificio=un_dato("select nombre from edificio where id='$id'");
		$titulo="Modificacion de dispositivo quemado";
		$tit_modi="MODIFICACION DE DISPOSITIVO QUEMADO";
		$campos=";%ROT-Id.</td><td><strong>$id_quemado";
		$campos.=";%TXT-equipo-equipo-$equipo-30";
		$campos.=";%ARE-observaciones-observaciones-$observaciones-1-50";
		$campos.=";%FEC-fecha incidente-fecha_incidente-$fecha_incidente-10";
		$campos.=";%SEL-responsable-responsable-select usuario,nombre from usuarios order by 2-nombre+usuario-$nombre-$usuario";
		$campos.=";%SEL-edificio-edificio-select id,nombre from edificio order by 2-nombre+id-$edificio-$id";
		$campos.=";%FEC-fecha ingreso-fecha_ingreso-$fecha_ingreso-10";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-id_quemado-$id_quemado";
		$campos.=";%CHK-borrar-borrar-s-N";
		$submit="aceptar-Aceptar-coabm_quemados.php";
		mi_panta($tit_modi,$campos,$submit);
		break;
	case "graba_modi":
		$id_quemado=$_POST["id_quemado"];
		$equipo=$_POST["equipo"];
		$observaciones=$_POST["observaciones"];
		$fecha_incidente=a_fecha_sistema($_POST["fecha_incidente"]);
		$fecha_ingreso=a_fecha_sistema($_POST["fecha_ingreso"]);
		$responsable=$_POST["responsable"];
		$edificio=$_POST["edificio"];
		$borrar=$_POST["borrar"];
		if($borrar=="s")
		{
			mi_query("delete from quemados where id_quemado='$id_quemado'","Error al borrar el registro");
			mensaje("Se borro el registro $id_quemado");
		}else
		{
			mi_query("update quemados set equipo='$equipo',observaciones='$observaciones',fecha_incidente='$fecha_incidente',responsable='$responsable',edificio='$edificio',fecha_ingreso='$fecha_ingreso' where id_quemado='$id_quemado'","Error al modificar quemados");
			mensaje("Modificaci&oacute;n de $id_quemado grabada");
		}
		delay();
		break;
	default:
		$hoy=hoy();
		$tit_alta="NUEVO DISPOSITIVO QUEMADO";
		$campos="%ROT-FECHA ALTA:##$hoy";
		$campos.=";%TXT-equipo-equipo--30";
		$campos.=";%ARE-observaciones-observaciones--1-50";
		$campos.=";%FEC-fecha incidente-fecha incidente--10";
		$campos.=";%SEL-responsable-responsable-select usuario,nombre from usuarios order by 2-nombre+usuario";
		$campos.=";%SEL-edificio-edificio-select id,nombre from edificio order by 2-nombre+id";
		$campos.=";%OCU-fecha_ingreso-$hoy";
		$campos.=";%OCU-panta-graba_alta";
		mi_panta($tit_alta,$campos,$submit);
		$hay=un_dato("select count(*) from quemados");
		if($hay)
		{
			//raya();
			$hoy_sis=a_fecha_sistema($hoy);
			//trace("hoy: $hoy_sis");
			$este_mes=substr($hoy_sis,0,7);
			//trace("Este mes $este_mes");
			$quemados_mes=un_dato("select count(*) from quemados where left(fecha_incidente,7)='$este_mes'");
			//mensaje("Cantidad de incidentes del mes: $quemados_mes");
			$este_anio=substr($hoy_sis,0,4);
			$quemados_anio=un_dato("select count(*) from quemados where left(fecha_incidente,4)='$este_anio'");
			mensaje("Cantidad de incidentes del mes: $quemados_mes. Del a&ntilde;o: $quemados_anio");
			//raya();
			$titulos="id;equipo;detalle;fecha incidente;fecha ingreso;responsable;edificio";
			$sql="select q.id_quemado,q.equipo,q.observaciones,q.fecha_incidente,q.fecha_ingreso,u.nombre as usuario,e.nombre as edificio from quemados q,edificio e,usuarios u where q.edificio=e.id and q.responsable=u.usuario order by q.fecha_incidente;coabm_quemados.php+id_quemado+panta+modi";
			mi_titulo($subtitulo);
			tabla_cons($titulos,$sql,1,"silver","#8EC99F",0,"ACTUALIZ.","MODIFICAR","","Quemados;Quemados;Quemados");
		}else
		{
			mensaje("No hay dispositivos quemados para mostrar");
		}
		un_boton("Volver","Volver","copanel.php");
		break;
}
cierre();
?>