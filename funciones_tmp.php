<?
function mi_panta($titulo,$campos,$submit,$ventana="",$ajax=0)
{
	mi_titulo(strtr($titulo,"+",";"));
	$multipart=(!(strpos($campos,"%FIL")=== false)) ? "MULTI" : "";
	if($ventana<>"")
	{
		if($ajax)
			mi_form("w,$multipart","cualquiera",1);
		else
			mi_form("w,$multipart");
	}else
	{
		if($ajax)
			mi_form("i,$multipart","cualquiera",1);
		else
			mi_form("i,$multipart");
	}
	mi_tabla("i",0);
	$entrada=explode(";",$campos);
	foreach($entrada as $celda)
	{
		$largo=strlen($celda);
		// El campo es un select
		if(!(strpos($celda,"%SEL")=== false))
		{
			$param_sel=explode("-",$celda);
			$campo=$param_sel[1];
			$rotulo=$param_sel[2];
			$sql=$param_sel[3];
			$columna=$param_sel[4];
			if(count($param_sel)==5)
			{			//echo("Equipo: <input type='text' name='equipo' value='$equipo' size='30'  onChange='pedirGrabar()'>");

				$def_rot="Elegir";
				$def_cod="Elegir";
			}else
			{
				$def_rot=$param_sel[5];
				$def_cod=$param_sel[6];
			}
			if($columna=="0")
			{
				$sql=strtr($sql,"+",";");
			}else
			{
				$columna=strtr($columna,"+",";");
			}
			//echo("<tr><td>$campo - $rotulo - $sql - $columna</td></tr>");
			mi_select($campo,$rotulo,$sql,$columna,$def_rot,$def_cod);
			//mi_tabla("i",0);
		}
		// El campo es un check box
		if(!(strpos($celda,"%CHK")=== false))
		{
			$param_chk=explode("-",$celda);
			$rotulo=$param_chk[1];
			$campo=$param_chk[2];
			$ini=$param_chk[3];
			$check=$param_chk[4];
			//echo("<tr><td>$rotulo - $cmapo - $ini - $check</td></tr>");
			mi_box($rotulo,$campo,$ini,$check);
			//mi_tabla("i",0);
		}
		// El campo es un texto
		if(!(strpos($celda,"%TXT")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=$param_txt[1];
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			$largo=$param_txt[4];
			$evento=$param_txt[5];
			$funcion=$param_txt[6];
			mi_text($rotulo,$campo,$ini,$largo,$evento,$funcion);
			//echo("<td><input type='text' name='$celda'></td></tr>");
		}
		if(!(strpos($celda,"%PWD")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=$param_txt[1];
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			$largo=$param_txt[4];
			mi_password($rotulo,$campo,$ini,$largo);
			//echo("<td><input type='text' name='$celda'></td></tr>");
		}
		// El campo es hidden
		if(!(strpos($celda,"%OCU")=== false))
		{
			$param_ocu=explode("-",$celda);
			$campo=$param_ocu[1];
			$valor=$param_ocu[2];
			mi_oculto($campo,$valor);
			// echo("<input type='hidden' name='$campo' value='$valor'>");
		}
		// El campo es solamente un rotulo
		if(!(strpos($celda,"%ROT")=== false))
		{
			$param_rot=explode("-",$celda);
			$rotulo=$param_rot[1];
			$rotulo=strtr($rotulo,"+",";");
			$rotulo=str_replace("@","&nbsp;",$rotulo);
			if(strpos($rotulo,"<tr>") === false)
				$rotulo="<tr><td><strong>" . $rotulo . "</strong></td></tr>";
			$rotulo=str_replace("##","</td><td>",$rotulo);
			echo($rotulo);
		}
		if(!(strpos($celda,"%ARE")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=$param_txt[1];
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			$filas=$param_txt[4];
			$columnas=$param_txt[5];
			mi_textarea($rotulo,$campo,$ini,$filas,$columnas);
			//echo("<td><input type='text' name='$celda'></td></tr>");
		}
		if(!(strpos($celda,"%FEC")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=strtoupper($param_txt[1]);
			$campo=$param_txt[2];
			$ini=$param_txt[3];
			if($ini==0 or $ini=="")
				$ini=un_dato("select curdate()");
			
			if((strpos($ini,"/")==4 or strpos($ini,"-")==4) and (strpos($ini,"/",5)==7 or strpos($ini,"-",5)==7))
			{
				$ini=a_fecha_arg($ini);
			}
			mi_text($rotulo,$campo,$ini,10);
		}
		if(!(strpos($celda,"%FIL")=== false))
		{
			$param_txt=explode("-",$celda);
			$rotulo=strtoupper($param_txt[1]);
			mi_file($rotulo);
		}
	}
	mi_tabla("f",1);
	echo("<br>");
	if(!(strpos($submit,"-")=== false))
	{
		$param_submit=explode("-",$submit);
		$variable=$param_submit[0];
		$rotulo=$param_submit[1];
		$anterior=$param_submit[2];
		$evento=$param_submit[3];
		$funcion=$param_submit[4];	
	}else
	{
		$variable="primera";
		$anterior=$submit;
		$evento="";
		$funcion="";
	}
	botones_submit($variable,$rotulo,$anterior,$evento,$funcion);
}
	
	
function mi_form($que_parte,$nombre_form="cualquiera",$ajax=0)
{
	if(!(strpos($que_parte,"i")=== false) or !(strpos($que_parte,"w")=== false))
	{
		if($ajax)
		{
			$accion="";
		}else
		{
			$esta_pagina=$_SERVER['PHP_SELF'];
			$accion=" action='$esta_pagina'";
		}
		if($que_parte=="w")
		{
			if(!(strpos($que_parte,"multi")=== false))
			{
				echo("<form method='post' name='$nombre_form' $action target='_blank'>");
			}else
			{
				echo("<form method='post' name='$nombre_form' $action enctype='multipart/form-data' target='_blank'>");
			}
		}else
		{
			if(!(strpos($que_parte,"multi")=== false))
			{
				echo("<form method='post' name='$nombre_form' $action>");
			}else
			{
				echo("<form method='post' name='$nombre_form' $action enctype='multipart/form-data'>");
			}
		}
	}else
	{
		echo("</form>");
	}
}



function mi_box($rotulo,$campo,$ini,$check)
{
//	Agrega un campo check box a un formulario
//	Requiere una tabla abierta y un formulario abierto
//	Agrega una fila a la tabla
	echo("<tr><td><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td>");
	echo("<td><input type='checkbox' name='$campo' value='$ini'");
	if($check=="s")
	{
		echo("checked");
	}
	echo("></td></tr>");
}
function mi_text($rotulo,$campo,$ini,$largo,$evento,$funcion)
{
//	Agrega un campo de texto a un formulario. 
//	Requiere que exista una tabla abierta y un formulario abierto
//	Agrega una fila a la tabla
	echo("<tr><td><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td>");
	if($evento<>"")
	{
		echo("<td><input type='text' name='$campo' value='$ini' size='$largo' $evento='$funcion();'></td></tr>");
	}else
	{
		echo("<td><input type='text' name='$campo' value='$ini' size='$largo' ></td></tr>");
	}
}

function mi_password($rotulo,$campo,$ini,$largo)
{
//	Agrega un campo de texto a un formulario. 
//	Requiere que exista una tabla abierta y un formulario abierto
//	Agrega una fila a la tabla
	echo("<tr><td><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td>");
	echo("<td><input type='password' name='$campo' value='$ini' size='$largo'></td></tr>");
}

function mi_oculto($campo,$valor)
{
//	Agrega un campo oculto en un formulario, para pasar valores ya definidos
	echo("<input type='hidden' name='$campo' value='$valor'>");
}
function mi_select_celda($campo,$sql,$columna)
{
// Funcion similar a la anterior pero sin el rotulo.
// En este caso no agrega una fila sino que usa la fila actual de la tabla
// y solamente agrega una celda para la lista desplegable
// Tampoco admite una columna para rotulos y otra para datos. Devuelve lo
// mismo que se muestra en la lista.
	echo("<td><select name='$campo'>");
	if($columna=="0")
	{
		$alista=explode(";",$sql);
		$total=count($alista);
		for( $i=0 ;$i<$total ; $i+2 )
		{
			$item=$alista[$i];
			echo("<option value='$item'>");
			$i++;
			$item=$alista[$i];
			echo("$item\n");
			$i++;
		}
	}else
	{		
		$lista=mysql_query($sql) or die("Error: no se pudo armar la lista $lista");
		echo("<option selected value='Elegir'>Elegir\n");
		while ($i=mysql_fetch_array($lista))
		{
			$dato=$i[$columna];
			echo("<option value='$dato'>$dato\n");
		}
	}	
	echo("</select></td>");
}	

function mi_select($campo,$rotulo,$sql,$columna,$default_rot="Elegir",$default_cod="Elegir")
{
	echo("<tr><td><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td>");
	echo("<td><select name='$campo'>");
	if($columna=="0")
	{
		$alista=explode(";",$sql);
		$total=count($alista);
		for( $i=0 ;$i<$total ; $i+2 )
		{
			$item=$alista[$i];
			echo("<option value='$item'>");
			$i++;
			$item=$alista[$i];
			echo("$item\n");
			$i++;
		}
	}else
	{		
		$lista=mysql_query($sql) or die("Error: no se pudo armar la lista $lista");
		$cols=explode(";",$columna);
		$total=count($cols);
		$mostrar=$cols[0];
		$resultado=$cols[1];
		if(strpos($columna,";")=== false)
		{
			$resultado=$mostrar;
		}
		echo("<option selected value='$default_cod'>$default_rot\n");
		while ($i=mysql_fetch_array($lista))
		{
			$dat=$i[$resultado];
			//$dat=$dat*1;
			$rot=$i[$mostrar];
			echo("<option value='" . $dat . "'>$rot\n");
		}
	}	
	echo("</select></td></tr>");
}	

function mi_textarea($rotulo,$campo,$ini='',$filas=4,$columnas=20)
{
//	Agrega un campo de text_area a un formulario.
//	Requiere que exista una tabla abierta y un formulario abierto
//	Agrega una fila a la tabla
	echo("<tr><td colspan=2><strong>" . strtoupper(htmlspecialchars($rotulo)) . "</strong></td></tr>");
	echo("<tr><td colspan=2><TEXTAREA name='$campo' rows='$filas' cols='$columnas'>$ini</TEXTAREA></td></tr>");
}




?>