<?
include_once('coacceso.php');
include_once('cofunciones_especificas.php');
include_once('cofunciones.php');
echo("<HTML><HEAD><TITLE>Modulo de comunicaciones COPETIN</TITLE></HEAD>");
require_once("cobody.php");
require_once("cocnx.php");
//trace("<font align='right'>$panta</font>");
mi_titulo("Mensajes");
//$volver
$home=home($uid);
$submit="submit-Aceptar-$home";
if(isset($_GET["volver"]))
{
	$volver=$_GET["volver"];
}else
{
	if(isset($_POST["volver"]))
	{
		$volver=$_POST["volver"];
	}else
	{
		$volver="coforo.php";
	}
}
if(isset($_GET["panta"]))
{
	$panta=$_GET["panta"];
}else
{
	$panta=$_POST["panta"];
}
switch($panta)
{
case "detalle_conv":
	if(isset($_GET["id_conv"]))
	{
		$id_conv=$_GET["id_conv"];
	}else
	{
		$id_conv=$_POST["id_conv"];
	}
	$tema=un_dato("select tema from conversacion where id_conv='$id_conv'");
	$tema=htmlentities(strtr($tema,"-","#"));
	$tit_mensaje="TEMA: $tema";
	$campos.=";%ARE-mensaje nuevo-mensaje--5-80";
	$campos.=";%SEL-estado-estado-VISIBLE+VISIBLE+OCULTO+OCULTO-0-VISIBLE-VISIBLE";
	$campos.=";%SEL-modo_cre-modo-OCULTABLE+OCULTABLE+NO OCULTABLE+NO OCULTABLE-0-OCULTABLE-OCULTABLE";
	$campos.=";%SEL-para-destinatario-select usuario,nombre from usuarios order by 2-nombre+usuario-Rodrigo Camps-rcamps";
	$campos.=";%OCU-panta-procesar_mensaje";
	$campos.=";%OCU-volver-$home";
	$campos.=";%OCU-id_conv-$id_conv";
	$submit="submit-Aceptar-$home";
	mi_panta($tit_mensaje,$campos,$submit);
	echo("<hr>");
	$rotulos="id;fecha;de;para;mensaje;rta. a msje.";
	$sql="select id_foro,fecha,usuario,para,mensaje,respuesta from foro where conversacion='$id_conv' and (estado<>'OCULTO' or estado is null) and (modo_usu<>'LEIDO' or modo_usu is null) and (vencimiento>=curdate() or vencimiento is null) and (usuario='$uid' or para='$uid') order by id_foro desc;coforo.php+id_foro+panta+ocultar+volver+$volver";
	tabla_cons($rotulos,$sql,"","","","0","ESTADO","ver/responder","");
	echo("<hr>");
	break;
case "procesar_mensaje":
	$id_conv=$_POST["id_conv"];
	$mensaje=htmlentities(strtr($_POST["mensaje"],"#","-"));
	$estado=$_POST["estado"];
	$modo_cre=$_POST["modo_cre"];
	$para=$_POST["para"];
	mi_query("insert into foro set usuario='$uid',fecha=curdate(),conversacion='$id_conv',mensaje='$mensaje',estado='$estado',modo_cre='$modo_cre',para='$para'","Error al agregar el mensaje");
	mi_query("update conversacion set mensajes=mensajes+1,fecha=curdate() where id_conv='$id_conv'","Error al actualizar la cantidad de mensajes de la conversacion");
	$asunto=un_dato("select tema from conversacion where id_conv='$id_conv'");
	$texto_mail="Tiene un nuevo mensaje en Copetin con el texto: " . $mensaje;
	mandar_mail($para,$uid,$asunto,$texto_mail,$uid,"logo_copetin.jpeg",1);
	mensaje("Mensaje agregado");
	delay("coforo.php?panta=detalle_conv&id_conv=$id_conv&volver=$volver");
	break;	
case "procesar_alta":
	//$id_agencia=un_dato("select max(id_agencia from agencia");
	$tema=htmlentities($_POST["tema"]);
	$qry_alta="insert into conversacion set tema='$tema',fecha=curdate(),usuario='$uid',estado='ACTIVO',mensajes=0";
	mi_query($qry_alta,"Error al agregar el tema $tema al foro");
	mensaje("Tema de conversaci&oacute;n agregado");
	delay("coforo.php?volver=$volver");
	break;
case "ocultar":
	$id_foro=$_POST["id_foro"];
	$id_conv=un_dato("select conversacion from foro where id_foro='$id_foro'");
	$modo_cre=un_dato("select modo_cre from foro where id_foro='$id_foro'");
	$mensaje=un_dato("select mensaje from foro where id_foro='$id_foro'");
	$mensaje=html_entity_decode($mensaje);
	//$mensaje=">>$mensaje";
	//$respuesta=str_pad($mensaje,200," ",STR_PAD_LEFT);
	$respuesta="";
	//$modo_usu=un_dato("select modo_usu from foro where id_foro='$id_foro'");
	//$estado=un_dato("select estado from foro where id_foro='$id_foro'");
	//$vencimiento=un_dato("select modo_cre from foro where id_foro='$id_foro'");
	//$campos="%OCU-panta-detalle_conv";
	//$id_conv=un_dato("select id_conv from foro where id_foro='$id_foro'");
	//$campos.=";%OCU-id_conv-$id_conv";
	$para=un_dato("select usuario from foro where id_foro='$id_foro'");
	$tit_panta="Responder o Finalizar mensaje $id_foro";
	$campos="%ROT-MENSAJE:";
	$campos.=";%ROT-##$mensaje";
	$campos.=";%ROT-Para##$para";
	$campos.=";%ARE-respuesta-respuesta-$respuesta-5-80";
	$campos.=";%CHK-finalizar-finalizar-S-n";
	$campos.=";%OCU-panta-responder";
	$campos.=";%OCU-mensaje-$mensaje";
	$campos.=";%OCU-id_conv-$id_conv";
	$campos.=";%OCU-para-$para";
	$campos.=";%OCU-mensaje-$mensaje";
	$campos.=";%OCU-id_foro-$id_foro";
	$campos.=";%OCU-modo_cre-$modo_cre";
	mi_panta($tit_panta,$campos,$submit);
	//delay("coforo.php?panta=detalle_conv&id_conv=$id_conv&volver=$volver");
	break;
case "responder":
	$respuesta=htmlentities(strtr($_POST["respuesta"],"#","-"));
	$id_foro=$_POST["id_foro"];
	$mensaje=$_POST["mensaje"];
	$id_conv=$_POST["id_conv"];
	$modo_cre=$_POST["modo_cre"];
	$para=$_POST["para"];
	$finalizar=$_POST["finalizar"];
	if($respuesta<>"")
	{	
		mi_query("insert into foro set usuario='$uid',fecha=curdate(),conversacion='$id_conv',mensaje='$respuesta',estado='VISIBLE',modo_cre='$modo_cre',para='$para',respuesta='$id_foro'","Error al agregar el mensaje");
		mi_query("update conversacion set mensajes=mensajes+1,fecha=curdate() where id_conv='$id_conv'","Error al actualizar la cantidad de mensajes de la conversacion");
		$asunto=un_dato("select tema from conversacion where id_conv='$id_conv'");
		$texto_mail="$uid le responde a su mensaje $mensaje en Copetin con el texto: " . $respuesta;
		mandar_mail($para,$uid,$asunto,$texto_mail,$uid,"logo_copetin.jpeg",1);
		mensaje("Mensaje grabado");
	}
	if($modo_cre=="OCULTABLE")
	{
		if($finalizar=="S")
		{
			mi_query("update foro set modo_usu='LEIDO' where id_foro='$id_foro'","Error al ocultar el mensaje");
			mensaje("Mensaje $id_foro finalizado.");
		}
	}else
	{
		mensaje("Este mensaje no puede ser finalizado.");
	}
	mensaje("Volviendo...");
	delay("coforo.php?panta=detalle_conv&id_conv=$id_conv&volver=$home");
	break;
case "todos":
	mi_titulo("Todos los temas");
	$rotulos="id;ultima actualiz.;usuario;tema";
	$sql="select id_conv,fecha,usuario,tema from conversacion order by fecha desc;coforo.php+id_conv+panta+detalle_conv+volver+$volver";
	tabla_cons($rotulos,$sql,"","","","0","CONVERSACION","ingresar","");
	volver("$volver");
	break;	
default:
	$tit_alta="NUEVO TEMA";
	$campos.=";%TXT-Tema-tema--50";
	$campos.=";%OCU-panta-procesar_alta";
	$campos.=";%OCU-volver-$volver";
	mi_panta($tit_alta,$campos,$submit);
	echo("<hr>");
	mi_titulo("Temas actualizados durante el a&#241;o actual");
	$rotulos="id;ultima actualiz.;usuario;tema";
	$sql="select id_conv,fecha,usuario,tema from conversacion where year(fecha)=year(curdate()) order by fecha desc;coforo.php+id_conv+panta+detalle_conv+volver+$volver";
	tabla_cons($rotulos,$sql,"","","","0","CONVERSACION","ingresar","");
	volver("$volver");
	un_boton("aceptar","Ver&nbsp;todos&nbsp;los&nbsp;temas","coforo.php","panta","todos");
}
//trace("$volver");
//volver("$volver");
echo("</body></html>");
?>
