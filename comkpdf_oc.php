<?php
require_once('tcpdf/config/lang/spa.php');
require_once('tcpdf/tcpdf.php');
include('coclases.php');
include('cofunciones.php');
mysql_connect('localhost','cartuchos','kartuch05');
mysql_select_db('cartuchos');

// Esto es temporal, hasta que pase todo esto adentro de la clase
$idOc=321;
$sqlOc="select * from ocCabecera where idOc='$idOc'";
$qry=mi_query($sqlOc);
$datos= mysql_fetch_array($qry);
//$idOc=$datos["idOc"];
$numeroOc=$datos["numeroOc"];
$idProveedor=$datos["idProveedor"];
$fechaGeneracion=$datos["fechaGeneracion"];
$idFormaPago=$datos["idFormaPago"];
$subtotal=$datos["subtotal"];
$iva=$datos["iva"];
$total=$datos["total"];
$idPlazoEntrega=$datos["idPlazoEntrega"];
$idEstadoAutoriz=$datos["idEstadoAutoriz"];
$idNivelAutoriz=$datos["idNivelAutoriz"];
$idMoneda=$datos["idMoneda"];
$tipoCambio=$datos["tipoCambio"];
$idSectorSolic=$datos["idSectorSolic"];
$solicitante=$datos["solicitante"];
$idEstadoPago=$datos["idEstadoPago"];
$numerosOp=$datos["numerosOp"];
$idEstadoOc=$datos["idEstadoOc"];
$presupuesto=$datos["presupuesto"];
$idImputacion=$datos["idImputacion"];
$idRubro=$datos["idRubro"];
$idTipoGasto=$datos["idTipoGasto"];	

// Fin parte temporal

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Rodrigo Camps');
$pdf->SetTitle('ORDEN DE COMPRA');
$pdf->SetSubject('VEINFAR');
$pdf->SetKeywords('TCPDF, PDF, orden de compra, veinfar, copetin');

$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 048', PDF_HEADER_STRING);

$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

$pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->setLanguageArray($l);

// ---------------------------------------------------------

$pdf->SetFont('helvetica', 'B', 10);

$pdf->AddPage();
$pdf->Setx(12);
$pdf->SetY(35);
$pdf->Setx(12);
$pdf->Write(0, 'JOSE ENRIQUE RODO 5685 - C.A.B.A.', '', 0, 'L', true, 0, false, false, 0);
$pdf->Setx(12);
$pdf->Write(0, 'PLANTA RODO 4139-5891 / 92 / 93', '', 0, 'L', true, 0, false, false, 0);
$pdf->Setx(12);
$pdf->Write(0, 'PLANTA PIEDRABUENA: 4601-6910', '', 0, 'L', true, 0, false, false, 0);
$pdf->SetY(35);
$pdf->Setx(12);
$pdf->Write(0, 'FECHA: ' . $fechaGeneracion, '', 0, 'R', true, 0, false, false, 0);
$pdf->Setx(12);
$pdf->Write(0, 'O.C. Nro.: ' . $numeroOc, '', 0, 'R', true, 0, false, false, 0);
$pdf->Setx(12);
$pdf->Write(0, 'PRESUPUESTO: ' . $presupuesto, '', 0, 'R', true, 0, false, false, 0);


$pdf->SetY(60);
$pdf->Setx(13);
$pdf->SetFont('helvetica', '', 10);

// -----------------------------------------------------------------------------

$tbl = <<<EOD
<table cellspacing="0" cellpadding="1" border="0" style="background-color:#7A378B">
    <tr>
        <td bgcolor='#660066' width="15%"><strong>Señores</strong></td>
    </tr>
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');
$pdf->SetY(65);
$proveedor=un_dato("select razon from proveedores where codigo='$idProveedor'");
$domicilio=un_dato("select domicilio from proveedores where codigo='$idProveedor'");
$telefono=un_dato("select telefono from proveedores where codigo='$idProveedor'");
$fax=un_dato("select fax from proveedores where codigo='$idProveedor'");
$formaPago=un_dato("select forma_de_pago from proveedores where codigo='$idProveedor'");
$pdf->Setx(12);
$pdf->SetFont('helvetica', 'B', 10);
$pdf->Write(0, $proveedor, '', 0, 'L', true, 0, false, false, 0);
$pdf->Setx(12);
$pdf->SetFont('helvetica', '', 8);
$pdf->Write(0, "DIRECCION: $domicilio", '', 0, 'L', true, 0, false, false, 0);
$pdf->Setx(12);
$pdf->Write(0,"TELEFONO: $telefono", '', 0, 'L', true, 0, false, false, 0);
$pdf->Setx(12);
$pdf->Write(0,"FAX: $fax", '', 0, 'L', true, 0, false, false, 0);
$pdf->Setx(12);
$pdf->Write(0,"FORMA DE PAGO: $formaPago", '', 0, 'L', true, 0, false, false, 0);
// -----------------------------------------------------------------------------

$pdf->SetY(95);
$moneda=un_dato("select moneda from moneda where idMoneda='$idMoneda'");
$items="select item,descripcion,cantidad,unitario,total from ocDetalle where idOc='$idOc'";
$encabezados="item;descripcion;cantidad;$moneda;total $moneda";
$fondo="#7A378B";
$tablaItems=tabla_cons_var($encabezados,$items,$fondo,$frente);
$pdf->writeHTML($tablaItems, true, false, false, false, '');

$pdf->Setx(12);
$pdf->Write(0,"__________________________________________________", '', 0, 'R', true, 0, false, false, 0);
$pdf->Write(0,"SUBTOTAL: $subtotal $moneda", '', 0, 'R', true, 0, false, false, 0);
$pdf->Write(0,"IVA: $iva $moneda", '', 0, 'R', true, 0, false, false, 0);
$pdf->Write(0,"TOTAL: $total $moneda", '', 0, 'R', true, 0, false, false, 0);

$pdf->Setx(50);


$pdf->SetY($pdf->GetY()-2);

$tbl = <<<EOD
<table cellspacing="0" cellpadding="1" border="0" style="background-color:#7A378B">
    <tr>
        <td bgcolor='#7A378B' width="15%"><strong>OBSERVACIONES</strong></td>
    </tr>
</table>
EOD;

$pdf->SetX(12);
$pdf->writeHTML($tbl, true, false, false, false, '');
$pdf->SetY($pdf->GetY()-2);
$plazoEntrega=un_dato("select plazoEntrega from plazoEntrega where idPlazoEntrega='$idPlazoEntrega'");
$pdf->Setx(11);
$pdf->Write(0,"PLAZO DE ENTREGA: $plazoEntrega", '', 0, 'L', true, 0, false, false, 0);
$pdf->SetY($pdf->GetY()+2);
$pdf->Ln();

$tbl = <<<EOD
<table cellspacing="0" cellpadding="1" border="0" style="background-color:#7A378B">
    <tr>
        <td bgcolor='#CC00CC' width="100%"><strong>AUTORIZADO POR</strong></td>
    </tr>
</table>
EOD;

$pdf->SetX(160);
$pdf->writeHTML($tbl, true, false, false, false, '');


$pdf->Sety($pdf->GetY()-2);
$sql="select * from autorizOc where idOc='$idOc'";
$resultado=mi_query($sql);
while($datos=mysql_fetch_array($resultado)){
    $idAutoriz=$datos["idAutoriz"];
    $usuario=$datos["usuario"];
    $fecha=$datos["fecha"];
    $firma="$fecha | $usuario";
    $pdf->Setx(160);
    $pdf->Write(0,$firma, '', 0, 'L', true, 0, false, false, 0);
}

$pdf->Ln();
$pdf->Ln();
$pdf->Ln();

$tbl = <<<EOD
<table cellspacing="0" cellpadding="1" border="0" style="background-color:#7A378B">
    <tr>
        <td bgcolor='#CC00CC' width="100%" align="center"><strong>ATENCION</strong></td>
    </tr>
</table>
EOD;
$pdf->Setx($pdf->Getx()+5);
$pdf->SetX(10);
$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->Sety($pdf->GetY()-2);
$pdf->SetFont('helvetica', '', 6);
$texto="EL HORARIO DE ENTREGA ES EXCLUSIVAMENTE DE 7 A 12 Y DE 13 A 15 HS. LA MERCADERIA DEBERA ENTREGARSE UNICAMENTE EN JOSE ENRIQUE RODO 5685 - C.A.B.A.";
$pdf->SetX(10);
$pdf->Write(0,$texto, '', 0, 'L', true, 0, false, false, 0);
$texto="LOS REACTIVOS, COLUMNAS O MATERIAL QUE REQUIERA SER ENTREGADO CON CERTIFICADO DEBERA VENIR JUNTO CON EL PEDIDO, DE LO CONTRARIO NO SE ACEPTARA ";
$pdf->SetX(10);
$pdf->Write(0,$texto, '', 0, 'L', true, 0, false, false, 0);
$texto="LA MERCADERIA. NO SERAN RECONOCIDAS LAS MERCADERIAS Y FACTURAS ENTREGADAS FUERA DEL HORARIO Y LUGAR INDICADOS.";
$pdf->SetX(10);
$pdf->Write(0,$texto, '', 0, 'L', true, 0, false, false, 0);
$pdf->SetX(10);


$pdf->Output('ordenCompraVeinfar.pdf', 'I');

