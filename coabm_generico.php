<?
include 'coacceso.php';
include 'cofunciones.php';
include 'cofunciones_especificas.php';
?>
<HTML>

<HEAD>
<TITLE>Control de Pedidos de Tinta</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$submit="Aceptar-Aceptar-coabm_generico.php";
mi_titulo("Administraci&oacute;n de Tablas del Sistema");
if(isset($_POST["panta"]))
{
	$panta=$_POST["panta"];
}
if(isset($_GET["panta"]))
{
	$panta=$_GET["panta"];
}
//trace($panta);
//$tabla=$_POST["tabla"];
switch($panta)
{
	case "crear":
		$submit="Aceptar-Aceptar-coabm_generico.php";
		//$que_tabla=$_POST["que_tabla"];
		$mitabla=$_POST["mitabla"];
		$campo=$_POST["campo"];
		$tipo=$_POST["tipo"];
		$tamanio=$_POST["tamanio"];
		$nulo=$_POST["nulo"];
		$indice=$_POST["indice"];
		$auto=$_POST["auto"];
		$sql=$_POST["sql"];
		$borrar=$_POST["borrar"];
		if(isset($borrar))
		{
			if($borrar=="S")
			{
				//mi_query("drop table if exists $que_tabla","No fue posible borrar la tabla $que_tabla");
				mi_query("drop table if exists $mitabla","No fue posible borrar la tabla $mitabla");
				//mensaje("Se borr&oacute; la tabla $que_tabla");
				mensaje("Se borr&oacute; la tabla $mitabla");
				delay();
				break;
			}else
			{
				//mensaje("No se borr&oacute; la tabla $que_tabla");
				mensaje("No se borr&oacute; la tabla $mitabla");
				delay();
				break;
			}
		}
		if($campo=="" and $sql<>"")
		{
			$sql.=")";
			mensaje("Se complet&oacute; la estructura de la tabla:<br>$sql");	
			//mi_query($sql,"Error al crear la tabla $que_tabla");
			mi_query($sql,"Error al crear la tabla $mitabla");
			un_boton();
			die();
		}
		//if($que_tabla<>"")
		if($mitabla<>"")
		{
			//$sql="create table $que_tabla (";
			$sql="create table $mitabla (";	
		}else
		{
			$sql.=",";
		}
		
		//if(tabla_existe("cartuchos",$que_tabla))
		if(tabla_existe("cartuchos",$mitabla))
		{
			//$titulo="LA TABLA $que_tabla YA EXISTE";
			$titulo="LA TABLA $mitabla YA EXISTE";
			$campos="%CHK-borrar-borrar-S-n";
			//$campos.=";%OCU-que_tabla-$que_tabla";
			$campos.=";%OCU-que_tabla-$mitabla";
			$campos.=";%OCU-panta-crear";
			mi_panta($titulo,$campos,$submit);
			break;
		}
		mensaje("continuar creando una nueva tabla");
		$sql.="$campo $tipo";
		$titulo="CREACION DE TABLA";
		$campos="%TXT-campo-campo--20";
		$campos.=";%SEL-tipo-tipo-varchar+varchar+int+int+char+char+float+float+date+date+text+text-0";
		$campos.=";%TXT-longitud-tamanio-0-6";
		$campos.=";%CHK-no nulo-nulo-S-n";
		if(!strpos($sql,"key")===0)
			$campos.=";%CHK-indice-indice-S-n";
		if(!strpos($sql,"auto")===0)
			$campos.=";%CHK-auto-auto-S-n";
		if($tamanio<>"" and $tamanio<>"0" and $tipo<>"date")
		{
			$sql.="($tamanio)";
		}
		if(($tamanio=="" or $tamanio==0) and ($tipo=="char" or $tipo=="varchar"))
		{
			$sql.="(10)";
		}
		if($nulo=="S" and $tipo<>"char" and $tipo<>"varchar")
			$sql.=" not null";
		if($auto=="S")
			$sql.=" auto_increment";
		if($indice=="S")
		$sql.=",primary key($campo)";
		$campos.=";%OCU-sql-$sql";
		$campos.=";%OCU-panta-crear";
		mi_panta($titulo,$campos,$submit);
		break;
	case "modi":
		//$que_tabla=$_POST["tabla"];
		$mitabla=$_POST["mitabla"];
		//trace("La tabla es $mitabla");
		//if(strpos($que_tabla,"Elegir") === false)
		if(strpos($mitabla,"Elegir") === false)
		{
			//mi_titulo("ALTA DE REGISTRO EN TABLA " . strtoupper($que_tabla));
			mi_titulo("ALTA DE REGISTRO EN TABLA " . strtoupper($mitabla));
			//abm_alta(1,$que_tabla,"Grabar");
			abm_alta(1,$mitabla,"Grabar","mitabla");
			//mi_titulo("ABM de $que_tabla - primera parte");
			mi_titulo("ABM de $mitabla - primera parte");
			//abm($que_tabla,1);
			abm($mitabla,1,"","mitabla");
		}else
		{
			mi_titulo("no se eligio ninguna tabla");
		}
		un_boton();
		break;
	case "alta2":
		//$que_tabla=$_POST["tabla"];
		$mitabla=$_POST["mitabla"];
		$nom_campo=$_POST["nom_campo"];
		//mi_titulo("ALTA DE REGISTRO EN LA TABLA " . strtoupper($tabla));
		mi_titulo("ALTA DE REGISTRO EN LA TABLA " . strtoupper($mitabla));
		//abm_alta(2,$que_tabla,"Grabar");
		abm_alta(2,$mitabla,"Grabar","mitabla");
		$submit="Aceptar-Aceptar-coabm_generico.php";
		//$campos.=";%OCU-tabla-$que_tabla";
		$campos.=";%OCU-mitabla-$mitabla";
		$campos.=";%OCU-panta-modi";
		mi_panta("",$campos,$submit);
		break;
	case "detalle":
		//$que_tabla=$_POST["tabla"];
		$mitabla=$_POST["mitabla"];
		$cclave=$_POST["clave"];	
		//trace("cclave es $cclave");
		$valor=$_POST[$cclave];
		//trace("La clave es $cclave y el valor es $valor");
		mi_titulo("Pantalla de modificaciones");
		//abm_modi(1,$que_tabla,$cclave,$valor,"Grabar");
		abm_modi(1,$mitabla,$cclave,$valor,"Grabar","mitabla");
		$submit="Aceptar-Aceptar-coabm_generico.php";
		break;
	case "graba_modi":
		//$que_tabla=$_POST["tabla"];
		$mitabla=$_POST["mitabla"];
		$zclave=$_POST["cclave"];
		//$valor=$_POST[$clave];
		$baja=$_POST["baja"];
		$dato=$_POST["valor"];
		//trace("zclave es $zclave, dato es $dato");
		if(isset($baja))
		{
			//abm_baja($que_tabla,$zclave,$dato);
			//trace("La tabla ahora es $mitabla en graba_modi");
			abm_baja($mitabla,$zclave,$dato);
			//mi_titulo("se dio de baja la clave $zclave=$dato de la tabla $que_tabla");
			mi_titulo("se dio de baja la clave $zclave=$dato de la tabla $mitabla");
			
		}else
		{
			mi_titulo("GRABACION DE LA MODI");
			//trace("La clave es $zclave.");
			//abm_modi(2,$tabla,$zclave,$valor,"Grabar");
			//abm_modi(2,$mitabla,$zclave,$valor,"Grabar");
			abm_modi(2,$mitabla,$zclave,$dato,"Grabar");
		}
		$submit="Aceptar-Aceptar-coabm_generico.php";
		//$campos.=";%OCU-tabla-$que_tabla";
		$campos.=";%OCU-mitabla-$mitabla";
		$campos.=";%OCU-panta-modi";
		mi_panta("",$campos,$submit);
		break;
	case "estru":
		/*
		if(isset($_POST["tabla"]))
		{
			$que_tabla=$_POST["tabla"];
		}
		if(isset($_GET["tabla"]))
		{
			$que_tabla=$_GET["tabla"];
		}
		 * */
		if(isset($_POST["mitabla"]))
		{
			$mitabla=$_POST["mitabla"];
		}
		if(isset($_GET["mitabla"]))
		{
			$mitabla=$_GET["mitabla"];
		}
		
		//if(strpos($que_tabla,"Elegir") === false)
		if(strpos($mitabla,"Elegir") === false)
		{
			//mi_titulo("MODIFICACION DE LA ESTRUCTURA DE LA TABLA " . strtoupper($tabla));
			$submit="Aceptar-Aceptar-coabm_generico.php";
			//$titulo="Modificacion de la estructura de la tabla $que_tabla";
			$titulo="Modificacion de la estructura de la tabla $mitabla";
			$campos="%SEL-ACCION-ACCION-AGREGAR+AGREGAR+CAMBIAR+CAMBIAR+BORRAR+BORRAR-0";
			$campos.=";%OCU-panta-modiestru";
			//$campos.=";%OCU-que_tabla-$que_tabla";
			$campos.=";%OCU-mitabla-$mitabla";
			mi_panta($titulo,$campos,$submit);
			mi_titulo("Campos de la tabla");
			mi_titulo("Campos de la tabla $mitabla");
			//tabla_cons("nombre;tipo;longitud;clave;inicial;extra","show columns from $que_tabla",1,"silver","#E5DBB0",0);
			tabla_cons("nombre;tipo;longitud;clave;inicial;extra","show columns from $mitabla",1,"silver","#E5DBB0",0);

		}else
		{
			mi_titulo("no se eligio ninguna tabla");
		}
		un_boton();
		break;
	case "modiestru":
		$accion=$_POST["ACCION"];
		//$que_tabla=$_POST["que_tabla"];
		$mitabla=$_POST["mitabla"];
		if($accion=="AGREGAR")
		{
			//$titulo="AGREGADO DE CAMPO a la tabla $que_tabla";
			$titulo="AGREGADO DE CAMPO a la tabla $mitabla";
			$campos="%TXT-campo-que_campo--20";
			$campos.=";%SEL-tipo-tipo-varchar+varchar+int+int+char+char+float+float+date+date+text+text-0";
			$campos.=";%TXT-longitud-tamanio-0-6";
			$campos.=";%CHK-no nulo-nulo-S-n";
			$campos.=";%CHK-indice-indice-S-n";
			$campos.=";%CHK-auto-auto-S-n";
			//$campos.=";%OCU-que_tabla-$que_tabla";
			$campos.=";%OCU-mitabla-$mitabla";
			//$campos.=";%OCU-que_campo-$que_campo";
			$campos.=";%OCU-panta-estruagrego";
			mi_panta($titulo,$campos,$submit);		
		}else
		{
			//$que_tabla=$_POST["que_tabla"];
			$mitabla=$_POST["mitabla"];
			//$titulo="$accion campos en la estructura de la tabla $que_tabla";
			$titulo="$accion campos en la estructura de la tabla $mitabla";
			//$campos="%SEL-que_campo-Campo-desc $que_tabla-Field";
			$campos="%SEL-que_campo-Campo-desc $mitabla-Field";
			$campos.=";%OCU-panta-sigue_estru";
			$campos.=";%OCU-accion-$accion";
			//$campos.=";%OCU-que_tabla-$que_tabla";
			$campos.=";%OCU-mitabla-$mitabla";
			mi_panta($titulo,$campos,$submit);
		}
		break;
	case "sigue_estru":
		$accion=$_POST["accion"];
		//$que_tabla=$_POST["que_tabla"];
		$mitabla=$_POST["mitabla"];
		$que_campo=$_POST["que_campo"];
		if($accion=="BORRAR")
		{
			//$sql="alter table $que_tabla drop $que_campo";
			$sql="alter table $mitabla drop $que_campo";
			$borro_tmp=mi_query("drop table if exists tmp","Error al borrar la tabla tmp");
			//$bckp=mi_query("create table tmp select * from $que_tabla","Error al crear la tabla tmp");
			$bckp=mi_query("create table tmp select * from $mitabla","Error al crear la tabla tmp");
			$hacer=mi_query($sql,"Error al ejecutar la consulta");
			//mensaje("Se elimin&oacute; el campo $que_campo de la tabla $que_tabla");
			mensaje("Se elimin&oacute; el campo $que_campo de la tabla $mitabla");
			//delay("coabm_generico.php?panta=estru&que_tabla=$que_tabla");
			delay("coabm_generico.php?panta=estru&mitabla=$mitabla");
		}else
		{
			//$titulo="MODIFICACION DE CAMPO $que_campo de la tabla $que_tabla";
			$titulo="MODIFICACION DE CAMPO $que_campo de la tabla $mitabla";
			//$campos="%TXT-campo-campo--20";
			$campos=";%SEL-tipo-tipo-varchar+varchar+int+int+char+char+float+float+date+date+text+text-0";
			$campos.=";%TXT-longitud-tamanio-0-6";
			$campos.=";%CHK-no nulo-nulo-S-n";
			$campos.=";%CHK-indice-indice-S-n";
			$campos.=";%CHK-auto-auto-S-n";
			//$campos.=";%OCU-que_tabla-$que_tabla";
			$campos.=";%OCU-mitabla-$mitabla";
			$campos.=";%OCU-que_campo-$que_campo";
			$campos.=";%OCU-panta-estrumodi";
			mi_panta($titulo,$campos,$submit);		
		}
		//mensaje("Estoy en sigue_estru con $accion, $tabla, $que_campo");
		un_boton();
		break;
	case "estrumodi":
		//$que_tabla=$_POST["que_tabla"];
		$mitabla=$_POST["mitabla"];
		$que_campo=$_POST["que_campo"];
		$tipo=$_POST["tipo"];
		$nulo=$_POST["nulo"];
		$indice=$_POST["indice"];
		$auto=$_POST["auto"];
		$tamanio=$_POST["tamanio"];
		//$sql="alter table $que_tabla modify $que_campo $tipo";
		$sql="alter table $mitabla modify $que_campo $tipo";
		if($tamanio<>"" and $tamanio<>"0" and $tipo<>"date")
			$sql.="($tamanio)";
		if($nulo=="S" and $tipo<>"char" and $tipo<>"varchar")
			$sql.=" not null";
		if($auto=="S")
			$sql.=" auto_increment";
		mi_query($sql,"Error al ejecutar la consulta");
		if($indice=="S")
			//mi_query("create index $que_campo on $que_tabla($que_campo)","Error al crear el indice");
			mi_query("create index $que_campo on $mitabla($que_campo)","Error al crear el indice");
		//mensaje("Se modific&oacute; el campo $que_campo de la tabla $que_tabla");
		mensaje("Se modific&oacute; el campo $que_campo de la tabla $mitabla");
		//delay("coabm_generico.php?panta=estru&tabla=$que_tabla");
		delay("coabm_generico.php?panta=estru&mitabla=$mitabla");
		break;
	case "estruagrego":
		//$que_tabla=$_POST["que_tabla"];
		$mitabla=$_POST["mitabla"];
		$que_campo=$_POST["que_campo"];
		$tipo=$_POST["tipo"];
		$nulo=$_POST["nulo"];
		$indice=$_POST["indice"];
		$auto=$_POST["auto"];
		$tamanio=$_POST["tamanio"];
		//$sql="alter table $que_tabla add $que_campo $tipo";
		$sql="alter table $mitabla add $que_campo $tipo";
		if($tamanio<>"" and $tamanio<>"0" and $tipo<>"date")
			$sql.="($tamanio)";
		if($nulo=="S" and $tipo<>"char" and $tipo<>"varchar")
			$sql.=" not null";
		if($auto=="S")
			$sql.=" auto_increment";
		mi_query($sql,"Error al ejecutar la consulta");
		if($indice=="S")
			//mi_query("create index $que_campo on $que_tabla($que_campo)","Error al crear el indice");
			mi_query("create index $que_campo on $mitabla($que_campo)","Error al crear el indice");
		//mensaje("Se agreg&oacute; el campo $que_campo a la tabla $que_tabla");
		mensaje("Se agreg&oacute; el campo $que_campo a la tabla $mitabla");
		//delay("coabm_generico.php?panta=estru&qtabla=$que_tabla");
		delay("coabm_generico.php?panta=estru&mitabla=$mitabla");
		break;
	default:
		mi_titulo("Actualizar una tabla");
		que_tabla("cartuchos","modi","mitabla");
		mi_titulo("Modificar estructura de una tabla");
		que_tabla("cartuchos","estru","mitabla");
		$titulo="CREACION DE TABLA";
		$submit="Aceptar-Aceptar-copanel.php";
		mensaje("o crear una tabla nueva:");
		$sql="";
		//$campos="%TXT-tabla-que_tabla--20";
		$campos="%TXT-tabla-mitabla--20";
		$campos.=";%TXT-campo-campo--20";
		$campos.=";%SEL-tipo-tipo-varchar+varchar+int+int+char+char+float+float+date+date-0";
		$campos.=";%TXT-longitud-tamanio-0-6";
		$campos.=";%CHK-no nulo-nulo-S-n";
		$campos.=";%CHK-indice-indice-S-n";
		$campos.=";%CHK-auto-auto-S-n";
		$campos.=";%OCU-sql-$sql";
		$campos.=";%OCU-panta-crear";
		mi_panta($titulo,$campos,$submit);
		break;
}
	
?>
</body>
</html>
