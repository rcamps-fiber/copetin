<?php
include 'coacceso.php';
include('coclases.php');
include('cofunciones.php');
$submit="aceptar-Aceptar-cocontrolOc.php";
if(isset($_POST["panta"]))
{
	$panta=$_POST["panta"];
}else
{
	if(isset($_GET["panta"]))
	{
		$panta=$_GET["panta"];
	}
}
switch($panta)
{
	case "aprobar_borrar":
		$id_pedido=$_POST["id_pedido"];
		$qry=mi_query("select * from pedidos_pv where id_pedido='$id_pedido'");
		while($datos=mysql_fetch_array($qry))
		{
			$id_cliente=$datos["id_cliente"];
			$cliente=un_dato("select razon from clientes where id_cliente='$id_cliente'");
			$id_art=$datos["id_art"];
			$articulo=un_dato("select descripcion from articulos where id_art='$id_art'");
			$id_condicion=$datos["id_condicion"];
			$condicion=un_dato("select condicion_vta from condicion_pv where id_condicion='$id_condicion'");
			$cantidad=$datos["cantidad"];
			$precio=$datos["precio"];
			$observaciones=$datos["observaciones"];
			$fecha=a_fecha_arg(hoy());
			$id_estado=$datos["id_estado"];
			$estado=un_dato("select estado from estado_pv where id_estado='$id_estado'");
		}
		$observaciones=(isset($_POST["observaciones"])) ? $_POST["observaciones"] : $observaciones;
		// Modificar datos
		$titulo="Aprobacion de Pedido de venta $id_pedido";
		$campos.=";%ROT-FECHA+<strong>$fecha";
		$campos.=";%ROT-CLIENTE+<strong>$cliente";
		$campos.=";%ROT-ARTICULO+<strong>" . strtoupper($articulo);
		$campos.=";%ROT-CANTIDAD+<strong>" . number_format($cantidad,0,',','.');
		//$campos.=";%TXT-cantidad-cantidad-$cantidad-10";
		$campos.=";%ROT-PRECIO+<strong>" . number_format($precio,2,',','.');
		//$campos.=";%TXT-precio-precio-$precio-10";
		$campos.=";%ROT-CONDICION+<strong>" . strtoupper($condicion);
		//$campos.=";%SEL-id_condicion-condicion-select id_condicion,condicion from condicion_pv order by 1-condicion+id_condicion-$condicion-$id_condicion";
		$campos.=";%TXT-observaciones-observaciones-$observaciones-50";
		//$campos.=";%SEL-id_estado-estado-select id_estado,estado from estado_pv where id_estado in(1,6) order by 2-estado+id_estado-$estado-$id_estado";
		if($id_estado==9)
			$campos.=";%SEL-id_estado-estado-select id_estado,estado from estado_pv where id_estado=10 order by 2-estado+id_estado";
		else
			$campos.=";%SEL-id_estado-estado-select id_estado,estado from estado_pv where id_estado in(1,6) order by 2-estado+id_estado";
		$campos.=";%OCU-id_pedido-$id_pedido";
		$campos.=";%OCU-fecha-$fecha";
		$campos.=";%OCU-id_art-$id_art";
		$campos.=";%OCU-cantidad-$cantidad";
		$campos.=";%OCU-precio-$precio";
		$campos.=";%OCU-id_cliente-$id_cliente";
		$campos.=";%OCU-id_condicion-$id_condicion";
		$campos.=";%OCU-panta-graba_aprobacion";
		mi_panta($titulo,$campos,$submit);
		break;
	case "graba_aprobacion_borrar":
		$id_pedido=$_POST["id_pedido"];
		//Seguir por aca para que grabe los datos si se modifican y ver como hacer para dar por facturado.
		$id_cliente=$_POST["id_cliente"];
		$cliente=un_dato("select razon from clientes where id_cliente='$id_cliente'");
		$id_art=$_POST["id_art"];
		$articulo=un_dato("select descripcion from articulos where id_art='$id_art'");
		$id_condicion=$_POST["id_condicion"];
		$condicion=un_dato("select condicion_vta from condicion_pv where id_condicion='$id_condicion'");
		$fecha=$_POST["fecha"];
		$id_estado=$_POST["id_estado"];
		$estado=un_dato("select estado from estado_pv where id_estado='$id_estado'");
		$cantidad=$_POST["cantidad"];
		$precio=$_POST["precio"];
		$observaciones=$_POST["observaciones"];
		//trace("Las observaciones ahora son $observaciones");
		$id_vendedor=un_dato("select id from usuario where usr='$uid'");
		//trace("El uid es $uid y el id es $id_vendedor");
		$vendedor=un_dato("select nombre from usuario where id='$id_vendedor'");
		//trace("El vendedor es $vendedor");
		//trace("Fecha: $fecha. Cliente: $id_cliente. Art: $id_art. Cond.: $id_condicion. Cant.: $cantidad, Precio: $precio, Observaciones: $observaciones, Uid: $uid - Vendedor: $id_vendedor");
		//$sql_grabapv="update pedidos_pv set  cantidad='$cantidad',precio='$precio',id_condicion='$id_condicion',observaciones='$observaciones',id_estado='$id_estado' where id_pedido='$id_pedido'";
		if($id_estado=="Elegir")
		{
			mensaje("Falta indicar estado");
			un_boton("aceptar","Aceptar","cocontroloc.php","panta;id_pedido;observaciones","aprobar;$id_pedido;$observaciones");
			break;
		}
		$sql_grabapv="update pedidos_pv set observaciones='$observaciones',id_estado='$id_estado' where id_pedido='$id_pedido'";
		$parametros="Vendedor: $vendedor. <p>Cliente: $cliente. <p>Art.: $articulo. <p>Cantidad: $cantidad. <p>Condicion: $condicion. <p>Precio: $precio. <p>Estado del pedido: $estado. <p>Fecha: $fecha";
		mi_query($sql_grabapv);
		mensaje("Se actualizo el pedido $id_pedido con los parametros:<p>$parametros");
		un_boton();
		break;
	case "ver_aprobar":
	    $titulos="id;numero;proveedor;fecha;total;Autorizado;moneda;sector;pago";
	    mi_titulo("Ordenes de Compra para Aprobar");
	    $sql="select c.idOc,c.numeroOc,p.razon,c.fechaGeneracion,c.total,a.estadoAutoriz,m.moneda,s.SectorSolic,e.estadoPago from ocCabecera c,proveedores p,moneda m,sectorSolic s,estadoPago e,estadoAutoriz a where c.idProveedor=p.codigo and c.idMoneda=m.idMoneda and s.idSectorSolic=c.idSectorSolic and e.idEstadoPago=c.idEstadoPago and a.idEstadoAutoriz=c.idEstadoAutoriz and c.idNivelAutoriz>1 and c.idEstadoOc=2;cocontroloc.php+idOc+panta+aprobar";
	    //trace($sql);
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;0;2;0;0;0;0;0;0;0","DETALLE","APROBAR","","Ordenes de compra $estado;$estado;ordenes");
	    un_boton();
	    break;
	case "ver_enviar":
	    $titulos="id;numero;proveedor;fecha;total;Autorizado;moneda;sector;pago";
	    mi_titulo("Ordenes de Compra para Enviar");
	    $sql="select c.idOc,c.numeroOc,p.razon,c.fechaGeneracion,c.total,a.estadoAutoriz,m.moneda,s.SectorSolic,e.estadoPago from ocCabecera c,proveedores p,moneda m,sectorSolic s,estadoPago e,estadoAutoriz a where c.idProveedor=p.codigo and c.idMoneda=m.idMoneda and s.idSectorSolic=c.idSectorSolic and e.idEstadoPago=c.idEstadoPago and a.idEstadoAutoriz=c.idEstadoAutoriz and c.idEstadoOc=5 and c.idEstadoAutoriz=2;cocontroloc.php+idOc+panta+enviar";
	    //trace($sql);
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;0;2;0;0;0;0;0;0;0","DETALLE","ENVIAR","","Ordenes de compra $estado;$estado;ordenes");
	    un_boton();
	    break;
	case "enviar":
	    $idOc=$_POST["idOc"];
	    $orden= new ORDENCOMPRA();
	    $orden->idOc=$idOc;
	    icaja("relative",0,15,1850,40,"gray");
	    mi_tabla("i");
	    echo("<tr><td>");
	    un_boton("aceptar","IMPRIMIR ORDEN","coocpdf.php","idOc","$orden->idOc");
	    echo("</td><td>");
	    un_boton("","Volver");
	    echo("</td></tr>");
	    mi_tabla("f");
	    fcaja();	    
	    $orden->mostrarOrden();
	    $estadoOc=un_dato("select estadoOc from estadoOc where idEstadoOc='$orden->idEstadoOc'");
	    $orden->enviar();
	    //un_boton("","","","panta","ver_aprobar");
	    break;
	case "procesar_envio":
	    $idEstadoOc=$_POST["idEstadoOc"];
	    $idOc=$_POST["idOc"];
	    //trace("Estoy en procesar apro con $idEstadoAutoriz");
	    $orden= new ORDENCOMPRA();
	    $orden->idOc=$idOc;
	    $orden->cargarDatosOc();
	    $orden->idEstadoOc=$idEstadoOc;
	    $orden->grabaEnviar();		
	    un_boton("","","","panta","ver_enviar");
	    break;
	case "aprobar":
	    $idOc=$_POST["idOc"];
	    $orden= new ORDENCOMPRA();
	    $orden->idOc=$idOc;
	    icaja("relative",0,15,1850,40,"gray");
	    mi_tabla("i");
	    echo("<tr><td>");
	    un_boton("aceptar","IMPRIMIR ORDEN","coocpdf.php","idOc","$orden->idOc");
	    echo("</td><td>");
	    un_boton("","Volver");
	    echo("</td></tr>");
	    mi_tabla("f");
	    fcaja();	    
	    $orden->mostrarOrden();
	    $estadoOc=un_dato("select estadoOc from estadoOc where idEstadoOc='$orden->idEstadoOc'");
	    $orden->aprobar();
	    //un_boton("","","","panta","ver_aprobar");
	    break;
	case "procesar_apro":
	    $idEstadoAutoriz=$_POST["idEstadoAutoriz"];
	    $idOc=$_POST["idOc"];
	    //trace("Estoy en procesar apro con $idEstadoAutoriz");
	    $orden= new ORDENCOMPRA();
	    $orden->idOc=$idOc;
	    $orden->cargarDatosOc();
	    if($idEstadoAutoriz==2){
		$orden->idEstadoOc=3;
		$orden->idEstadoAutoriz=($idNivelAutoriz==3) ? 1 : 2;
		$orden->grabaAprobar();		
	    }
	    if($idEstadoAutoriz==3){
		$orden->idEstadoOc=2;
		$orden->idEstadoAutoriz=3;
		$orden->grabaAprobar();		
	    }
	    un_boton("","","","panta","ver_aprobar");
	    break;
	case "detalle":
	    $idOc=$_POST["idOc"];
	    $orden= new ORDENCOMPRA();
	    $orden->idOc=$idOc;	    
	    icaja("relative",0,15,1850,40,"gray");
	    mi_tabla("i");
	    echo("<tr><td>");
	    un_boton("aceptar","IMPRIMIR ORDEN","coocpdf.php","idOc","$orden->idOc");
	    echo("</td><td>");
	    un_boton("","Volver");
	    echo("</td></tr>");
	    mi_tabla("f");
	    fcaja();	    
	    $orden->mostrarOrden();
	    $estadoOc=un_dato("select estadoOc from estadoOc where idEstadoOc='$orden->idEstadoOc'");
	    un_boton("","","","panta;estadoOc","ver;$estadoOc");
	    break;
	case "ver":
	    $estadoOc=$_POST["estadoOc"];
	    $idEstadoOc=un_dato("select idEstadoOc from estadoOc where estadoOc='$estadoOc'");
	    //mensaje("Estoy en ver con Estado: $estadoOc.");
	    $titulos="id;numero;proveedor;fecha;total;Autorizado;moneda;sector;pago";
	    mi_titulo("Ordenes de Compra en estado $estadoOc");
	    $sql="select c.idOc,c.numeroOc,p.razon,c.fechaGeneracion,c.total,a.estadoAutoriz,m.moneda,s.SectorSolic,e.estadoPago from ocCabecera c,proveedores p,moneda m,sectorSolic s,estadoPago e,estadoAutoriz a where c.idProveedor=p.codigo and c.idMoneda=m.idMoneda and s.idSectorSolic=c.idSectorSolic and e.idEstadoPago=c.idEstadoPago and a.idEstadoAutoriz=c.idEstadoAutoriz and idEstadoOc='$idEstadoOc';cocontroloc.php+idOc+panta+detalle";
	    //trace($sql);
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;0;2;0;0;0;0;0;0;0","DETALLE","ENTRAR","","Ordenes de compra $estado;$estado;ordenes");
	    un_boton();
	    break;
	default:
	    // Ordenes por estado	    
	    $sql="select e.estadoOc,count(*) from ocCabecera o,estadoOc e where o.idEstadoOc=e.idEstadoOc group by 1;cocontroloc.php+estadoOc+panta+ver";
	    $titulos="estado;casos";
	    mi_titulo("Ordenes de Compra por estado");
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0",0,"DETALLE","Ver los casos","");
	    un_boton("","Volver","coinsumos.php");
	    // Para Aprobar
	    $sql="select 'Para aprobar',count(*) from ocCabecera o,estadoOc e where o.idEstadoOc=e.idEstadoOc and o.idNivelAutoriz>1 and o.idEstadoOc=2 group by 1;cocontroloc.php+estadoOc+panta+ver_aprobar";
	    $titulos="estado;casos";
	    mi_titulo("Ordenes de Compra para Aprobar");
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0",0,"DETALLE","Ver los casos","");
	    un_boton("","Volver","coinsumos.php");
	    // Para Enviar al Proveedor
	    $sql="select e.estadoOc,count(*) from ocCabecera o,estadoOc e where o.idEstadoOc=e.idEstadoOc and o.idEstadoOc=5 and o.idestadoAutoriz=2 group by 1;cocontroloc.php+estadoOc+panta+ver_enviar";
	    //trace($sql);
	    $titulos="estado;casos";
	    mi_titulo("Ordenes de Compra para Enviar al proveedor");
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0",0,"DETALLE","Ver los casos","");
	    un_boton("","Volver","coinsumos.php");
	    break;
}

?>
