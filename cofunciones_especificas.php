<?
// Para el pedido de compra de cartuchos (copedido.php)
function mostrarPedido($quePedido){
    $programa=$_SERVER['PHP_SELF'];
    $submit="aceptar-Confirmar-$volver";    
    $q=mi_query("select * from pedido_cab where numero='$quePedido'");
    $datos=  mysql_fetch_array($q);
    $queFecha=$datos["fecha"];
    $queFecha=  a_fecha_arg($queFecha);
    $queProveedor=$datos["proveedor"];
    $queRazon=un_dato("select razon from proveedores where codigo='$queProveedor'");
    $queTipo=$datos["tipo"];
    $tit_panta="Agregar item";
    if($queTipo=="TODOS"){
	$campos.=";%SEL-codigo_int-cartucho-select codigo_int,descripcion from cartuchos order by 2-descripcion+codigo_int";
    }else{
        $campos.=";%SEL-codigo_int-cartucho-select codigo_int,descripcion from cartuchos where tipo='$queTipo' order by 2-descripcion+codigo_int";
    }
    $campos.=";%TXT-cantidad-cantidad--10";
    $campos.=";%TXT-precio-precio--10";
    $campos.=";%TXT-observaciones-obs--50";
    $campos.=";%OCU-pedido-$quePedido";
    $campos.=";%OCU-proveedor-$queProveedor";
    $campos.=";%OCU-tipo-$queTipo";
    $campos.=";%OCU-senia-1";
    $campos.=";%OCU-panta-seguir";
    mi_panta($tit_panta,$campos,$submit);    
    $columnas="item;cod.Veinfar;cod.orig;cod.prov.;marca;color;tipo;stock;cant.;precio;observ.";
    $decimales="0;0;0;0;0;0;0;0;0;2";
    //trace($programa);    
    $quePrecio=(strstr($programa,"coconficmp.php")) ? "precio_conf" : "precio_prev";
    $sql="select p.item,p.cod_int,p.cod_orig,p.cod_prv,c.marca,c.color,c.tipo,p.stock,p.cantidad,p." . "$quePrecio,p.obs";
    $sql.=" from pedido_det p,cartuchos c where p.cod_int=c.codigo_int and numero='$quePedido' order by p.item;$programa+item+pedido+$quePedido+panta+item";
    mi_titulo("Pedido Nro. $quePedido<br><hr align='center' width='100px'>Fecha: $queFecha - Proveedor $queProveedor $queRazon");
    //trace($sql);    
    tabla_cons($columnas,$sql,1,"silver","#8EC99F",$decimales);
    if(strstr($programa,"coconficmp.php")){
	$total_conf=un_dato("select sum($quePrecio*cantidad) from pedido_det where numero='$quePedido'");
	$iva_conf=$total_conf*0.105;
	$total=$total_conf+$iva_conf;
	mi_query("update pedido_cab set total_conf='$total_conf',iva_conf='$iva_conf' where numero='$quePedido'","$programa. Imposible actualizar el total previo del pedido");
	echo("<table width='20%' align='right' border='1'>");
	echo("<tr><td><strong>Importe bruto</strong></td><td>" . number_format($total_conf,2,",",".") . "</td></tr>");
	echo("<tr><td><strong>IVA 10.5</strong></td><td><u>" . number_format($iva_conf,2,",","."). "</u></td></tr>");
    }else{
	$total_prev=un_dato("select sum($quePrecio*cantidad) from pedido_det where numero='$quePedido'");
	$iva_prev=$total_prev*0.105;
	$total=$total_prev+$iva_prev;
	mi_query("update pedido_cab set total_prev='$total_prev',iva_prev='$iva_prev' where numero='$quePedido'","$programa. Imposible actualizar el total previo del pedido");
	echo("<table width='20%' align='right' border='1'>");
	echo("<tr><td><strong>Importe bruto</strong></td><td>" . number_format($total_prev,2,",",".") . "</td></tr>");
	echo("<tr><td><strong>IVA 10.5</strong></td><td><u>" . number_format($iva_prev,2,",","."). "</u></td></tr>");
    }
    echo("<tr><td><strong>Total neto</strong></td><td>" . number_format($total,2,",",".") . "</td></tr>");
    mi_tabla("f");
    echo("<hr width='80%' align='left'><br><br><br><br>");
    $observaciones=un_dato("select if(isnull(observaciones),'',observaciones) from pedido_cab where numero='$quePedido'");
    //trace($observaciones);
    $titulo="";
    $campos="%ARE-notas complementarias-observaciones-$observaciones-2-50";
    $campos.=";%OCU-pedido-$quePedido;%OCU-panta-confirma";
    $submit="aceptar-Confirmar-copanel.php";	
    mi_panta($titulo,$campos,$submit);
   return;
}


function orden_inc()
{
	//Borrar ordenes de compra de insumos incompletas
	$incompletas=mi_query("select id_oc from orden_cab where estado=99","Error al buscar ordenes incompletas");
	$msj="";
	while($datos=mysql_fetch_array($incompletas))
	{
		$id_oc=$datos["id_oc"];
		mi_query("delete from orden_det where id_oc='$id_oc'","Error al borrar el detalle de ordenes incompletas");
		$msj.="Oc Nro. $id_oc, ";
	}
	mi_query("delete from orden_cab where estado=99","Error al borrar cabecera de ordenes incompletas");
	if($msj<>"")
	{
		$msj="Se borraron las siguientes ordenes: $msj";
		$msj=substr($msj,0,strlen($msj)-2) . ".";
		mensaje($msj);
	}
}

function mandar_mail($aquien,$dequien,$asunto,$texto,$cc="",$logo="logo_copetin.jpeg",$msj=1)
{
	require_once 'Mail.php';
	require_once("Mail/mime.php");
	$email_aquien=un_dato("select email from usuarios where usuario='$aquien'");
	$nombre_aquien=un_dato("select nombre from usuarios where usuario='$aquien'");
	//$email_dequien="rodrigocamps@veinfar.com.ar";
	if($dequien=="7")
	{
		$dequien=$cc;
		$nombre_dequien="Administrador de insumos";
	}else
	{
		$nombre_dequien=un_dato("select nombre from usuarios where usuario='$dequien'");
	}
	$email_dequien=un_dato("select email from usuarios where usuario='$dequien'");
	$email_cc=un_dato("select email from usuarios where usuario='$cc'");
	$nombre_cc=un_dato("select nombre from usuarios where usuario='$cc'");
	$texto_html=str_replace("\n","<br>",$texto);
	$mensaje="<html>";
	$mensaje.='<div id="logo"><img src="' . $logo . '" align="left" border=0></div>';
	$mensaje.='<div id="cuerpo"><h3>Sistema Copetin</h3>';
	$mensaje.="Estimado/a $nombre_aquien<br>$texto_html";
	$mensaje.="<br><br><br>Atte, $nombre_dequien</div></html>";
	$texto_txt=str_replace("<br>","\n",$texto);
	$version_texto="Estimado/a $nombre_aquien\n$texto_txt\n\nAtte, $nombre_dequien.";
	$destino = $email_aquien . "," . $email_cc;
	//$destino = $email_aquien;
	$encabezado['From']    = $email_dequien;
	//$encabezado['env from']    = $email_dequien;
	$encabezado['To']      = $email_aquien;
	$encabezado['Subject'] = $asunto;
	$encabezado['Cc'] = $email_cc;
	//$encabezado['username']="rodrigocamps";
	//$encabezado['password']="uchumyxs";
	//$encabezado['auth']="true";
	$cuerpo = $mensaje;
	$params['sendmail_path'] = '/usr/lib/sendmail';
	// Crear el objeto correo con el metodo Mail::factory
	$imagen="imagenes/$logo";
	$mime= new Mail_mime("\n");
	$mime->setTXTBody($version_texto);
	$mime->setHTMLBody($mensaje);
	$mime->addHTMLImage($imagen," image/jpeg");
	$cuerpo=$mime->get();
	$hdrs=$mime->headers($encabezado);
	$mail=& Mail::factory('sendmail',$params);
	$mail->send($destino,$hdrs,$cuerpo);
	if($msj)
		mensaje("Mail enviado a $email_aquien con copia a $email_cc.");
}

function crear_post($dequien,$asunto,$texto,$msj=1)
{
	require_once 'Mail.php';
	require_once("Mail/mime.php");
	$email_aquien="blog.techinfo.0043@veinfar.com.ar";
	$email_dequien=un_dato("select email from usuarios where usuario='$dequien'");
	$email_dequien="rodrigocamps@veinfar.com.ar";
	$nombre_dequien=un_dato("select nombre from usuarios where usuario='$dequien'");
	$email_cc="rodrigocamps@veinfar.com.ar";
	//$texto_html=str_replace("\n","<br>",$texto);
	//$mensaje="<html>";
	//$mensaje.='<div id="cuerpo"><h3>Sistema Copetin</h3>';
	$mensaje="$asunto $texto";
	//$mensaje.="</div></html>";
	//$texto_txt=str_replace("<br>","\n",$texto);
	//$version_texto="$texto_txt";
	$destino = $email_aquien;
	//$destino = $email_aquien;
	$encabezado['From']    = $email_dequien;
	$encabezado['To']      = $email_aquien;
	$encabezado['Subject'] = $asunto;
	$encabezado['Cc'] = $email_cc;
	//$cuerpo = $mensaje;
	$params['sendmail_path'] = '/usr/lib/sendmail';
	// Crear el objeto correo con el metodo Mail::factory
	//$imagen="imagenes/$logo";
	$mime= new Mail_mime("\n");
	$mime->setTXTBody($mensaje);
	$mime->setHTMLBody($mensaje);
	//$mime->addHTMLImage($imagen," image/jpeg");
	$cuerpo=$mime->get();
	$hdrs=$mime->headers($encabezado);
	$mail=& Mail::factory('sendmail',$params);
	$mail->send($destino,$hdrs,$cuerpo);
	if($msj)
		mensaje("Post enviado a $email_aquien.");
}



function ver_item($item)
{
	return(un_dato("select estado from panel where item_panel='$item'"));
}


function linea_menu($item="",$mensaje="",$estado=0,$volver="copanel.php")
{
	echo("<ul><a href='$volver?item=$item&estado=$estado'>$mensaje</a></ul>");
}

function actu_item()
{
	if(isset($_GET["item"]))
	{
		$item=$_GET["item"];
		$estado=$_GET["estado"];
		mi_query("update panel set estado='$estado' where item_panel='$item'","Error al actualizar el panel");
	}
}

// Ejemplo de uso:
// Para consultar el estado de un item:
// if(ver_item("crt_fal"))

// Para cambiar el estado y mostrar el mensaje:
// actu_item();
?>
