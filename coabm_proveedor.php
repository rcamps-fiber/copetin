<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Abm proveedores</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$volver=un_dato("select url from perfiles where prf='$prf'");
$submit="aceptar-Aceptar-$volver";
mi_titulo("ABM de Proveedores");
$panta=$_POST["panta"];
switch($panta)
{
	case "graba_alta":
		$razon=limpiar($_POST["razon"]);
		$domicilio=limpiar($_POST["domicilio"]);
		$telefono=limpiar($_POST["telefono"]);
		$telefono.=" ";
		$contacto=limpiar($_POST["contacto"]);
		$mail=limpiar($_POST["mail"]);
		$observaciones=limpiar($_POST["observaciones"]);
		$fecha_ini=a_fecha_sistema($_POST["fecha_ini"]);
		$fecha_ult=$fecha_ini;
		$id_clase=($_POST["clase"]=="Elegir") ? 1 : $_POST["clase"];
		$yaexiste=un_dato("select count(*) from proveedores where razon='$razon'");
		if($yaexiste)
		{
			mensaje("Proveedor ya existente.");
		}else
		{
			mi_query("insert into proveedores set razon='$razon',domicilio='$domicilio',telefono='$telefono',contacto='$contacto',mail='$mail',observaciones='$observaciones',fecha_ini='$fecha_ini',fecha_ult='$fecha_ult',id_clase='$id_clase'","Error al agregar un proveedor");
			mensaje("Se agrego el proveedor $razon a la tabla");
		}
		delay();
		break;
	case "modi":
		$codigo=$_POST["codigo"];
		$cons=mi_query("select * from proveedores where codigo='$codigo'","Error al obtener el registro del proveedor $codigo");
		$datos=mysql_fetch_array($cons);
		$razon=$datos["razon"];
		$domicilio=$datos["domicilio"];
		$telefono=$datos["telefono"];
		$contacto=$datos["contacto"];
		$mail=$datos["mail"];
		$observaciones=$datos["observaciones"];
		$fecha_ini=$datos["fecha_ini"];
		$id_clase=$datos["id_clase"];
		$clase=un_dato("select clase from clase where id_clase='$id_clase'");
		//trace($fecha_ini);
		$fecha_ini=a_fecha_arg($fecha_ini);
		$fecha_ult=$datos["fecha_ult"];
		$fecha_ult=a_fecha_arg($fecha_ult);
		$id_clase=$datos["id_clase"];	
		$titulo="Modificacion del proveedor $razon";
		$tit_modi="MODIFICACION DE PROVEEDOR";
		$campos=";%ROT-Id. registro</td><td><strong>$codigo";
		$campos.=";%TXT-razon-razon-$razon-25";
		$campos.=";%TXT-domicilio-domicilio-$domicilio-25";
		$campos.=";%TXT-telefono-telefono-$telefono-30";
		$campos.=";%TXT-contacto-contacto-$contacto-30";
		$campos.=";%TXT-mail-mail-$mail-35";
		$campos.=";%TXT-observaciones-observaciones-$observaciones-30";
		$campos.=";%FEC-fecha inicio-fecha_ini-$fecha_ini-10";
		$campos.=";%FEC-fecha ult.compra-fecha_ult-$fecha_ult-10";
		$campos.=";%SEL-clase-clase-select id_clase,clase from clase order by 2-clase+id_clase-$clase-$id_clase";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-codigo-$codigo";
		$campos.=";%CHK-borrar-borrar-s-N";
		$submit="aceptar-Aceptar-coabm_proveedor.php";
		mi_panta($tit_modi,$campos,$submit);
		break;
	case "graba_modi":
		$codigo=$_POST["codigo"];
		$razon=limpiar($_POST["razon"]);
		$domicilio=limpiar($_POST["domicilio"]);
		$telefono=limpiar($_POST["telefono"]);
		$telefono.=" ";
		$contacto=limpiar($_POST["contacto"]);
		$mail=limpiar($_POST["mail"]);
		$observaciones=limpiar($_POST["observaciones"]);
		$fecha_ini=a_fecha_sistema($_POST["fecha_ini"]);
		$fecha_ult=$fecha_ini;
		$id_clase=($_POST["clase"]=="Elegir") ? 1 : $_POST["clase"];
		$borrar=$_POST["borrar"];
		if($borrar=="s")
		{
			mi_query("delete from proveedores where codigo='$codigo'","Error al borrar el registro del proveedor $codigo");
			mensaje("Se borro el registro del proveedor $codigo");
		}else
		{
			mi_query("update proveedores set  razon='$razon',domicilio='$domicilio',telefono='$telefono',contacto='$contacto',mail='$mail',observaciones='$observaciones',fecha_ini='$fecha_ini',fecha_ult='$fecha_ult',id_clase='$id_clase' where codigo='$codigo'","Error al modificar el proveedor $codigo");
			mensaje("Modificaci&oacute;n del proveedor $codigo grabada");
		}
		delay();
		break;
	default:
		$tit_alta="NUEVO PROVEEDOR";
		$campos_alta.=";%TXT-razon-razon--25";
		$campos_alta.=";%TXT-domicilio-domicilio--25";
		$campos_alta.=";%TXT-telefono-telefono--30";
		$campos_alta.=";%TXT-contacto-contacto--25";
		$campos_alta.=";%TXT-mail-mail--25";
		$campos_alta.=";%TXT-observaciones-observaciones--30";
		$campos_alta.=";%FEC-fecha inicio-fecha_ini--10";
		$campos_alta.=";%SEL-clase-clase-select id_clase,clase from clase order by 2-clase+id_clase";
		$campos_alta.=";%OCU-panta-graba_alta";
		//$submit="aceptar-Aceptar-coabm_proveedor.php";
		mi_panta($tit_alta,$campos_alta,$submit);
		$titulos="codigo;razon;domicilio;telefono;contacto;mail;observaciones;fecha inicio;ultima compra;clase";
		$hay=un_dato("select count(*) from proveedores");
		if($hay)
		{
			raya();
			if(isset($_POST['filtro']))
			{
				$filtro=$_POST['filtro'];
				$que_filtro=($filtro=='Elegir') ? "" : " and p.id_clase='$filtro'";
				$descrip=un_dato("select clase from clase where id_clase='$filtro'");
				$filtrado=($filtro=='Elegir') ? "" : "Filtrado por clase='$descrip'";
			}else
			{
				$filtro="";
				$filtrado="";
				$que_filtro="";
			}
			$sql="select p.codigo,p.razon,p.domicilio,p.telefono,p.contacto,p.mail,p.observaciones,p.fecha_ini,p.fecha_ult,c.clase from proveedores p,clase c  where p.id_clase=c.id_clase $que_filtro order by 2;coabm_proveedor.php+codigo+panta+modi";
			mi_titulo("ABM DE PROVEEDORES");
			$campos="%SEL-filtro-clase-select id_clase,clase from clase order by 2-clase+id_clase";
			raya();
			mi_panta("Filtro",$campos,$submit);
			mi_titulo("Listado de proveedores $filtrado");
			tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;0;2;2;0","ACTUALIZ.","MODIFICAR","","ABM de proveedores;abm proveedores;abm_proveedores");
		}else
		{
			mensaje("No hay proveedores para mostrar");
		}
		un_boton("Volver","Volver",$volver);
		break;
}

?>
</BODY>
</HTML>
