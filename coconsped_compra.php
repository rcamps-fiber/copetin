<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Consulta de pedido de compra");
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("Consulta de Pedido de Compra");
$panta=$_POST["panta"];
switch($panta)
{
	case "procesar":
		$numero=$_POST["numero"];
		$fecha=a_fecha_arg(un_dato("select fecha from pedido_cab where numero='$numero'"));
		$usuario=un_dato("select usuario from pedido_cab where numero='$numero'");
		$proveedor=un_dato("select proveedor from pedido_cab where numero='$numero'");
		$razon=un_dato("select razon from proveedores where codigo='$proveedor'");
		$iva_conf=un_dato("select iva_conf from pedido_cab where numero='$numero'");
		$total_conf=un_dato("select total_conf from pedido_cab where numero='$numero'");
		$observaciones=un_dato("select observaciones from pedido_cab where numero='$numero'");
		//         Aca va la pantalla con los datos de cabecera
		mi_tabla("i");
		mi_fila("d","Numero;$numero;;Fecha:;$fecha");
		mi_fila("d","Proveedor:;$razon;;Usuario:;$usuario");
		mi_tabla("f");
		$sql="select d.item,d.cod_orig,concat(c.marca,' ',c.color,' ',c.tipo,' ',c.descripcion) as descripcion,d.cantidad,d.precio_conf,d.obs from pedido_det d,cartuchos c where d.numero='$numero' and d.cod_int=c.codigo_int";
		$rotulos="item;codigo;descripcion;cantidad;precio;observaciones";
		$decimales="0;0;0;0;2;0";
		$color1="silver";
		$color2="#8EC99F";
		tabla_cons($rotulos,$sql,1,$color1,$color2,$decimales);
		mi_tabla("i");
		mi_fila("d","Iva:;$iva_conf");
		mi_fila("d","Total:;$total_conf");
		mi_tabla("f");
		if($observaciones=="")
			raya();
		else
			mensaje($observaciones);
		//raya();
		un_boton();
		break;
	default:
		$titulo="Consulta de pedidos de compra";
		$rotulos="numero;fecha;proveedor";
		$decimales=0;
		$sql="select p.numero,p.fecha,v.razon from pedido_cab p,proveedores v where p.estado='FINALIZADO' and p.proveedor=v.codigo order by 2 desc;coconsped_compra.php+numero+panta+procesar";
		$color1="silver";
		$color2="#8EC99F";
		tabla_cons($rotulos,$sql,1,$color1,$color2,$decimales);
		un_boton("Aceptar","Aceptar","copanel.php");
		break;
}
cierre();