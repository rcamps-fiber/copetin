<?
include 'coacceso.php';
include 'cofunciones.php'; 
//include 'cofunciones_especificas.php';
include_once('coclases.php');
//apertura("Alta orden de compra");
$submit="aceptar-Aceptar-coinsumos.php";
require_once("cocnx.php");
mi_titulo("CONSULTA DE SOLICITUDES DE RECAMBIO");
if(isset($_POST["panta"]))
{
	$panta=$_POST["panta"];
}
switch($panta)
{
    case "procesar":
	$usuario=$_POST["usuario"];
	$codigo_int=$_POST["codigo_int"];
	$codigo=$_POST["codigo"];
	$modelo=$_POST["modelo"];
	$desde=$_POST["desde"];
	$desdeSis=  a_fecha_sistema($desde);
	$hasta=$_POST["hasta"];
	$hastaSis=  a_fecha_sistema($hasta);
	//trace("usuario: $usuario");
	//trace("cartucho: $codigo_int");
	//trace("impresora: $codigo");
	//trace("Desde $desde");
	//trace("Hasta $hasta");
	$filtro="";
	$descrip_filtro="";
	$hoy=hoy();
	if($usuario<>"0"){
	    $filtro.=" and s.usuario='$usuario' ";
	    $descrip_filtro.=" Usuario $usuario. ";
	}
	if($codigo_int<>"0"){
	    $filtro.=" and s.cartucho='$codigo_int' ";
	    $codigo_corto=un_dato("select codigo_corto from cartuchos where codigo_int='$codigo_int'");
	    $codigo_orig=un_dato("select codigo_orig from cartuchos where codigo_int='$codigo_int'");
	    $descrip_filtro.=" Cartucho: id. $codigo_int. ($codigo_corto - $codigo_orig)";
	}
	if($codigo<>"0"){
	    $impresora=un_dato("select concat('Cod.: ',codigo,' ',marca,' ',modelo,' ') as impresora from impresoras where codigo='$codigo'");
	    $filtro.=" and s.impresora='$codigo' ";
	    $descrip_filtro.=" Impresora: $impresora. ";
	}
	if($modelo<>""){
	    $filtro.=" and instr(i.modelo,'$modelo') ";
	    $descrip_filtro.=" Modelo: $modelo. ";
	}
	if($desde<>$hoy){
	    $filtro.=" and s.fecha>='$desdeSis' ";
	    $descrip_filtro.=" Desde: $desde. ";
	}
	if($hasta<>$hoy){
	    $filtro.=" and s.fecha<='$hastaSis' ";
	    $descrip_filtro.=" Hasta: $hasta. ";
	}
	//trace("Filtro: $filtro");
	$sql="select s.numero as id,s.fecha,s.usuario,concat(s.puesto,' - ',p.observaciones,' ',p.descripcion) as puesto,concat('Cod.: ',s.impresora,' ',i.marca,' ',i.modelo,' ') as impresora,concat('Cod.: ',c.codigo_corto,' (',c.codigo_orig,') ',c.tipo,' ',c.color) as cartucho from solicitudes s,impresoras i,cartuchos c,puestos p where s.impresora=i.codigo and c.codigo_int=s.cartucho and s.puesto=p.codigo $filtro;coconsu_solrec.php+id+panta+detalle+usuario+$usuario+codigo_int+$codigo_int+codigo+$codigo+modelo+$modelo+desde+$desde+hasta+$hasta";
	//$sql="select s.numero,s.fecha,s.usuario,s.puesto,s.impresora,s.cartucho from solicitudes s where 1=1 $filtro";
	//trace($sql);
	$rotulos="id;fecha;solicitante;puesto;impresora;cartucho";
	mi_titulo("Solicitudes de recambio de cartucho");
	mi_titulo($descrip_filtro);
	tabla_cons($rotulos,$sql,"","#B0A2A2","#CBC0C0","0","Detalle","Ver","0","SOLICITUDES DE RECARGA;solicitudes;solicitudes");
	//tabla_cons($rotulos,$sql);
	//tabla_cons($titulo, $sql, $borde, $color, $cuerpo, $decimales, $tit_lnk, $btn_lnk, $tgt, $param_excel);
	un_boton();
	break;
    case "detalle":
	$numero=$_POST["id"];
	$qid=mi_query("select * from solicitudes where numero='$numero'");
	$datos=mysql_fetch_array($qid);
	$fecha=a_fecha_arg($datos["fecha"]);
	$usuario=$datos["usuario"];
	$impresora=$datos["impresora"];
	$desc_imp=un_dato("select concat('Cod.: ',codigo,' ',marca,' ',modelo,' ') as impresora from impresoras where codigo='$impresora'");
	$cartucho=$datos["cartucho"];
	$desc_cart=un_dato("select concat('Cod. ',codigo_corto,' (',codigo_orig,') ',tipo,' ',color,' ',descripcion) as cartucho from cartuchos where codigo_int='$cartucho'"); 
	$puesto=$datos["puesto"];
	$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
	$observaciones=$datos["observaciones"];
	$estado=$datos["estado"];
	$anterior=  a_fecha_arg($datos["anterior"]);
	$usuarioFiltro=$_POST["usuario"];
	$codigo_intFiltro=$_POST["codigo_int"];
	$codigoFiltro=$_POST["codigo"];
	$modeloFiltro=$_POST["modelo"];
	$desdeFiltro=$_POST["desde"];
	$hastaFiltro=$_POST["hasta"];
	//trace("Estoy en detalle con numero: $numero, $fecha, $usuario,$desc_imp, $cartucho, $puesto,$observaciones,$estado,$anterior");
	$pantaDet=new PANTALLA();
	$pantaDet->titulo="<br>DETALLE SOLICITUD DE RECAMBIO NRO. $numero";
	$pantaDet->AgregarROT("FECHA: @$fecha");
	$pantaDet->AgregarROT("USUARIO: @$usuario");
	$pantaDet->AgregarROT("PUESTO: @$desc_puesto");
	$pantaDet->AgregarROT("IMPRESORA: @$desc_imp");
	$pantaDet->AgregarROT("CARTUCHO: @$desc_cart");
	$pantaDet->AgregarROT("OBSERVACIONES: @$observaciones");
	$pantaDet->AgregarROT("PEDIDO ANTERIOR: @$anterior");
	$pantaDet->AgregarROT("ESTADO: @$estado");
	$pantaDet->AgregarOCU("panta","procesar");
	$pantaDet->AgregarOCU("usuario",$usuarioFiltro);
	$pantaDet->AgregarOCU("codigo_int",$codigo_intFiltro);
	$pantaDet->AgregarOCU("codigo",$codigoFiltro);
	$pantaDet->AgregarOCU("modelo",$modeloFiltro);
	$pantaDet->AgregarOCU("desde",$desdeFiltro);
	$pantaDet->AgregarOCU("hasta",$hastaFiltro);
	$pantaDet->recuadro=1;
	$pantaDet->izquierda=420;
	$pantaDet->ColorFrente="#efc6d3";
	$pantaDet->ColorFondo="gray";		
	$pantaDet->FactorAltura=20;
	$pantaDet->IncrementoAltura=30;
	//$pantaDet->regresar="procesar";
	$pantaDet->MostrarPantalla();
	//un_boton();
	break;
    default:
	$hoy=hoy();
	$pantaConsu=new PANTALLA();
	$pantaConsu->titulo="Filtro de la consulta";
	$pantaConsu->AgregarSEL("usuario", "usuario", "usuario","0","Elegir",usuarios);	
	$pantaConsu->AgregarSELCustom3("cartucho","codigo_int","cartucho","codigo_int",0,"Elegir","select codigo_int,concat('Cod. ',codigo_corto,' (',codigo_orig,') ',tipo,' ',color,' ',descripcion) as cartucho from cartuchos order by 2");	
	$pantaConsu->AgregarSEL("impresora","codigo","modelo","0","Elegir","impresoras");
	$pantaConsu->AgregarTXT("modelo impresora","modelo","",15);
	$pantaConsu->AgregarFEC("desde","desde",$hoy);
	$pantaConsu->AgregarFEC("hasta","hasta",$hoy);
	$pantaConsu->AgregarOCU("panta","procesar");
	//$panta_alta->regresar="coinsumos.php";
	$pantaConsu->FactorAltura=30;
	$pantaConsu->IncrementoAltura=30;
	$pantaConsu->recuadro=1;
	$pantaConsu->ColorFrente="#f7e8aa";
	$pantaConsu->ColorFondo="gray";		
	$pantaConsu->izquierda=420;		
	$pantaConsu->ColorFrente="#efc6d3";
	$pantaConsu->regresar="copanel.php";
	//$panta_alta->ColorFondo="#083D06";
	$pantaConsu->MostrarPantalla();
	
		break;
}
?>
