<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Consulta y Modificacion de OT");
require_once("cobody.php");
require_once("cocnx.php");
$home=home($uid);
$mismo=home();
$submit="aceptar-Aceptar-$home";
$submit2="aceptar-Aceptar-$mismo";
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("Consulta y Modificacion de Ordenes de Trabajo");
$panta=$_POST["panta"];
switch($panta)
{
	case "modi":
		$id_sol=$_POST["id_sol"];
		$cons=mi_query("select * from soltrab where id_sol='$id_sol'","Error al obtener la solicitud");
		$datos=mysql_fetch_array($cons);
		$fecha_sol=a_fecha_arg($datos["fecha_sol"]);
		$fecha_prog=a_fecha_arg($datos["fecha_prog"]);
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$tipo_problema=$datos["tipo_problema"];
		$def_tp=un_dato("select problema from tipo_problema where id='$tipo_problema'");
		$dispositivo=$datos["dispositivo"];
		$desc_dispo=un_dato("select dispositivo from dispositivo where id='$dispositivo'");
		$descripcion=$datos["descripcion"];
		$observaciones=$datos["observaciones"];
		$estado=$datos["estado"];
		$desc_estado=un_dato("select estado from estado_ot where id='$estado'");
		$obs_tec=$datos["obs_tec"];
		$tarea=$datos["tarea"];
		$akb=$datos["akb"];
		$fecha_mod=a_fecha_arg($datos["fecha_mod"]);
		$fin=a_fecha_arg($datos["fin"]);
		$prioridad=$datos["prioridad"];
		$tecnico=$datos["tecnico"];
		$titulo="Modificacion de Orden de Trabajo Nro. $id_sol";
		$campos="%TXT-fecha solicitud-fecha_sol-$fecha_sol-10";
		$campos.=";%SEL-usuario-usuario-select usuario,nombre from usuarios where perfil=2 order by 1-nombre+usuario-$nombre-$usuario";
		$campos.=";%ROT-PUESTO</td><td>$desc_puesto";
		$campos.=";%SEL-tipo_problema-tipo de problema-select id,problema from tipo_problema order by freq desc-problema+id-$def_tp-$tipo_problema";		
		$campos.=";%SEL-dispositivo-dispositivo-select id,dispositivo from dispositivo order by freq desc-dispositivo+id-$desc_dispo-$dispositivo";
		//$campos.=";%TXT-descripcion-descripcion-$descripcion-50";
		$campos.=";%ARE-descripcion-descripcion-$descripcion-5-80";
		//$campos.=";%TXT-analisis preliminar-observaciones-$observaciones-50";
		$campos.=";%ARE-analisis preliminar-observaciones-$observaciones-5-80";
		$campos.=";%TXT-fecha programada-fecha_prog-$fecha_prog-10";
		$campos.=";%SEL-estado-estado-select id,estado from estado_ot-estado+id-$desc_estado-$estado";
		//$campos.=";%TXT-diagnostico-obs_tec-$obs_tec-50";
		$campos.=";%ARE-Observ. tecnico-obs_tec-$obs_tec-5-80";
		//$campos.=";%TXT-trabajo realizado-tarea-$tarea-50";
		$campos.=";%ARE-tarea realizada-tarea-$tarea-5-80";
		$campos.=";%SEL-tecnico-tecnico-select usuario,usuario from usuarios where perfil in(1,4)-usuario+usuario-$tecnico-$tecnico";		
		$campos.=";%TXT-fecha trabajo-fecha_mod-$fecha_mod-10";
		$campos.=";%TXT-fecha finalizacion-fin-$fin-10";
		$campos.=";%SEL-prioridad-prioridad-$prioridad+$prioridad+ALTA+ALTA+BAJA+BAJA+NORMAL+NORMAL-0";
		//trace("Akb es $akb");
		$akb=($akb) ? 's' : 'n';
		$campos.=";%CHK-Base de Conocimientos-akb-S-$akb";		
		$campos.=";%CHK-elimina-elimina-1-n";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-id_sol-$id_sol";
		$submit="aceptar-Modificar-$mismo";	
		mi_panta($titulo,$campos,$submit);
		break;
	case "graba_modi":
		$id_sol=$_POST["id_sol"];
		$fecha_sol=a_fecha_sistema($_POST["fecha_sol"]);
		$fecha_prog=a_fecha_sistema($_POST["fecha_prog"]);
		$usuario=$_POST["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$_POST["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$tipo_problema=$_POST["tipo_problema"];
		$def_tp=un_dato("select problema from tipo_problema where id='$tipo_problema'");
		$dispositivo=$_POST["dispositivo"];
		$desc_dispo=un_dato("select dispositivo from dispositivo where id='$dispositivo'");
		$descripcion=$_POST["descripcion"];
		$observaciones=$_POST["observaciones"];
		$estado=$_POST["estado"];
		$desc_estado=un_dato("select estado from estado_ot where id='$estado'");
		$obs_tec=$_POST["obs_tec"];
		$tarea=$_POST["tarea"];
		$fecha_mod=a_fecha_sistema($_POST["fecha_mod"]);
		$fin=a_fecha_sistema($_POST["fin"]);
		$prioridad=$_POST["prioridad"];
		$tecnico=$_POST["tecnico"];	
		$akb=($_POST["akb"]=='S') ? 1 : 0;
		//trace("Akb es $akb");
		$elimina=$_POST["elimina"];
		if($elimina=="1")
		{
			mi_query("delete from soltrab where id_sol='$id_sol'","Error al eliminar la ot.");
			mensaje("Se elimin&oacute; la ot $id_sol.");
		}else
		{
			$puesto=un_dato("select puesto from usu_puesto where usuario='$usuario'");
			mi_query("update soltrab set fecha_sol='$fecha_sol',usuario='$usuario',puesto='$puesto',tipo_problema='$tipo_problema',dispositivo='$dispositivo',descripcion='$descripcion',observaciones='$observaciones',fecha_prog='$fecha_prog',estado='$estado',obs_tec='$obs_tec',tarea='$tarea',fecha_mod='$fecha_mod',fin='$fin',prioridad='$prioridad',tecnico='$tecnico',akb='$akb' where id_sol='$id_sol'","Error al modificar la ot.");
//-----------------------------------------------------------
			//Agregado (24/10): base de conocimientos
			//Si akb fue marcado y estado finalizado
			//die("Akb es $akb y estado es $estado");
			if ($akb == 1 and $estado==4)
			{
				//genero el título
				//trace("El problema es $problema");
				$titu_conoc = "(".$def_tp." - ".$desc_dispo.") ".substr($descripcion,0,14)."...";
				$prob_conoc = $descripcion;
				$solu_conoc = $tarea;
				//genero las claves: con las palabras del problema, dispositivo y descripcion
				//que tengan longitud mayor a 2
				$junto_palabras = $problema." ".$dispositivo." ".$descripcion;
				$palabras = explode(" ", $junto_palabras);
				$c = 0;
				$c1 = 0;
				$clav_c = array();
				while ($c < count($palabras)){
					if (strlen($palabras[$c]) > 2){
						$clav_c[$c1] = $palabras[$c];
						$c1++;
					}
					$c++;
				}
				$clav_conoc = implode(", ", $clav_c);
				//traigo la fecha de alta
				$f_alta_conoc = un_dato("select fecha_prog from soltrab where id_sol = $id_sol;");

				//ahora guardo estos datos en la base conocimientos si todavia no existen.
				$ya_existe=un_dato("select count(*) from conocimientos where titulo='$titu_conoc' and problema='$prob_conoc'");
				if($ya_existe)
					mi_query("update conocimientos set titulo='$titu_conoc',problema='$prob_conoc',solucion='$solu_conoc',claves='$clav_conoc',fecha_alta='$f_alta_conoc',fecha_modi=curdate() where titulo='$titu_conoc' and problema='$prob_conoc'");
				else
					mi_query("insert into conocimientos (titulo, problema, solucion, claves, fecha_alta, fecha_modi) VALUES ('$titu_conoc', '$prob_conoc', '$solu_conoc', '$clav_conoc', '$f_alta_conoc', curdate())");
			}
//----------------------------------------------------------	
			mensaje("Se actualiz&oacute; en la ot nro. $id_sol.");
		}
		un_boton();
		break;
	default:
		$titulo="FILTRO";
		$filtro="";
		$fecha_sol_desde=$_POST["fecha_sol_desde"];
		if($fecha_sol_desde=="" or $fecha_sol_desde=="dd/mm/aaaa")
		{
			$fecha_sol_desde="dd/mm/aaaa";
		}else
		{
			$filtro.=" and s.fecha_sol >= '" . a_fecha_sistema($fecha_sol_desde) ."' ";
		}
		$fecha_sol_hasta=$_POST["fecha_sol_hasta"];
		if($fecha_sol_hasta=="" or $fecha_sol_hasta=="dd/mm/aaaa")
		{
			$fecha_sol_hasta="dd/mm/aaaa";
		}else
		{
			$filtro.=" and s.fecha_sol <= '" . a_fecha_sistema($fecha_sol_hasta) . "' ";
		}
		$usuario=$_POST["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");		
		$tipo_problema=$_POST["tipo_problema"];
		$def_tp=un_dato("select problema from tipo_problema where id='$tipo_problema'");
		$vencidas=$_POST["vencidas"];
		if($vencidas=="" or $vencidas=="Elegir")
		{
			$vencidas="Elegir";
		}
		if($vencidas=="Si")
		{
			$filtro.=" and s.fecha_prog < curdate() ";
		}
		if($vencidas=="No")
		{
			$filtro.=" and s.fecha_prog>= curdate() ";
		}
		$estado=$_POST["estado"];
		$desc_estado=un_dato("select estado from estado_ot where id='$estado'");
		if($estado=="" or $estado=="Elegir")
		{
			$desc_estado="Elegir";
			$estado="Elegir";
		}else
		{
			$filtro.=" and s.estado='$estado' ";
		}
		$tecnico=$_POST["tecnico"];
		if($tecnico=="" or $tecnico=="Elegir")
		{
			$tecnico="Elegir";
		}else
		{
			$filtro.=" and s.tecnico='$tecnico' ";
		}
		if($tipo_problema=="" or $tipo_problema=="Elegir")
		{
			$tipo_problema="Elegir";
			$def_tp="Elegir";
		}else
		{
			$filtro.=" and s.tipo_problema='$tipo_problema' ";
		}
		if($usuario=="" or $usuario=="Elegir")
		{
			$usuario="Elegir";
			$nombre="Elegir";
		}else
		{
			$filtro.=" and s.usuario='$usuario' ";
		}
		$campos.=";%SEL-estado-estado-select 'Elegir' as id,'Elegir' as estado from estado_ot union select id,estado from estado_ot-estado+id-$desc_estado-$estado";	$campos.=";%SEL-tecnico-tecnico-$tecnico+$tecnico+Elegir+Elegir+alejandro+alejandro+rcamps+rcamps-0";
		$campos.=";%SEL-vencidas-vencidas-Elegir+Elegir+Si+Si+No+No-0";
		$campos.=";%SEL-tipo_problema-tipo de problema-select 'Elegir' as id,'Elegir' as problema from tipo_problema union select id,problema from tipo_problema-problema+id-$def_tp-$tipo_problema";
		$campos.=";%SEL-usuario-usuario-select 'Elegir' as usuario,'Elegir' as nombre from usuarios  union select usuario,nombre from usuarios where perfil=2-nombre+usuario-$nombre-$usuario";
		$campos.=";%TXT-fecha solicitud desde-fecha_sol_desde-$fecha_sol_desde-10";
		$campos.=";%TXT-fecha solicitud hasta-fecha_sol_hasta-$fecha_sol_hasta-10";		
		$submit="aceptar-Filtrar-$home";	
		mi_panta($titulo,$campos,$submit);
		//trace("El filtro es: <br>$filtro");
		$titulos="id;fecha solicitud;usuario;puesto;tipo de problema;dispositivo;descripcion;analisis preliminar;fecha programada;estado;diagnostico;trabajo realizado;tecnico;fecha trabajo;fecha terminacion;prioridad";
		$sql="select s.id_sol,s.fecha_sol,u.nombre,p.descripcion as puesto,t.problema,d.dispositivo,s.descripcion as desc_problema,s.observaciones,s.fecha_prog,e.estado,s.obs_tec,s.tarea,s.tecnico,s.fecha_mod,s.fin,s.prioridad";
		$sql.=" from soltrab s,estado_ot e,puestos p,tipo_problema t,dispositivo d,usuarios u";
		$sql.=" where s.estado=e.id and s.puesto=p.codigo and s.tipo_problema=t.id and s.dispositivo=d.id and s.usuario=u.usuario $filtro";
		$sql.=" order by id_sol desc;coabm_ot.php+id_sol+panta+modi";
		//mi_titulo("Ordenes de Trabajo");
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","MODIFICAR","","Listado");
		un_boton("Volver","Volver",$home);
		break;
}
cierre();
?>
