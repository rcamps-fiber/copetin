<?
require_once 'Spreadsheet/Excel/Writer.php';

// Datos
$archivo="tmp/orden_compra.xls";
$id_oc=$_POST["id_oc"];
$proveedor=$_POST["proveedor"];
$razon=$_POST["razon"];
$domicilio=$_POST["domicilio"];
$forma_de_pago=$_POST["forma_de_pago"];
$atencion=$_POST["atencion"];
$entrega=$_POST["entrega"];
$responsable=$_POST["responsable"];
$horario=$_POST["horario"];
$lugar=$_POST["lugar"];
$aviso=$_POST["aviso"];
$leyenda=$_POST["leyenda"];
$presupuesto=$_POST["presupuesto"];
$telefono=$_POST["telefono"];
$fax=$_POST["fax"];

include_once("cofunciones.php");
$fecha=hoy();

// Genero el documento
$orden= new Spreadsheet_Excel_Writer($archivo);
// Agrego una hoja
$hoja =& $orden->addWorksheet("orden de compra nro. $id_oc");
$hoja->setInputEncoding('utf-8');

$hoja->setRow(0,25);
// Formato para el membrete
$veinfar=& $orden->addFormat();
$veinfar->setFontFamily('Times New Roman');
$veinfar->setSize(15);
$veinfar->setBold();
$veinfar->setColor("blue");
$veinfar->setPattern(0);
$veinfar->setBottom(1);
$veinfar->setAlign("center");
for($n=5; $n<=9; $n++) $hoja->write(0,$n,'',$veinfar);
$hoja->write(0,0,"VEINFAR I.C.S.A",$veinfar);
$hoja->mergeCells(0,0,0,9);

$banner =& $orden->addFormat();
$banner->setSize(8);
$banner->setAlign("top");
$hoja->write(1,0,"Administracion: Piedrabuena 4190 - zip C1439GVX - Buenos Aires-Argentina-Tel/Fax: 54-11-4601-6910-Email: laboratorios@veinfar.com.ar",$banner);
for($n=5; $n<=9; $n++) $hoja->write(1,$n,'',$banner);
$hoja->mergeCells(1,0,1,9);

$titulo =& $orden->addFormat();
$titulo->setBold();
$titulo->setSize(14);
$titulo->setAlign("left");
$derecha =& $orden->addFormat();
$derecha->setAlign("right");
$hoja->write(3,0,"ORDEN DE COMPRA",$titulo);
$hoja->write(5,0,"Sres.",$derecha);
$hoja->write(5,1,$razon);
$hoja->write(6,1,$domicilio);

$subrayado=& $orden->addFormat();
$subrayado->setUnderline(1);
$subrayado->setBold();
$subrayado->setAlign("center");
for($n=5; $n<=7; $n++) $hoja->write(5,$n,'',$subrayado);
$hoja->write(5,5,"NUEVOS TEL/FAX DE COMPRAS",$subrayado);
$hoja->mergeCells(5,5,5,7);

$centrado=& $orden->addFormat();
$centrado->setAlign("center");
$centrado->setBold();
for($n=5; $n<=7; $n++) $hoja->write(6,$n,'',$centrado);
$hoja->write(6,5,"4139-5691",$centrado);
$hoja->mergeCells(6,5,6,7);

$hoja->write(7,1,"Tel. $telefono");
for($n=5; $n<=7; $n++) $hoja->write(7,$n,'',$centrado);
$hoja->write(7,5,"4139-5692",$centrado);
$hoja->mergeCells(7,5,7,7);

$hoja->write(8,1,"Fax. $fax");
for($n=5; $n<=7; $n++) $hoja->write(8,$n,'',$centrado);
$hoja->write(8,5,"4139-5693",$centrado);
$hoja->mergeCells(8,5,8,7);

// Forma de pago
$hoja->write(11,0,"Forma de pago");
$borde=& $orden->addFormat();
$borde->setTop(2);
$borde->setBottom(2);
$borde->setLeft(2);
$borde->setRight(2);
$borde->setAlign("center");
$borde->setVAlign("vcenter");
$borde->setNumFormat("0.00");
for($n=0; $n<=3; $n++) $hoja->write(13,$n,'',$borde);
for($n=0; $n<=3; $n++) $hoja->write(14,$n,'',$borde);
$hoja->write(13,0,$forma_de_pago,$borde);
$hoja->mergeCells(13,0,14,3);


$hoja->write(14,0,"",$borde);
$hoja->write(14,3,"",$borde);
$negrita=& $orden->addFormat();
$negrita->setTop(2);
$negrita->setBottom(2);
$negrita->setLeft(2);
$negrita->setRight(2);
$negrita->setBold();

for($n=5; $n<=7; $n++) $hoja->write(12,$n,'',$negrita);
$hoja->write(12,5,"Nro. de pedido de compra: $id_oc",$negrita);
$hoja->mergeCells(12,5,12,7);

for($n=5; $n<=7; $n++) $hoja->write(13,$n,'',$negrita);
$hoja->write(13,5,"ATENCION: $atencion",$negrita);
$hoja->mergeCells(13,5,13,7);

for($n=5; $n<=7; $n++) $hoja->write(14,$n,'',$negrita);
$hoja->write(14,5,"Segun presupuesto nro. $presupuesto",$negrita);
$hoja->mergeCells(14,5,14,7);

for($n=5; $n<=7; $n++) $hoja->write(15,$n,'',$negrita);
$hoja->write(15,5,"Fecha del pedido: $fecha",$negrita);
$hoja->mergeCells(15,5,15,7);

//$hoja->write(12,7,"",$negrita);
//$hoja->write(13,7,"",$negrita);
//$hoja->write(14,7,"",$negrita);
//$hoja->write(15,7,"",$negrita);

$hoja->write(16,0,"Plazo de entrega");

// Plazo de entrega
for($n=0; $n<=3; $n++) $hoja->write(18,$n,'',$borde);
for($n=0; $n<=3; $n++) $hoja->write(19,$n,'',$borde);
$hoja->write(18,0,$entrega,$borde);
$hoja->mergeCells(18,0,19,3);



//for($n=0; $n<=2; $n++) $hoja->write(16,$n,'',$borde);
//$hoja->write(18,0,$entrega,$borde);
//$hoja->mergeCells(18,0,19,2);

$hoja->write(19,0,"",$borde);
$hoja->write(19,2,"",$borde);
$hoja->write(21,0,"Suministre los siguientes elementos");

// Titulo de las columnas
$negrita->setAlign("center");
$hoja->write(23,0,"Nro. COD.",$negrita);
for($n=1; $n<=3; $n++) $hoja->write(23,$n,'',$negrita);
$hoja->write(23,1,"DESCRIPCION",$negrita);
$hoja->mergeCells(23,1,23,3);
$hoja->write(23,4,"UNIDAD",$negrita);
$hoja->write(23,5,"CANTIDAD",$negrita);
$hoja->write(23,6,"COSTO UNIT.",$negrita);
$hoja->write(23,7,"TOTAL",$negrita);


$hoja->setColumn(1,1,25);
$hoja->setColumn(4,6,13);
$columnas =& $orden->addFormat();
$columnas->setSize(14);
$columnas->setLeft(1);
$columnas->setRight(1);
$columnas1 =& $orden->addFormat();
$columnas1->setSize(14);
$columnas1->setLeft(1);
$columnas1->setRight(1);
$columnas1->setNumFormat("0.000");
$columnas2 =& $orden->addFormat();
$columnas2->setSize(14);
$columnas2->setLeft(1);
$columnas2->setRight(1);
$columnas2->setNumFormat("0.00");

require_once("cocnx.php");

// ahora los datos del pedido
$renglon=24;
$altura="19.5";
$qry=mi_query("select * from orden_det where id_oc='$id_oc' and estado=0 order by id_det","Error al buscar la orden");
while($datos=mysql_fetch_array($qry))
{
	$insumo=$datos["insumo"];
	$descripcion=$datos["descripcion"];
	$unidad=$datos["unidad"];
	$cantidad=$datos["cantidad"];
	$costo=$datos["precio"];
	$total=$datos["total"];
	//$hoja->setRow($renglon,$altura);
	$hoja->write($renglon,0,$insumo,$columnas);
	for($n=1; $n<=3; $n++) $hoja->write($renglon,$n,'',$columnas);
	$hoja->write($renglon,1,$descripcion,$columnas);
	$hoja->mergeCells($renglon,1,$renglon,3);
	$hoja->setColumn(5,7,10);
	$hoja->write($renglon,4,$unidad,$columnas);
	$hoja->write($renglon,5,$cantidad,$columnas);
	$hoja->write($renglon,6,$costo,$columnas1);
	$hoja->write($renglon,7,$total,$columnas2);
	$renglon++;

}
$hoja->setRow($renglon,"1");
for($n=0; $n<=7; $n++) $hoja->write($renglon,$n,'',$borde);
$hoja->mergeCells($renglon,1,$renglon,3);
$renglon++;

// Calculo totales
$subtotal=un_dato("select sum(total) from orden_det where id_oc='$id_oc' and estado=0");
$iva_21=un_dato("select sum(total*.21) from orden_det where id_oc='$id_oc' and estado=0 and iva=21");
$iva_105=un_dato("select sum(total*.105) from orden_det where id_oc='$id_oc' and estado=0 and iva=10.5");
$iva_105=$iva_105+0.00;
$iva_21=$iva_21+0.00;
$total=$subtotal+$iva_21+$iva_105;
$borde_izq =& $orden->addFormat();
$borde_izq->setLeft(2);
$hoja->write($renglon,6,"Subtotal",$borde_izq);
$hoja->write($renglon,7,$subtotal,$borde);
$hoja->write($renglon+1,6,"IVA 10.5%",$borde_izq);
$hoja->write($renglon+1,7,$iva_105,$borde);
$hoja->write($renglon+2,6,"IVA 21%",$borde_izq);
$hoja->write($renglon+2,7,$iva_21,$borde);
$borde_tb =& $orden->addFormat();
$borde_tb->setLeft(2);
$borde_tb->setBottom(2);
$hoja->write($renglon+3,6,"Total",$borde_tb);
$hoja->write($renglon+3,7,$total,$borde);


// Firma: Formateo las celdas antes de unirlas
$firma =& $orden->addFormat();
$firma->setTop(1);
$firma->setSize(10);
$firma->setBold();
$firma->setAlign("center");
for($n=0; $n<=1; $n++) $hoja->write($renglon+4,$n,'',$firma);
for($n=0; $n<=1; $n++) $hoja->write($renglon+5,$n,'',$firma);
$hoja->write($renglon+4,0,$responsable,$firma);
$hoja->write($renglon+5,0,"Aprobado por",$firma);
$hoja->mergeCells($renglon+5,0,$renglon+5,1);
$hoja->mergeCells($renglon+4,0,$renglon+4,1);


// Horario: Formateo las celdas antes de unirlas
$titulo2 =& $orden->addFormat();
$banner->setAlign("top");
$titulo2->setBold();
$titulo2->setSize(10);
$titulo2->setAlign("center");
for($n=0; $n<=7; $n++) $hoja->write($renglon+8,$n,'',$titulo2);
$hoja->write($renglon+8,0,$horario,$titulo2);
$hoja->mergeCells($renglon+8,0,$renglon+8,7);


// Aviso: Formateo las celdas antes de unirlas
for($n=0; $n<=7; $n++) $hoja->write($renglon+9,$n,'',$titulo2);
$hoja->write($renglon+9,0,$lugar,$titulo2);
$hoja->mergeCells($renglon+9,0,$renglon+9,7);


// Aviso: Formateo las celdas antes de unirlas
$banner2 =& $orden->addFormat();
$banner2->setAlign("center");
$banner2->setTop(1);
$banner2->setBottom(1);
$banner2->setLeft(1);
$banner2->setRight(1);
for($n=0; $n<=7; $n++) $hoja->write($renglon+10,$n,'',$banner2);
$hoja->write($renglon+10,0,$aviso,$banner2);
$hoja->mergeCells($renglon+10,0,$renglon+10,7);




// Leyenda: Formateo las celdas antes de unirlas
for($n=0; $n<=7; $n++) $hoja->write($renglon+12,$n,'',$banner2);
$hoja->write($renglon+12,0,$leyenda,$banner2);
$hoja->mergeCells($renglon+12,0,$renglon+12,7);


$hoja->fitToPages(1,1);
$hoja->hideGridLines();
//$orden->send($archivo);
$orden->close();
include_once('coacceso.php');
include_once('cofunciones_especificas.php');
include_once("cofunciones.php");
require_once("cobody.php");
mi_titulo("Generacion de Orden de Compra en Excel");
ventana("tmp/orden_compra.xls",$titulo="Orden de Compra");
un_boton("volver","Volver","coinsumos.php");
?>