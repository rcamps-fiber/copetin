<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Administracion de garantias");
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Aceptar-copanel.php";
mi_titulo("Administracion de Garantias");
$panta=$_POST["panta"];
switch($panta)
{
	case "graba_alta":
		$equipo=$_POST["equipo"];
		$detalle=$_POST["detalle"];
		$fecha_compra=a_fecha_sistema($_POST["fecha_compra"]);
		$fecha=a_fecha_sistema($_POST["fecha"]);
		$plazo=$_POST["plazo"];
		$proveedor=$_POST["proveedor"];
		$factura=$_POST["factura"];
		$responsable=$_POST["responsable"];
		$puesto=$_POST["puesto"];
		$serial_num=$_POST["serial_num"];
		$id_gasto=$_POST["id_gasto"];
		mi_query("insert into garantias set equipo='$equipo',detalle='$detalle',fecha_compra='$fecha_compra',plazo='$plazo',proveedor='$proveedor',factura='$factura',responsable='$responsable',puesto='$puesto',fecha='$fecha',serial_num='$serial_num',id_gasto='$id_gasto'");
		mensaje("Se agrego una nueva garantia.");
		delay();
		break;
	case "modi":
		$id_garantia=$_POST["id_garantia"];
		$cons=mi_query("select * from garantias where id_garantia='$id_garantia'","Error al obtener el registro");
		$datos=mysql_fetch_array($cons);
		$equipo=$datos["equipo"];
		$descripcion=un_dato("select descripcion from equipo where id_equipo='$equipo'");
		$detalle=$datos["detalle"];
		$fecha_compra=a_fecha_arg($datos["fecha_compra"]);
		$fecha=a_fecha_arg($datos["fecha"]);
		$plazo=$datos["plazo"];
		$proveedor=$datos["proveedor"];
		$razon=un_dato("select razon from proveedores where codigo='$proveedor'");
		$factura=$datos["factura"];
		$responsable=$datos["responsable"];
		$puesto=$datos["puesto"];
		$serial_num=$datos["serial_num"];
		$id_gasto=$datos["id_gasto"];
		$usu_resp=un_dato("select nombre from usuarios where usuario='$responsable'");
		$titulo="Modificacion de Garantia";
		$tit_modi="MODIFICACION DE GARANTIA";
		$campos=";%ROT-Id.</td><td><strong>$id_garantia";
		$campos.=";%SEL-equipo-equipo-select id_equipo,descripcion from equipo order by 2-descripcion+id_equipo-$descripcion-$equipo";
		$campos.=";%ARE-descripcion-detalle-$detalle-1-50";
		$campos.=";%TXT-S/N-serial_num-$serial_num-50";
		$campos.=";%FEC-fecha compra-fecha_compra-$fecha_compra-10";
		$campos.=";%TXT-meses de garantia-plazo-$plazo-10";
		$campos.=";%SEL-proveedor-proveedor-select codigo,razon from proveedores where id_clase=1 order by 2-razon+codigo-$razon-$proveedor";
		$campos.=";%TXT-factura-factura-$factura-20";
		$campos.=";%SEL-responsable-responsable-select usuario,nombre from usuarios order by 2-nombre+usuario-$usu_resp-$responsable";
		$campos.=";%TXT-puesto-puesto-$puesto-10";
		$campos.=";%FEC-fecha alta-fecha-$fecha-10";
		$campos.=";%TXT-ID GASTO-id_gasto-$id_gasto-10";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-id_garantia-$id_garantia";
		$campos.=";%CHK-borrar-borrar-s-N";
		$submit="aceptar-Aceptar-coabm_garantias.php";
		mi_panta($tit_modi,$campos,$submit);
		break;
	case "graba_modi":
		$id_garantia=$_POST["id_garantia"];
		$equipo=$_POST["equipo"];
		$detalle=$_POST["detalle"];
		$fecha_compra=a_fecha_sistema($_POST["fecha_compra"]);
		$fecha=a_fecha_sistema($_POST["fecha"]);
		$plazo=$_POST["plazo"];
		$proveedor=$_POST["proveedor"];
		$factura=$_POST["factura"];
		$responsable=$_POST["responsable"];
		$puesto=$_POST["puesto"];
		$serial_num=$_POST["serial_num"];
		$id_gasto=$_POST["id_gasto"];
		$borrar=$_POST["borrar"];
		if($borrar=="s")
		{
			mi_query("delete from garantias where id_garantia='$id_garantia'","Error al borrar el registro");
			mensaje("Se borro el registro $id_garantia");
		}else
		{
			mi_query("update garantias set equipo='$equipo',detalle='$detalle',fecha_compra='$fecha_compra',plazo='$plazo',proveedor='$proveedor',factura='$factura',responsable='$responsable',puesto='$puesto',fecha='$fecha',serial_num='$serial_num',id_gasto='$id_gasto' where id_garantia='$id_garantia'","Error al modificar garantias");
			mensaje("Modificaci&oacute;n de $id_garantia grabada");
		}
		delay();
		break;
	default:
		$hoy=hoy();
		$tit_alta="NUEVA GARANTIA";
		$campos="%ROT-FECHA ALTA:##$hoy";
		$campos.=";%SEL-equipo-equipo-select id_equipo,descripcion from equipo order by 2-descripcion+id_equipo";
		$campos.=";%ARE-descripcion-detalle--1-50";
		$campos.=";%TXT-S/N-serial_num-50-10";
		$campos.=";%FEC-fecha compra-fecha_compra--10";
		$campos.=";%TXT-meses de garantia-plazo-12-10";
		$campos.=";%SEL-proveedor-proveedor-select codigo,razon from proveedores where id_clase=1 order by 2-razon+codigo";
		$campos.=";%TXT-factura-factura--20";
		$campos.=";%SEL-responsable-responsable-select usuario,nombre from usuarios order by 2-nombre+usuario";
		$campos.=";%TXT-puesto-puesto--10";
		$campos.=";%TXT-ID GASTO-id_gasto--10";
		$campos.=";%OCU-fecha-$hoy";
		$campos.=";%OCU-panta-graba_alta";
		mi_panta($tit_alta,$campos,$submit);
		$hay=un_dato("select count(*) from garantias");
		if($hay)
		{
			raya();
			$que_filtro="";
			$filtro=$_GET['filtro'];
			if($filtro<>"")
			{
				switch($filtro)
				{
					case "TODAS":
						$que_filtro="";
						$subtitulo="Todas las garantias";
						break;
					case "VIGENTES":
						$que_filtro=" and date_add(g.fecha_compra,interval g.plazo month)>=curdate()";
						$subtitulo="Garantias Vigentes";
						break;
					case "VENCIDAS":
						$que_filtro=" and date_add(g.fecha_compra,interval g.plazo month)<curdate()";
						$subtitulo="Garantias Vencidas";
						break;
				}
			}else
			{
				$que_filtro=" and date_add(g.fecha_compra,interval g.plazo month)>=curdate()";
				$subtitulo="Garantias Vigentes";
			}
			$titulos="id;equipo;detalle;S/N;fecha compra;plazo;proveedor;factura;responsable";
			$sql="select g.id_garantia,e.descripcion,g.detalle,g.serial_num,g.fecha_compra,g.plazo,p.razon,g.factura,u.nombre from garantias g,equipo e,proveedores p,usuarios u where g.equipo=e.id_equipo and g.proveedor=p.codigo and g.responsable=u.usuario $que_filtro order by g.fecha_compra;coabm_garantias.php+id_garantia+panta+modi";
			//trace($sql);
			//mi_titulo("GARANTIAS CARGADAS");
			mi_tabla("i");
			echo("<tr><td><a href=coabm_garantias.php?filtro=VIGENTES>Vigentes</a></td>");
			echo("<td><a href=coabm_garantias.php?filtro=VENCIDAS>Vencidas</a></td><td>");
			echo("<td><a href=coabm_garantias.php?filtro=TODAS>Todas</a></td><td>");
			mi_tabla("f");
			mi_titulo($subtitulo);
			tabla_cons($titulos,$sql,1,"silver","#8EC99F",0,"ACTUALIZ.","MODIFICAR","","Garantias;Garantias;Garantias");
		}else
		{
			mensaje("No hay garantias para mostrar");
		}
		un_boton("Volver","Volver","copanel.php");
		break;
}
cierre();
?>