<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Presupuesto de sistemas");
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Aceptar-copanel.php";
mi_titulo("Presupuesto de Sistemas");
if(isset($_GET["panta"]))
{
	$panta=$_GET["panta"];
}else
{
	$panta=$_POST["panta"];
}

ver_caja("Pantalla $panta","absolute",780,0,230,0,"pink");
switch($panta)
{
	case "nuevo":
		$periodo=un_dato("select max(left(mes,4))+1 from presupuesto");
		$per_actu=un_dato("select left(curdate(),4)+1");
		if($periodo<$per_actu)
			$periodo=$per_actu;
		$tit_panta="Presupuesto Nuevo";
		$campos="%TXT-periodo-periodo-$periodo-4";
		$campos.=";%OCU-panta-generar";
		$submit="aceptar-Aceptar-copresup_gastos.php";
		mi_panta($tit_panta,$campos,$submit);
		break;
	case "generar":
		//mensaje("Estoy en generar");
		/*
		Tengo que generar registros en la tabla presupuesto. Es uno por mes (enero a dic) por cada rubro existente.
		Tengo que calcular y completar los campos que pueda calcular:
		promedio historico (sale de la tabla gastos),
		promedio bejerman (sale del sistema Bejerman)
		mes (200701, 200702 etc.),
		rubro sale de la tabla rubro,
		previsto, sale de la tabla renov_hardware
		*/
		$periodo=$_POST["periodo"];
		mensaje("Agregando presupuesto para el periodo $periodo");
		$qrubros=mi_query("select rubro from rubro");
		while($datos=mysql_fetch_array($qrubros))
		{
			$rubro=$datos["rubro"];
			for($i=1; $i<=12; $i++)
			{
				$mes="00" . $i;
				$largo=strlen($mes);
				$mes=substr($mes,$largo-2);
				$mes=$periodo . $mes;
				//trace("Rubro: $rubro, mes: $mes");
				// Tengo que calcular el valor historico para este rubro de la tabla gastos
				$meses_prom=un_dato("select count(distinct left(fecha_imput,7)) from gastos");
				$total_prom=un_dato("select sum(importe_total) from gastos where rubro='$rubro'");
				$prom_historico=$total_prom/$meses_prom;
				switch($rubro)
				{
					case "insumos":
						// Si el rubro es insumos tengo que calcular el promedio de consumo mensual del ultimo anio.
						$consumo_12=un_dato("select sum(total_conf) from pedido_cab where fecha between date_sub(curdate(),interval 1 year ) and curdate()");
						$previsto=$consumo_12/12;
						//trace("Consumo: $consumo_12. Prom.: $prom_consumo");
						break;
					case "hardware":
						// Si el rubro es hardware, tengo que calcular todo lo que haya ya previsto en la tabla renov_hardware
						$previsto= un_dato("SELECT sum(if(substring(fecha_prog,6,2)='".substr($mes,4)."',ppto_estimado,0)) FROM renov_hardware WHERE left(fecha_prog,4) = '".$periodo."'");
						break;
					case "abonos":
						$previsto=un_dato("select sum(max_importe) from abonos");
						break;
					case "software":
						$que_mes=substr($mes,4,2);
						$previsto=un_dato("select sum(presupuesto) from desarrollo_software where left(fecha_prog,4)='$periodo' and substr(fecha_prog,6,2)='$que_mes'");
						break;
					default:
						$previsto=0;
				}
				//Real inicializa en cero
				$real = 0;
				//Desvio inicializa en cero
				$desvio = 0;
				// Tengo que calcular para ese rubro el historico de Bejerman
				$prom_bejerman=0;
				$existe=un_dato("select count(*) from presupuesto where mes='$mes' and rubro='$rubro'");
				if(!$existe)
				{
					//Armo la consulta en función de los datos obtenidos, y pongo en presup el valor de previsto
					mi_query("insert into presupuesto set mes='$mes',rubro='$rubro',valor_real='$real',desvio='$desvio',prom_historico='$prom_historico',presup='$previsto',previsto='$previsto',prom_bejerman='$prom_bejerman'","Error al insertar un registro en el presupuesto");
					$que_mes=substr($mes,5,2);
					$que_per=substr($mes,0,4);
					ver_caja("Periodo: $que_mes/$que_per Rubro $rubro","relative",300,30,300,0,"pink");
				}
			}
		}
		delay();
		break;
	case "actu":
		//Actualizacion de presupuesto
		//Armo la pantalla a mostrar. Luego voy a case "actuPeriodo"
		$actual=un_dato("select year(curdate())");
		$tit_panta="Actualizar Presupuesto";
		$campos.=";%SEL-periodo-periodo-select distinct(left(mes,4)) as cod_per from presupuesto-cod_per-$actual-$actual";
		//$campos="%SEL-periodo-periodo-$sel_periodos-0";
		$campos.=";%OCU-panta-actuPeriodo";
		$submit="aceptar-Aceptar-copresup_gastos.php";
		mi_panta($tit_panta,$campos,$submit);
		break;
	case "actuPeriodo":
		$campo_per=$_POST["periodo"];
		mensaje("Periodo: ".$campo_per);
		//Empiezo a armar la tabla que me mostrará los datos por rubros, luego iré a "rubroPeriodo"
		$ap_rub='S';
		if(tabla_existe("copetin","presupuesto"))
		{
			$sql_borrar=mysql_query("drop temporary table if exists presup_mes") or die("Error: no fue posible borrar la tabla presup_mes");
		}	

		$meses = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
		//Armo la consulta con todo lo que necesito
		$sql="create temporary table presup_mes select rubro, left(mes,4) as periodo, ";
		for($i=1; $i<=12; $i++){
			$mes="00" . $i;
			$largo=strlen($mes);
			$mes=substr($mes,$largo-2);
			$z = $i - 1;
			$sql.="sum(if(right(mes,2)='$mes',presup,0)) as $meses[$z], ";
		}
		$sql = substr($sql, 0, strlen($sql)-2);
		$sql.=" from presupuesto WHERE left(mes,4) = '".$campo_per."' group by 1,2";
		//trace($sql);
		//die();
		//trace("Estoy en actuPeriodo");
		mi_query($sql,"Error al armar la tabla de presup_mes");
		parametros("crear");
		
		if($campo_rub<>$campo_rub_orig)
			parametros("filtro","rubro","$campo_rub","Rubro","in");
		if($campo_per<>$campo_per_orig)
			parametros("filtro","periodo","$campo_per","Periodo","in");
		parametros("agrup","rubro","$ap_rub","Rubro");

		//Armo el encabezado de la tabla
		$encabezado = "";
		for($i=0; $i<12; $i++){
			$encabezado.=$meses[$i].";";
		}
		$encabezado .= "Accion";

		$ap_ini=parametros("agrup_ini");
		$ap_fin=" group by periodo" . parametros("agrup_fin");
		$encabezado=parametros("agrup_cab") . $encabezado;
		$sql = "create temporary table tmp_presupmes select " . $ap_ini;
		for($i=0; $i<12; $i++){
				$sql.="sum(" . $meses[$i] .  ")  as " . $meses[$i] . ",";
		}
		$sql = substr($sql, 0, strlen($sql)-1)." from presup_mes where 1=1 " . $ap_fin;

		mi_query("drop temporary table if exists tmp_presupmes","Error al borrar la tabla tmp_presupmes");
		mi_query($sql,"Error al crear tabla temporal tmp_presupmes");
		// Calculo totales (Rodrigo)
		for($i=0; $i<12; $i++){
			$total=un_dato("select sum($meses[$i]) from tmp_presupmes where rubro<>'Total'");
			$sql="update tmp_presupmes set $meses[$i]=$total where rubro='Total'";
			mi_query($sql,"Error al calcular el total para $meses[$i]");
		}
		$sql="select * from tmp_presupmes";
		$hay=un_dato("select count(*) from tmp_presupmes");
		if($hay)
		{
			tabla_cons($encabezado,$sql.";copresup_gastos.php+rubro+panta+rubroPeriodo+periodo+".$_POST['periodo']."","1","","",2,"","Entrar",0,"Actualizacion presupuesto;actu_presup;actu_presup");
		}
		un_boton("volver","Volver", "copresup_gastos.php", "panta", "actu");
		break;
	case "rubroPeriodo":
		//Tomo los parámetros que necesito
		$rubro = $_POST["rubro"];
		$periodo = $_POST["periodo"];
		mensaje($rubro." ".$periodo);
		//Armo la tabla que me mostrará los detalles del rubro para ese mes y periodo
		$titulos="id;Mes;Prom. Hist;Pres. Ant;Previsto;Presupuesto";
		//Chequeo si existe un presupuesto anterior
		$existe_anterior = un_dato("SELECT count(*) from presupuesto where (left(mes,4))='".($periodo-1)."' and rubro='$rubro'");
		if ($existe_anterior != 0){
			$sql="select p.id, (right(p.mes,2)), p.prom_historico, sum(s.presup), p.previsto, p.presup from presupuesto p, presupuesto s where left(p.mes,4) = ".$periodo." and p.rubro = '".$rubro."' and left(s.mes,4) = ".($periodo-1)." and s.rubro = '".$rubro."' and (right(s.mes,2)) = (right(p.mes,2)) group by 1 order by (right(p.mes,2)) asc" ;
		}else{
			$sql="select id, (right(mes,2)), prom_historico, sum(0), previsto, sum(presup) from presupuesto where left(mes,4) = ".$periodo." and rubro = '".$rubro."' group by 1 order by (right(mes,2)) asc" ;
		}
		//Tabla con un botón "Modificar" para cada registro, luego va a "modiMes"
		tabla_cons($titulos,$sql.";copresup_gastos.php+id+panta+modiMes+periodo+".$periodo."",1,"","","0;0;2;2;2;2","ACTUALIZ.","Modificar");
		un_boton("volver","Volver", "copresup_gastos.php", "panta;periodo", "actuPeriodo;$periodo");		
		break;
	case "modiMes":
		//Tomo los parámetros que necesito
		$id=$_POST["id"];
		$periodo = $_POST["periodo"];
		$mes=un_dato("select (right(mes,2)) from presupuesto where id=$id");
		$rubro=un_dato("select rubro from presupuesto where id=$id");
		$presup=un_dato("select presup from presupuesto where id=$id");
		//Traigo el presupuesto anterior correspondiente
		//Chequeo si existe un presupuesto anterior
		$existe_anterior = un_dato("select count(presup) from presupuesto where (left(mes, 4)) = '".($periodo-1)."' and (right(mes,2)) = '".$mes."' and rubro ='".$rubro."'");
		if ($existe_anterior != 0)
			$presu_ant = un_dato("select presup from presupuesto where (left(mes, 4)) = '".($periodo-1)."' and (right(mes,2)) = '".$mes."' and rubro ='".$rubro."'");
		else
			$presu_ant = 0;
		$prom_historico=un_dato("select prom_historico from presupuesto where id=$id");
		//Armo la página a mostrar sólo con la edición de presupuesto. Luego voy a case "graba_modi_mes"
		$titulo="Actualizaci&oacute;n del rubro $rubro, periodo $mes / $periodo";
		$campos_pantalla="%OCU-id-$id;";
		$campos_pantalla.="%OCU-mes-$mes;";
		$campos_pantalla.="%OCU-periodo-$periodo;";
		$campos_pantalla.="%ROT-<tr><td>Prom Historico</td><td>$prom_historico</td></tr>;";
		$campos_pantalla.="%ROT-<tr><td>Presup Anterior</td><td>$presu_ant</td></tr>;";
		$campos_pantalla.="%TXT-presupuesto-presup-$presup;";
		$campos_pantalla.="%OCU-panta-graba_modi_mes";
		$submit="aceptar-Modificar-copresup_gastos.php+rubroPeriodo+periodo+$periodo+rubro+$rubro";	
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
	case "graba_modi_mes":
		//Traigo los datos necesarios
		$id=$_POST["id"];
		$periodo = $_POST["periodo"];
		$mes=$_POST["mes"];
		$presup=$_POST["presup"];
		$rubro= un_dato("select rubro from presupuesto where id= $id");
		//Actualizo presup para ese registro
		mi_query("update presupuesto set presup='$presup' where id='$id'","copresup_gastos.php Linea 295. Imposible grabar modificacion de presupuesto.");
		mensaje("Se modific&oacute; el presupuesto del mes $mes para el Periodo $periodo.");
		un_boton("aceptar","Aceptar", "copresup_gastos.php", "panta;periodo;rubro", "rubroPeriodo;$periodo;$rubro");
		break;
	case "ctrl":
		//Control y seguimiento presupuestario
		//Armo la pantalla a mostrar. Luego voy a case "ctrlPeriodo"
		$actual=un_dato("select year(curdate())");
		$tit_panta="Seguimiento y Control Presupuestario";
		$campos.=";%SEL-periodo-periodo-select distinct(left(mes,4)) as cod_per from presupuesto-cod_per-$actual-$actual";
		$campos.=";%SEL-vista-vista-TOTAL+TOTAL+DETALLE+DETALLE+EVOLUCION+EVOLUCION-0";
		$campos.=";%OCU-panta-ctrlPeriodo";
		$submit="aceptar-Aceptar-copresup_gastos.php";
		mi_panta($tit_panta,$campos,$submit);
		break;
	case "ctrlPeriodo":
		//Tomo los parametros
		$vista=$_POST["vista"];
		$campo_per=$_POST["periodo"];
		mi_titulo("Control Presupuestario - Periodo $campo_per");

		//Actualizo los datos de presupuesto
		//Cambio los datos hasta el mes actual - 1
		$hasta =  substr(hoy(),3,2) - 1;
		$qrubros=mi_query("select rubro from rubro");
		while($datos=mysql_fetch_array($qrubros))
		{
			$rubro=$datos["rubro"];
			for($i=1; $i<=$hasta; $i++)
			{
				$mes="00" . $i;
				$largo=strlen($mes);
				$mes=substr($mes,$largo-2);
				//real
				$real = un_dato("SELECT sum(if(substring(fecha_imput,6,2)='".$mes."',importe_total,0)) FROM gastos where left(fecha_imput,4)='".$campo_per."' and rubro = '".$rubro."'");
				if ($real == 0)
					$valor_real = 0;
				else
					$valor_real = $real;
				//desvio
				$presup = un_dato("SELECT presup FROM presupuesto where (left(mes,4)) = '$campo_per' and (right(mes,2)) = '$mes' and rubro = '$rubro'");
				if ($presup > 0)
				{
					$desvio=($valor_real-$presup)/$presup*100;
				}else
				{
					$desvio = 0;
				}
				//Actualizo presupuesto con valor_real y desvio
				mi_query("update presupuesto set valor_real=$valor_real, desvio = $desvio where rubro='$rubro' and mes ='". $campo_per.$mes."'","copresup_gastos.php Linea 349. Imposible grabar modificacion de presupuesto.");
			}
		}
		// Actualizo los gastos de insumos en el presupuesto
		//pedido_a_gasto(); Esta funcion es para correr una sola vez
		$q1=mi_query("select id,mes from presupuesto where rubro='insumos' and left(mes,4)=$campo_per","Error al obtener los registros de insumos para actualizar");
		while($datos=mysql_fetch_array($q1))
		{
			$id=$datos["id"];
			$mes=$datos["mes"];
			$valor_real=un_dato("select sum(total_conf) from pedido_cab where  concat(left(fecha,4),substr(fecha,6,2))='$mes' and estado='FINALIZADO'");
			mi_query("update presupuesto set valor_real='$valor_real' where id='$id'","Error al actualizar el presupuesto con el valor real de insumos");
		}

		switch($vista)
		{
		case "TOTAL":
			$mes_actual=mes(un_dato("select month(curdate())-1"));
			mi_titulo("Acumulado anual hasta $mes_actual. Importes con iva");
			$sql="select 'Total' as rubro,round(sum(presup),2) as presupuesto,round(sum(valor_real),2) as vreal,((sum(valor_real)-sum(presup))/sum(presup))*100 as desvio from presupuesto where left(mes,4)='$campo_per' and right(mes,2)<month(curdate())";
			tabla_cons("Rubro;Presup.;Real;Desvio",$sql.";copresup_gastos.php+rubro+panta+detalle_tot+periodo+$periodo+pnt_ant+ctrlPeriodo+vista+$vista","1","","",2,"DETALLE","ENTRAR",0,"Control Presupuestario;ctl_presup;ctl_presup");
			$real=un_dato("select sum(valor_real) from presupuesto where left(mes,4)='$campo_per'");
			$ppto=un_dato("select sum(presup) from presupuesto where left(mes,4)='$campo_per'");
			$ppto=($ppto==0) ?1 : $ppto;
			$relac=round($real/$ppto*100,2);
			$real=round($real,2);
			mensaje("Ppto. total $periodo: $ppto. Gasto real acum.: $real");
			mensaje("Se utiliz&#243; un $relac % del presupuesto del per&#237;odo");
			break;
		case "EVOLUCION":
			$rubro=$_POST["rubro"];
			if(isset($_POST["rubro"]))
			{
				$rubro=$_POST["rubro"];
				$filtro=" and rubro='$rubro'";
				$filtro=($rubro=="TODOS") ? "" : $filtro;
			}else
			{
				$filtro="";
			}
			mi_titulo("Evoluci&oacute;n del presupuesto");
			mensaje("Importes con iva");
			if($rubro<>"")
				mi_titulo("Rubro: $rubro.");
			$sql="create temporary table evol_presup (dato varchar(12),enero float,febrero float,marzo float,abril float,mayo float,junio float,julio float,agosto float,septiembre float,octubre float,noviembre float,diciembre float)";
			mi_query($sql,"Error al armar la tabla de evolucion del presupuesto");
			$presup_ene=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','01') $filtro");
			$real_ene=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','01') $filtro");
			$presup_feb=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','02') $filtro");
			$real_feb=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','02') $filtro");
			$presup_mar=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','03') $filtro");
			$real_mar=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','03') $filtro");
			$presup_abr=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','04') $filtro");
			$real_abr=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','04') $filtro");
			$presup_may=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','05') $filtro");
			$real_may=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','05') $filtro");
			$presup_jun=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','06') $filtro");
			$real_jun=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','06') $filtro");
			$presup_jul=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','07') $filtro");
			$real_jul=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','07') $filtro");
			$presup_ago=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','08') $filtro");
			$real_ago=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','08') $filtro");
			$presup_sep=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','09') $filtro");
			$real_sep=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','09') $filtro");
			$presup_oct=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','10') $filtro");
			$real_oct=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','10') $filtro");
			$presup_nov=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','11') $filtro");
			$real_nov=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','11') $filtro");
			$presup_dic=un_dato("select sum(presup) from presupuesto where mes=concat('$campo_per','12') $filtro");
			$real_dic=un_dato("select sum(valor_real) from presupuesto where mes=concat('$campo_per','12') $filtro");
			mi_query("insert into evol_presup set dato='presupuesto',enero='$presup_ene',febrero='$presup_feb',marzo='$presup_mar',abril='$presup_abr',mayo='$presup_may',junio='$presup_jun',julio='$presup_jul',agosto='$presup_ago',septiembre='$presup_sep',octubre='$presup_oct',noviembre='$presup_nov',diciembre='$presup_dic'");
			mi_query("insert into evol_presup set dato='real',enero='$real_ene',febrero='$real_feb',marzo='$real_mar',abril='$real_abr',mayo='$real_may',junio='$real_jun',julio='$real_jul',agosto='$real_ago',septiembre='$real_sep',octubre='$real_oct',noviembre='$real_nov',diciembre='$real_dic'");
			$sql="select * from evol_presup";
			$encabezado="dato;enero;febrero;marzo;abril;mayo;junio;julio;agosto;septiembre;octubre;noviembre;diciembre";
			tabla_cons($encabezado,$sql,"1","","",2,"","",0,"Control Presupuestario;ctl_presup;ctl_presup");
			$tit_panta="Por Rubro";
			$campos.=";%SEL-rubro-Rubro-select distinct rubro from gastos order by 1-rubro-TODOS-TODOS";
			$campos.=";%OCU-panta-ctrlPeriodo";
			$campos.=";%OCU-vista-EVOLUCION";
			$campos.=";%OCU-periodo-$campo_per";
			$submit="aceptar-Aceptar-copresup_gastos.php";
			mi_panta($tit_panta,$campos,$submit);
			$leyenda=mkgrafico("evol_presup","","dato");
			graficar("Presupuesto de gastos","meses","importe",250,750,"linepoints",$leyenda);
			//un_boton("volver","Volver", "copresup_gastos.php", "panta", "ctrl");
			break;
		case "DETALLE":
			mi_titulo("Abierto por rubro y mes. Importes con IVA");
			//Comienzo con la tabla a mostrar
			$ap_rub='S';
			if(tabla_existe("copetin","presupuesto"))
			{
				$sql_borrar=mysql_query("drop temporary table if exists presup_mes") or die("Error: no fue posible borrar la tabla presup_mes");
			}

			$meses = array("enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre");
			$campos = array("presup", "valor_real", "desvio");
			$campos_n = array("_pres","_real","_desvio");
			//Armo la consulta con todos los campos necesarios
			$sql="create temporary table presup_mes select id, rubro, left(mes,4) as periodo, ";

			for($i=1; $i<=12; $i++){
				$mes="00" . $i;
				$largo=strlen($mes);
				$mes=substr($mes,$largo-2);
				for ($j=0;$j<3;$j++){
					$z = $i - 1;
					$sql.="sum(if(right(mes,2)='$mes',$campos[$j],0)) as $meses[$z]$campos_n[$j], ";
				}
			}
			$sql = substr($sql, 0, strlen($sql)-2);
			$sql.=" from presupuesto WHERE left(mes,4) = '".$campo_per."' group by 1,2,3";
			//trace($sql);
			//die();
			mi_query($sql,"Error al armar la tabla de presup_mes");

			parametros("crear");
			parametros("filtro","rubro","$filtro_rub","Rubro","=");
			parametros("filtro","periodo","$filtro_per","Periodo","=");
			if($campo_rub<>$campo_rub_orig)
				parametros("filtro","rubro","$campo_rub","Rubro","in");
			if($campo_per<>$campo_per_orig)
				parametros("filtro","periodo","$campo_per","Periodo","in");
			parametros("agrup","rubro","$ap_rub","Rubro");

			//Preparo el encabezado de la tabla
			$encabezado = "";
			for($i=0; $i<$hasta; $i++){
				for ($j=0;$j<3;$j++){
					if ($j == 2)
						$encabezado.=$meses[$i].$campos_n[$j]." %;";
					else
						$encabezado.=$meses[$i].$campos_n[$j].";";
				}
			}
			//Reemplazo _ por " "
			$encabezado = str_replace("_", " ", $encabezado);

			$ap_ini=parametros("agrup_ini");
			$ap_fin=" group by periodo" . parametros("agrup_fin");
			$encabezado=parametros("agrup_cab") . $encabezado;
			$sql = "create temporary table tmp_presupmes select " . $ap_ini;
			for($i=0; $i<$hasta; $i++){
				for ($j=0;$j<3;$j++){
					$sql.="sum(".$meses[$i].$campos_n[$j]."),";
				}
			}
			$sql = substr($sql, 0, strlen($sql)-1)." from presup_mes where 1=1 " . $ap_fin;

			mi_query("drop temporary table if exists tmp_presupmes","Error al borrar la tabla tmp_presupmes");
			mi_query($sql,"Error al crear tabla temporal tmp_presupmes");
			//trace($sql);
			//die();
			$sql="select * from tmp_presupmes";
			$hay=un_dato("select count(*) from tmp_presupmes");
			if($hay)
			{
				//Armo la tabla
				$encabezado=substr($encabezado,0,strlen($encabezado)-1);
				//$xls=excel("hola","Hola","show tables","una_hoja","un_archivo");
				//excel_lnk($xls);
				tabla_cons($encabezado,$sql.";copresup_gastos.php+rubro+panta+detalle_ctl+periodo+$periodo+hasta+$hasta+pnt_ant+ctrlPeriodo+vista+$vista","1","","",2,"DETALLE","ENTRAR",0,"Control Presupuestario;ctl_presup;ctl_presup");
				//tabla_cons($titulo,$sql,$borde,$color,$cuerpo,$decimales="0",$tit_lnk="DETALLE",$btn_lnk="Entrar",$tgt="",$param_excel="")
			}
			break;
		}
		un_boton("volver","Volver", "copresup_gastos.php", "panta", "ctrl");
		break;
	case "detalle_tot":
		$periodo=$_POST["periodo"];
		$hasta =  substr(hoy(),3,2) - 1;
		$pnt_ant=$_POST["pnt_ant"];
		$vista=$_POST["vista"];
		$mes_actual=mes(un_dato("select month(curdate())-1"));
		mi_titulo("Total acumulado del periodo $periodo hasta $mes_actual por Rubro. Importes con IVA");
		$sql="select rubro,round(sum(ifnull(presup,0)),2) as presupuesto,round(sum(ifnull(valor_real,0)),2) as vreal,concat(round(ifnull(((sum(ifnull(valor_real,0))-sum(ifnull(presup,0)))/sum(ifnull(presup,0))),0)*100,2),'%') as desvio from presupuesto where left(mes,4)='$periodo' and right(mes,2)<month(curdate()) group by rubro union select 'Total' as rubro,round(sum(presup),2) as presupuesto,round(sum(valor_real),2) as vreal,concat(round(ifnull(((sum(ifnull(valor_real,0))-sum(ifnull(presup,0)))/sum(ifnull(presup,0))),0)*100,2),'%') as desvio from presupuesto where left(mes,4)='$periodo' and right(mes,2)<month(curdate()) group by 1";
		tabla_cons("Rubro;Presup.;Real;Desvio",$sql.";copresup_gastos.php+rubro+panta+detalle_ctl+periodo+$periodo+hasta+$hasta+pnt_ant+detalle_tot","1","","",2,"DETALLE","ENTRAR",0,"Control Presupuestario;ctl_presup;ctl_presup");
		un_boton("volver","Volver", "copresup_gastos.php", "panta;periodo;vista", "$pnt_ant;$periodo;$vista");
		break;
	case "detalle_ctl":
		$periodo=$_POST["periodo"];
		$rubro=$_POST["rubro"];
		$hasta=$_POST["hasta"];
		$pnt_ant=$_POST["pnt_ant"];
		mi_titulo("Control Presupuestario $periodo. Rubro $rubro.");
		$sql="select right(mes,2) as mes,round(presup,2) as presup,round(valor_real,2) as valor_real,concat(round(((valor_real-presup)/presup*100),2),'%') as desvio from presupuesto where left(mes,4)='$periodo' and rubro='$rubro' and right(mes,2)<=$hasta union select 'Total' as mes,round(sum(presup),2) as presup,round(sum(valor_real),2) as valor_real,concat(round(((sum(valor_real)-sum(presup))/sum(presup)*100),2),'%') as desvio from presupuesto where left(mes,4)='$periodo' and rubro='$rubro' and right(mes,2)<=$hasta group by 1 order by 1";
		$sql=$sql . " ;copresup_gastos.php+mes+rubro+$rubro+panta+det_gastos+periodo+$periodo+pnt_ant+detalle_ctl+hasta+$hasta";
		$encabezado="mes;presupuesto;valor real;desvio";
		//trace($sql);
		//die();
		if($rubro=="Total" or $rubro=="total")
		{
			mensaje("Rubro inexistente");
		}else
		{
		//trace($sql);
			tabla_cons($encabezado,$sql,"1","","",2,"DETALLE","ENTRAR","","Control Presupuestario $rubro;$rubro;$rubro");
			$real=un_dato("select sum(valor_real) from presupuesto where left(mes,4)='$periodo' and rubro='$rubro'");
			$ppto=un_dato("select sum(presup) from presupuesto where left(mes,4)='$periodo' and rubro='$rubro'");
			$relac=round($real/$ppto*100,2);
			mensaje("Se utiliz&#243; un $relac % del presupuesto del per&#237;odo");			
		}
		un_boton("volver","Volver", "copresup_gastos.php", "panta;periodo", "$pnt_ant;$periodo");
		break;
	case "det_gastos":
		$mes=$_POST["mes"];
		$rubro=$_POST["rubro"];
		$periodo=$_POST["periodo"];
		$pnt_ant=$_POST["pnt_ant"];
		$hasta=$_POST["hasta"];
		mi_titulo("Rubro: $rubro. Periodo: $mes" . "/" . "$periodo");
		mi_titulo("DETALLE DE GASTOS");
		//mensaje("Estoy en panta det_gastos con mes $mes rubro $rubro periodo $periodo panta ant $pnt_ant");
		if($mes<>"total" and $mes<>"Total")
		{
			if($rubro=="insumos")
			{
				$sql="select c.fecha,c.numero,p.razon,c.total_conf as importe from pedido_cab c,proveedores p where c.estado='FINALIZADO' and left(c.fecha,4)='$periodo' and substr(c.fecha,6,2)='$mes' and c.proveedor=p.codigo";
				tabla_cons("fecha;pedido;proveedor;importe",$sql,"1","","","0;0;0;2");
				$total_acum=un_dato("select sum(c.total_conf) from pedido_cab c,proveedores p where c.estado='FINALIZADO' and left(c.fecha,4)='$periodo' and substr(c.fecha,6,2)='$mes' and c.proveedor=p.codigo");
				$total_acum=number_format($total_acum,"2",",",".");
				mensaje("Total: $total_acum");
			}
			if($rubro<>"insumos")
			{
				$sql="select fecha_imput,rubro,concepto,fac_numero,proveedor,importe_total from gastos where left(fecha_imput,4)='$periodo' and substring(fecha_imput,6,2)='$mes' and rubro='$rubro'";
				tabla_cons("fecha;rubro;concepto;factura;proveedor;importe",$sql,"1","","","0;0;0;0;0;2");
				$total_acum=un_dato("select sum(importe_total) from gastos where left(fecha_imput,4)='$periodo' and substring(fecha_imput,6,2)='$mes' and rubro='$rubro'");
				$total_acum=number_format($total_acum,"2",",",".");
				mensaje("Total: $total_acum");
			}
		}
		else
		{
			mensaje("No se puede mostrar detalle para total");
		}
		un_boton("volver","Volver", "copresup_gastos.php", "panta;periodo;rubro;hasta", "$pnt_ant;$periodo;$rubro;$hasta");
		break;
	default:
		$mensaje="Presupuesto de Sistemas";
		echo("<ul>");
		echo($mensaje);
		echo("<ul><a href='copresup_gastos.php?panta=nuevo'>Nuevo Presupuesto</a></ul>");
		echo("<ul><a href='copresup_gastos.php?panta=actu'>Actualizacion Presupuesto</a></ul>");
		echo("<ul><a href='copresup_gastos.php?panta=ctrl'>Seguimiento y Control Presupuestario</a></ul>");
		echo("</ul>");
		un_boton("aceptar","Aceptar", "copanel.php");
		
}
cierre();
?>