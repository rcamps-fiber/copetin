<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Evolucion de los gastos</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Aceptar-copanel.php";
$titulo="Evolucion de los gastos";
$panta=$_POST["panta"];
switch($panta)
{
case "procesar":
	$hora_ini=time();
	//trace($hora_ini);
	// Recupero parametros
	$campo_rub=$_POST["campo_rub"];
	$campo_prv=$_POST["campo_prv"];
	$campo_per=$_POST["campo_per"];
	$filtro_rub=$_POST["filtro_rub"];
	$filtro_prv=$_POST["filtro_prv"];
	$filtro_per=$_POST["filtro_per"];
	$ap_rub=$_POST["ap_rub"];
	$ap_prv=$_POST["ap_prv"];
	$campo_rub_orig=$_POST["campo_rub_orig"];
	$campo_prv_orig=$_POST["campo_prv_orig"];
	$campo_per_orig=$_POST["campo_per_orig"];
	$con_iva=$_POST["con_iva"];
	$importe=($con_iva=="S") ? "importe_total" : "importe";
	mi_titulo($titulo);
	if(tabla_existe("copetin","gastos"))
	{
		$sql_borrar=mysql_query("drop temporary table if exists evol_gastos") or die("Error: no fue posible borrar la tabla evol_gastos");
	}	
	$sql="create temporary table evol_gastos select upper(rubro) as rubro,upper(proveedor) as proveedor,left(fecha_imput,4) as periodo,sum(if(substring(fecha_imput,6,2)='01',$importe,0)) as enero,";
	$sql.="sum(if(substring(fecha_imput,6,2)='02',$importe,0)) as febrero,";
	$sql.="sum(if(substring(fecha_imput,6,2)='03',$importe,0)) as marzo,";
	$sql.="sum(if(substring(fecha_imput,6,2)='04',$importe,0)) as abril,";
	$sql.="sum(if(substring(fecha_imput,6,2)='05',$importe,0)) as mayo,";
	$sql.="sum(if(substring(fecha_imput,6,2)='06',$importe,0)) as junio,";
	$sql.="sum(if(substring(fecha_imput,6,2)='07',$importe,0)) as julio,";
	$sql.="sum(if(substring(fecha_imput,6,2)='08',$importe,0)) as agosto,";
	$sql.="sum(if(substring(fecha_imput,6,2)='09',$importe,0)) as septiembre,";
	$sql.="sum(if(substring(fecha_imput,6,2)='10',$importe,0)) as octubre,";
	$sql.="sum(if(substring(fecha_imput,6,2)='11',$importe,0)) as noviembre,";
	$sql.="sum(if(substring(fecha_imput,6,2)='12',$importe,0)) as diciembre,";
	$sql.="sum($importe) as total";
	$sql.=" from gastos group by 1,2,3";
	//trace($sql);
	
	//$creo_resumen=mysql_query($sql) or die("Error: no se pudo armar la tabla de ventas por mes<br>$sql");
	mi_query($sql,"Error al armar la tabla de evol_gastos");
	$hora_2=time();
	//trace($hora_2-$hora_ini);
	$encabezado="enero;febrero;marzo;abril;mayo;junio;julio;agosto;septiembre;octubre;noviembre;diciembre;total";
	
	parametros("crear");
	parametros("filtro","rubro","$filtro_rub","Rubro","=");
	parametros("filtro","proveedor","$filtro_prv","Proveedor","=");
	parametros("filtro","periodo","$filtro_per","Periodo","=");
	if($campo_rub<>$campo_rub_orig)
		parametros("filtro","rubro","$campo_rub","Rubro","in");
	if($campo_prv<>$campo_prv_orig)
		parametros("filtro","proveedor","$campo_prv","Proveedor","in");
	if($campo_per<>$campo_per_orig)
		parametros("filtro","periodo","$campo_per","Periodo","in");
	parametros("agrup","rubro","$ap_rub","Agrupado por Rubro");
	parametros("agrup","proveedor","$ap_prv","Agrupado por Proveedor");

	$encabezado="enero;febrero;marzo;abril;mayo;junio;julio;agosto;septiembre;octubre;noviembre;diciembre;total";
	$filtro=parametros("mkfiltro");
	$ap_ini=parametros("agrup_ini");
	$ap_fin=" group by periodo" . parametros("agrup_fin");
	$encabezado="Periodo;" . parametros("agrup_cab") . $encabezado;
	$sql="create temporary table tmp_evolgas select periodo," . $ap_ini . "sum(enero) as enero ,sum(febrero) as febrero,sum(marzo) as marzo,sum(abril) as abril,sum(mayo) as mayo,sum(junio) as junio,sum(julio) as julio,sum(agosto) as agosto,sum(septiembre) as septiembre,sum(octubre) as octubre,sum(noviembre) as noviembre,sum(diciembre) as diciembre,sum(total) as total from evol_gastos where 1=1 " . $filtro . $ap_fin;
		
	//trace($sql);
	mi_query("drop temporary table if exists tmp_evolgas","Error al borrar la tabla tmp_evolgas");
	$hora_3=time();
	//trace($hora_3-$hora_2);
	mi_query($sql,"Error al crear tabla temporal tmp_evolgas");
	$sql="select * from tmp_evolgas";
	$hay=un_dato("select count(*) from tmp_evolgas");
	parametros("imprimir");
	if($hay)
	{
		tabla_cons($encabezado,$sql,"0","","",0,"","","","$titulo;gastos;gastos_sis");
		if($ap_rub<>"S" and $ap_prv<>"S")
		{
			//trace($filtro);
			$leyenda=mkgrafico("evol_gastos",$filtro,"periodo");
			graficar("Gastos en pesos","meses","importe",250,750,"linepoints",$leyenda);
		}
	}
	$hora_fin=time();
	//trace($hora_fin-$hora_3);
	volver("");
	break;
default:
	// Pantalla inicial de ingreso de parametros
	
	$qry_rub=mi_query("select distinct upper(rubro) as rubro from gastos order by 1","Error al obtener los rubros");
	$campo_docs="";
	while($datos=mysql_fetch_array($qry_rub))
	{
		if($campo_rub<>"")
			$campo_rub.=",";
		$campo_rub.=$datos["rubro"];
	}
	$campo_rub_orig=$campo_rub;
	$qry_prv=mi_query("select distinct upper(proveedor) as proveedor from gastos order by 1","Error al obtener los proveedores");
	$campo_prv="";
	while($datos=mysql_fetch_array($qry_prv))
	{
		if($campo_prv<>"")
			$campo_prv.=",";
		$campo_prv.=$datos["proveedor"];
	}
	$campo_prv_orig=$campo_prv;
	$qry_per=mi_query("select distinct left(fecha_imput,4) as per from gastos order by 1","Error al obtener los periodos");
	$campo_per="";
	while($datos=mysql_fetch_array($qry_per))
	{
		if($campo_per<>"")
			$campo_per.=",";
		$campo_per.=$datos["per"];
		//$campo_per=str_replace("-","/",$campo_per);
	}
	$campo_per_orig=$campo_per;
	
	$campos="%ROT-<tr><td><strong>Filtros</strong></td></tr>";
	$campos.=";%SEL-filtro_rub-Rubro-select distinct rubro from gastos order by 1-rubro-TODOS-TODOS";
	$campos.=";%TXT-o Elija rubros-campo_rub-$campo_rub-40";
	$campos.=";%ROT-<tr><td colspan='2'><hr></td></tr>";
	$campos.=";%SEL-filtro_prv-Proveedor-select distinct proveedor from gastos order by 1-proveedor-TODOS-TODOS";
	$campos.=";%TXT-o Elija proveedor-campo_prv-$campo_prv-40";
	$campos.=";%ROT-<tr><td colspan='2'><hr></td></tr>";
	$campos.=";%SEL-filtro_per-Periodo-select distinct left(fecha_imput,4) as per from gastos order by 1-per-TODOS-TODOS";
	$campos.=";%TXT-o Elija periodo-campo_per-$campo_per-40";
	$campos.=";%ROT-<tr><td colspan='2'><hr></td></tr>";

	$campos.=";%ROT-<tr><td><strong>Agrupamiento</strong></td></tr>";
	$campos.=";%CHK-Rubro-ap_rub-S-n";
	$campos.=";%CHK-Proveedor-ap_prv-S-n";
	$campos.=";%ROT-<tr><td colspan='2'><hr></td></tr>";
	$campos.=";%ROT-<tr><td><strong>Importes</strong></td></tr>";
	$campos.=";%CHK-Con Iva-con_iva-S-n";
	$campos.=";%ROT-<tr><td colspan='2'><hr></td></tr>";
	$campos.=";%OCU-panta-procesar";
	$campos.=";%OCU-campo_rub_orig-$campo_rub_orig";
	$campos.=";%OCU-campo_prv_orig-$campo_prv_orig";
	$campos.=";%OCU-campo_per_orig-$campo_per_orig";
	mi_panta($titulo,$campos,"aceptar-Aceptar-copanel.php");
}

cierre();
?>
