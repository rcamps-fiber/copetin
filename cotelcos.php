<?php
include 'coacceso.php';
include('coclases.php');
include('cofunciones.php');
$submit="aceptar-Aceptar-copanel.php";
if(isset($_POST["panta"]))
{
	$panta=$_POST["panta"];
}else
{
	if(isset($_GET["panta"]))
	{
		$panta=$_GET["panta"];
	}
}
ver_caja("pantalla: $panta","relative", 1500, -60, 250,20,"#cc99cc");
//ver_caja("pantalla: $panta","relative", 1560, 10, 100,20,"#005959");

//ver_caja($contenido, $pos="relative", $izq=0, $arriba=0, $ancho=800, $alto=100, $fondo="gray");
switch($panta)
{
	case "alta_factura":
	    mi_titulo("Alta de Factura");
	    $id_servicio=0;
	    $nombre_servicio='Elegir';
	    $nro_factura="";
	    $fecha_factura_arg=hoy();
	    $importe=0;
	    $vencimiento_arg=hoy();
	    $proximo_venc_arg=hoy();
	    $fecha_pago=hoy();
	    $observaciones="";
	    $pantaAbmFactura=new PANTALLA();
	    $pantaAbmFactura->titulo="ALTA DE FACTURA";	
	    //$pantaAbmFactura->AgregarSEL("servicio","id_servicio","nombre_servicio",$id_servicio,"Elegir","telcos");
	    //$pantaAbmFactura->AgregarSELCustom2("servicio","id_servicio","nombre_servicio","select id_servicio,nombre_servicio from telcos where anulado<>'S'");
	    $pantaAbmFactura->AgregarSELCustom3("servicio","id_servicio","nombre_servicio","id_servicio",$id_servicio,$nombre_servicio,"select id_servicio,nombre_servicio from telcos where anulado<>'S'");
	    $pantaAbmFactura->AgregarTXT("nro.factura","nro_factura",$nro_factura,10);
	    $pantaAbmFactura->AgregarFEC("fecha factura","fecha_factura",$fecha_factura_arg);
	    $pantaAbmFactura->AgregarTXT("importe","importe",$importe,10);
	    $pantaAbmFactura->AgregarFEC("vencimiento","vencimiento",$vencimiento_arg);
    	    $pantaAbmFactura->AgregarFEC("prox. venc.","proximo_venc",$proximo_venc_arg);                                         
	    $pantaAbmFactura->AgregarFEC("fecha de pago","fecha_pago",$fecha_pago_arg);
	    $pantaAbmFactura->AgregarCHK("pagado","pagado","S","N");
	    $pantaAbmFactura->AgregarARE("observaciones","observaciones",$observaciones,2,40);	    
	    $pantaAbmFactura->AgregarOCU("panta","graba_alta_factura");
	    $pantaAbmFactura->regresar=$_SERVER['PHP_SELF'] . "+abm_facturas";
	    $pantaAbmFactura->FactorAltura=34;
	    $pantaAbmFactura->IncrementoAltura=32;
	    $pantaAbmFactura->recuadro=1;
	    $pantaAbmFactura->izquierda=400;
	    $pantaAbmFactura->tope=0;
	    $pantaAbmFactura->alto=850;
	    $pantaAbmFactura->ancho=700;
	    $pantaAbmFactura->ColorFrente="#ff9999";
	    $pantaAbmFactura->ColorFondo="#cc99cc";
	    $pantaAbmFactura->MostrarPantalla();
	    break;
	case "graba_alta_factura":
	    $hoy=hoy();
	    $hoy_sis=a_fecha_sistema($hoy);
	    $id_servicio=$_POST["id_servicio"];
	    $nombre_servicio=un_dato("select nombre_servicio from telcos where id_servicio='$id_servicio'");
	    $nombre_servicio=($nombre_servicio) ? $nombre_servicio : "Elegir" ;
	    $nro_factura=$_POST["nro_factura"];
	    $fecha_factura_arg=$_POST["fecha_factura"];
	    //trace("Fecha factura arg $fecha_factura_arg");
	    //trace("Hoy es $hoy");
	    $fecha_factura_sis=a_fecha_sistema($fecha_factura_arg);
	    //trace("Fecha factura sis $fecha_factura_sis");
	    $importe=$_POST["importe"];
	    $vencimiento_arg=$_POST["vencimiento"];
	    $vencimiento_sis=a_fecha_sistema($vencimiento_arg);
	    //trace("Venc. arg $vencimiento_arg");
	    //trace("Venc. sis $vencimiento_sis");
	    $proximo_venc_arg=$_POST["proximo_venc"];
	    $proximo_venc_sis=a_fecha_sistema($proximo_venc_arg);
	    $fecha_pago_arg=$_POST["fecha_pago"];
	    $fecha_pago_sis=a_fecha_sistema($fecha_pago_arg);
	    //trace("Fecha pago arg x$fecha_pago_arg x");
	    //trace("Fecha pago sis $fecha_pago_sis");
	    $pagado=$_POST["pagado"];
	    //trace("Pagado es $pagado");
	    $pagado=($pagado=='S') ? 'S' : 'N';
	    //trace("Pagado es $pagado");
	    $observaciones=$_POST["observaciones"];
	    $forzar=$_POST["forzar"];
	    $msj="";
	    if($id_servicio==0){
		$msj="Falta elegir un servicio. ";
	    }
	    if($nro_factura==""){
		$msj.="Falta numero de factura. ";
	    }
	    $existe=un_dato("select count(*) from telco_fact where nro_factura='$nro_factura'");
	    //trace("Existe vale $existe");
	    if($existe){
		$msj.="Ya existe ese nro. de factura.  ";
	    }
	    if($importe<=0){
		$msj.="Importe invalido. ";
	    }
	    if($fecha_factura_sis>$hoy_sis){
		$msj.="Fecha de la factura invalida. ";
	    }
	    if($fecha_pago_arg<>"" and $fecha_pago_sis>$hoy_sis){
		$msj.="Fecha de pago invalida. ";
	    }
	    if($vencimiento_sis<$fecha_factura_sis){
		$msj.="Vencimiento o fecha de pago invalidos. ";
	    }
	    if($proximo_venc_sis<$vencimiento_sis){
		$msj.="Proximo vencimiento invalido. ";
	    }
	    if(!fecha_ok($fecha_factura_sis)){
		$msj.="Fecha de factura invalida. ";
	    }
	    if((!fecha_ok($fecha_pago_sis) and $fecha_pago_arg<>"")){
		$msj.="Fecha de pago invalida. ";
	    }
	    if(!fecha_ok($vencimiento_sis)){
		$msj.="Fecha de vencimiento invalida. ";
	    }
	    if(!fecha_ok($proximo_venc_sis)){
		$msj.="Fecha de proximo vencimiento invalida. ";
	    }
	    if($msj<>"" and $forzar<>'S'){
		mensaje($msj);
		$pantaAbmFactura=new PANTALLA();
		$pantaAbmFactura->titulo="ALTA DE FACTURA";	
		//$pantaAbmFactura->AgregarSEL("servicio","id_servicio","nombre_servicio",$id_servicio,$nombre_servicio,"telcos");
		$pantaAbmFactura->AgregarSELCustom3("servicio","id_servicio","nombre_servicio","id_servicio",$id_servicio,$nombre_servicio,"select id_servicio,nombre_servicio from telcos where anulado<>'S'");
		$pantaAbmFactura->AgregarTXT("nro.factura","nro_factura",$nro_factura,10);
		$pantaAbmFactura->AgregarFEC("fecha factura","fecha_factura",$fecha_factura_arg);
		$pantaAbmFactura->AgregarTXT("importe","importe",$importe,10);
		$pantaAbmFactura->AgregarFEC("vencimiento","vencimiento",$vencimiento_arg);
		$pantaAbmFactura->AgregarFEC("prox. venc.","proximo_venc",$proximo_venc_arg);                                         
		$pantaAbmFactura->AgregarFEC("fecha de pago","fecha_pago",$fecha_pago_arg);
		$pantaAbmFactura->AgregarCHK("pagado","pagado","S",strtolower($pagado));
		$pantaAbmFactura->AgregarARE("observaciones","observaciones",$observaciones,2,40);	    
		$pantaAbmFactura->AgregarCHK("Forzar errores","forzar","S","N");
		$pantaAbmFactura->AgregarOCU("panta","graba_alta_factura");
		$pantaAbmFactura->regresar=$_SERVER['PHP_SELF'] . "+abm_facturas";
		$pantaAbmFactura->FactorAltura=34;
		$pantaAbmFactura->IncrementoAltura=32;
		$pantaAbmFactura->recuadro=1;
		$pantaAbmFactura->izquierda=400;
		$pantaAbmFactura->tope=0;
		$pantaAbmFactura->alto=850;
		$pantaAbmFactura->ancho=700;
		$pantaAbmFactura->ColorFrente="#ff9999";
		$pantaAbmFactura->ColorFondo="#cc99cc";
		$pantaAbmFactura->MostrarPantalla();
		break;
	    }
	    mi_query("insert into telco_fact set id_servicio='$id_servicio',nro_factura='$nro_factura',fecha_factura='$fecha_factura_sis',importe='$importe',vencimiento='$vencimiento_sis',proximo_venc='$proximo_venc_sis',fecha_pago='$fecha_pago_sis',pagado='$pagado',observaciones='$observaciones',anulado='N'");
	    // Actualizar telcos proximo venc y ultimo importe
	    $proximo_venc=un_dato("select max(proximo_venc) from telco_fact where id_servicio='$id_servicio' and anulado<>'S'");
	    $ultimo_importe=un_dato("select importe from telco_fact where id_servicio='$id_servicio' and anulado<>'S' having max(vencimiento)");
	    mi_query("update telcos set proximo_venc='$proximo_venc',ultimo_importe='$ultimo_importe' where id_servicio='$id_servicio'");
	    mensaje("Se grabo el alta de la factura $nro_factura");
	    un_boton("Aceptar","Volver","","panta","alta_factura");
	    break;
	case "filtro_facturas":
	    $id_servicio=0;
	    $nombre_servicio='Elegir';
	    $proveedor="";
	    $id_edificio=0;
	    $nombre_edif="Elegir";
	    $nro_factura="";
	    $fecha_desde=hoy();
	    $fecha_hasta=hoy();
	    $importe_desde=0;
	    $importe_hasta=0;
	    $vencimiento_desde=hoy();
	    $vencimiento_hasta=hoy();
	    $pagado='N';
	    $no_pagado='N';
	    $pantaFiltroFactura=new PANTALLA();
	    $pantaFiltroFactura->titulo="FILTRO DE FACTURAS";	
	    //$pantaFiltroFactura->AgregarSEL("servicio","id_servicio","nombre_servicio",$id_servicio,"Elegir","telcos");
	    $pantaFiltroFactura->AgregarSELCustom3("servicio","id_servicio","nombre_servicio","id_servicio",$id_servicio,$nombre_servicio,"select id_servicio,nombre_servicio from telcos where anulado<>'S'");
	    $pantaFiltroFactura->AgregarSELCustom3("proveedor","proveedor","proveedor","proveedor",$proveedor,"Elegir","select distinct proveedor from telcos where anulado<>'S'");
	    $pantaFiltroFactura->AgregarSEL("Edificio","id","nombre",$id_edificio,$nombre_edif,"edificio");
	    $pantaFiltroFactura->AgregarTXT("nro.factura","nro_factura",$nro_factura,10);
	    $pantaFiltroFactura->AgregarFEC("fecha desde","fecha_desde",$fecha_desde);
	    $pantaFiltroFactura->AgregarFEC("fecha hasta","fecha_hasta",$fecha_hasta);
	    $pantaFiltroFactura->AgregarTXT("importe desde","importe_desde",$importe_desde,10);
	    $pantaFiltroFactura->AgregarTXT("importe hasta","importe_hasta",$importe_hasta,10);
	    $pantaFiltroFactura->AgregarFEC("vto. desde","vencimiento_desde",$vencimiento_desde);
	    $pantaFiltroFactura->AgregarFEC("vto. hasta","vencimiento_hasta",$vencimiento_hasta);
	    $pantaFiltroFactura->AgregarCHK("pagado","pagado","S","N");
	    $pantaFiltroFactura->AgregarCHK("no pagado","no_pagado","S","N");
	    $pantaFiltroFactura->AgregarOCU("panta","consu_facturas");
	    $pantaFiltroFactura->regresar=$_SERVER['PHP_SELF'] . "+abm_facturas";
	    $pantaFiltroFactura->FactorAltura=30;
	    $pantaFiltroFactura->IncrementoAltura=30;
	    $pantaFiltroFactura->recuadro=1;
	    $pantaFiltroFactura->izquierda=400;
	    $pantaFiltroFactura->tope=0;
	    $pantaFiltroFactura->alto=850;
	    $pantaFiltroFactura->ancho=700;
	    $pantaFiltroFactura->ColorFrente="#ff9999";
	    $pantaFiltroFactura->ColorFondo="#cc99cc";
	    $pantaFiltroFactura->MostrarPantalla();
	    break;
	case "consu_facturas":
	    mi_titulo("Modificacion de Facturas");
	    $hoy=hoy();
	    $hoy_sis=a_fecha_sistema($hoy);
	    //trace("Hoy es $hoy");
	    if(isset($_POST["filtro"])){
		$filtro_mod=$_POST["filtro"];
		//trace("And the filtro is $filtro_mod");
		$filtro=strtr($filtro_mod,":","'");
		$filtro=strtr($filtro,"/","-");
		//trace("And the filtro original is $filtro");
		$desc_fil=$_POST["desc_fil"];
	    }else{
		$filtro="";
		$desc_fil="";
		$id_servicio=$_POST["id_servicio"];
		$proveedor=$_POST["proveedor"];
		$id_edificio=$_POST["id"];
		$nombre_edif=un_dato("select nombre from edificio where id='$id_edificio'");
		$nro_factura=$_POST["nro_factura"];
		$fecha_desde_arg=$_POST["fecha_desde"];
		$fecha_desde_sis=a_fecha_sistema($fecha_desde_arg);
		$fecha_hasta_arg=$_POST["fecha_hasta"];
		$fecha_hasta_sis=a_fecha_sistema($fecha_hasta_arg);
		$importe_desde=$_POST["importe_desde"];
		$importe_hasta=$_POST["importe_hasta"];
		$vencimiento_desde_arg=$_POST["vencimiento_desde"];
		$vencimiento_desde_sis=a_fecha_sistema($vencimiento_desde_arg);
		$vencimiento_hasta_arg=$_POST["vencimiento_hasta"];
		$vencimiento_hasta_sis=a_fecha_sistema($vencimiento_hasta_arg);
		$pagado=$_POST["pagado"];
		$no_pagado=$_POST["no_pagado"];
		//trace("Pagado vale $pagado");
		if($id_servicio<>0){
		    $filtro.=" and f.id_servicio='$id_servicio' ";
		    $nombre_servicio=un_dato("select nombre_servicio from telcos where id_servicio='$id_servicio'");
		    $desc_fil.=" Servicio: $nombre_servicio. ";
		}
		if($proveedor<>""){
		    $filtro.=" and t.proveedor='$proveedor' ";
		    $desc_fil.=" Proveedor $proveedor. ";
		}
		if($id_edificio<>0){
		    $filtro.=" and t.id_edificio='$id_edificio' ";
		    $desc_fil.=" Edificio $nombre_edif. ";
		}
		if($nro_factura<>""){
		    $filtro.=" and f.nro_factura='$nro_factura' ";
		    $desc_fil.=" Factura Nro. $nro_factura. ";
		}
		if($fecha_desde_arg<>$hoy){
		    $filtro.=" and f.fecha_factura>='$fecha_desde_sis' ";
		    $desc_fil.=" Fecha de factura desde $fecha_desde_arg. ";
		}
		if($fecha_hasta_arg<>$hoy){
		    $filtro.=" and f.fecha_factura<='$fecha_hasta_sis' ";
		    $desc_fil.=" Fecha de factura hasta $fecha_hasta_arg. ";
		}
		if($importe_desde>0){
		    $filtro.=" and f.importe>='$importe_desde' ";
		    $desc_fil.=" Importe desde $importe_desde.";
		}
		if($importe_hasta>0){
		    $filtro.=" and f.importe<='$importe_hasta' ";
		    $desc_fil.=" Importe hasta $importe_hasta.";
		}
		if($vencimiento_desde_arg<>$hoy){
		    $filtro.=" and f.vencimiento>='$vencimiento_desde_sis' ";
		    $desc_fil.=" Vencimiento desde $vencimiento_desde_arg. ";
		}
		if($vencimiento_hasta_arg<>$hoy){
		    $filtro.=" and f.vencimiento<='$vencimiento_hasta_sis' ";
		    $desc_fil.=" Vencimiento hasta $vencimiento_hasta_arg. ";
		}
		if($pagado=='S'){
		    $filtro.=" and f.pagado='S' ";
		    $desc_fil.=" Facturas pagadas. ";
		}
		if($no_pagado=='S'){
		    $filtro.=" and f.pagado='N' ";
		    $desc_fil.=" Facturas impagas. ";
		}
	    }
	    mensaje($desc_fil);
	    $filtro_mod=strtr($filtro,"'",":");
	    $filtro_mod=strtr($filtro_mod,"-","/");
	    $titulos="id;id serv.;servicio;factura;importe;vencimiento;pago";
	    $sql="select f.id_fac,f.id_servicio,t.nombre_servicio,f.nro_factura,f.importe,f.vencimiento,f.fecha_pago from telco_fact f,telcos t where t.id_servicio=f.id_servicio and t.anulado<>'S' and f.anulado<>'S' $filtro order by f.vencimiento desc;cotelcos.php+id_fac+panta+modi_factura+filtro+$filtro_mod+desc_fil+$desc_fil";
	    //trace($sql);
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;0;2;0;0","DETALLE","MODIFICAR","","Facturas de Telcos;fac_telcos_consu;fac_telcos_consu");
	    un_boton("Aceptar","Volver","","panta","filtro_facturas");
	    break;
	case "modi_factura":
	    $id_fac=$_POST["id_fac"];
	    $filtro=$_POST["filtro"];
	    $filtro_orig=strtr($filtro,":","'");
	    $desc_fil=$_POST["desc_fil"];
	    $datos=mysql_fetch_array(mi_query("select * from telco_fact where id_fac='$id_fac'"));
	    $id_servicio=$datos[id_servicio];
	    $nombre_servicio=un_dato("select distinct nombre_servicio from telcos where id_servicio='$id_servicio'");
	    $nro_factura=$datos["nro_factura"];
	    $fecha_factura_arg=a_fecha_arg($datos["fecha_factura"]);
	    $importe=$datos["importe"];
	    $vencimiento_arg=a_fecha_arg($datos["vencimiento"]);
	    $proximo_venc_arg=a_fecha_arg($datos["proximo_venc"]);
	    $fecha_pago_arg=a_fecha_arg($datos["fecha_pago"]);
	    $pagado=$datos["pagado"];
	    $observaciones=$datos["observaciones"];
	    $anulado=$datos["anulado"];
	    $pantaModiFactura=new PANTALLA();
	    $pantaModiFactura->titulo="MODIFICACION DE FACTURA";	
	    //$pantaModiFactura->AgregarSEL("servicio","id_servicio","nombre_servicio",$id_servicio,$nombre_servicio,"telcos");
	    $pantaModiFactura->AgregarSELCustom3("servicio","id_servicio","nombre_servicio","id_servicio",$id_servicio,$nombre_servicio,"select id_servicio,nombre_servicio from telcos where anulado<>'S'");
	    $pantaModiFactura->AgregarTXT("nro.factura","nro_factura",$nro_factura,10);
	    $pantaModiFactura->AgregarFEC("fecha factura","fecha_factura",$fecha_factura_arg);
	    $pantaModiFactura->AgregarTXT("importe","importe",$importe,10);
	    $pantaModiFactura->AgregarFEC("vencimiento","vencimiento",$vencimiento_arg);
    	    $pantaModiFactura->AgregarFEC("prox. venc.","proximo_venc",$proximo_venc_arg);                                         
	    $pantaModiFactura->AgregarFEC("fecha de pago","fecha_pago",$fecha_pago_arg);
	    $pantaModiFactura->AgregarCHK("pagado","pagado","S",strtolower($pagado));
	    $pantaModiFactura->AgregarARE("observaciones","observaciones",$observaciones,2,40);
	    $pantaModiFactura->AgregarCHK("Borrar factura","borrar","S","n");
	    $pantaModiFactura->AgregarOCU("id_fac",$id_fac);
	    $pantaModiFactura->AgregarOCU("panta","graba_modi_factura");
	    $pantaModiFactura->AgregarOCU("filtro",$filtro);
	    $pantaModiFactura->AgregarOCU("desc_fil",$desc_fil);
	    $pantaModiFactura->regresar=$_SERVER['PHP_SELF'] . "+consu_facturas+filtro+$filtro+desc_fil+$desc_fil";
	    $pantaModiFactura->FactorAltura=34;
	    $pantaModiFactura->IncrementoAltura=32;
	    $pantaModiFactura->recuadro=1;
	    $pantaModiFactura->izquierda=400;
	    $pantaModiFactura->tope=0;
	    $pantaModiFactura->alto=850;
	    $pantaModiFactura->ancho=700;
	    $pantaModiFactura->ColorFrente="#ff9999";
	    $pantaModiFactura->ColorFondo="#cc99cc";
	    $pantaModiFactura->MostrarPantalla();
	    //un_boton("Aceptar","Volver","","panta;filtro;desc_fil","consu_facturas;$filtro;$desc_fil");
	    break;
	case "graba_modi_factura":
	    $hoy=hoy();
	    $hoy_sis=a_fecha_sistema($hoy);
	    $id_fac=$_POST["id_fac"];
	    $id_servicio=$_POST["id_servicio"];
	    $nombre_servicio=un_dato("select nombre_servicio from telcos where id_servicio='$id_servicio'");
	    $nro_factura=$_POST["nro_factura"];
	    $fecha_factura_arg=$_POST["fecha_factura"];
	    //trace("Fecha factura arg $fecha_factura_arg");
	    $fecha_factura_sis=a_fecha_sistema($fecha_factura_arg);
	    //trace("Fecha factura sis $fecha_factura_sis");
	    $importe=$_POST["importe"];
	    $vencimiento_arg=$_POST["vencimiento"];
	    $vencimiento_sis=a_fecha_sistema($vencimiento_arg);
	    $proximo_venc_arg=$_POST["proximo_venc"];
	    $proximo_venc_sis=a_fecha_sistema($proximo_venc_arg);
	    //trace("Venc. arg $vencimiento_arg");
	    //trace("Venc. sis $vencimiento_sis");
	    $fecha_pago_arg=$_POST["fecha_pago"];
	    $fecha_pago_sis=a_fecha_sistema($fecha_pago_arg);
	    //trace("Fecha pago arg $fecha_pago_arg");
	    //trace("Fecha pago sis $fecha_pago_sis");
	    $pagado=$_POST["pagado"];
	    $pagado=($pagado=='S') ? 'S' : 'N';
	    $observaciones=$_POST["observaciones"];
	    $filtro=$_POST["filtro"];
	    $desc_fil=$_POST["desc_fil"];
	    $borrar=$_POST["borrar"];
	    $anulado=($borrar=='S') ? 'S' : 'N';
	    $forzar=$_POST["forzar"];
	    $msj="";
	    if($id_servicio==0){
		$msj="Falta elegir un servicio. ";
	    }
	    if($nro_factura==""){
		$msj.="Falta numero de factura. ";
	    }
	    $existe=un_dato("select count(*) from telco_fact where nro_factura='$nro_factura' and id_fac<>'$id_fac'");
	    //trace("Existe vale $existe");
	    if($existe){
		$msj.="Ya existe ese nro. de factura para otro servicio. ";
	    }
	    if($importe<=0){
		$msj.="Importe invalido. ";
	    }
	    if($fecha_factura_sis>$hoy_sis){
		$msj.="Fecha de la factura invalida. ";
	    }
	    if(($fecha_pago_arg<>"" and $fecha_pago_sis>$hoy_sis)){
		$msj.="Fecha de pago invalida. ";
	    }
	    if($vencimiento_sis<$fecha_factura_sis){
		$msj.="Vencimiento o fecha de pago invalidos. ";
	    }
	    if($proximo_venc_sis<$vencimiento_sis){
		$msj.="Proximo vencimiento invalido. ";
	    }
	    if(!fecha_ok($fecha_factura_sis)){
		$msj.="Fecha de factura invalida. ";
	    }
	    if((!fecha_ok($fecha_pago_sis) and $fecha_pago_arg<>"")){
		$msj.="Fecha de pago invalida. ";
	    }
	    if(!fecha_ok($vencimiento_sis)){
		$msj.="Fecha de vencimiento invalida. ";
	    }
	    if(!fecha_ok($proximo_venc_sis)){
		$msj.="Fecha de proximo vencimiento invalida. ";
	    }
	    if($msj<>"" and $forzar<>'S'){
		mensaje($msj);
		$pantaModiFactura=new PANTALLA();
		$pantaModiFactura->titulo="MODIFICACION DE FACTURAS";	
		//$pantaModiFactura->AgregarSEL("servicio","id_servicio","nombre_servicio",$id_servicio,$nombre_servicio,"telcos");
		$pantaModiFactura->AgregarSELCustom3("servicio","id_servicio","nombre_servicio","id_servicio",$id_servicio,$nombre_servicio,"select id_servicio,nombre_servicio from telcos where anulado<>'S'");
		$pantaModiFactura->AgregarTXT("nro.factura","nro_factura",$nro_factura,10);
		$pantaModiFactura->AgregarFEC("fecha factura","fecha_factura",$fecha_factura_arg);
		$pantaModiFactura->AgregarTXT("importe","importe",$importe,10);
		$pantaModiFactura->AgregarFEC("vencimiento","vencimiento",$vencimiento_arg);
		$pantaModiFactura->AgregarFEC("prox. venc.","proximo_venc",$proximo_venc_arg);                                         
		$pantaModiFactura->AgregarFEC("fecha de pago","fecha_pago",$fecha_pago_arg);
		$pantaModiFactura->AgregarCHK("pagado","pagado","S",strtolower($pagado));
		$pantaModiFactura->AgregarARE("observaciones","observaciones",$observaciones,2,40);
		$pantaModiFactura->AgregarCHK("Borrar factura","borrar","S","N");		
		$pantaModiFactura->AgregarCHK("Forzar errores","forzar","S","N");
		$pantaModiFactura->AgregarOCU("panta","graba_modi_factura");
		$pantaModiFactura->AgregarOCU("filtro",$filtro);
		$pantaModiFactura->AgregarOCU("desc_fil",$desc_fil);
		$pantaModiFactura->regresar=$_SERVER['PHP_SELF'] . "+abm_facturas";
		$pantaModiFactura->FactorAltura=34;
		$pantaModiFactura->IncrementoAltura=32;
		$pantaModiFactura->recuadro=1;
		$pantaModiFactura->izquierda=400;
		$pantaModiFactura->tope=0;
		$pantaModiFactura->alto=850;
		$pantaModiFactura->ancho=700;
		$pantaModiFactura->ColorFrente="#ff9999";
		$pantaModiFactura->ColorFondo="#cc99cc";
		$pantaModiFactura->MostrarPantalla();
		break;
	    }
	    mi_query("update telco_fact set id_servicio='$id_servicio',nro_factura='$nro_factura',fecha_factura='$fecha_factura_sis',importe='$importe',vencimiento='$vencimiento_sis',proximo_venc='$proximo_venc_sis',fecha_pago='$fecha_pago_sis',pagado='$pagado',observaciones='$observaciones',anulado='$anulado' where id_fac='$id_fac'");
	    // Actualizar telcos proximo venc y ultimo importe
	    $proximo_venc=un_dato("select max(proximo_venc) from telco_fact where id_servicio='$id_servicio' and anulado<>'S'");
	    $ultimo_importe=un_dato("select importe from telco_fact where id_servicio='$id_servicio' and anulado<>'S' having max(vencimiento)");
	    mi_query("update telcos set proximo_venc='$proximo_venc',ultimo_importe='$ultimo_importe' where id_servicio='$id_servicio'");
	    mensaje("Se grabo la modificacion de la factura $nro_factura");
	    //un_boton("Aceptar","Volver","","panta","modi_factura");
	    un_boton("Aceptar","Volver","","panta;filtro;desc_fil","consu_facturas;$filtro;$desc_fil");
	    break;
	case "alta_servicio":
	    //mi_titulo("ALTA DE SERVICIO");
	    $nombre_servicio="";
	    $descripcion="";
	    $proveedor="";
	    $ejecutivo_nombre="";
	    $ejecutivo_tel="";
	    $ejecutivo_email="";
	    $soporte_tel="";
	    $soporte_email="";
	    $datos_tecnicos="";
	    $datos_cuenta="";
	    $observaciones="";
	    $proximo_venc=0;
	    $ultimo_importe=0;
	    $estado_servicio="Activo";
	    $punto_entrada="Rack sistemas";
	    $pantaAbmServicio=new PANTALLA();
	    $pantaAbmServicio->titulo="ALTA DE SERVICIO";	
	    $pantaAbmServicio->AgregarTXT("nombre","nombre_servicio",$nombre_servicio,30);
	    $pantaAbmServicio->AgregarTXT("descripcion","descripcion",$descripcion,40);
	    $pantaAbmServicio->AgregarTXT("proveedor","proveedor",$proveedor,30);
	    $pantaAbmServicio->AgregarSEL("Edificio","id","nombre",1,"PIEDRABUENA","edificio");
	    $pantaAbmServicio->AgregarTXT("ejecutivo","ejecutivo_nombre",$ejecutivo_nombre,30);
	    $pantaAbmServicio->AgregarTXT("tel. ejecutivo","ejecutivo_tel",$ejecutivo_tel,30);
	    $pantaAbmServicio->AgregarTXT("email ejecutivo","ejecutivo_email",$ejecutivo_email,30);
	    $pantaAbmServicio->AgregarTXT("tel. soporte","soporte_tel",$soporte_tel,30);
	    $pantaAbmServicio->AgregarTXT("email soporte","soporte_email",$soporte_email,30);
	    $pantaAbmServicio->AgregarARE("datos tecnicos","datos_tecnicos",$datos_tecnicos,2,62);
	    $pantaAbmServicio->AgregarARE("datos cuenta","datos_cuenta",$datos_cuenta,2,62);
	    $pantaAbmServicio->AgregarARE("observaciones","observaciones",$observaciones,2,62);
	    $pantaAbmServicio->AgregarFEC("proximo_venc","proximo venc",0);
	    $pantaAbmServicio->AgregarTXT("ultimo importe","ultimo_importe",$ultimo_importe,10);
	    $pantaAbmServicio->AgregarTXT("estado servicio","estado_servicio",$estado_servicio,10);
	    $pantaAbmServicio->AgregarTXT("punto de entrada","punto_entrada",$punto_entrada,10);
	    $pantaAbmServicio->AgregarOCU("panta","graba_alta_servicio");
	    $pantaAbmServicio->regresar=$_SERVER['PHP_SELF'] . "+abm_servicios";
	    $pantaAbmServicio->FactorAltura=34;
	    $pantaAbmServicio->IncrementoAltura=32;
	    $pantaAbmServicio->recuadro=1;
	    $pantaAbmServicio->izquierda=400;
	    $pantaAbmServicio->tope=0;
	    $pantaAbmServicio->alto=850;
	    $pantaAbmServicio->ancho=700;
	    $pantaAbmServicio->ColorFrente="#ff9999";
	    $pantaAbmServicio->ColorFondo="#cc99cc";
	    $pantaAbmServicio->MostrarPantalla();
	    break;
	case "graba_alta_servicio":
	    $nombre_servicio=$_POST["nombre_servicio"];
	    $descripcion=$_POST["descripcion"];
	    $proveedor=$_POST["proveedor"];
	    $id_edificio=$_POST["id"];
	    $ejecutivo_nombre=$_POST["ejecutivo_nombre"];
	    $ejecutivo_tel=$_POST["ejecutivo_tel"];
	    $ejecutivo_email=$_POST["ejecutivo_email"];
	    $soporte_tel=$_POST["soporte_tel"];
	    $soporte_email=$_POST["soporte_email"];
	    $datos_tecnicos=$_POST["datos_tecnicos"];
	    $datos_cuenta=$_POST["datos_cuenta"];
	    $observaciones=$_POST["observaciones"];
	    $proximo_venc_arg=$_POST["proximo_venc"];
	    //trace("El venc. arg es $proximo_venc_sis");
	    $proximo_venc_sis=a_fecha_sistema($proximo_venc_arg);
	    //trace("El venc sis es $proximo_venc_sis");
	    $ultimo_importe=$_POST["ultimo_importe"];
	    $estado_servicio=$_POST["estado_servicio"];
	    $punto_entrada=$_POST["punto_entrada"];
	    $msj="";
	    if($nombre_servicio==""){
		$msj.="Servicio invalido. ";		
	    }
	    $existe=un_dato("select count(*) from telcos where nombre_servicio='$nombre_servicio'");
	    if($existe){
		$msj.="El servicio $nombre_servicio ya existe. ";
	    }
	    if($msj<>""){
		mensaje($msj);
	    }else{
		mi_query("insert into telcos set nombre_servicio='$nombre_servicio',descripcion='$descripcion',proveedor='$proveedor',id_edificio='$id_edificio',ejecutivo_nombre='$ejecutivo_nombre',ejecutivo_tel='$ejecutivo_tel',ejecutivo_email='$ejecutivo_email',soporte_tel='$soporte_tel',soporte_email='$soporte_email',datos_tecnicos='$datos_tecnicos',datos_cuenta='$datos_cuenta',observaciones='$observaciones',proximo_venc='$proximo_venc_sis',ultimo_importe='$ultimo_importe',estado_servicio='$estado_servicio',punto_entrada='$punto_entrada'");		    
	      mensaje("Se grabo el alta del servicio $nombre_servicio");
	    }
	    un_boton("Aceptar","Volver","","panta","alta_servicio");
	    break;
	case "consulta_servicios":
	    mi_titulo("Consulta de Servicios");
	    $titulos="id;servicio;descripcion;proveedor;planta;datos cuenta;vencimiento;importe;estado";	    
	    $sql="select t.id_servicio,t.nombre_servicio,t.descripcion,t.proveedor,e.nombre,t.datos_cuenta,t.proximo_venc,t.ultimo_importe,t.estado_servicio from telcos t,edificio e where t.id_edificio=e.id and t.anulado<>'S';cotelcos.php+id_servicio+panta+modi_servicio";
	    //trace($sql);
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;0;0;0;0;2;0","DETALLE","MODIFICAR","","Servicios de Telcos;telcos;telcos");
	    un_boton("Aceptar","Volver","","panta","abm_servicios");
	    break;
	case "modi_servicio":
	    $id_servicio=$_POST["id_servicio"];
	    $qry=mi_query("select * from telcos where id_servicio='$id_servicio'");
	    $datos=  mysql_fetch_array($qry);
	    //mensaje("Estoy en modificacion servicio con $id_servicio");
	    $nombre_servicio=$datos["nombre_servicio"];
	    $descripcion=$datos["descripcion"];
	    $proveedor=$datos["proveedor"];
	    $ejecutivo_nombre=$datos["ejecutivo_nombre"];
	    $ejecutivo_tel=$datos["ejecutivo_tel"];
	    $ejecutivo_email=$datos["ejecutivo_email"];
	    $soporte_tel=$datos["soporte_tel"];
	    $soporte_email=$datos["soporte_email"];
	    $datos_tecnicos=$datos["datos_tecnicos"];
	    $datos_cuenta=$datos["datos_cuenta"];
	    $observaciones=$datos["observaciones"];
	    $proximo_venc=a_fecha_arg($datos["proximo_venc"]);
	    //$proximo_venc_sis=  a_fecha_sistema($proximo_venc);
	    $ultimo_importe=$datos["ultimo_importe"];
	    $estado_servicio=$datos["estado_servicio"];
	    $punto_entrada=$datos["punto_entrada"];
	    $id_edificio=$datos["id_edificio"];
	    $nombre=un_dato("select nombre from edificio where id='$id_edificio'");
	    $pantaAbmServicio=new PANTALLA();
	    $pantaAbmServicio->titulo="MODIFICACION DE SERVICIO";	
	    $pantaAbmServicio->AgregarTXT("nombre","nombre_servicio",$nombre_servicio,30);
	    $pantaAbmServicio->AgregarTXT("descripcion","descripcion",$descripcion,40);
	    $pantaAbmServicio->AgregarTXT("proveedor","proveedor",$proveedor,30);
	    $pantaAbmServicio->AgregarSEL("Edificio","id","nombre",$id_edificio,$nombre,"edificio");
	    $pantaAbmServicio->AgregarTXT("ejecutivo","ejecutivo_nombre",$ejecutivo_nombre,30);
	    $pantaAbmServicio->AgregarTXT("tel. ejecutivo","ejecutivo_tel",$ejecutivo_tel,30);
	    $pantaAbmServicio->AgregarTXT("email ejecutivo","ejecutivo_email",$ejecutivo_email,30);
	    $pantaAbmServicio->AgregarTXT("tel. soporte","soporte_tel",$soporte_tel,30);
	    $pantaAbmServicio->AgregarTXT("email soporte","soporte_email",$soporte_email,30);
	    $pantaAbmServicio->AgregarARE("datos tecnicos","datos_tecnicos",$datos_tecnicos,2,62);
	    $pantaAbmServicio->AgregarARE("datos cuenta","datos_cuenta",$datos_cuenta,2,62);
	    $pantaAbmServicio->AgregarARE("observaciones","observaciones",$observaciones,2,62);
	    $pantaAbmServicio->AgregarFEC("proximo_venc","proximo venc",$proximo_venc);
	    $pantaAbmServicio->AgregarTXT("ultimo importe","ultimo_importe",$ultimo_importe,10);
	    $pantaAbmServicio->AgregarTXT("estado servicio","estado_servicio",$estado_servicio,10);
	    $pantaAbmServicio->AgregarTXT("punto de entrada","punto_entrada",$punto_entrada,10);
	    $pantaAbmServicio->AgregarCHK("Borrar registro","borrar","S","N");
	    $pantaAbmServicio->AgregarOCU("id_servicio",$id_servicio);
    	    $pantaAbmServicio->AgregarOCU("panta","graba_modi_servicio");
	    $pantaAbmServicio->regresar=$_SERVER['PHP_SELF'] . "+consulta_servicios";
	    $pantaAbmServicio->FactorAltura=31;
	    $pantaAbmServicio->IncrementoAltura=31;
	    $pantaAbmServicio->recuadro=1;
	    $pantaAbmServicio->izquierda=400;
	    $pantaAbmServicio->tope=0;
	    $pantaAbmServicio->alto=850;
	    $pantaAbmServicio->ancho=700;
	    $pantaAbmServicio->ColorFrente="#ff9999";
	    $pantaAbmServicio->ColorFondo="#cc99cc";
	    $pantaAbmServicio->MostrarPantalla();
	    break;
	case "graba_modi_servicio":
	    $id_servicio=$_POST["id_servicio"];
	    //trace("El id del servicio es $id_servicio");
	    $nombre_servicio=$_POST["nombre_servicio"];
	    $descripcion=$_POST["descripcion"];
	    $proveedor=$_POST["proveedor"];
	    $id_edificio=$_POST["id"];
	    $ejecutivo_nombre=$_POST["ejecutivo_nombre"];
	    $ejecutivo_tel=$_POST["ejecutivo_tel"];
	    $ejecutivo_email=$_POST["ejecutivo_email"];
	    $soporte_tel=$_POST["soporte_tel"];
	    $soporte_email=$_POST["soporte_email"];
	    $datos_tecnicos=$_POST["datos_tecnicos"];
	    $datos_cuenta=$_POST["datos_cuenta"];
	    $observaciones=$_POST["observaciones"];
	    $proximo_venc=a_fecha_sistema($_POST["proximo_venc"]);
	    $ultimo_importe=$_POST["ultimo_importe"];
	    $estado_servicio=$_POST["estado_servicio"];
	    $punto_entrada=$_POST["punto_entrada"];
	    $borrar=$_POST["borrar"];
	    $msj="";
	    if($borrar=="S"){
		mi_query("update telcos set anulado='$borrar' where id_servicio='$id_servicio'");
		$msj="Se elimino el servicio $nombre_servicio";		
	    }
	    if($nombre_servicio==""){
		$msj.="Servicio invalido. ";		
	    }
	    $existe=un_dato("select count(*) from telcos where nombre_servicio='$nombre_servicio' and id_servicio<>'$id_servicio'");
	    if($existe){
		$msj.="El servicio $nombre_servicio ya existe con otro id. ";
	    }
	    if($msj<>""){
		mensaje($msj);
	    }else{
		mi_query("update telcos set nombre_servicio='$nombre_servicio',descripcion='$descripcion',proveedor='$proveedor',id_edificio='$id_edificio',ejecutivo_nombre='$ejecutivo_nombre',ejecutivo_tel='$ejecutivo_tel',ejecutivo_email='$ejecutivo_email',soporte_tel='$soporte_tel',soporte_email='$soporte_email',datos_tecnicos='$datos_tecnicos',datos_cuenta='$datos_cuenta',observaciones='$observaciones',proximo_venc='$proximo_venc',ultimo_importe='$ultimo_importe',estado_servicio='$estado_servicio',punto_entrada='$punto_entrada' where id_servicio='$id_servicio'");		    
	      mensaje("Se grabo la modificacion del servicio $nombre_servicio");
	    }
	    un_boton("Aceptar","Volver","","panta","consulta_servicios");
	    break;
	case "abm_servicios":
	    mi_titulo("ABM Servicios");
	    un_boton("aceptar","Alta de servicio","","panta","alta_servicio");
	    un_boton("aceptar","Consulta de servicios","","panta","consulta_servicios");
	    linea();
	    un_boton("Aceptar","Volver");
	    break;
	case "abm_facturas":
	    mi_titulo("ABM Facturas");
	    un_boton("aceptar","Alta de Factura","","panta","alta_factura");
	    un_boton("aceptar","Consulta y Modificacion de facturas","","panta","filtro_facturas");
	    un_boton("aceptar","Facturas no informadas","","panta","filtro_noinfo");
	    linea();
	    un_boton("Aceptar","Volver");
	    break;
	case "filtro_noinfo":
	    $id_servicio=0;
	    $nombre_servicio='Elegir';
	    $hoy=hoy_sis();
	    //trace($hoy);
	    $anio_vto=substr($hoy,0,4);
	    //trace("Anio vto: $anio_vto");
	    $mes_vto=substr($hoy,5,2);
	    $mesanio_vto=$mes_vto . "/" . $anio_vto;
	    //trace("Mes vto: $mes_vto");
	    //trace("Mes Anio vto: $mesanio_vto");
	    $pantaConsuNoinfo=new PANTALLA();
	    $pantaConsuNoinfo->titulo="CONSULTA DE FACTURAS FALTANTES";	
	    //$pantaConsuNoinfo->AgregarSEL("servicio","id_servicio","nombre_servicio",0,"Elegir","telcos");
	    $pantaConsuNoinfo->AgregarSELCustom3("servicio","id_servicio","nombre_servicio","id_servicio",$id_servicio,$nombre_servicio,"select id_servicio,nombre_servicio from telcos where anulado<>'S' and estado_servicio='Activo'");
	    //$pantaConsuNoinfo->AgregarROT("<2>Dejar en blanco para mostrar todos los meses");
	    $pantaConsuNoinfo->AgregarTXT("Mes vencimiento inicial","mes_vto_ini",'01/2013',6);
	    $pantaConsuNoinfo->AgregarTXT("Mes vencimiento final","mes_vto_fin",$mesanio_vto,6);
	    $pantaConsuNoinfo->AgregarOCU("panta","proc_noinfo");
	    $pantaConsuNoinfo->regresar=$_SERVER['PHP_SELF'] . "+abm_facturas";
	    $pantaConsuNoinfo->FactorAltura=34;
	    $pantaConsuNoinfo->IncrementoAltura=32;
	    $pantaConsuNoinfo->recuadro=1;
	    $pantaConsuNoinfo->izquierda=400;
	    $pantaConsuNoinfo->tope=0;
	    $pantaConsuNoinfo->alto=850;
	    $pantaConsuNoinfo->ancho=700;
	    $pantaConsuNoinfo->ColorFrente="#ff9999";
	    $pantaConsuNoinfo->ColorFondo="#cc99cc";
	    $pantaConsuNoinfo->MostrarPantalla();	    
	    break;
	case "proc_noinfo":
	    $id_servicio=$_POST["id_servicio"];
	    $mes_vto_ini=$_POST["mes_vto_ini"];
	    //trace("El mes_vto_ini es $mes_vto_ini");
	    $mes_vto_fin=$_POST["mes_vto_fin"];
	    //trace("El mes_vto_fin es $mes_vto_fin");
	    if($mes_vto_ini==""){
		$fecha_mes_inicial="2013-01-01";
		//trace("Fecha mes inicial por defecto es $fecha_mes_inicial");
	    }else{
		$fecha_mes_inicial=substr($mes_vto_ini,3,4) . '-' . substr($mes_vto_ini,0,2) . '-01';
		//trace("Fecha mes inicial $mes_inicial");
	    }
	    $mes_inicial=substr($fecha_mes_inicial,5,2);
	    $anio_inicial=substr($fecha_mes_inicial,0,4);
	    if($mes_vto_fin==""){
		$hoy=hoy();
		$hoy_sis=a_fecha_sistema($hoy);
		$anio_final=substr($hoy_sis,0,4);
		$mes_final=substr($hoy_sis,5,2);
		//trace("Mes final por defecto es $mes_final y anio final es $anio_final");
	    }else{
		$anio_final=substr($mes_vto_fin,3,4);
		$mes_final=substr($mes_vto_fin,0,2);
		//trace("Mes final $mes_final y anio final $anio_final");
	    }
	    $fecha_mes_final=$anio_final . '-' . $mes_final . '-01';
	    //Validacion de fechas
	    if(!fecha_ok($fecha_mes_inicial)){
		mensaje("Fecha inicial invalida");
		un_boton("Aceptar","Volver","","panta","filtro_noinfo");
	    }
	    //trace("Fecha final $fecha_mes_final");
	    if(!fecha_ok($fecha_mes_final)){
		mensaje("Fecha final invalida");
		un_boton("Aceptar","Volver","","panta","filtro_noinfo");
	    }
	    $anio_final=($mes_final==12) ? $anio_final +1 : $anio_final;
	    $mes_final=($mes_final==12) ? 1 : $mes_final + 1;
	    $mes_final=substr( "00" . $mes_final,strlen($mes_final),2);
	    //trace("Mes final es $mes_final");
	    //trace("Anio final es $anio_final");
	    $fecha_mes_final=$anio_final . "-" . $mes_final . "-01";
	    //trace("Fecha mes final $fecha_mes_final");
	    $mes=substr($fecha_mes_inicial,5,2);
	    $anio=substr($fecha_mes_inicial,0,4);
	    //trace("Fecha mes es $fecha_mes");
	    //trace("El mes es $mes y el anio es $anio");
	    $anio_final=substr($fecha_mes_final,0,4);
	    //trace("El anio final es $anio_final");
	    $mes_final=substr($fecha_mes_final,5,2);
	    //trace("Mes final es $mes_final");
	    $anio_final=($mes_final==12) ? $anio_final +1 : $anio_final;
	    $mes_final=($mes_final==12) ? 1 : $mes_final + 1;
	    $mes_final=substr( "00" . $mes_final,strlen($mes_final),2);
	    //trace("Mes final es $mes_final");
	    //trace("Anio final es $anio_final");
	    //$fecha_mes_final=$anio_final . "-" . $mes_final . "-01";	    
	    //trace("Fecha mes final es $fecha_mes_final");
	    //die();
	    if($id_servicio==0){
		mensaje("Mostrar facturas faltantes de Todos los servicios entre $fecha_mes_inicial y $fecha_mes_final");
		$qry_servicios=mi_query("select id_servicio,nombre_servicio from telcos where anulado<>'S' and estado_servicio='Activo'");
		mi_tabla("i",1);
		mi_fila("t color","PERIODO;ESTADO");
		while($datos=  mysql_fetch_array($qry_servicios)){
		    $id_servicio=$datos["id_servicio"];
		    $nombre_servicio=$datos["nombre_servicio"];
		    mi_fila("t color","$nombre_servicio;");
		    $casos=1;
		    $fecha_mes=$fecha_mes_inicial;
		    $mes=substr($fecha_mes_inicial,5,2);
		    $anio=substr($fecha_mes_inicial,0,4);
		    while($fecha_mes<$fecha_mes_final){
			//trace("Paso $casos ---------------------------------");
			//trace("Fecha mes $fecha_mes - Fecha mes final $fecha_mes_final");
			if($casos>30){
			    die();
			}
			$casos++;
			$aniomes=$anio . "-" . $mes;
			//trace("Aniomes es $aniomes");
			$sql="select count(*) from telco_fact where id_servicio='$id_servicio' and left(vencimiento,7)='$aniomes' and anulado<>'S'";
			//trace($sql);
			$existe=un_dato($sql);
			if(!$existe){
			    mi_fila("x","$aniomes;FALTA");
			    //trace("PERIODO: $aniomes");
			}else{
			    mi_fila("x","$aniomes;OK");
			}
			$anio=($mes==12) ? $anio+1 : $anio;
			$mes=($mes==12) ? 1 : $mes+1;
			$mes=substr( "00" . $mes,strlen($mes),2);
			$fecha_mes=$anio . "-" . $mes . "-01";	   
		    }		    
		}
		mi_tabla();
	    }else{
		// Especifico un servicio en particular
		$nombre_servicio=un_dato("select nombre_servicio from telcos where id_servicio='$id_servicio'");
		mensaje("Mostrar facturas faltantes de serv. $id_servicio - $nombre_servicio entre $fecha_mes_inicial y $fecha_mes_final");
		mi_tabla("i",1);
		$fecha_mes=$fecha_mes_inicial;
		$mes=substr($fecha_mes_inicial,5,2);
		$anio=substr($fecha_mes_inicial,0,4);
		$casos=1;
		while($fecha_mes<$fecha_mes_final){
		    //trace("Fecha mes $fecha_mes - Fecha mes final $fecha_mes_final");
		    //trace("Paso $casos ---------------------------------");
		    if($casos>30){
			die();
		    }
		    $casos++;
		    $aniomes=$anio . "-" . $mes;
		    //trace("Aniomes es $aniomes");
		    $sql="select count(*) from telco_fact where id_servicio='$id_servicio' and left(vencimiento,7)='$aniomes' and anulado<>'S'";
		    //trace($sql);
		    $existe=un_dato($sql);
		    if(!$existe){
			mi_fila("x","PERIODO: $aniomes;FALTA");
			//trace("PERIODO: $aniomes");
		    }else{
			mi_fila("x","PERIODO: $aniomes;OK");
		    }
		    $anio=($mes==12) ? $anio+1 : $anio;
		    $mes=($mes==12) ? 1 : $mes+1;
		    $mes=substr( "00" . $mes,strlen($mes),2);
		    $fecha_mes=$anio . "-" . $mes . "-01";	   
		}
		mi_tabla();
	    }
	    un_boton("Aceptar","Volver","","panta","filtro_noinfo");
	    break;	  
	case "agenda":
	    mi_titulo("Agenda");
	    un_boton("aceptar","ELEGIR DIA","","panta","dia");
	    un_boton("aceptar","SEMANA","","panta","semana");
	    un_boton("aceptar","MES","","panta","mes");
	    un_boton("aceptar","MES PROXIMO","","panta","mes_proximo");
	    //mensaje("Aqui se podran ver los proximos vencimientos de servicios");
	    un_boton("Aceptar","Volver");
	    break;
	case "dia":
	    $pantaFecha=new PANTALLA();
	    $pantaFecha->titulo="CONSULTA DE VENCIMIENTOS DEL DIA";	
	    $pantaFecha->AgregarFEC("fecha","fecha","");
    	    $pantaFecha->AgregarOCU("panta","procesa_dia");
	    $pantaFecha->regresar=$_SERVER['PHP_SELF'] . "+agenda";
	    $pantaFecha->FactorAltura=31;
	    $pantaFecha->IncrementoAltura=31;
	    $pantaFecha->recuadro=1;
	    $pantaFecha->izquierda=400;
	    $pantaFecha->tope=0;
	    $pantaFecha->alto=850;
	    $pantaFecha->ancho=700;
	    $pantaFecha->ColorFrente="#ff9999";
	    $pantaFecha->ColorFondo="#cc99cc";
	    $pantaFecha->MostrarPantalla();
	    break;
	case "procesa_dia":
	    $fecha=$_POST["fecha"];
	    $fecha_sis=a_fecha_sistema($fecha);
	    mi_titulo("Vencimientos del dia $fecha");
	    mensaje("Facturas cargadas");
	    $titulos="id;servicio;factura;importe;vencimiento";
	    $sql="select f.id_fac,t.nombre_servicio,f.nro_factura,f.importe,f.vencimiento from telco_fact f,telcos t where t.id_servicio=f.id_servicio and f.pagado='N' and f.vencimiento='$fecha_sis' and t.anulado<>'S' and f.anulado<>'S' order by f.vencimiento desc;cotelcos.php+id_fac+panta+detalle_agenda+vista+dia";
	    //trace($sql);
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;0;2;0;0","DETALLE","CONSULTAR","","Facturas de Telcos;fac_telcos_dia;fac_telcos_dia");	    
	    mensaje("Servicios agendados");
	    $titulos2="id;nombre;descripcion;vencimiento;importe";
	    $sql2="select id_servicio,nombre_servicio,descripcion,proximo_venc,ultimo_importe from telcos where proximo_venc='$fecha_sis' and anulado<>'S'";
	    tabla_cons($titulos2,$sql2,1,"silver","#E5DBB0","0;0;0;0;2","DETALLE","CONSULTAR","","Facturas de Telcos;fac_telcos_dia2;fac_telcos_dia2");	
	    un_boton("Aceptar","Volver","","panta","agenda");
	    break;
	    mensaje("Estoy en procesa_dia con $fecha");
	    un_boton("Aceptar","Volver","","panta","agenda");
	    break;
	case "semana":
	    $hoy=hoy();
	    $lunes=lunes($hoy);
	    $domingo=domingo($hoy);
	    $lunes_sis=a_fecha_sistema($lunes);
	    $domingo_sis=a_fecha_sistema($domingo);
	    mi_titulo("Vencimientos de la semana del $lunes al $domingo");
	    mensaje("Facturas cargadas");
	    $titulos="id;servicio;factura;importe;vencimiento";
	    $sql="select f.id_fac,t.nombre_servicio,f.nro_factura,f.importe,f.vencimiento from telco_fact f,telcos t where t.id_servicio=f.id_servicio and f.pagado='N' and f.vencimiento>='$lunes_sis' and f.vencimiento <='$domingo_sis' and t.anulado<>'S' and f.anulado<>'S' order by f.vencimiento desc;cotelcos.php+id_fac+panta+detalle_agenda+vista+semana";
	    //trace($sql);
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;0;2;0;0","DETALLE","CONSULTAR","","Facturas de Telcos;fac_telcos_sem;fac_telcos_sem");	    
	    mensaje("Servicios agendados");
	    $titulos2="id;nombre;descripcion;vencimiento;importe";
	    $sql2="select id_servicio,nombre_servicio,descripcion,proximo_venc,ultimo_importe from telcos where proximo_venc>='$lunes_sis' and proximo_venc<='$domingo_sis' and anulado<>'S'";
	    tabla_cons($titulos2,$sql2,1,"silver","#E5DBB0","0;0;0;0;2","DETALLE","CONSULTAR","","Facturas de Telcos;fac_telcos_sem2;fac_telcos_sem2");	
	    un_boton("Aceptar","Volver","","panta","agenda");
	    break;
	case "mes":
	    mi_titulo("Vencimientos del mes");
	    $hoy=hoy();
	    //trace($hoy);
	    $hoy_sis=a_fecha_sistema($hoy);
	    //trace($hoy_sis);
	    $este_mes=substr($hoy_sis,0,7);
	    //trace($este_mes);
	    //mensaje("Aqui van los vencimientos del mes $este_mes");
	    mensaje("Facturas cargadas");
	    $titulos="id;servicio;factura;importe;vencimiento";
	    $sql="select f.id_fac,t.nombre_servicio,f.nro_factura,f.importe,f.vencimiento from telco_fact f,telcos t where t.id_servicio=f.id_servicio and f.pagado='N' and left(f.vencimiento,7)='$este_mes' and t.anulado<>'S' and f.anulado<>'S' order by f.vencimiento desc;cotelcos.php+id_fac+panta+detalle_agenda+vista+mes";
	    //trace($sql);
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;2;2;0;0","DETALLE","CONSULTAR","","Facturas de Telcos;fac_telcos;fac_telcos");	    
	    mensaje("Servicios agendados");
	    $titulos2="id;nombre;descripcion;vencimiento;importe";
	    $sql2="select id_servicio,nombre_servicio,descripcion,proximo_venc,ultimo_importe from telcos where left(proximo_venc,7)='$este_mes' and anulado<>'S'";
	    tabla_cons($titulos2,$sql2,1,"silver","#E5DBB0","0;0;0;0;2","DETALLE","CONSULTAR","","Facturas de Telcos;fac_telcos2;fac_telcos2");	
	    un_boton("Aceptar","Volver","","panta","agenda");
	    break;
	case "mes_proximo":
	    mi_titulo("Vencimientos del mes proximo");
	    $hoy=hoy();
	    $fecha_comp=sumar_meses($hoy,1);
	    //trace("La fecha a comparar es $fecha_comp");
	    //trace($hoy);
	    $hoy_sis=a_fecha_sistema($hoy);
	    //trace($hoy_sis);
	    $proximo_mes=substr($fecha_comp,0,7);
	    
	    //trace($este_mes);
	    mensaje("Facturas cargadas");
	    $titulos="id;servicio;factura;importe;vencimiento";
	    $sql="select f.id_fac,t.nombre_servicio,f.nro_factura,f.importe,f.vencimiento from telco_fact f,telcos t where t.id_servicio=f.id_servicio and f.pagado='N' and left(f.vencimiento,7)='$proximo_mes' and t.anulado<>'S' and f.anulado<>'S' order by f.vencimiento desc;cotelcos.php+id_fac+panta+detalle_agenda+vista+mes";
	    //trace($sql);
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;2;0","DETALLE","CONSULTAR","","Facturas de Telcos;fac_cargadas;fac_cargadas");	
	    mensaje("Servicios agendados");
	    $titulos2="id;nombre;descripcion;vencimiento;importe";
	    $sql2="select id_servicio,nombre_servicio,descripcion,proximo_venc,ultimo_importe from telcos where left(proximo_venc,7)='$proximo_mes' and anulado<>'S'";
	    tabla_cons($titulos2,$sql2,1,"silver","#E5DBB0","0;0;0;0;2","DETALLE","CONSULTAR","","Facturas de Telcos;serv_agendados;serv_agendados");	
	    un_boton("Aceptar","Volver","","panta","agenda");
	    break;
	case "detalle_agenda":
	    $id_fac=$_POST["id_fac"];
	    $vista=$_POST["vista"];
	    $datos=mysql_fetch_array(mi_query("select * from telco_fact where id_fac='$id_fac'"));
	    $id_servicio=$datos["id_servicio"];
	    //trace("El servicio es $id_servicio");
	    $nombre_servicio=un_dato("select distinct nombre_servicio from telcos where id_servicio='$id_servicio'");
	    $nro_factura=$datos["nro_factura"];
	    $fecha_factura_arg=a_fecha_arg($datos["fecha_factura"]);
	    $importe=$datos["importe"];
	    $vencimiento_arg=a_fecha_arg($datos["vencimiento"]);
	    $proximo_venc_arg=a_fecha_arg($datos["proximo_venc"]);
	    $fecha_pago_arg=a_fecha_arg($datos["fecha_pago"]);
	    $pagado=$datos["pagado"];
	    $observaciones=$datos["observaciones"];
	    $pantaConsuFactura=new PANTALLA();
	    $pantaConsuFactura->titulo="CONSULTA DE FACTURA";	
	    $pantaConsuFactura->AgregarROT("ID FAC##$id_fac");
	    $pantaConsuFactura->AgregarROT("SERVICIO##$nombre_servicio");
	    $pantaConsuFactura->AgregarROT("nro. factura##$nro_factura");
	    $pantaConsuFactura->AgregarROT("fecha factura##$fecha_factura_arg");
	    $pantaConsuFactura->AgregarROT("importe##$importe");
	    $pantaConsuFactura->AgregarROT("vencimiento##$vencimiento_arg");
	    $pantaConsuFactura->AgregarROT("prox. venc.##$proximo_venc_arg");
	    $pantaConsuFactura->AgregarROT("fecha de pago##$fecha_pago_arg");
	    $pantaConsuFactura->AgregarROT("pagado##$pagado");
	    $pantaConsuFactura->AgregarROT("observaciones##$oservaciones");
	    $pantaConsuFactura->AgregarOCU("id_fac",$id_fac);
	    $pantaConsuFactura->AgregarOCU("panta",$vista);
	    //$pantaConsuFactura->AgregarOCU("filtro",$filtro);
	    //$pantaConsuFactura->AgregarOCU("desc_fil",$desc_fil);
	    $pantaConsuFactura->regresar=$_SERVER['PHP_SELF'] . "+$vista";
	    $pantaConsuFactura->FactorAltura=34;
	    $pantaConsuFactura->IncrementoAltura=32;
	    $pantaConsuFactura->recuadro=1;
	    $pantaConsuFactura->izquierda=400;
	    $pantaConsuFactura->tope=0;
	    $pantaConsuFactura->alto=850;
	    $pantaConsuFactura->ancho=700;
	    $pantaConsuFactura->ColorFrente="#ff9999";
	    $pantaConsuFactura->ColorFondo="#cc99cc";
	    $pantaConsuFactura->MostrarPantalla();
	    break;
	case "informes":
	    espacio();
	    mi_titulo("Informes");
	    un_boton("aceptar","FACTURAS FALTANTES/IMPAGAS","","panta","info_impagas");
	    linea();
	    un_boton();
	    break;
	case "info_impagas":
	    $desde=hoy();
	    trace("desde es $desde");
	    $desde='01/' . substr($desde,3);
	    trace("Ahora desde es $desde");
	    $pantaInfo=new PANTALLA();
	    $pantaInfo->titulo="INFORME IMPAGAS";	
	    $pantaInfo->AgregarFEC("desde","desde",$desde);
    	    $pantaInfo->AgregarOCU("panta","proc_info_impagas");
	    $pantaInfo->regresar=$_SERVER['PHP_SELF'];
	    $pantaInfo->FactorAltura=31;
	    $pantaInfo->IncrementoAltura=31;
	    $pantaInfo->recuadro=1;
	    $pantaInfo->izquierda=400;
	    $pantaInfo->tope=0;
	    $pantaInfo->alto=850;
	    $pantaInfo->ancho=700;
	    $pantaInfo->ColorFrente="#ff9999";
	    $pantaInfo->ColorFondo="#cc99cc";
	    $pantaInfo->MostrarPantalla();
	    break;
	case "proc_info_impagas":
	    $desde=$_POST["desde"];
	    $desde_sis=a_fecha_sistema($desde);
	    mi_titulo("Informe de facturas impagas o faltantes desde $desde");
	    mi_query("drop table if exists telco_impagas");
	    mi_query("create temporary table telco_impagas select distinct left(f.vencimiento,7) as periodo,s.id_servicio,s.nombre_servicio,'0000000000000000' as factura,00000000000.00 as importe,curdate() as vencimiento from telco_fact f,telcos s where s.anulado='N' and s.estado_servicio='Activo' and f.anulado='N' and f.vencimiento>='$desde_sis'");
	    mi_query("update telco_impagas i,telco_fact f set i.factura=f.nro_factura,i.importe=f.importe,i.vencimiento=f.vencimiento where i.periodo=left(f.vencimiento,7) and i.id_servicio=f.id_servicio and f.anulado='N' and f.pagado='N'");
	    mi_query("update telco_impagas set factura=null,vencimiento=null where importe=0 and factura=0");
	    $rotulos="periodo;servicio;factura;importe;vencimiento";
	    $sql="select periodo,nombre_servicio,factura,importe,vencimiento from telco_impagas order by 1,2";
	    tabla_cons($rotulos, $sql,"","","","0;0;0;2;0","","","","Listado de facturas impagas o faltantes;telcos;telco_impagas");
	    un_boton("Aceptar","Aceptar","cotelcos.php","panta","informes");
	    break;
	case "consultas":
	    //mi_titulo("Consultas");
	    $pantaConsultas=new PANTALLA();
	    $pantaConsultas->titulo="CONSULTAS";	
	    $pantaConsultas->AgregarTXT("buscar el texto","texto","",30);
    	    $pantaConsultas->AgregarOCU("panta","procesar_consultas");
	    $pantaConsultas->regresar=$_SERVER['PHP_SELF'];
	    $pantaConsultas->FactorAltura=31;
	    $pantaConsultas->IncrementoAltura=31;
	    $pantaConsultas->recuadro=1;
	    $pantaConsultas->izquierda=400;
	    $pantaConsultas->tope=0;
	    $pantaConsultas->alto=850;
	    $pantaConsultas->ancho=700;
	    $pantaConsultas->ColorFrente="#ff9999";
	    $pantaConsultas->ColorFondo="#cc99cc";
	    $pantaConsultas->MostrarPantalla();
	    break;
	case "procesar_consultas":
	    $texto=$_POST["texto"];
	    $texto=str_replace("'"," ",$texto);	    
	    $texto=str_replace("-"," ",$texto);
	    $texto=str_replace('"','',$texto);
	    //mensaje("estoy en procesar consultas con $texto");
	    $sql="select id_servicio,nombre_servicio,descripcion,datos_tecnicos,datos_cuenta,observaciones,proximo_venc,ultimo_importe,estado_servicio,proveedor from telcos where instr(nombre_servicio,'$texto') or instr(descripcion,'$texto') or instr(ejecutivo_nombre,'$texto') or instr(ejecutivo_email,'$texto') or instr(ejecutivo_tel,'$texto') or instr(soporte_tel,'$texto') or instr(soporte_email,'$texto') or instr(datos_tecnicos,'$texto') or instr(datos_cuenta,'$texto') or instr(observaciones,'$texto') or instr(proveedor,'$texto') or instr(punto_entrada,'$texto');cotelcos.php+id_servicio+panta+consu_fac+texto+$texto";
	    $titulos="id;servicio;descripcion;datos tecnicos;datos cuenta;observaciones;vencimiento;importe;estado;proveedor";

	    //trace($sql);
	    mi_titulo("Consulta de servicios con $texto");
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;2;0","DETALLE","CONSULTAR","","Facturas de Telcos;fac_telcos;fac_telcos");	
	    un_boton("Aceptar","Aceptar","cotelcos.php","panta","consultas");   
	    break;
	case "consu_fac":
	    $id_servicio=$_POST["id_servicio"];
	    $texto=$_POST["texto"];
	    mi_titulo("Consulta de facturas del servicio $id_servicio");
	    //mensaje("Estoy en consu_fac con id $id_servicio y $texto");
	    $titulos="id;id serv.;servicio;factura;importe;vencimiento;pago";
	    $sql="select f.id_fac,f.id_servicio,t.nombre_servicio,f.nro_factura,f.importe,f.vencimiento,f.fecha_pago from telco_fact f,telcos t where t.id_servicio=f.id_servicio and t.anulado<>'S' and f.anulado<>'S' and t.id_servicio='$id_servicio' order by f.vencimiento desc;cotelcos.php+id_fac+panta+deta_fac+texto+$texto+id_servicio+$id_servicio";
	    //trace($sql);
	    tabla_cons($titulos,$sql,1,"silver","#E5DBB0","0;0;0;0;2;0;0","DETALLE","CONSULTAR","","Facturas de Telcos;fac_telcos;fac_telcos");
	    un_boton("Aceptar","Volver","","panta;texto;id_servicio","procesar_consultas;$texto;$id_servicio");
	    break;
	case "deta_fac":
	    $id_fac=$_POST["id_fac"];
	    $texto=$_POST["texto"];
	    $id_servicio=$_POST["id_servicio"];
	    //mensaje("Estoy en deta_fac con id $id_fac del servicio $id_servicio y el texto $texto");
	    $sql="select * from telco_fact where id_fac='$id_fac'";
	    $qry=mi_query($sql);
	    $datos=mysql_fetch_array($qry);
	    $nro_factura=$datos["nro_factura"];
	    $fecha_factura=a_fecha_arg($datos["fecha_factura"]);
	    $importe=$datos["importe"];
	    $vencimiento=a_fecha_arg($datos["vencimieto"]);
	    $fecha_pago=a_fecha_arg($datos["fecha_pago"]);
	    $observaciones=$datos["observaciones"];
	    $anulado=$datos["anulado"];
	    $proximo_venc=a_fecha_arg($datos["proximo_venc"]);
	    $pagado=$datos["pagado"];
	    $pantaConsultas=new PANTALLA();
	    $pantaConsultas->titulo="CONSULTAS";	
	    $pantaConsultas->AgregarROT("NRO. FACTURA ##$nro_factura");
	    $pantaConsultas->AgregarROT("FECHA FACTURA ##$fecha_factura");
	    $pantaConsultas->AgregarROT("IMPORTE ##importe");
	    $pantaConsultas->AgregarROT("VENCIMIENTO ##$vencimiento");
	    $pantaConsultas->AgregarROT("FECHA PAGO ##$fecha_pago");
	    $pantaConsultas->AgregarROT("OBSERVACIONES ##$observaciones");
	    $pantaConsultas->AgregarROT("ANULADO ##$anulado");
	    $pantaConsultas->AgregarROT("PROXIMO VTO. ##$proximo_venc");
	    $pantaConsultas->AgregarROT("PAGADO ##$pagado");
    	    $pantaConsultas->AgregarOCU("panta","consu_fac");
	    $pantaConsultas->AgregarOCU("texto",$texto);
	    $pantaConsultas->AgregarOCU("id_servicio",$id_servicio);
	    $pantaConsultas->AgregarOCU("id_fac",$id_fac);
	    $pantaConsultas->regresar=$_SERVER['PHP_SELF'] . "+consultas";
	    $pantaConsultas->FactorAltura=31;
	    $pantaConsultas->IncrementoAltura=31;
	    $pantaConsultas->recuadro=1;
	    $pantaConsultas->izquierda=400;
	    $pantaConsultas->tope=0;
	    $pantaConsultas->alto=850;
	    $pantaConsultas->ancho=700;
	    $pantaConsultas->ColorFrente="#ff9999";
	    $pantaConsultas->ColorFondo="#cc99cc";
	    $pantaConsultas->MostrarPantalla();
	    break;
	default:
	    // Menu Telcos
	    // Muestro el men	u de acceso a las distintas funciones del modulo.
	    espacio();
	    mi_titulo("Modulo de Administracion de Telecomunicaciones");
	    un_boton("aceptar","ABM Servicios","","panta","abm_servicios");
	    un_boton("aceptar","ABM Facturas","","panta","abm_facturas");
	    un_boton("aceptar","Agenda","","panta","agenda");
	    un_boton("aceptar","Informes","","panta","informes");
	    un_boton("aceptar","Consultas","","panta","consultas");
	    linea();
	    un_boton("","Volver","copanel.php");
	    break;
}

?>
