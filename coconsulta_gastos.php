<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Ingreso de Gastos");
require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Aceptar-coabm_gastos.php";
mi_titulo("Registro de Gastos del Area de Sistemas");
if(isset($_GET["panta"]))
{
		$panta=$_GET["panta"];
}else
{
		$panta=$_POST["panta"];
}
switch($panta)
{
	case "graba_alta":
		//trace("Estoy en graba alta");
		$fecha_imput=a_fecha_sistema($_POST["fecha_imput"]);
		$fecha_ing=un_dato("select curdate()");
		$rubro=$_POST["rubro"];
		$concepto=$_POST["concepto"];
		$importe=$_POST["importe"];
		$importe_iva=$_POST["importe_iva"];
		$tipo_iva=$_POST["tipo_iva"];
		$importe_total=$_POST["importe_total"];
		$id_equipo=$_POST["id_equipo"];
		$equipo=un_dato("select descripcion from equipo where id_equipo='$id_equipo'");
		//trace("Equipo es $equipo. Id: $id_equipo");
		$plazo=$_POST["plazo"];
		$responsable=$_POST["responsable"];
		$puesto=$_POST["puesto"];
		$serial_num=$_POST["serial_num"];
		if(floatval($importe_iva)==0 and floatval($importe)<>0 and floatval($importe_total)<>0 and floatval($tipo_iva)==0)
		{
			$importe_iva=$importe_total-$importe;
			$tipo_iva=round($importe_iva/$importe*100,2);
		}
		if(floatval($importe)<>0 and floatval($tipo_iva)<>0 and floatval($importe_iva)==0 and floatval($importe_total)==0)
		{
			$importe_iva=round($importe*$tipo_iva/100,2);
			$importe_total=round($importe*(1+$tipo_iva/100),2);
		}
		if(floatval($importe_total)<>0 and floatval($tipo_iva)<>0 and floatval($importe)==0 and floatval($importe_iva)==0)
		{
			$importe=round($importe_total/(1+$tipo_iva/100),2);
			$importe_iva=$importe_total-$importe	;
		}
		$fac_numero=str_replace("-"," ",$_POST["fac_numero"]);
		//echo($fac_numero);
		$proveedor=$_POST["proveedor"];
		$cod_prov=un_dato("select codigo from proveedores where instr(razon,'$proveedor')");
		//trace("El codigo de proveedor es $cod_prov");
		//die();
		if($cod_prov==0)
		{
			mi_query("insert into proveedores set razon='$proveedor',observaciones='Carga automatica coabm_gastos',fecha_ini=curdate(),fecha_ult=curdate(),id_clase=1","Error al cargar un proveedor");
			$cod_prov=un_dato("select codigo from proveedores where instr(razon,'$proveedor')");
		}
		$observaciones=$_POST["observaciones"];
		$mensaje="";
		if($rubro=="" or $rubro=="TODOS")
			$mensaje="Falta especificar un rubro. ";
		if(!is_numeric($importe))
			$mensaje.="El importe debe ser numerico. ";
		if($mensaje<>"")
		{
			mensaje("Error: $mensaje");
			delay("coabm_gastos.php?fecha_imput=$fecha_imput&fecha_ing=$fecha_ing&rubro=$rubro&concepto=$concepto&importe=$importe&fac_numero=$fac_numero&proveedor=$proveedor&observaciones=$observaciones&responsable=$responsable&id_equipo=$id_equipo&plazo=$plazo&serial_num=$serial_num&puesto=$puesto");
			break;
		}
		mi_query("insert into gastos set fecha_ing='$fecha_ing',fecha_imput='$fecha_imput',rubro='$rubro',concepto='$concepto',importe='$importe',importe_iva='$importe_iva',tipo_iva='$tipo_iva',importe_total='$importe_total',fac_numero='$fac_numero',proveedor='$proveedor',observaciones='$observaciones'","Error al insertar un nuevo gasto");
		mensaje("Se agrego un nuevo gasto.");
		if($id_equipo<>"Elegir")
		{
			//trace("Tengo que grabar la garantia");
			$id_gasto=mysql_insert_id();
			//trace("El id gasto es $id_gasto");
			mi_query("insert into garantias set equipo='$id_equipo',detalle='$concepto',fecha_compra='$fecha_imput',plazo='$plazo',proveedor='$cod_prov',factura='$fac_numero',responsable='$responsable',puesto='$puesto',fecha='$fecha_ing',serial_num='$serial_num',id_gasto='$id_gasto'");
			mensaje("Se grabo la garantia");
		}
		un_boton("","","",$ocu="fecha_imput;fac_numero;proveedor;observaciones",$que_valor="$fecha_imput;$fac_numero;$proveedor;$observaciones");
		delay("coabm_gastos.php?fecha_imput=$fecha_imput&fac_numero=$fac_numero&proveedor=$proveedor&observaciones=$observaciones");
		break;
	case "modi":
		if(isset($_GET["id_gasto"]))
		{
			$id_gasto=$_GET["id_gasto"];
			$fecha_imput=$_GET["fecha_imput"];
			$fecha_imput=a_fecha_arg($fecha_imput);
			$fecha_ing=$_GET["fecha_ing"];
			$fecha_ing=a_fecha_sistema($fecha_ing);
			$rubro=$_GET["rubro"];
			$concepto=$_GET["concepto"];
			$importe=$_GET["importe"];
			$importe_iva=$_GET["importe_iva"];
			$tipo_iva=$_GET["tipo_iva"];
			$importe_total=$_GET["importe_total"];
			$fac_numero=str_replace("-"," ",$_GET["fac_numero"]);
			$proveedor=$_GET["proveedor"];
			$observaciones=$_GET["observaciones"];
			$id_equipo=$_GET["id_equipo"];
			$equipo=un_dato("select descripcion from equipo where id_equipo='$id_equipo'");
			$plazo=$_GET["plazo"];
			$serial_num=$_GET["serial_num"];
			$responsable=$_GET["responsable"];
			$nombre=un_dato("select nombre from usuarios where usuario='$responsable'");
			$puesto=$_GET["puesto"];
			$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		}else
		{
			$id_gasto=$_POST["id_gasto"];
			//trace("El id gasto es $id_gasto");
			$cons=mi_query("select * from gastos where id_gasto='$id_gasto'","Error al obtener el registro");
			$datos=mysql_fetch_array($cons);
			$fecha_imput=a_fecha_arg($datos["fecha_imput"]);
			$fecha_ing=a_fecha_arg($datos["fecha_ing"]);
			$rubro=$datos["rubro"];
			$concepto=$datos["concepto"];
			$importe=$datos["importe"];
			$importe_iva=$datos["importe_iva"];
			$tipo_iva=$datos["tipo_iva"];
			$importe_total=$datos["importe_total"];
			$fac_numero=str_replace("-"," ",$datos["fac_numero"]);
			$proveedor=$datos["proveedor"];
			$observaciones=$datos["observaciones"];
			$cons2=mi_query("select * from garantias where id_gasto='$id_gasto'");
			$datos2=mysql_fetch_array($cons2);
			$id_equipo=$datos2["equipo"];
			//trace("El id equipo es $id_equipo");
			$equipo=un_dato("select descripcion from equipo where id_equipo='$id_equipo'");
			$plazo=$datos2["plazo"];
			$serial_num=$datos2["serial_num"];
			$responsable=$datos2["responsable"];
			$nombre=un_dato("select nombre from usuarios where usuario='$responsable'");
			$puesto=$datos2["puesto"];
			$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		}
		//trace("El equipo es $id_equipo $equipo. Puesto $puesto");
		$titulo="Modificacion de Registro de Eventos";
		if($rubro=="")
			$rubro="TODOS";
		$tit_modi="MODIFICACION DE GASTOS";
		$campos=";%ROT-Id. registro</td><td><strong>$id_gasto";
		$campos.=";%FEC-fecha ingreso-fecha_ing-$fecha_ing-10";
		$campos.=";%FEC-fecha imputacion-fecha_imput-$fecha_imput-10";
		$campos.=";%SEL-rubro-rubro-select rubro from rubro order by 1-rubro-$rubro-$rubro";
		$campos.=";%ARE-concepto-concepto-$concepto-5-75";
		$campos.=";%TXT-importe-importe-$importe-10";
		$campos.=";%TXT-iva-importe_iva-$importe_iva-10";
		$campos.=";%SEL-tipo_iva-tipo iva-$tipo_iva+$tipo_iva+21+21+10.5+10.5-0";
		$campos.=";%TXT-total-importe_total-$importe_total-10";
		$campos.=";%TXT-factura-fac_numero-$fac_numero-50";
		$campos.=";%TXT-proveedor-proveedor-$proveedor-50";
		$campos.=";%ARE-observaciones-observaciones-$observaciones-5-75";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-id_gasto-$id_gasto";
		$campos.=";%CHK-borrar-borrar-s-N";
		$campos.=";%ROT-Datos para la garantia";
		$campos.=";%SEL-id_equipo-equipo-select id_equipo,descripcion from equipo order by 2-descripcion+id_equipo-$equipo-$id_equipo";
		$campos.=";%TXT-meses garantia-plazo-$plazo-5";
		$campos.=";%TXT-S/N-serial_num-$serial_num-50";
		$campos.=";%SEL-responsable-responsable-select usuario,nombre from usuarios order by 2-nombre+usuario-$nombre-$responsable";
		$campos.=";%SEL-puesto-puesto-select codigo,descripcion from puestos order by 2-descripcion+codigo-$desc_puesto-$puesto";
		mi_panta($tit_modi,$campos,$submit);
		break;
	case "graba_modi":
		$id_gasto=$_POST["id_gasto"];
		$fecha_imput=a_fecha_sistema($_POST["fecha_imput"]);
		//trace("La fecha es $fecha_imput");
		$fecha_ing=a_fecha_sistema($_POST["fecha_ing"]);
		$rubro=$_POST["rubro"];
		$concepto=$_POST["concepto"];
		$importe=$_POST["importe"];
		$importe_iva=$_POST["importe_iva"];
		$tipo_iva=$_POST["tipo_iva"];
		$importe_total=$_POST["importe_total"];
		$fac_numero=str_replace("-"," ",$_POST["fac_numero"]);
		$proveedor=$_POST["proveedor"];
		$cod_prov=un_dato("select codigo from proveedores where instr(razon,'$proveedor')");
		$observaciones=$_POST["observaciones"];
		$id_equipo=$_POST["id_equipo"];
		$equipo=un_dato("select descripcion from equipo where id_equipo='$id_equipo'");
		$plazo=$_POST["plazo"];
		$responsable=$_POST["responsable"];
		$puesto=$_POST["puesto"];
		$serial_num=$_POST["serial_num"];
		$borrar=$_POST["borrar"];
		if($borrar=="s")
		{
			mi_query("delete from gastos where id_gasto='$id_gasto'","Error al borrar el registro");
			mensaje("Se borro el registro $id_gasto");
		}else
		{
			$mensaje="";
			if($rubro=="" or $rubro=="TODOS")
				$mensaje="Falta especificar un rubro. ";
			if(!is_numeric($importe))
				$mensaje.="El importe debe ser numerico. ";
			if($mensaje<>"")
			{
				mensaje("Error: $mensaje");
				delay("coabm_gastos.php?panta=modi&id_gasto=$id_gasto&fecha_imput=$fecha_imput&fecha_ing=$fecha_ing&rubro=$rubro&concepto=$concepto&importe=$importe&fac_numero=$fac_numero&proveedor=$proveedor&observaciones=$observaciones");
				break;
			}		
			mi_query("update gastos set fecha_ing='$fecha_ing',fecha_imput='$fecha_imput',rubro='$rubro',concepto='$concepto',importe='$importe',importe_iva='$importe_iva',tipo_iva='$tipo_iva',importe_total='$importe_total',fac_numero='$fac_numero',proveedor='$proveedor',observaciones='$observaciones' where id_gasto='$id_gasto'","Error al modificar el registro de gastos");
			mensaje("Modificaci&oacute;n de $id_gasto grabada");
			$id_garantia=un_dato("select id_garantia from garantias where id_gasto='$id_gasto'");
			//trace("Se modificara la garantia $id_garantia del gasto $id_gasto");
			mi_query("update garantias set equipo='$id_equipo',detalle='$concepto',fecha_compra='$fecha_imput',plazo='$plazo',proveedor='$cod_prov',factura='$fac_numero',responsable='$responsable',puesto='$puesto',fecha='$fecha_ing',serial_num='$serial_num' where id_garantia='$id_garantia'");
			mensaje("Se actualizo la garantia");
		}
		delay();
		break;
	case "filtrar":
		$tit_filtro="Busqueda filtrada";
		$campos.=";%FEC-ingreso desde-fecha_ing_dde--10";
		$campos.=";%FEC-ingreso hasta-fecha_ing_hta--10";
		$campos.=";%FEC-imputacion desde-fecha_imp_dde--10";
		$campos.=";%FEC-imputacion hasta-fecha_imp_hta--10";
		$campos.=";%SEL-id_rubro-rubro-select id_rubro,rubro from rubro order by 2-rubro+id_rubro";
		$campos.=";%TXT-concepto-concepto--15";
		$campos.=";%TXT-factura-fac_numero--15";
		$campos.=";%SEL-proveedor-proveedor-select razon from proveedores where id_clase=1 order by 1-razon+razon";
		$campos.=";%OCU-panta-procesa_filtro";
		mi_panta($tit_filtro,$campos,$submit);
		break;
	case "procesa_filtro":
		$fecha_ing_dde_desc=$_POST["fecha_ing_dde"];
		$fecha_ing_dde=a_fecha_sistema($fecha_ing_dde);
		$fecha_ing_hta_desc=$_POST["fecha_ing_hta"];
		$fecha_ing_hta=a_fecha_sistema($fecha_ing_hta);
		$fecha_imp_dde_desc=$_POST["fecha_imp_dde"];
		$fecha_imp_dde=a_fecha_sistema($fecha_imp_dde);
		$fecha_imp_hta_desc=$_POST["fecha_imp_hta"];
		$fecha_imp_hta=a_fecha_sistema($fecha_imp_hta);
		$id_rubro=$_POST["id_rubro"];
		$rubro=un_dato("select rubro from rubro where id_rubro='$id_rubro'");
		$concepto=$_POST["concepto"];
		$fac_numero=$_POST["fac_numero"];
		$proveedor=$_POST["proveedor"];
		$filtro="where 1=1";
		$desc_filtro="";
		if(!($fecha_ing_dde_desc==hoy() and $fecha_ing_hta_desc==hoy()))
		{
			$filtro.=" and fecha_ing between '$fecha_ing_dde' and '$fecha_ing_hta'";
			$desc_filtro.= "INGRESO entre $fecha_ing_dde_desc y $fecha_ing_hta_desc";
		}
		if(!($fecha_imp_dde_desc==hoy() and $fecha_imp_hta_desc==hoy()))
		{
			$filtro.=" and fecha_imput between '$fecha_imp_dde' and '$fecha_imp_hta'";
			$desc_filtro.= "IMPUTACION entre $fecha_imp_dde_desc y $fecha_imp_hta_desc";
		}
		if($id_rubro<>"Elegir")
		{
			$filtro.=" and rubro='$rubro'";
			$desc_filtro.=" RUBRO: $rubro";
		}
		if($concepto<>"")
		{
			$filtro.=" and instr(concepto,'$concepto')";
			$desc_filtro.=" CONCEPTO: $concepto";
		}
		if($fac_numero<>"")
		{
			$filtro.=" and fac_numero='$fac_numero'";
			$desc_filtro.=" FACTURA: $fac_numero";
		}
		if($proveedor<>"Elegir")
		{
			$filtro.=" and proveedor='$proveedor'";
			$desc_filtro.=" PROVEEDOR: $proveedor";
		}
		/*
		if($categoria<>"Elegir")
		{
			$filtro.=" and categoria='$categoria'";
			$desc_cat=un_dato("select descripcion from cat_repuestos where categoria='$categoria'");
			$desc_filtro="categoria $desc_cat";
		}
		if($marca<>"Elegir")
		{
			$filtro.=" and marca='$marca'";
			$desc_marca=un_dato("select descripcion from marca_repuestos where marca='$marca'");
			$desc_filtro.=" marca $desc_marca";
		}
		if($caracteristica<>"")
		{
			$filtro.=" and instr(caracteristicas,'$caracteristica')";
			$desc_filtro.=" caracteristica $caracteristica";
		}
		*/
		$titulos="id;ingresado;imputado;rubro;concepto;importe sin iva;total;factura;proveedor;observaciones";
		$sql="select id_gasto,fecha_ing,fecha_imput,rubro,concepto,importe,importe_total,fac_numero,proveedor,observaciones from gastos $filtro order by fecha_imput desc;coabm_gastos.php+id_gasto+panta+modi";
		//trace($filtro);
		raya();
		un_boton("aceptar","Filtrar","","panta","filtrar");
		raya();	
		mi_titulo("GASTOS REGISTRADOS");
		mi_titulo("filtrado por: $desc_filtro");
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;0;2;2;0","ACTUALIZ.","MODIFICAR","","Gastos registrados;Gastos registrados;gastos_registrados");
		volver("");
		break;
	default:
		if(isset($_GET["fecha_imput"]))
		{
			$fecha_imput=$_GET["fecha_imput"];
			$fecha_imput=a_fecha_arg($fecha_imput);
			$rubro=$_GET["rubro"];
			$equipo=$_GET["equipo"];
			$concepto=$_GET["concepto"];
			$plazo=$_GET["plazo"];
			$serial_num=$_GET["serial_num"];
			$responsable=$_GET["responsable"];
			$nombre=un_dato("select nombre from usuarios where usuario='$responsable'");
			$puesto=$_GET["puesto"];
			$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
			$importe=$_GET["importe"];
			$importe_iva=$_GET["importe_iva"];
			$tipo_iva=$_GET["tipo_iva"];
			$importe_total=$_GET["importe_total"];
			$fac_numero=str_replace("-"," ",$_GET["fac_numero"]);
			$proveedor=$_GET["proveedor"];
			$observaciones=$_GET["observaciones"];
		}
		if(isset($_POST["fecha_imput"]))
		{
			$fecha_imput=$_POST["fecha_imput"];
			$fecha_imput=a_fecha_arg($fecha_imput);
			$rubro=$_POST["rubro"];
			$equipo=$_POST["equipo"];
			$concepto=$_POST["concepto"];
			$plazo=$_POST["plazo"];
			$serial_num=$_POST["serial_num"];
			$responsable=$_POST["responsable"];
			$nombre=un_dato("select nombre from usuarios where usuario='$responsable'");
			$puesto=$_POST["puesto"];
			$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
			$importe=$_POST["importe"];
			$importe_iva=$_POST["importe_iva"];
			$tipo_iva=$_POST["tipo_iva"];
			$importe_total=$_POST["importe_total"];
			$fac_numero=str_replace("-"," ",$_POST["fac_numero"]);
			$proveedor=$_POST["proveedor"];
			$observaciones=$_POST["observaciones"];
		}
		$tit_alta="NUEVO GASTO";
		$campos="%FEC-fecha imputacion-fecha_imput-$fecha_imput-10";
		//$campos.=";%TXT-rubro-rubro--50";
		if($rubro=="")
			$rubro="TODOS";
		if($tipo_iva=="")
			$tipo_iva="21";
		if($responsable=="")
		{
			$responsable="Elegir";
			$nombre="Elegir";
		}
		if($equipo=="")
		{
			$id_equipo="Elegir";
			$equipo="Elegir";
		}
		if($puesto=="")
		{
			$puesto="Elegir";
			$desc_puesto="Elegir";
		}
		$campos.=";%SEL-rubro-rubro-select rubro from rubro order by 1-rubro-$rubro-$rubro";
		$campos.=";%ARE-concepto-concepto-$concepto-5-75";
		$campos.=";%TXT-importe-importe-$importe-10";
		$campos.=";%TXT-iva-importe_iva-$importe_iva-10";
		$campos.=";%SEL-tipo_iva-tipo iva %-$tipo_iva+$tipo_iva+21+21+10.5+10.5-0";
		$campos.=";%TXT-total-importe_total-$importe_total-10";
		$campos.=";%TXT-factura-fac_numero-$fac_numero-50";
		$campos.=";%TXT-proveedor-proveedor-$proveedor-50";
		$campos.=";%ARE-observaciones-observaciones-$observaciones-5-75";
		$campos.=";%ROT-Datos para la garantia";
		$campos.=";%SEL-id_equipo-equipo-select id_equipo,descripcion from equipo order by 2-descripcion+id_equipo-$id_equipo-$equipo";
		$campos.=";%TXT-meses garantia-plazo-$plazo-5";
		$campos.=";%TXT-S/N-serial_num-$serial_num-50";
		//$campos.=";%TXT-responsable-responsable-$responsable-10";
		$campos.=";%SEL-responsable-responsable-select usuario,nombre from usuarios order by 2-nombre+usuario-$responsable-$nombre";
		//$campos.=";%TXT-puesto-puesto-$puesto-10";
		$campos.=";%SEL-puesto-puesto-select codigo,descripcion from puestos order by 2-descripcion+codigo-$puesto-$desc_puesto";
		$campos.=";%OCU-panta-graba_alta";
		$submit="aceptar-Aceptar-copanel.php";
		mi_panta($tit_alta,$campos,$submit);
		$titulos="id;ingresado;imputado;rubro;concepto;importe sin iva;total;factura;proveedor;observaciones";
		$hay=un_dato("select count(*) from gastos");
		if($hay)
		{
			raya();
			un_boton("aceptar","Filtrar","","panta","filtrar");
			raya();
			$sql="select id_gasto,fecha_ing,fecha_imput,rubro,concepto,importe,importe_total,fac_numero,proveedor,observaciones from gastos order by fecha_imput desc;coabm_gastos.php+id_gasto+panta+modi";
			mi_titulo("GASTOS REGISTRADOS");
			tabla_cons($titulos,$sql,1,"silver","#8EC99F","0;0;0;0;0;2;2;0","ACTUALIZ.","MODIFICAR","","Gastos registrados;Gastos registrados;gastos_registrados");
		}else
		{
			mensaje("No hay gastos para mostrar");
		}
		un_boton("Volver","Volver","copanel.php");
		break;
}
cierre();
?>

