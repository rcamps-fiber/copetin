<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>

<HEAD>
<TITLE>Consulta y Modificacion de OT</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
mi_titulo("Consulta y Modificacion de Ordenes de Mantenimiento");
$panta=$_POST["panta"];
switch($panta)
{
	case "modi":
		$id_mant=$_POST["id_mant"];
		$cons=mi_query("select * from mantenimiento where id_mant='$id_mant'","Error al obtener la solicitud");
		$datos=mysql_fetch_array($cons);
		$fecha_solicitud=a_fecha_arg($datos["fecha_solicitud"]);
		$hora_solicitud=$datos["hora_solicitud"];
		$edificio=$datos["edificio"];
		$desc_edificio=un_dato("select nombre from edificio where id='$edificio'");
		$reclamo=$datos["reclamo"];
		$solicitante=$datos["solicitante"];
		$sector=$datos["sector"];
		$estado=$datos["estado"];
		$desc_estado=un_dato("select estado from estado_ot where id='$estado'");
		$usuario=$datos["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=$datos["puesto"];
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$observaciones=$datos["observaciones"];
		$tecnico=$datos["tecnico"];
		$fecha_prog=a_fecha_arg($datos["fecha_prog"]);
		$obs_tec=$datos["obs_tec"];
		$prioridad=$datos["prioridad"];
		$tarea=$datos["tarea"];
		$fecha_mod=a_fecha_arg($datos["fecha_mod"]);
		$fin=a_fecha_arg($datos["fin"]);
		$titulo="Modificacion de Orden de Mantenimiento Nro. $id_mant";
		
		$campos="%FEC-fecha solicitud-fecha_solicitud-$fecha_solicitud-10";
		$campos.=";%TXT-hora solicitud-hora_solicitud-$hora_solicitud-5";
		$campos.=";%SEL-edificio-edificio-select id,nombre from edificio order by 2-nombre+id-$desc_edificio-$edificio";
		$campos.=";%ARE-reclamo-reclamo-$reclamo-5-50";
		$campos.=";%TXT-solicitante-solicitante-$solicitante-20";
		$campos.=";%TXT-sector-sector-$sector-15";
		$campos.=";%SEL-estado-estado-select id,estado from estado_ot-estado+id-$desc_estado-$estado";
		$campos.=";%SEL-usuario-usuario-select usuario,nombre from usuarios where perfil=2 order by 1-nombre+usuario-$nombre-$usuario";
		$campos.=";%ARE-analisis preliminar-observaciones-$observaciones-5-50";
		$campos.=";%SEL-tecnico-tecnico-select usuario,usuario from usuarios where perfil in(5,6)-usuario+usuario-$tecnico-$tecnico";
		$campos.=";%FEC-fecha programada-fecha_prog-$fecha_prog-10";
		$campos.=";%ARE-diagnostico-obs_tec-$obs_tec-5-50";
		$campos.=";%SEL-prioridad-prioridad-$prioridad+$prioridad+ALTA+ALTA+BAJA+BAJA+NORMAL+NORMAL-0";
		$campos.=";%ARE-trabajo realizado-tarea-$tarea-5-50";
		$campos.=";%FEC-fecha finalizacion-fin-$fin-10";
		$campos.=";%CHK-elimina-elimina-1-n";
		$campos.=";%OCU-panta-graba_modi";
		$campos.=";%OCU-id_mant-$id_mant";
		$submit="aceptar-Modificar-coabm_om.php";	
		mi_panta($titulo,$campos,$submit);
		break;
	case "graba_modi":
		$id_mant=$_POST["id_mant"];
		$fecha_solicitud=a_fecha_sistema($_POST["fecha_solicitud"]);
		$hora_solicitud=$_POST["hora_solicitud"];
		$edificio=$_POST["edificio"];
		$reclamo=$_POST["reclamo"];
		$solicitante=$_POST["solicitante"];
		$sector=$_POST["sector"];
		$estado=$_POST["estado"];
		$usuario=$_POST["usuario"];
		$puesto=un_dato("select puesto from usu_puesto where usuario='$usuario'");
		$observaciones=$_POST["observaciones"];
		$tecnico=$_POST["tecnico"];
		$fecha_prog=a_fecha_sistema($_POST["fecha_prog"]);
		$obs_tec=$_POST["obs_tec"];
		$prioridad=$_POST["prioridad"];
		$tarea=$_POST["tarea"];
		$fecha_mod=a_fecha_sistema($_POST["fecha_mod"]);
		$fin=a_fecha_sistema($_POST["fin"]);
		$desc_puesto=un_dato("select descripcion from puestos where codigo='$puesto'");
		$desc_estado=un_dato("select estado from estado_ot where id='$estado'");
		$elimina=$_POST["elimina"];
		if($elimina=="1")
		{
			mi_query("delete from mantenimiento where id_mant='$id_mant'","Error al eliminar la om.");
			mensaje("Se elimin&oacute; la om $id_mant.");
		}else
		{
			$admin=un_dato("select usuario from usuarios where perfil=5");
			mi_query("update mantenimiento set fecha_solicitud='$fecha_solicitud',hora_solicitud='$hora_solicitud',edificio='$edificio',reclamo='$reclamo',solicitante='$solicitante',sector='$sector',estado='$estado',usuario='$usuario',puesto='$puesto',observaciones='$observaciones',tecnico='$tecnico',fecha_prog='$fecha_prog',obs_tec='$obs_tec',prioridad='$prioridad',tarea='$tarea',fecha_mod=curdate(),fin='$fin' where id_mant='$id_mant'","Error al modificar la om.$id_mant");
			mensaje("Se actualiz&oacute; en la om nro. $id_mant.");
			$asunto="Modificacion de Orden de Mantenimiento";
			$texto="Informamos que ha sido modificada la op '$id_mant'";
			mandar_mail($usuario,$admin,$asunto,$texto,$tecnico,"logo_copetin.jpeg",1);
		}
		un_boton();
		break;
	default:
		$titulo="FILTRO";
		$filtro="";
		$fecha_sol_desde=$_POST["fecha_sol_desde"];
		if($fecha_sol_desde=="" or $fecha_sol_desde=="dd/mm/aaaa")
		{
			$fecha_sol_desde="dd/mm/aaaa";
		}else
		{
			$filtro.=" and s.fecha_solicitud >= '" . a_fecha_sistema($fecha_sol_desde) ."' ";
		}
		$fecha_sol_hasta=$_POST["fecha_sol_hasta"];
		if($fecha_sol_hasta=="" or $fecha_sol_hasta=="dd/mm/aaaa")
		{
			$fecha_sol_hasta="dd/mm/aaaa";
		}else
		{
			$filtro.=" and s.fecha_solicitud <= '" . a_fecha_sistema($fecha_sol_hasta) . "' ";
		}
		$usuario=$_POST["usuario"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");		
		$solicitante=$_POST["solicitante"];
		$sector=$_POST["sector"];
		$edificio=$_POST["edificio"];
		$vencidas=$_POST["vencidas"];
		if($vencidas=="" or $vencidas=="Elegir")
		{
			$vencidas="Elegir";
		}
		if($vencidas=="Si")
		{
			$filtro.=" and s.fecha_prog < curdate() ";
		}
		if($vencidas=="No")
		{
			$filtro.=" and s.fecha_prog>= curdate() ";
		}
		$estado=$_POST["estado"];
		$desc_estado=un_dato("select estado from estado_ot where id='$estado'");
		if($estado=="" or $estado=="Elegir")
		{
			$desc_estado="Elegir";
			$estado="Elegir";
		}else
		{
			$filtro.=" and s.estado='$estado' ";
		}
		$tecnico=$_POST["tecnico"];
		if($tecnico=="" or $tecnico=="Elegir")
		{
			$tecnico="Elegir";
		}else
		{
			$filtro.=" and s.tecnico='$tecnico' ";
		}
		if($usuario=="" or $usuario=="Elegir")
		{
			$usuario="Elegir";
			$nombre="Elegir";
		}else
		{
			$filtro.=" and s.usuario='$usuario' ";
		}
		if($solicitante=="" or $solicitante=="Elegir")
		{
			$solicitante="Elegir";
		}else
		{
			$filtro.=" and s.solicitante='$solicitante'";
		}
		if($edificio=="" or $edificio=="Elegir")
		{
			$edificio="Elegir";
		}else
		{
			$filtro.=" and s.edificio='$edificio'";
		}
		if($sector=="" or $sector=="Elegir")
		{
			$sector="sector";
		}else
		{
			$filtro.=" and s.sector='$sector'";
		}
		$campos.=";%SEL-estado-estado-select 'Elegir' as id,'Elegir' as estado from estado_ot union select id,estado from estado_ot-estado+id-$desc_estado-$estado";	$campos.=";%SEL-tecnico-tecnico-$tecnico+$tecnico+Elegir+Elegir+alejandro+alejandro+rcamps+rcamps-0";
		$campos.=";%TXT-solicitante-solicitante-$solicitante-15";
		$campos.=";%SEL-vencidas-vencidas-Elegir+Elegir+Si+Si+No+No-0";
		$campos.=";%SEL-usuario-usuario-select 'Elegir' as usuario,'Elegir' as nombre from usuarios  union select usuario,nombre from usuarios where perfil=2-nombre+usuario-$nombre-$usuario";
		$campos.=";%TXT-fecha solicitud desde-fecha_sol_desde-$fecha_sol_desde-10";
		$campos.=";%TXT-fecha solicitud hasta-fecha_sol_hasta-$fecha_sol_hasta-10";		
		$submit="aceptar-Filtrar-copanel_mant.php";	
		mi_panta($titulo,$campos,$submit);
		//trace("El filtro es: <br>$filtro");
		$titulos="OM;fecha solicitud;hora;edificio;sector;solicitante;reclamo;estado;usuario;puesto;observaciones;tecnico;fecha prog.;diagnostico;prioridad;tarea";
		$sql="select s.id_mant,s.fecha_solicitud,s.hora_solicitud,e.nombre as edificio,s.sector,s.solicitante,s.reclamo,t.estado,s.usuario,s.puesto,s.observaciones,s.tecnico,s.fecha_prog,s.obs_tec,s.prioridad,s.tarea from mantenimiento s,edificio e,estado_ot t,puestos p,usuarios u where s.estado=t.id and s.puesto=p.codigo and s.usuario=u.usuario and s.edificio=e.id $filtro order by id_mant desc;coabm_om.php+id_mant+panta+modi";
		//mi_titulo("Ordenes de Mantenimiento");
		tabla_cons($titulos,$sql,1,"silver","#8EC99F","0","ACTUALIZ.","MODIFICAR","","Listado");
		un_boton("Volver","Volver","copanel_mant.php");
		break;
}

?>
</BODY>
</HTML>
