<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>
<HEAD>
<TITLE>Solicitud de recambio de cartuchos</TITLE>
</HEAD>
<?
require_once("cobody.php");
require_once("cocnx.php");
//mi_titulo("Solicitud de Recambio de Cartuchos");
$home=home($uid);
$submit="submit-Aceptar-$home";
//$submit="aceptar-Confirmar-cocerrar.php";
$volver=$_GET["volver"];
if(isset($volver))
{
	$submit="aceptar-Confirmar-$home";
}

// MENSAJES NUEVOS
//trace("El usuario es $uid");
actu_item();
$msg_sql="select count(*) from foro where estado='VISIBLE' and (modo_usu='NO LEIDO' or modo_usu is null) and para='$uid'";
$hay_msg=un_dato($msg_sql);
if($hay_msg>0)
{

	if(ver_item("msg"))
	{
		$mensaje="Hay $hay_msg mensajes/es nuevos.";
		linea_menu("msg",$mensaje,0,$home);
	}else
	{
		$mensaje="Ocultar mensajes";
		linea_menu("msg",$mensaje,1,$home);

		$rotulos="id;fecha;de;para;tema;mensaje;rta. a msje.";
		$sql="select f.id_foro,f.fecha,f.usuario,f.para,c.tema,f.mensaje,f.respuesta from foro f,conversacion c where f.conversacion=c.id_conv and (f.estado<>'OCULTO' or f.estado is null) and (f.modo_usu<>'LEIDO' or f.modo_usu is null) and (f.vencimiento>=curdate() or f.vencimiento is null) and (f.para='$uid' or f.usuario='$uid') order by f.id_foro desc;coforo.php+id_foro+panta+ocultar+volver+cosolrec.php";
		tabla_cons($rotulos,$sql,"","","","0","ESTADO","ver/responder","");
		un_boton("aceptar","ver&nbsp;temas","coforo.php?volver=cosolrec.php");
	}	
}

$panta=$_POST["panta"];
switch($panta)
{
	case "cartucho":
		$usuario=$_POST["usuario"];
		$fecha=$_POST["fecha"];
		$impresora=$_POST["impresora"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		//$puesto=un_dato("select puesto from puesto_imp where impresora=$impresora");
		$puesto=un_dato("select puesto from usu_puesto where usuario='$usuario'");
		$puesto_desc=un_dato("select descripcion from puestos where codigo='$puesto'");
		//$puesto_desc=un_dato("select p.descripcion from puestos p,puesto_imp i where p.codigo=i.puesto and i.impresora=$impresora");
		$impre_desc=un_dato("select concat(marca,' ',modelo) as descrip from impresoras where codigo=$impresora");
		$cartucho=un_dato("select cod_cart from cart_imp where cod_imp=$impresora");		
		$cart_desc=un_dato("select concat(codigo_orig,' ',marca,' ',color,' ',ifnull(tipo,' '),' ',ifnull(descripcion,'')) from cartuchos where codigo_int=$cartucho");
		$titulo="Solicitud de Recambio de cartucho";
		$campo_fecha="%ROT-<tr><td><strong>Fecha: " . $fecha . "</strong></td></tr>;";
		$campo_fecha.="%OCU-fecha-$fecha;";
		$campo_usuario="%ROT-<tr><td><strong>Solicitante: $nombre</strong></td></tr>;";
		$campo_usuario.="%OCU-usuario-$uid;";
		$campo_impresora="%ROT-<tr><td><strong>Impresora:## $impre_desc</strong></td></tr>;";
		$campo_impresora.="%OCU-impresora-$impresora;";
		$campo_cartucho="%SEL-cartucho-cartucho-select cod_cart,concat(codigo_orig,' ',marca,' ',color) as cart_des from cart_imp,cartuchos where cod_imp=$impresora and codigo_int=cod_cart-cart_des+cod_cart-$cart_desc-$cartucho;";
		$campo_observaciones="%ARE-observaciones-observaciones--5-80;";
		$campos_ocultos="%OCU-panta-grabar";
		$campos_pantalla=$campo_fecha . $campo_usuario . $campo_impresora . $campo_cartucho . $campo_observaciones . $campos_ocultos;
		//$submit="aceptar-Confirmar-cocerrar.php";
		mi_panta($titulo,$campos_pantalla,$submit);
		break;
	case "grabar":
		require_once 'Mail.php';
		require_once("Mail/mime.php");
		mi_titulo("Solicitud de Recambio de Cartucho");
		$fecha=$_POST["fecha"];
		$usuario=$_POST["usuario"];
		$impresora=$_POST["impresora"];
		$cartucho=$_POST["cartucho"];
		$observaciones=$_POST["observaciones"];
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		$puesto=un_dato("select puesto from usu_puesto where usuario='$usuario'");
		//$puesto=un_dato("select puesto from puesto_imp where impresora=$impresora");
		//$puesto_desc=un_dato("select p.descripcion from puestos p,puesto_imp i where p.codigo=i.puesto and i.impresora=$impresora");
		$puesto_desc=un_dato("select descripcion from puestos where codigo='$puesto'");
		$impre_desc=un_dato("select concat(marca,' ',modelo) as descrip from impresoras where codigo=$impresora");
		$cart_desc=un_dato("select concat(codigo_orig,' ',marca,' ',color,' ',ifnull(tipo,' '),' ',ifnull(descripcion,'')) from cartuchos where codigo_int=$cartucho");
		$stock=un_dato("select cantidad from stock where cartucho=$cartucho");
		$fecha_sis=a_fecha_sistema($fecha);
		if($stock>0)
		{
			$estado="PENDIENTE";
		}else
		{
			$estado="SIN STOCK";
		}
		// Averiguo cuando fue elultimo pedido de este usuario.
		$anterior=un_dato("select a.fecha from solicitudes a where a.numero=(select max(b.numero) from solicitudes b where b.impresora='$impresora' and b.cartucho='$cartucho' and b.usuario='$usuario')");
		$graba_sql="insert into solicitudes set fecha='$fecha_sis',usuario='$usuario',puesto=$puesto,impresora=$impresora,cartucho=$cartucho,observaciones='$observaciones',estado='$estado',anterior='$anterior'";
		mi_query($graba_sql,"cosolrec.php. Linea 65. Imposible grabar la solicitud de recarga");
		mensaje("Se grab&oacute; la solicitud de recarga.");
		mi_tabla("i");
		echo("<tr><td>Usuario: $usuario - $nombre</td></tr>");
		echo("<tr><td>Fecha: $fecha</td></tr>");
		echo("<tr><td>Puesto: $puesto - $puesto_desc</td></tr>");
		echo("<tr><td>Impresora: $impresora - $impre_desc</td></tr>");
		echo("<tr><td>Cartucho: $cartucho - $cart_desc</td></tr>");
		echo("<tr><td>Stock disponible: $stock</td></tr>");
		mi_tabla("f");
		// Mail para el usuario
		$sql_admin="select email from usuarios where perfil=1";
		$qry_admin=  mi_query($sql_admin);
		$email_admin="";
		while($datos=mysql_fetch_array($qry_admin)){
		    $email_admin.=$datos["email"] . ",";
		}
		//$nom_admin=un_dato("select nombre from usuarios where perfil=1");
		//$admin=un_dato("select usuario from usuarios where perfil=1");			
		$email=un_dato("select email from usuarios where usuario='$usuario'");
		$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
		//$email_admin=un_dato("select email from usuarios where usuario='$admin'");
		$mensaje="<html>";
		$mensaje.='<div id="logo"><img src="logo_copetin.jpeg" align="left" border=0></div>';
		$mensaje.='<div id="cuerpo"><h3>Sistema Copetin</h3>';
		$mensaje.="Estimado/a $nombre<br>Hemos recibido su solicitud de recambio de cartucho de tinta codigo $cartucho - $cart_desc para la impresora $impre_desc. A la brevedad le estaremos efectuando la tarea, o de surgir inconvenientes se le informara debidamente.<br><br>Atte, Sistema Copetin.";
		$mensaje.="</div></html>";
		$version_texto="Estimado/a $nombre\nHemos recibido su solicitud de recambio de cartucho de tinta codigo $cartucho - $cart_desc para la impresora $impre_desc. A la brevedad le estaremos efectuando la tarea, o de surgir inconvenientes se le informara debidamente.\nAtte, Sistema Copetin.";
		//$destino = $email . "," . $email_admin;
		$destino=un_dato("select email from usuarios where perfil='1'");
		//trace("Destino: $destino");
		$encabezado['From']    = $destino;
		//trace("De: $email_admin");
		$encabezado['To']      = $email . "," . $email_admin;
		//trace("De  $encabezado['From']");
		//trace("Para $encabezado['To']");
		$encabezado['Subject'] = 'Solicitud de Recambio de Cartucho';
		$cuerpo = $mensaje;
		$params['sendmail_path'] = '/usr/lib/sendmail';
		// Crear el objeto correo con el metodo Mail::factory
		$imagen="imagenes/logo_copetin.jpeg";
		$mime= new Mail_mime("\n");
		$mime->setTXTBody($version_texto);
		$mime->setHTMLBody($mensaje);
		$mime->addHTMLImage($imagen," image/jpeg");
		$cuerpo=$mime->get();
		$hdrs=$mime->headers($encabezado);
		$mail=& Mail::factory('sendmail',$params);
		$mail->send($email_admin,$hdrs,$cuerpo);	
		delay("cocerrar.php");
		//un_boton("aceptar","Aceptar","cosolrec.php");
		break;
	case "queprog":
		$tipo_sol=$_POST["tipo_sol"];
		switch($tipo_sol)
		{
			case "soporte":
				mensaje("Cargando solicitud de soporte...");
				delay("cosolsoporte.php");
				break;
			case "mantenimiento":
				mensaje("Cargando solicitud de mantenimiento...");
				delay("cosolmant.php");
				break;
			case "descargas":
				mensaje("Cargando funcion de download...");
				delay("codownload.php");
				break;
			case "compartir":
				mensaje("Cargando funcion de upload...");
				delay("coupload.php");
				break;
			case "insumos":
				mensaje("Cargando solicitud de insumos...");
				delay("cosolinsumos.php");
				break;
			case "ordenes":
				mensaje("Cargando ordenes de compra...");
				delay("coordenes_usu.php");
				break;
			default:
				$titulo="Solicitud de Recambio de cartucho";
				$fecha=a_fecha_arg(un_dato("select curdate()"));
				$nombre=un_dato("select nombre from usuarios where usuario='$uid'");
				$imp_def_cod=un_dato("select i.impresora from puesto_imp i,usu_puesto u,impresoras x where i.impresora=x.codigo and x.activa and i.puesto=u.puesto and u.usuario='$uid'");
				$imp_def_rot=un_dato("select modelo from impresoras where codigo=$imp_def_cod");
				//$campo_fecha="%ROT-<tr><td><strong>Fecha: " . $fecha . "</strong></td></tr>;";
				$campos="%ROT-<tr><td><strong>Fecha: " . $fecha . "</strong></td></tr>";
				$campos.=";%OCU-fecha-$fecha";
				//$campo_fecha.="%OCU-fecha-$fecha;";
				//$campo_usuario="%ROT-<tr><td><strong>Solicitante: $nombre</strong></td></tr>;";
				$campos.=";%ROT-<tr><td><strong>Solicitante: $nombre</strong></td></tr>";
				//$campo_usuario.="%OCU-usuario-$uid;";
				$campos.=";%OCU-usuario-$uid";
				//$campo_impresora="%SEL-impresora-impresora-select i.codigo as impresora,";
				$campos.=";%SEL-impresora-impresora-select i.codigo as impresora,concat(i.marca,' ',i.modelo) as descrip from impresoras i,usu_puesto u,puesto_imp p where i.activa and i.codigo=p.impresora and p.puesto=u.puesto and u.usuario='$uid'-descrip+impresora-$imp_def_rot-$imp_def_cod";
				//$campo_impresora.="concat(i.marca,' ',i.modelo) as descrip from impresoras i,usu_puesto";
				//$campo_impresora.=" u,puesto_imp p where i.codigo=p.impresora and p.puesto=u.puesto and";
				//$campo_impresora.=" u.usuario='$uid'-descrip+impresora-$imp_def_rot-$imp_def_cod;";
				//$campos_ocultos="%OCU-panta-cartucho";
				$campos.=";%OCU-panta-cartucho";
				//$campos_pantalla=$campo_fecha . $campo_usuario . $campo_impresora . $campos_ocultos;
				//mi_panta($titulo,$campos_pantalla,$submit);
				mi_panta($titulo,$campos,$submit);
		}
		break;
	default:
		$titulo="Solicitud a Sistemas";
		$campos="%SEL-tipo_sol-tipo de solicitud-cartuchos+cartuchos+soporte+soporte+mantenimiento+mantenimiento+descargas+descargas+compartir+compartir archivos+insumos+insumos+ordenes+ordenes de compra-0";
		$campos.=";%OCU-panta-queprog";
		mi_panta($titulo,$campos,$submit);
	break;
}
?>
</BODY>
</HTML>
