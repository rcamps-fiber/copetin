<?
// Informe diario de produccion para Abel
// Este programa se ejecuta automaticamente desde un shell script programado en crontab
// No hay una entrada de menu en el sistema para ejecutarlo
// 08/08/2013
include 'coacceso.php';
include('coclases.php');
include('cofunciones.php');

$hoy=hoy();
//$hoy='2013-08-06';
//trace("Hoy es $hoy");
require("mail/AttachmentMail.php");
require("mail/Mail.php");
require("mail/Multipart.php");
$hoy=hoy();
$hoy="11-09-2013";
$lunes=lunes($hoy);
$domingo=domingo($hoy);
$lunes_sis=a_fecha_sistema($lunes);
$domingo_sis=a_fecha_sistema($domingo);
$cuantos=un_dato("select count(*) from telco_fact f,telcos t where t.id_servicio=f.id_servicio and f.pagado='N' and f.vencimiento>='$lunes_sis' and f.vencimiento <='$domingo_sis' and t.anulado<>'S' and f.anulado<>'S' order by f.vencimiento desc");
if($cuantos){
    $dest=mi_query("select u.email from infos_mail i,usuarios u where u.usuario=i.usuario and i.cod_info=1");
    $to="";
    while($datos=mysql_fetch_array($dest))
    {
	    $aquien=$datos["email"];
	    $to.=",$aquien";
    }
    // Solo para pruebas, borrar en produccion
    //$to="rodrigocamps@veinfar.com.ar";
    //trace($to);
    // fin pruebas

    $msgOK="Envio correcto a $to\n\n";
    $msgFAILED="Fallo el envio a $to\n\n";
    $subject=$message="Sistema Copetin: Vencimientos de facturas de telcos de la semana $sem";
    $mail2=new AttachmentMail($to,$subject,"","copetin");
    $texto="<p>Adjunto envio informe semanal de vencimientos de facturas de telcos.";

    $texto.="<p>Vencimientos de la semana del $lunes al $domingo";
    $rotulos="id;servicio;factura;importe;vencimiento";
    $sql="select f.id_fac,t.nombre_servicio,f.nro_factura,f.importe,f.vencimiento from telco_fact f,telcos t where t.id_servicio=f.id_servicio and f.pagado='N' and f.vencimiento>='$lunes_sis' and f.vencimiento <='$domingo_sis' and t.anulado<>'S' and f.anulado<>'S' order by f.vencimiento desc";

    //trace($texto);
    $mail2->setBodyHtml($texto);

    $xls1=excel("Vencimiento Telcos semana",$rotulos,$sql,"vto_telcos","vto_telcos");
    $mp1=new Multipart($xls1);
    $mail2->addAttachment($mp1);


    if($mail2->send())
	    echo $msgOK;
    else
	    echo $msgFAILED;
}else{
    echo("No hay vencimientos esta semana");
}
?>
