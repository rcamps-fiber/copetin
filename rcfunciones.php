<?

/* 
Libreria estandarizada de funciones
Inicio: 3/5/2006
Etapa primera:
Se volcar�n al principio de cada archivo de funciones las funciones revisadas y
estandarizadas. Se dejar�n abajo las que se mantienen diferentes o que son espec�ficas
Etapa segunda:
Se utilizara un s�lo archivo, ubicado en la carpeta general de librerias /opt/lampp/lib/php
*/
 
 //03-05-2006
// Crea una etiqueta form html de inicio o fin
function mi_form($que_parte,$nombre_form="cualquiera")
{
	if($que_parte=="i")
	{
		$esta_pagina=$_SERVER['PHP_SELF'];
		echo("<form method='post' name='$nombre_form' action='$esta_pagina'>");
	}else
	{
		echo("</form>");
	}
}

 //03-05-2006
// Crea una etiqueta tabla html de inicio o fin con o sin borde y con color o sin
function mi_tabla($que_parte="i",$borde=0)
{
// Crea una tabla o la termina, dependiendo del parametro que_parte
// Puede tener borde o no
// Se puede definir con color (fondo teal con borde silver) o sin, incluyendo la palabra color en 
// el parametro que_parte.
	if(substr($que_parte,0,1)=="i")
	{
		if(strpos($que_parte,"color")>0)
		{
			echo("<table bgcolor='teal' align='center' border='$borde' bordercolor='silver'>");
		}else
		{
			echo("<table align='center' border='$borde'>");
		}	
	}else
	{
		echo("</table>");
	}
}

// 03/05/2006 
// Agrega una fila a una tabla
function mi_fila($que_parte,$datos)
{
//	Agrega una fila a una tabla con color de fondo verde o plateado
//  Permite especificar si se trata de una fila de titulos o de datos (que_parte: t=titulo)
//  El contenido se pasa mediante el parametro datos, que sera una lista separada por punto y coma
//	conteniendo los valores de cada una de las celdas de la fila
	if(!strpos($que_parte,"color")=== false)
	{
		echo("<tr bgcolor='teal'>");
	}else
	{
		echo("<tr bgcolor='silver'>");
	}		
	if(!strpos($que_parte,"t")=== false )
	{
		$cab=explode(";",strtoupper($datos));
		foreach($cab as $col)
		{
			if(is_numeric($col))
			{
				echo("<td align='right'>$col</td>");
			}else
			{
				echo("<td align='center'>$col</td>");
			}
		}	
		echo("</tr>");
	}else
	{
		$cab=explode(";",$datos);
		foreach($cab as $col)
		{
			if(is_numeric($col))
			{
				echo("<td align='right'><strong>$col</strong></td>");
			}else
			{
				echo("<td align='center'><strong>$col</strong></td>");
			}
		}	
		echo("</tr>");
	}
}


// Fin de la libreria estandarizada

//1ra. copetin
function botones_submit($variable,$rotulo,$cancela)
{
//	$variable=nombre de la variable Aceptar
//	$rotulo=leyenda del boton Aceptar
//	$cancela=URL destino al cancelar

/* Esta funcion fue modificada para que permita al cancelar
ir a una pantalla en particular pasando parametros.
La funcion normal al cancelar simplemente volvia al destino indicado
en $cancela, pero sin posibilidad de pasar datos.
*/
	echo("<table align=center border=0><tr><td><input type=submit name='$variable' value='$rotulo'>");
	mi_form("f");
	echo("</td><td>");	
	$div=strpos($cancela,"+");
	if($div>0)
	{
		$datos=explode("+",$cancela);
		$tam=count($datos);
		$volver=$datos[0];
		$panta_volver=$datos[1];
		$tam=count($datos);
		//trace($tam);
		//print_r($datos);
		if($tam>1)
		{
			$ocultos="<input type='hidden' name='$variable' value='$rotulo'>";
			for($i=2; $i<=$tam; $i=$i+2)
			{
				$campo=$datos[$i];
				$valor=$datos[$i+1];
				$ocultos.="<input type='hidden' name='$campo' value='$valor'>";
			}
		}
	}else
	{
		$volver=$cancela;
		$panta_volver="nada";
		$ocultos="";
	}
	echo("<form method='post' action='$volver'>");
	echo("<input type='hidden' name='panta' value='$panta_volver'>");
	echo($ocultos);
	echo("<input type=submit name=cancelar value='Cancelar'>");
	mi_form("f");
	echo("</td></tr></table>");
}

// 2da. Mirage
function botones_submit($variable,$rotulo,$cancela)
{
//	$variable=nombre de la variable Aceptar
//	$rotulo=leyenda del boton Aceptar
//	$cancela=URL destino al cancelar
	echo("<table align=center border=0><tr valign='top'><td valign='center'><input type=submit name='$variable' value='$rotulo'>");
	
	echo("</td><td valign='top'>");
	echo("</form><form method='post' action='$cancela'>");
	echo("<input type=submit name=cancelar value='Cancelar'>");
	mi_form("f");
	echo("</td></tr></table>");
}

//3ra. wims
function botones_submit($variable,$rotulo,$cancela)
{
//	$variable=nombre de la variable Aceptar
//	$rotulo=leyenda del boton Aceptar
//	$cancela=URL destino al cancelar
	echo("<table align=center border=0><tr><td><input type=submit name=$variable value=$rotulo>");
	mi_form("f");
	echo("</td><td>");
	echo("<form method='post' action='$cancela'>");
	echo("<input type=submit name=cancelar value='Cancelar'>");
	mi_form("f");
	echo("</td></tr></table>");
}

//4ta. licitaciones (drogueria)
function botones_submit($variable,$rotulo,$cancela)
{
//	$variable=nombre de la variable Aceptar
//	$rotulo=leyenda del boton Aceptar
//	$cancela=URL destino al cancelar
	echo("<table align=center border=0><tr><td><input type=submit name='$variable' value='$rotulo'>");
	mi_form("f");
	echo("</td><td>");
	echo("<form method='post' action='$cancela'>");
	echo("<input type=submit name=cancelar value='Cancelar'>");
	mi_form("f");
	echo("</td></tr></table>");
}

//5ta. tablero
function botones_submit($variable,$rotulo,$cancela)
{
//	$variable=nombre de la variable Aceptar
//	$rotulo=leyenda del boton Aceptar
//	$cancela=URL destino al cancelar
	echo("<table align=center border=0><tr><td><input type=submit name='$variable' value='$rotulo'>");
	mi_form("f");
	echo("</td><td>");
	echo("<form method='post' action='$cancela'>");
	echo("<input type=submit name=cancelar value='Cancelar'>");
	mi_form("f");
	echo("</td></tr></table>");
}


?>