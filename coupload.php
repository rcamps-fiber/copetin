<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
?>
<HTML>
<HEAD>
<TITLE>Subir archivos al server</TITLE>
</HEAD>
<?

require_once("cobody.php");
require_once("cocnx.php");
$submit="aceptar-Aceptar-coupload.php";
$submit1="aceptar-Aceptar-cosolrec.php";
mi_titulo("INTERCAMBIO DE ARCHIVOS");
if(isset($_GET["panta"]))
{
		$panta=$_GET["panta"];
}else
{
		$panta=$_POST["panta"];
}
switch($panta)
{
	case "habilitar":
		$usu_habilitado=$_POST["usu_habilitado"];
		$archivo=$_POST["archivo"];
		$id_file=$_POST["id_file"];
		if($usu_habilitado<>"Elegir")
		{
			mi_query("insert into habilitados set id_file='$id_file',usu_habilitado='$usu_habilitado'","Error al agregar un usuario habilitado para el archivo $archivo");
			mensaje("Se agreg&oacute; correctamente el usuario $usu_habilitado");
			// Mail para el usuario
			$admin=un_dato("select usuario from usuarios where perfil=1");
			$asunto="Archivo compartido en Copetin";
			$texto="El sistema Copetin le informa que el usuario $uid ha subido el archivo $archivo al servidor y lo ha habilitado para su descarga.";
			$aquien=un_dato("select usuario from usuarios where nombre='$usu_habilitado'");
			mandar_mail($aquien,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
			$tit_pnt="Habilitar m&aacute;s usuarios para $archivo";
			$campos="%SEL-usu_habilitado-usuario-select usuario,nombre from usuarios order by 2-usuario+nombre";
			$campos.=";%OCU-archivo-$archivo";
			$campos.=";%OCU-id_file-$id_file";
			$campos.=";%OCU-panta-habilitar";
			mi_panta($tit_pnt,$campos,$submit);
		}else
		{
			mensaje("...volviendo...");
			delay();
		}
		break;
	case "mover":
		$descripcion=$_POST["descripcion"];
		$msg=subir_archivo(1,"repositorio");
		if($msg=="0")
		{
			mensaje("Hubo un error al subir el archivo");
		}else
		{
			$archivo=$msg;
			$ya_existe=un_dato("select count(*) from archivos_subidos where nombre_arch='$archivo'");
			if($ya_existe)
			{
				mensaje("Error: El archivo ya existe.");
				un_boton();
			}else
			{
				mi_query("insert into archivos_subidos set descripcion='$descripcion',nombre_arch='$archivo',usuario='$uid'","Error al guardar el archivo subido en la tabla");
				$id_file=mysql_insert_id();
				mensaje("$msg<br>Descripcion: $descripcion");
				$tit_pnt="Habilitar usuarios para $archivo";
				$campos="%SEL-usu_habilitado-usuario-select usuario,nombre from usuarios order by 2-usuario+nombre";
				$campos.=";%OCU-archivo-$archivo";
				$campos.=";%OCU-id_file-$id_file";
				$campos.=";%OCU-panta-habilitar";
				mi_panta($tit_pnt,$campos,$submit);
			}
		}
		break;
	case "detalle":
		if(isset($_GET["id_file"]))
		{
			$id_file=$_GET["id_file"];
		}else
		{
			$id_file=$_POST["id_file"];
		}
		$descripcion=un_dato("select descripcion from archivos_subidos where id_file='$id_file'");
		$nombre_arch=un_dato("select nombre_arch from archivos_subidos where id_file='$id_file'");
		$tit_pnt="Modificar Archivos Subidos";
		$campos="%ROT-Archivo:##$nombre_arch";
		$campos.=";%ARE-descripcion-descripcion-$descripcion-4-50";
		$campos.=";%CHK-borrar-borrar-s-N";
		$campos.=";%OCU-id_file-$id_file";
		$campos.=";%OCU-panta-graba_det";
		mi_panta($titulo,$campos,$submit1);
		raya();
		// Falta poner el alta de usuarios aca.
		mi_titulo("Administrar permisos");
		if(isset($_POST[usu_habilitado]))
		{
			mi_query("insert into habilitados set usu_habilitado='$usu_habilitado',id_file='$id_file'","Error al agregar un usuario para $nombre_arch");
			mensaje("usuario $usu_habilitado agregado.");
			// Mail para el usuario
			$admin=un_dato("select usuario from usuarios where perfil=1");
			$asunto="Archivo compartido en Copetin";
			$texto="El sistema Copetin le informa que el usuario $uid ha subido el archivo $archivo al servidor y lo ha habilitado para su descarga.";
			$aquien=un_dato("select usuario from usuarios where nombre='$usu_habilitado'");
			mandar_mail($aquien,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
		}
		$tit_pnt="Habilitar usuarios para $nombre_arch";
		$campos="%SEL-usu_habilitado-usuario-select usuario,nombre from usuarios order by 2-usuario+nombre";
		$campos.=";%OCU-id_file-$id_file";
		$campos.=";%OCU-panta-detalle";
		mi_panta($tit_pnt,$campos,$submit);
		$sql="select h.id_hab,a.nombre_arch,h.usu_habilitado from habilitados h,archivos_subidos a where h.id_file='$id_file' and a.id_file='$id_file' order by 1;coupload.php+id_hab+panta+modi_hab";
		$rotulos="id;archivo;usuario";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="MODIFICAR";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		un_boton();
		break;
	case "graba_det":
		$id_file=$_POST["id_file"];
		$nombre_arch=un_dato("select nombre_arch from archivos_subidos where id_file='$id_file'");
		$descripcion=$_POST["descripcion"];
		$borrar=$_POST["borrar"];
		if($borrar=="s")
		{
			mi_query("delete from archivos_subidos where id_file='$id_file'","Error al borrar el archivo");
			mi_query("delete from habilitados where id_file='$id_file'","Error al borrar los usuarios habilitados para el archivo $nombre_arch");
			if(file_exists($nombre_arch))
			{
				unlink($nombre_arch);
				mensaje("Se borro el archivo $id_file del servidor.");
			}
			mensaje("Se borro el archivo $nombre_arch de la tabla de archivos subidos.");
		}else
		{
			mi_query("update archivos_subidos set descripcion='$descripcion' where id_file='$id_file'","Error al modificar la descripcion del archivo.");
			mensaje("Modificaci&oacute;n de $nombre_arch grabada");
		}
		delay();
		break;		
	case "modi_hab":
		$id_hab=$_POST["id_hab"];
		$id_file=un_dato("select id_file from habilitados where id_hab='$id_hab'");
		$usu_habilitado=un_dato("select usu_habilitado from habilitados where id_hab='$id_hab'");
		$tit_pnt="MODIFICAR USUARIO";
		$nombre_arch=un_dato("select nombre_arch from archivos_subidos where id_file='$id_file'");
		$campos="%ROT-Archivo##$nombre_arch";
		$campos.=";%SEL-usu_habilitado-usuario-select usuario,nombre from usuarios order by 2-usuario+nombre-$usu_habilitado-$usu_habilitado";
		$campos.=";%CHK-borrar-borrar-s-N";
		$campos.=";%OCU-id_hab-$id_hab";
		$campos.=";%OCU-id_file-$id_file";
		$campos.=";%OCU-panta-graba_modhab";
		mi_panta($titulo,$campos,$submit);
		break;
	case "graba_modhab":
		$id_hab=$_POST["id_hab"];
		$usu_habilitado=$_POST["usu_habilitado"];
		$borrar=$_POST["borrar"];
		$id_file=$_POST["id_file"];
		$nombre_arch=un_dato("select nombre_arch from archivos_subidos where id_file='$id_file'");
		if($borrar=="s")
		{
			mi_query("delete from habilitados where id_hab='$id_hab'","Error al borrar el registro");
			mensaje("Se borro el usuario $usu_habilitado");
			// Mail para el usuario
			$admin=un_dato("select usuario from usuarios where perfil=1");
			$asunto="Archivo compartido en Copetin";
			$texto="El sistema Copetin le informa que el usuario $uid le ha revocado los permismos para descargar el archivo $nombre_arch del servidor.";
			$aquien=un_dato("select usuario from usuarios where nombre='$usu_habilitado'");
			mandar_mail($aquien,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
		}else
		{
			$usu_viejo=un_dato("select usu_habilitado from habilitados where id_hab='$id_hab'");
			if($usu_habilitado<>$usu_viejo)
			{
				mi_query("update habilitados set usu_habilitado='$usu_habilitado' where id_hab='$id_hab'","Error al modificar el usuario");
				mensaje("Modificaci&oacute;n de $usu_habilitado grabada");
				$admin=un_dato("select usuario from usuarios where perfil=1");
				$asunto="Archivo compartido en Copetin";
				$texto="El sistema Copetin le informa que el usuario $uid le ha revocado los permismos para descargar el archivo $nombre_arch del servidor.";
				$aquien=un_dato("select usuario from usuarios where nombre='$usu_viejo'");
				mandar_mail($aquien,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
				$texto="El sistema Copetin le informa que el usuario $uid le ha asignado los permismos para descargar el archivo $nombre_arch del servidor.";
				$aquien=un_dato("select usuario from usuarios where nombre='$usu_habilitado'");
				mandar_mail($aquien,$admin,$asunto,$texto,$admin,"logo_copetin.jpeg",1);
			}
		}
		delay("coupload.php?panta=detalle&id_file=$id_file");
		break;	
	default:
		$titulo="Subir archivos al servidor";
		$campos="%ARE-descripcion-descripcion--4-50";
		$campos.=";%FIL-Archivo";
		$campos.=";%OCU-panta-mover";
		mi_panta($titulo,$campos,$submit1);
		raya();
		mi_titulo("Administrar archivos subidos");
		$sql="select a.id_file,a.descripcion,a.nombre_arch from archivos_subidos a where a.usuario='$nombre' order by 1;coupload.php+id_file+panta+detalle";
		$rotulos="id;descripcion;archivo";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="MODIFICAR";
		tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
		break;
}

?>
</BODY>
</HTML>
