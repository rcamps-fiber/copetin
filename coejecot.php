<?
include 'coacceso.php';
include 'cofunciones_especificas.php';
include("cofunciones.php");
apertura("Ejecucion de Ordenes de Trabajo");
require_once("cobody.php");
require_once("cocnx.php");

$home=home($uid);
$submit="aceptar-Confirmar-$home";
$panta=$_POST["panta"];
switch($panta)
{
	case "detalle":
		$id_sol=$_POST["id_sol"];
		$tarea=$_POST["tarea"];
		$obs_tec=$_POST["obs_tec"];
		$estado_ot=$_POST["estado_ot"];
		if(!isset($estado_ot))
		{
			$estado="FINALIZADO";
			$id="4";
		}
		$sql="select s.fecha_prog,s.usuario,s.puesto,p.descripcion as desc_puesto,s.tipo_problema,t.problema,s.dispositivo,d.dispositivo as desc_dispo,s.descripcion as desc_prob,s.observaciones,s.horas_est ";
		$sql.="from soltrab s,tipo_problema t,puestos p,dispositivo d ";
		$sql.="where s.id_sol='$id_sol' and s.tipo_problema=t.id and s.dispositivo=d.id and s.puesto=p.codigo";
		$cns=mi_query($sql,"Error al obtener la ot");
		$datos=mysql_fetch_array($cns);
		$titulo="EJECUCION DE LA ORDEN DE TRABAJO Nro. $id_sol";
		$fecha_prog=a_fecha_arg($datos["fecha_prog"]);
		$usuario=$datos["usuario"];
		$puesto=$datos["puesto"];
		$desc_puesto=$datos["desc_puesto"];
		$descripcion=$datos["desc_prob"];
		$tipo_problema=$datos["tipo_problema"];
		$problema=$datos["problema"];
		$dispositivo=$datos["dispositivo"];
		$desc_dispo=$datos["desc_dispo"];
		$observaciones=$datos["observaciones"];
		$horas_est=$datos["horas_est"];
		$horas_reales=0;
		$campos="%ROT-FECHA PROG.</td><td>$fecha_prog";
		$campos.=";%ROT-SOLICITANTE</td><td>$usuario de $desc_puesto";
		$campos.=";%ROT-TIPO DE PROBLEMA</td><td>$problema";
		$campos.=";%ROT-DISPOSITIVO</td><td>$desc_dispo";
		$campos.=";%ROT-MOTIVO SOL.</td><td>$descripcion";
		$campos.=";%ROT-OBSERVACIONES</td><td>$observaciones";
		$campos.=";%ARE-tarea realizada-tarea-$tarea-5-80";
		$campos.=";%ARE-Observ. tecnico-obs_tec-$obs_tec-5-80";
		$campos.=";%SEL-estado_ot-estado-$id+$estado+4+FINALIZADO+3+EN EJECUCION-0";
		$campos.=";%ROT-HORAS ESTIMADAS</td><td>$horas_est";
		$campos.=";%TXT-HORAS REALES-horas_reales-$horas_reales-5";
		$campos.=";%CHK-Base de Conocimientos-akb-S-n";
		$campos.=";%OCU-id_sol-$id_sol";
		$campos.=";%OCU-panta-grabar";
		$campos.=";%OCU-fecha_sol-$fecha_prog";
		$campos.=";%OCU-usuario-$usuario";
		$campos.=";%OCU-desc_puesto-$desc_puesto";
		$campos.=";%OCU-problema-$problema";
		$campos.=";%OCU-dispositivo-$desc_dispo";
		$campos.=";%OCU-descripcion-$descripcion";
		$campos.=";%OCU-fecha_prog-$fecha_prog";
		$campos.=";%OCU-observaciones-$obsservaciones";
		mi_panta($titulo,$campos,$submit);
		break;
	case "grabar":
		require_once("Mail.php");
		require_once("Mail/mime.php");
		mi_titulo("Ejecucion de ordenes de trabajo");
		$id_sol=$_POST["id_sol"];
		$tarea=$_POST["tarea"];
		$obs_tec=$_POST["obs_tec"];
		$estado_ot=$_POST["estado_ot"];
		$estado_desc=un_dato("select estado from estado_ot where id='$estado_ot'");
		$fecha_prog=$_POST["fecha_prog"];
		//$tecnico=$_POST["tecnico"];
		$observaciones=$_POST["observaciones"];
		//$fecha_sol=$_POST["fecha_sol"];
		$usuario=$_POST["usuario"];
		$desc_puesto=$_POST["desc_puesto"];
		$problema=$_POST["problema"];
		$dispositivo=$_POST["dispositivo"];
		$descripcion=$_POST["descripcion"];
		$horas_reales=$_POST["horas_reales"];
		$akb=($_POST["akb"]=='S') ? 1 : 0;
		$hay_abono=un_dato("select count(*) from abonos where tecnico='$uid'");
		$aprobado=!$hay_abono;
		//trace("El tecnico es $uid y aprobado es $aprobado");
		// Validacion
		$correcto=1;
		$error="";
		if($tarea=="")
		{
			$correcto=0;
			$error.="No se completo la tarea realizada.";
		}
		if($horas_reales==0)
		{
			$correcto=0;
			$error.="No se completaron las horas insumidas por la tarea.";
		}
		if($correcto)
		{
			//trace("Estoy en correcto");
			mi_query("update soltrab set fecha_mod=curdate(),tarea='$tarea',obs_tec='$obs_tec',estado='$estado_ot',akb='$akb',horas_reales='$horas_reales',aprobado='$aprobado' where id_sol='$id_sol'","Error al ejecutar la ot");
			if($estado_ot==4)
			{
				//trace("Estoy en estado 4");
				mi_query("update soltrab set fin=curdate() where id_sol='$id_sol'","Error al ejecutar la ot");
				$pos_ini=strpos($descripcion,"MP:");
				//trace("La pos. ini es $pos_ini dentro de $descripcion");
				if(!(strpos($descripcion,"MP:") === false))
				{
					$pos_ini=strpos($descripcion,"MP:");
					$pos_fin=strpos($descripcion,"*");
					$id_mant=substr($descripcion,$pos_ini+3,$pos_fin-($pos_ini+3));
					//trace("El id es $id_mant");
					$frecuencia=un_dato("select frecuencia from controlable where id='$id_mant'");
					mi_query("update controlable set estado=4,ultimo=curdate(),proximo=date_add(curdate(),interval $frecuencia day),ot=$id_sol where id='$id_mant'","Error: imposible actualizar la tabla de mantenimiento preventivo con el estado finalizado");
				}

				//Agregado (31-08): base de conocimientos
				//Si akb fue marcado y estado finalizado
				if ($akb == 1){
					//genero el título
					$titu_conoc = "(".$problema." - ".$dispositivo.") ".substr($descripcion,0,14)."...";
					$prob_conoc = $descripcion;
					$solu_conoc = $tarea;
					//genero las claves: con las palabras del problema, dispositivo y descripcion
					//que tengan longitud mayor a 2
					$junto_palabras = $problema." ".$dispositivo." ".$descripcion;
					$palabras = explode(" ", $junto_palabras);
					$c = 0;
					$c1 = 0;
					$clav_c = array();
					while ($c < count($palabras)){
						if (strlen($palabras[$c]) > 2){
							$clav_c[$c1] = $palabras[$c];
							$c1++;
						}
						$c++;
					}
					$clav_conoc = implode(", ", $clav_c);
					//traigo la fecha de alta
					$f_alta_conoc = un_dato("select fecha_prog from soltrab where id_sol = $id_sol;");

					//ahora guardo estos datos en la base conocimientos
					mi_query("insert into conocimientos (titulo, problema, solucion, claves, fecha_alta, fecha_modi) VALUES ('$titu_conoc', '$prob_conoc', '$solu_conoc', '$clav_conoc', '$f_alta_conoc', curdate())");
				}
				mensaje("Orden de Trabajo Ejecutada con exito.");
			}else
			{
				mensaje("Orden de Trabajo actualizada con exito.");			
			}
			$fecha_mod=a_fecha_arg(un_dato("select curdate()"));
			mi_tabla("i");
			echo("<tr><td>Orden de trabajo: $id_sol</td></tr>");
			echo("<tr><td>Usuario: $usuario</td></tr>");
			echo("<tr><td>Fecha programada: $fecha_prog</td></tr>");
			echo("<tr><td>Problema: $problema</td></tr>");
			echo("<tr><td>Dispositivo: $dispositivo</td></tr>");
			echo("<tr><td>Descripcion: $descripcion</td></tr>");
			echo("<tr><td>Fecha: $fecha_mod</td></tr>");
			echo("<tr><td>Tarea realizada: $tarea</td></tr>");
			echo("<tr><td>Observ. tecnico: $obs_tec</td></tr>");
			echo("<tr><td>Estado: $estado_desc</td></tr>");
			echo("<tr><td>Base de Conoc.: $akb</td></tr>");
			mi_tabla("f");
			$nom_admin=un_dato("select nombre from usuarios where perfil=1");
			$admin=un_dato("select usuario from usuarios where perfil=1");			
			$email=un_dato("select email from usuarios where usuario='$usuario'");
			$nombre=un_dato("select nombre from usuarios where usuario='$usuario'");
			$email_admin=un_dato("select email from usuarios where usuario='$admin'");
			// Mail para el usuario
			$mensaje="<html>";
			$mensaje.='<div id="logo"><img src="logo_copetin.jpeg" align="left" border=0></div>';
			$mensaje.='<div id="cuerpo"><h3>Sistema Copetin</h3>';
			$mensaje.="Estimado/a $nombre<br>En relaci&oacute;n a su solicitud nro. $id_sol por un problema de $problema con $dispositivo, hemos realizado la siguiente tarea:<br><strong>$tarea</strong><br>quedando la orden en estado $estado_desc.<br><br>Atte, $nom_admin.";
			$mensaje.="</div></html>";
			$version_texto="Estimado/a $nombre\nEn relacion a su solicitud nro. $id_sol por un problema de $problema con $dispositivo, hemos realizado la siguiente tarea: \n$tarea\nquedando la orden en estado $estado_desc.\n\nAtte, $nom_admin.";
			$destino = $email . "," . $email_admin;
			$encabezado['From']    = $email_admin;
			$encabezado['To']      = $email;
			$encabezado['Subject'] = 'Solicitud de soporte tecnico';
			$cuerpo = $mensaje;
			$params['sendmail_path'] = '/usr/lib/sendmail';
			// Crear el objeto correo con el metodo Mail::factory
			$imagen="imagenes/logo_copetin.jpeg";
			$mime= new Mail_mime("\n");
			$mime->setTXTBody($version_texto);
			$mime->setHTMLBody($mensaje);
			$mime->addHTMLImage($imagen," image/jpeg");
			$cuerpo=$mime->get();
			$hdrs=$mime->headers($encabezado);
			$mail=& Mail::factory('sendmail',$params);
			$mail->send($destino,$hdrs,$cuerpo);	
			un_boton("Aceptar","Aceptar","coejecot.php");
			break;
		}else
		{
			mensaje($error);
			$campos="%OCU-panta-detalle";
			$campos.=";%OCU-id_sol-$id_sol";
			$campos.=";%OCU-tarea-$tarea";
			$campos.=";%OCU-obs_tec-$obs_tec";
			$campos.=";%OCU-estado_ot-$estado_ot";
			mi_panta("",$campos,$submit);
		}
		break;
	default:
		mi_titulo("Ejecucion de Ordenes de Trabajo");
		$rotulos="OT;USUARIO;PUESTO;UBICACION;FECHA PROGRAMADA.;TIPO PROBLEMA;DISPOSITIVO;DESCRIPCION;OBSERVACIONES;PRIORIDAD;ESTADO";
		$borde="silver";
		$cuerpo="#8EC99F";
		$decimales=0;
		$tit_lnk="DETALLE";
		$btn_lnk="EJECUTAR";
		$casos=un_dato("select count(*) from soltrab where estado=2 and tecnico='$uid'");
		if($casos)
		{
			$vencidas=un_dato("select count(*) from soltrab where estado in(2,3) and tecnico='$uid' and fecha_prog<curdate()");
			if($vencidas>=0)
			{
				mi_titulo("Ordenes de trabajo vencidas");
				$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_prog,t.problema,d.dispositivo,s.descripcion,s.observaciones,4-(!instr(s.descripcion,'MP:')+if(s.prioridad='ALTA',2,if(s.prioridad='NORMAL',1,0))) as preventivo,if(s.estado=2,'PROGRAMADO','EN EJECUCION') as estado from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado in(2,3) and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo and s.tecnico='$uid' and s.fecha_prog<curdate() order by preventivo,s.id_sol;coejecot.php+id_sol+panta+detalle";
				tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
			}
			$hoy=un_dato("select count(*) from soltrab where estado=2 and tecnico='$uid' and fecha_prog=curdate()");
			if($hoy>=0)
			{
				mi_titulo("Ordenes programadas para hoy");
				$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_prog,t.problema,d.dispositivo,s.descripcion,s.observaciones, 4-(!instr(s.descripcion,'MP:')+if(s.prioridad='ALTA',2,if(s.prioridad='NORMAL',1,0))) as preventivo,if(s.estado=2,'PROGRAMADO','EN EJECUCION') as estado from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado in(2,3) and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo and s.tecnico='$uid' and s.fecha_prog=curdate() order by preventivo, s.id_sol;coejecot.php+id_sol+panta+detalle";
				tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
			}
			$futuras=un_dato("select count(*) from soltrab where estado in(2,3) and tecnico='$uid' and fecha_prog>curdate()");
			if($vencidas>=0)
			{
				mi_titulo("Ordenes programadas para m&aacute;s adelante");
				$sql="select s.id_sol,u.nombre,p.descripcion as desc_puesto,p.ubicacion,s.fecha_prog,t.problema,d.dispositivo,s.descripcion,s.observaciones,4-(!instr(s.descripcion,'MP:')+if(s.prioridad='ALTA',2,if(s.prioridad='NORMAL',1,0))) as preventivo,if(s.estado=2,'PROGRAMADO','EN EJECUCION') as estado from soltrab s,puestos p,usuarios u,dispositivo d,tipo_problema t where s.estado in(2,3) and s.usuario=u.usuario and s.puesto=p.codigo and t.id=s.tipo_problema and d.id=s.dispositivo and s.tecnico='$uid' and s.fecha_prog>curdate() order by preventivo,s.id_sol;coejecot.php+id_sol+panta+detalle";
				tabla_cons($rotulos,$sql,$borde,$color,$cuerpo,$decimales,$tit_lnk,$btn_lnk);
			}
		}else
		{
			mensaje("No hay ORDENES DE TRABAJO programadas para ud.");
		}
		volver($uid);
		
		//$prf=un_dato("select perfil from usuarios where usuario='$uid' and clave= PASSWORD('$pwd')");
		//if($prf==1)
		//{
		//	$volver="copanel.php";
		//}else
		//{
		//	$volver="cocerrar.php";
		//}
		//un_boton("Volver","Volver",$volver);
		break;
}
?>
</BODY>
</HTML>
